public class CampaignMemberTriggerHandler {

    public static void addLeadToCampaingMember(list<campaignMember> listCampMember)
    {
        set<string> listLeadId = new set<string>();
        list<Product_Interest__c> listProductInterest = new list<Product_Interest__c>();
        list<lead> listUpdateLead = new list<lead>();
        
        //string campaignId = listCampMember.get(0).campaignId;
        map<string,string> campaignMapId = new map<string,string>();
        list<string> listCampaignID = new list<string>();
        List<String> listOfLeadID = new List<String>();
        for(campaignMember c : listCampMember)
        {
            campaignMapId.put(c.leadID,c.campaignID);
            listCampaignID.add(c.campaignID);
            listOfLeadID.add(c.LeadID);
        }
        
        list<Campaign_Product__c> listCampaignProduct = [select Product__c, Opportunity_Type__c, Amount__c, Campaign__c
                                          from Campaign_Product__c where Campaign__c in: listCampaignID];
        //                                  from Campaign_Product__c where Campaign__c =: campaignId];
        
        List<Lead> listLead = [SELECT ID,Primary_Campaign__c from Lead WHERE ID IN: listofLeadId];
        set<ID> InvalidLead = new Set<ID>();
        for(Lead leadRec : listLead){
            if(leadRec.Primary_Campaign__c !=null){
                InvalidLead.add(LeadRec.id);
            }
        }
        
        
        for (campaignMember c : listCampMember)
        {
            if(!InvalidLead.contains(c.LeadId)){
                listLeadId.add(c.LeadID);
            }
        }
        

            for (string leadId : listLeadId)
            {
                listUpdateLead.add(new Lead(id = leadId, Primary_Campaign__c = campaignMapId.get(leadId)));
                for (Campaign_Product__c c : listCampaignProduct)
                {
                    if (c.Campaign__c == campaignMapId.get(leadId))
                    {
                        Product_Interest__c pi = new Product_Interest__c(Lead__c = leadId, Product__c = c.Product__c
                            , Opportunity_Type__c = c.Opportunity_Type__c, Amount__c = c.Amount__c);
                        listProductInterest.add(pi);
                    }
                }
            }
            
            system.debug(listProductInterest);
            system.debug(listUpdateLead);
            
            try {
                insert listProductInterest;
                update listUpdateLead;
            }catch(exception ex){
                system.debug(ex);
            }
        
    }
    
    public static void checkPrimaryCampaign(list<campaignMember> listCampMember)
    {
        //string campaignId = listCampMember.get(0).campaignId;
        
        Set<String> CampaignIDSet = new Set<String>();
        
        list<string> listLeadId = new list<string>();
        //string errorText = '';
        boolean errorCheck = false;
        integer n = 0;
        
       for(campaignMember cMember : listCampMember){
            CampaignIDSet.add(cMember.campaignId);
            listLeadId.add(cMember.LeadId);
        }

        Set<String> ExisitngCmember = new Set<String>();
        for (campaignMember c: [select LeadId, Lead.name ,CampaignId
                                from campaignMember 
                                where LeadId in: listLeadId 
                                and CampaignId !=: CampaignIDSet])
        {
            ExisitngCmember.add(c.LeadId);
        }
        
        for(campaignMember cMember : listCampMember){
            if(ExisitngCmember.Contains(cMember.LeadId)){
                cMember.Firstname.addError(status_code__c.getValues('8003').status_message__c);
            }
        }
        
   }
    
    public static void deleteCampaignMember(list<campaignMember> listCampMember)
    {
        list<string> listLeadId = new list<string>();
        list<lead> listLead = new list<lead>();
        
        for (campaignMember c : listCampMember)
        {
            listLeadId.add(c.LeadID);
        }
        
        for (Lead l : [select id, primary_campaign__c from Lead where id in: listLeadId])
        {
            l.primary_campaign__c = null;
            listLead.add(l);
        }
        try {
            update listLead;
        }catch(exception ex){
            system.debug(ex);
        }
    }


}