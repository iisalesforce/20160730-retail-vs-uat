public with sharing class RTL_CustomerRelationshipExtension {
    public String acctId {get; set;}
    public Id accountId {get; set;}
    public String rtl_OtcAtmAdmIbMib {get; set;}
    public String rtl_MibStatus {get; set;}
    public String rtl_Suitability {get;set;}
    public Boolean initialised {get; set;}
    

    public RTL_CustomerRelationshipExtension(ApexPages.StandardController stdController){
        Account acct = (Account)stdController.getRecord();
        acct = [SELECT ID,Name,TMB_Customer_ID_PE__c,Account_Type__c,RTL_OTC_ATM_ADM_IB_MIB__c,RTL_MIB_Status__c,RTL_Suitability__c FROM Account WHERE Id =: acct.id ];
        //Call RTL_CvsAnalyticsDataService.CVSAnalyticsDataSOAP to get the result from webservice only for retail customer
        //Need to handle SSL and pass in RMID
        if(acct.Account_Type__c == 'Retail Customer'){
            RTL_CvsAnalyticsDataService.CVSAnalyticsDataSOAP wsObject = new RTL_CvsAnalyticsDataService.CVSAnalyticsDataSOAP();
            RTL_CvsAnalyticsDataService.CVSAnalyticsData result = wsObject.getCVSAnalyticsData(acct.TMB_Customer_ID_PE__c);
            acct.RTL_OTC_ATM_ADM_IB_MIB__c = result.UsagePercentage;//'10:20:20:25:25'
            acct.RTL_MIB_Status__c = result.MIBStatus;//'Applied'
            acct.RTL_Suitability__c = result.Suitability;
        }

        //persist the values into custom fields with action method
        acctId =  String.valueOf(acct.Id);
        accountId = acct.Id;
        rtl_OtcAtmAdmIbMib = acct.RTL_OTC_ATM_ADM_IB_MIB__c;
        rtl_MibStatus = acct.RTL_MIB_Status__c;
        rtl_Suitability = acct.RTL_Suitability__c;
        initialised = false;
    }

    /* this is action method called by visualforce page action={!updateAccount} */
    public void updateAccount() {

        if (!initialised) {
            
            Account acc = new Account(Id = accountId, RTL_OTC_ATM_ADM_IB_MIB__c = rtl_OtcAtmAdmIbMib, RTL_MIB_Status__c = rtl_MibStatus,RTL_Suitability__c = rtl_Suitability);
            update acc;
            initialised = true;
            /*
            try {
                update acc;
                initialised = true;
            }
            catch (Exception e) {
                System.debug('RTL_CvsAnalyticsDataService - Update Error MIB Status/% Usage: ' + e.getMessage());
            }*/
            
        }
        
    }
    
    /* check if the VF page is display by SF1 */
    public Boolean isSF1 {
        get {                   
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
                String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
                ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
                (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
            ) {
                return true;
            }else{
                return false;
            }
        }
    }    
}