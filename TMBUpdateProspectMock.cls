@isTest
global class TMBUpdateProspectMock implements WebServiceMock {
    public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
       
        System.debug(LoggingLevel.INFO, 'TMBServiceProxyMockImpl.doInvoke() - ' +
            '\n request: ' + request +
            '\n response: ' + response +
            '\n endpoint: ' + endpoint +
            '\n soapAction: ' + soapAction +
            '\n requestName: ' + requestName +
            '\n responseNS: ' + responseNS +
            '\n responseName: ' + responseName +
            '\n responseType: ' + responseType);
       
        TMBServiceProxy.ProspectResultDTO UpdateResult = new TMBServiceProxy.ProspectResultDTO(); 
            UpdateResult.status ='0000';
            UpdateResult.totalrecord = '1';
            UpdateResult.massage = '';
     
                
        TMBServiceProxy.UpdatePospectResponse_element UpdateElement = new TMBServiceProxy.UpdatePospectResponse_element();
        UpdateElement.UpdatePospectResult = UpdateResult;
       response.put('response_x', UpdateElement); 
   } 
}