@IsTest


public class RTL_OpportunityTriggerHandlerTest {  
    static {
        TestUtils.createAppConfig();
    }
    
    public static testmethod void RunPositiveTestOpportunityForChannelReferral(){
        System.debug(':::: RunPositiveTestOpportunityForChannelReferral Start ::::');
        
        TEST.startTest();
        List<User> retailUsers = RTL_TestUtility.createRetailTestUserOppt(true);
        User firstUser = retailUsers[0];//outbound channel
        User secondUser = retailUsers[1];//outbound channel
        User thirdUser = retailUsers[2];//branch channel
        TestInit.createUser(false);
        User adminUser = TestInit.us;  
        List<Account> accountList = null;
        List<Opportunity> opportunityList = null;
        String opptId = null;
        Opportunity updatedOppt = null;
        System.runAs(adminUser) {
        	//only system admin can create account
            accountList = RTL_TestUtility.createAccounts(1,true);
            //change the account ownership to retail user for opportunity creation later
            Account acct = accountList[0];
            acct.OwnerId = firstUser.Id;
            update acct;
        }
        System.runAs(firstUser) {
        	//create oppportunity with retail user so that the retail record type is set
            opportunityList = RTL_TestUtility.createOpportunity(accountList,true);
            opptId = opportunityList.get(0).Id;
            Opportunity oppt = [select OwnerId from Opportunity where Id = :opptId];
            //1st test case: the default channel referral is created
            List<RTL_Channel_Referral__c> defaultOpptReferrals = [Select RTL_Owner__c, RTL_First_Entry__c from RTL_Channel_Referral__c where RTL_Opportunity__c = :opptId];
            System.assertEquals(1, defaultOpptReferrals.size());
            //check if the opportunity owner is first user
            System.assertEquals(firstUser.Id, oppt.OwnerId);
            //check if the opportunity owner is set as channel referral owner by default
            System.assertEquals(firstUser.Id, defaultOpptReferrals[0].RTL_Owner__c);
            //2nd test case: change ownership, but the channel is the same (Outbound)                            
            oppt.OwnerId = secondUser.Id;
            //3rd test case: check first entry of first channel referral to be true
            System.assertEquals(true, defaultOpptReferrals[0].RTL_First_Entry__c);
            update oppt;            
        }
        System.runAs(secondUser) {
            //check if the opportunity owner is changed
            updatedOppt = [select OwnerId from Opportunity where Id = :opptId];
            System.assertEquals(secondUser.Id, updatedOppt.OwnerId);
            //check if no additional oppportunity referral is created
            List<RTL_Channel_Referral__c> newOpptReferrals = [Select RTL_Owner__c, RTL_First_Entry__c from RTL_Channel_Referral__c where RTL_Opportunity__c = :opptId];
            System.assertEquals(1, newOpptReferrals.size());
            //check if the oppportunity referral is not changed
            System.assertEquals(firstUser.Id, newOpptReferrals[0].RTL_Owner__c);  
            //check first entry of first channel referral to be true
            System.assertEquals(true, newOpptReferrals[0].RTL_First_Entry__c);
        }
        System.runAs(adminUser) {
            updatedOppt.OwnerId = thirdUser.Id;
            update updatedOppt;
        }
        System.runAs(thirdUser) {
            //check if the opportunity owner is changed
            Opportunity updatedOppt2 = [select OwnerId from Opportunity where Id = :opptId];
            System.assertEquals(thirdUser.Id, updatedOppt2.OwnerId);             
            List<RTL_Channel_Referral__c> updatedOpptReferrals = [Select RTL_Owner__c, RTL_End_Date__c, RTL_First_Entry__c from RTL_Channel_Referral__c where RTL_Opportunity__c = :opptId order by CreatedDate asc];
            System.assertEquals(2, updatedOpptReferrals.size());
            //check if additional channel refferal is created, the old referral has end date updated
            System.assertEquals(firstUser.Id, updatedOpptReferrals[0].RTL_Owner__c);  
            System.assert(updatedOpptReferrals[0].RTL_End_Date__c!=null);
            //check first entry of first channel referral to be true
            System.assertEquals(true, updatedOpptReferrals[0].RTL_First_Entry__c);            
            System.assertEquals(thirdUser.Id, updatedOpptReferrals[1].RTL_Owner__c);
            //check 2nd entry of first channel referral to be false
            System.assertEquals(false, updatedOpptReferrals[1].RTL_First_Entry__c);
        }
        TEST.stopTest();
        System.debug(':::: RunPositiveTestOpportunityForChannelReferral End ::::');
    }
}