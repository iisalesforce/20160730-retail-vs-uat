trigger CampaignTrigger on Campaign (before insert, before update) {

    Boolean RunTrigger = AppConfig__c.getValues('runtrigger').Value__c == 'true' ; 
    
    if (Trigger.isBefore && Trigger.isInsert)
    {
        system.debug('CampaignTrigger before insert');
        if( RunTrigger || Test.isRunningTest() )
        {
            CampaignTriggerHandler.checkDuplicateName(Trigger.new, new list<campaign>());
        }
    } 
    
    if (Trigger.isBefore && Trigger.isUpdate)
    {
        system.debug('CampaignTrigger before update');
        if( RunTrigger || Test.isRunningTest() )
        {
            CampaignTriggerHandler.checkDuplicateName(Trigger.new, Trigger.old);
        }
    } 

}