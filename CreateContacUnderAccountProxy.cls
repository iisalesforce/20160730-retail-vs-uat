public with sharing class CreateContacUnderAccountProxy {
	public  Account account;
    private ApexPages.StandardController standardController;	
    public CreateContacUnderAccountProxy(ApexPages.StandardController controller){
        standardController = controller;        
        account = (Account)standardController.getRecord();
    }    
    public  PageReference NextPage(){   
        // redirect to search screen
       PageReference result=Page.ContactCreationMobileLayoutV3;
       result.getParameters().put('accid', account.Id);      
       result.setRedirect(true); 
       return result;  
    }
}