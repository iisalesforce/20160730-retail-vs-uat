public class AddProductLineItemController {

    public List<SelectOption> ProductdomainSelectOption {get;set;}
    public String productdomainselected {get;set;}
    public List<ProductWrapper> ProductWrapperList {get;set;}
    public List<Product2> ProductsList {get;set;}
    public String pricebookid {get;set;}
    public List<opportunityLineItem> optLine {get;set;}
    public Boolean isSelect {get;set;}
    public Opportunity oppObj {get;set;}
    public map<string,string> mapProductName {get;set;}
    public Boolean isSave {get;set;}

    public AddProductLineItemController(apexPages.standardController sc)
    {
        isSave = false;
        isSelect = false;
        opportunity oppTemp = (Opportunity)sc.getRecord();
        oppObj = [select id, account.name, name from opportunity where id =: oppTemp.id];
        ProductWrapperList = new List<ProductWrapper>();
        mapProductName = new map<string,string>();
        
        ProductdomainSelectOption = new List<SelectOption>();                   
        ProductdomainSelectOption.add(new SelectOption('','Please Select Domain'));
        ProductdomainSelectOption.add(new SelectOption('1','1. Transactional Banking'));
        ProductdomainSelectOption.add(new SelectOption('2','2. Deposit and Investment'));
        ProductdomainSelectOption.add(new SelectOption('3','3. Funding & Borrowing'));
        ProductdomainSelectOption.add(new SelectOption('4','4. Risk Protection'));
        
        user u = [select segment__c from user where id =: userinfo.getUserId()];
        /*
        if (u.segment__c != null)
            pricebookid =  [select id from pricebook2 where name =: Price_book_access__c.getValues(u.segment__c).PricebookItem__c].get(0).id;
        else
            pricebookid = '';
        */
        string user_segment = '';
        if (u.segment__c != null)
            user_segment = u.segment__c;
        else
            user_segment = 'Default';
        pricebookid =  [select id from pricebook2 where name =: Price_book_access__c.getValues(user_segment).PricebookItem__c].get(0).id;
            
        optLine = new list<opportunityLineItem>();
    }
    
    public void productdomainselect()
    {
        ProductsList = new List<Product2>();
        ProductWrapperList = new List<ProductWrapper>();
        String domainselected = ''; 
        Boolean isNotFound = false;
        
        if(productdomainselected=='1')
        {
            domainselected = '%Transaction%';
            isNotFound = true;
        }else if(productdomainselected=='2'){
            domainselected = '%Deposit%';
            isNotFound = true;
        }else if(productdomainselected=='3'){
            domainselected = '%Fund%';
            isNotFound = true;
        }else if(productdomainselected=='4'){
            domainselected = '%Risk%';
            isNotFound = true;
        }
        
        for(pricebookEntry pb : [SELECT ID, product2.Name, product2.IsActive, product2.Family, product2.Product_Domain__c        
                                    From pricebookEntry
                                    WHERE pricebook2id =: pricebookid
                                    AND product2.IsActive =: true
                                    AND product2.Product_Domain__c Like : domainselected order by product2.Name])
        {
            ProductWrapper pw = new ProductWrapper();
            pw.pricebook = pb;
            pw.IsSelected = false;
            ProductWrapperList.add(pw); 
            mapProductName.put(pb.id,pb.product2.name);
        }
        
        if (ProductWrapperList.size() == 0 && isNotFound)
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, status_code__c.getValues('8011').status_message__c));
    }
    
    public void SelectDomain()
    {
        
		String pageName = ApexPages.CurrentPage().getUrl();
        system.debug('pageName='+pageName);
        if(pageName.containsIgnoreCase('mobile') && optLine.size()>0 ){
            doSaveMobile();
        }else{
            for (ProductWrapper pw : ProductWrapperList)
            {
                if (pw.IsSelected == true || Test.isRunningTest())
                    optLine.add(new opportunityLineItem(opportunityId = oppObj.id, PricebookEntryId = pw.pricebook.Id, Quantity = 1));
            }
            ProductWrapperList = new List<ProductWrapper>();
            if (optLine.size() > 0)
            {
                isSelect = true;
            }else
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, status_code__c.getValues('8004').status_message__c));
            system.debug(optLine);
        }
    }
    /*
    public void backSelectDomain()
    {
        isSelect = false;
        optLine = new list<opportunityLineItem>();
    }
    */
    
    public pageReference doSave()
    {
        try {
            for (opportunityLineItem o : optLine)
            {
            
                if ((o.RevisedStartMonth__c != null && o.RevisedStartYear__c == null) || (o.RevisedStartMonth__c == null && o.RevisedStartYear__c != null)
                    || (o.RevisedStartMonthFee__c != null && o.RevisedStartYearFee__c == null) || (o.RevisedStartMonthFee__c == null && o.RevisedStartYearFee__c != null))
                {
                    string errorText = '';
                    if (o.RevisedStartMonth__c != null && o.RevisedStartYear__c == null) errorText += '<br/>Please Select NI Expected Start Year';
                    if (o.RevisedStartMonth__c == null && o.RevisedStartYear__c != null) errorText += '<br/>Please Select NI Expected Start Month';
                    if (o.RevisedStartMonthFee__c != null && o.RevisedStartYearFee__c == null) errorText += '<br/>Please Select Fee Expected Start Year';
                    if (o.RevisedStartMonthFee__c == null && o.RevisedStartYearFee__c != null) errorText += '<br/>Please Select Fee Expected Start Month';
                    
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, mapProductName.get(o.pricebookentryid) + errorText));
                    return null;
                }
      
                if (o.UnitPrice == null) o.UnitPrice = 0;
                if (o.quantity == null) o.quantity = 0;
                if (o.Expected_Util_Year_NI__c == null) o.Expected_Util_Year_NI__c = 0.00;
                if (o.Expected_Util_Year_Fee__c == null) o.Expected_Util_Year_Fee__c = 0.00;
                
                if (o.UnitPrice < 0 || o.quantity < 0 || o.Tenor_Years__c < 0 || 
                    o.Notional_Amount__c < 0 || o.Expected_Util_Year_NI__c < 0 || o.Expected_Util_Year_Fee__c < 0 ||
                    o.Expected_Util_Year_NI__c > 100 || o.Expected_Util_Year_Fee__c > 100)
                {
                
                    string fieldNegative = '';
                    if (o.UnitPrice < 0) fieldNegative += '<br/>&nbsp;&nbsp;-Vol. / Limit (THB)';
                    if (o.quantity < 0) fieldNegative += '<br/>&nbsp;&nbsp;-Units of Acct. / Limits';
                    if (o.Tenor_Years__c < 0) fieldNegative += '<br/>&nbsp;&nbsp;-Tenor (Years)';
                    if (o.Notional_Amount__c < 0) fieldNegative += '<br/>&nbsp;&nbsp;-All Bank Vol. (Limit) for only Syndicate loan';
                    if (o.Expected_Util_Year_NI__c < 0) fieldNegative += '<br/>&nbsp;&nbsp;-NI % Expected Utz. / Year';
                    if (o.Expected_Util_Year_Fee__c < 0) fieldNegative += '<br/>&nbsp;&nbsp;-Fee % Expected Utz. / Year';
                    string errorTextNegative = '';
                    if (fieldNegative != '') 
                        errorTextNegative = status_code__c.getValues('8008').status_message__c 
                                            + fieldNegative;
                    
                    string fieldExceedValue = '';
                    if (o.Expected_Util_Year_NI__c > 100) fieldExceedValue += '<br/>&nbsp;&nbsp;-NI % Expected Utz. / Year';
                    if (o.Expected_Util_Year_Fee__c > 100) fieldExceedValue += '<br/>&nbsp;&nbsp;-Fee % Expected Utz. / Year';
                    string errorTextExceedValue = '';
                    if (fieldExceedValue != '') 
                        errorTextExceedValue = status_code__c.getValues('8015').status_message__c
                                               + fieldExceedValue;
                    
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,mapProductName.get(o.pricebookentryid) + '<br/>' + errorTextNegative + '<br/>' + errorTextExceedValue));
                    return null;
                
                }else {
                
                    o.Expected_Utilization_Vol_NI__c = o.Unitprice * o.Quantity * (o.Expected_Util_Year_NI__c/100);
                    o.Expected_Utilization_Vol_Fee__c = o.Unitprice * o.Quantity * (o.Expected_Util_Year_Fee__c/100);
                    
                    
                    if (o.OriginalStartMonth__c == null && o.OriginalStartYear__c == null && o.RevisedStartMonth__c != null && o.RevisedStartYear__c != null)
                    {
                     o.OriginalStartMonth__c = o.RevisedStartMonth__c;
                     o.OriginalStartYear__c = o.RevisedStartYear__c;
                    }
                    
                    if (o.OriginalStartMonthFee__c == null && o.OriginalStartYearFee__c == null && o.RevisedStartMonthFee__c != null && o.RevisedStartYearFee__c != null)
                    {
                     o.OriginalStartMonthFee__c = o.RevisedStartMonthFee__c;
                     o.OriginalStartYearFee__c = o.RevisedStartYearFee__c;
                    }
                    
                }
            }
    
       
            insert optLine;
            isSave = true;
            pageReference p = new ApexPages.StandardController(oppObj).view();
            p.setRedirect(true);
            return p;
        }catch(exception ex){
            system.debug(ex);
            return null;
        }
    }
    
    public void doSaveMobile()
    {
        try {
            boolean isNoError = true;
            for (opportunityLineItem o : optLine)
            {
            
                if ((o.RevisedStartMonth__c != null && o.RevisedStartYear__c == null) || (o.RevisedStartMonth__c == null && o.RevisedStartYear__c != null)
                    || (o.RevisedStartMonthFee__c != null && o.RevisedStartYearFee__c == null) || (o.RevisedStartMonthFee__c == null && o.RevisedStartYearFee__c != null))
                {
                    string errorText = '';
                    if (o.RevisedStartMonth__c != null && o.RevisedStartYear__c == null) errorText += '<br/>Please Select NI Expected Start Year';
                    if (o.RevisedStartMonth__c == null && o.RevisedStartYear__c != null) errorText += '<br/>Please Select NI Expected Start Month';
                    if (o.RevisedStartMonthFee__c != null && o.RevisedStartYearFee__c == null) errorText += '<br/>Please Select Fee Expected Start Year';
                    if (o.RevisedStartMonthFee__c == null && o.RevisedStartYearFee__c != null) errorText += '<br/>Please Select Fee Expected Start Month';
                    
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, mapProductName.get(o.pricebookentryid) + errorText));
                    isNoError = false;
                }
            
                if (o.UnitPrice == null) o.UnitPrice = 0;
                if (o.quantity == null) o.quantity = 0;
                if (o.Expected_Util_Year_NI__c == null) o.Expected_Util_Year_NI__c = 0.00;
                if (o.Expected_Util_Year_Fee__c == null) o.Expected_Util_Year_Fee__c = 0.00;
                
                if (o.UnitPrice < 0 || o.quantity < 0 || o.Tenor_Years__c < 0 || 
                    o.Notional_Amount__c < 0 || o.Expected_Util_Year_NI__c < 0 || o.Expected_Util_Year_Fee__c < 0 ||
                    o.Expected_Util_Year_NI__c > 100 || o.Expected_Util_Year_Fee__c > 100)
                {
                    
                    string fieldNegative = '';
                    if (o.UnitPrice < 0) fieldNegative += '<br/>&nbsp;&nbsp;-Vol. / Limit (THB)';
                    if (o.quantity < 0) fieldNegative += '<br/>&nbsp;&nbsp;-Units of Acct. / Limits';
                    if (o.Tenor_Years__c < 0) fieldNegative += '<br/>&nbsp;&nbsp;-Tenor (Years)';
                    if (o.Notional_Amount__c < 0) fieldNegative += '<br/>&nbsp;&nbsp;-All Bank Vol. (Limit) for only Syndicate loan';
                    if (o.Expected_Util_Year_NI__c < 0) fieldNegative += '<br/>&nbsp;&nbsp;-NI % Expected Utz. / Year';
                    if (o.Expected_Util_Year_Fee__c < 0) fieldNegative += '<br/>&nbsp;&nbsp;-Fee % Expected Utz. / Year';
                    string errorTextNegative = '';
                    if (fieldNegative != '') 
                        errorTextNegative = status_code__c.getValues('8008').status_message__c 
                                            + fieldNegative;
                    
                    string fieldExceedValue = '';
                    if (o.Expected_Util_Year_NI__c > 100) fieldExceedValue += '<br/>&nbsp;&nbsp;-NI % Expected Utz. / Year';
                    if (o.Expected_Util_Year_Fee__c > 100) fieldExceedValue += '<br/>&nbsp;&nbsp;-Fee % Expected Utz. / Year';
                    string errorTextExceedValue = '';
                    if (fieldExceedValue != '') 
                        errorTextExceedValue = status_code__c.getValues('8015').status_message__c
                                               + fieldExceedValue;
                    
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,mapProductName.get(o.pricebookentryid) + '<br/>' + errorTextNegative + '<br/>' + errorTextExceedValue));
                    isNoError = false;
                
                }else {
                
                    o.Expected_Utilization_Vol_NI__c = o.Unitprice * o.Quantity * (o.Expected_Util_Year_NI__c/100);
                    o.Expected_Utilization_Vol_Fee__c = o.Unitprice * o.Quantity * (o.Expected_Util_Year_Fee__c/100);
                    
                    
                    if (o.OriginalStartMonth__c == null && o.OriginalStartYear__c == null && o.RevisedStartMonth__c != null && o.RevisedStartYear__c != null)
                    {
                     o.OriginalStartMonth__c = o.RevisedStartMonth__c;
                     o.OriginalStartYear__c = o.RevisedStartYear__c;
                    }
                    
                    if (o.OriginalStartMonthFee__c == null && o.OriginalStartYearFee__c == null && o.RevisedStartMonthFee__c != null && o.RevisedStartYearFee__c != null)
                    {
                     o.OriginalStartMonthFee__c = o.RevisedStartMonthFee__c;
                     o.OriginalStartYearFee__c = o.RevisedStartYearFee__c;
                    }
                    
                }
            }
    
            if (isNoError)
            {
                insert optLine;
                isSave = true;
            }
        }catch(exception ex){
            system.debug(ex);
        }
    }
    
    public List<SelectOption> getFiscalYear()
    {
        List<SelectOption> fiscalyearoption = new List<SelectOption>();
        List<Account_Plan_Fiscal_Year__c> yearlistitem = [SELECT ID,Name,AD_Year__c,BE_Year__c FROM Account_Plan_Fiscal_Year__c WHERE ID!=null ORDER BY Name];
        fiscalyearoption.add(new SelectOption('','--None--'));
        for(Account_Plan_Fiscal_Year__c year : yearlistitem){
            fiscalyearoption.add(new SelectOption(year.AD_Year__c,year.AD_Year__c));
        }         
        return fiscalyearoption;
    }
    
    public class ProductWrapper 
    {
        public boolean IsSelected {get;set;}
        public pricebookEntry pricebook {get;set;}  
    }
    
}