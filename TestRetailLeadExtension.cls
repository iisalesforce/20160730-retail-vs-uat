@isTest
public class TestRetailLeadExtension {
    
    static{
        TestUtils.createAppConfig();
    }
    static testmethod void testPositiveLeadCreation(){
        Test.startTest();
        	Test.setCurrentPage(Page.RetailLeadCreation);
        	Lead lead = new Lead();
        	lead.LastName = 'xxx';
        	lead.RTL_Mobile_Number__c = '0984387103';
        	ApexPages.StandardController std = new ApexPages.StandardController(lead);
        	ApexPages.currentPage().getParameters().put('saveAndNew', 'saveAndNew');
        	RetailLeadExtension ext = new RetailLeadExtension(std);
        	ext.save();
        	ext.getisCreateMode();
        	ext.cancelLead();
        	ext.getDisplayS1();
        Test.stopTest();
        
    }
    
    static testmethod void testNegativeLeadCreation(){
        Test.startTest();
        	Test.setCurrentPage(Page.RetailLeadCreation);  
        	ApexPages.StandardController std = new ApexPages.StandardController(new Lead());
        	RetailLeadExtension ext = new RetailLeadExtension(std);
        	ext.save();
        	ext.getisCreateMode();
        Test.stopTest();
        
    }
    
    static testmethod void testPositiveLeadUpdate(){
        Lead lead = new Lead();
        lead.LastName = 'xxx';
        lead.Company = 'xxx';
        lead.RTL_Mobile_Number__c = '0984387103';
        insert lead;
        
        Test.startTest();
        	Test.setCurrentPage(Page.RetailLeadCreation);
        	ApexPages.StandardController std = new ApexPages.StandardController(lead);
        	RetailLeadExtension ext = new RetailLeadExtension(std);
        	ext.save();
        	ext.getisCreateMode();
        	ext.cancelLead();
        Test.stopTest();
        
    }
}