@isTest
public class AccountPlanProductStrategyExTest {

	public static Profile m_Admin;
	public static Profile m_SEProfile;
	public static Profile m_BDMProfile;


	public static User m_SeUser;
	public static User m_BDMUser;



	static {
		TestUtils.createAppConfig();
		TestUtils.createStatusCode();
		TestUtils.createDisqualifiedReason();
		TestUtils.CreateAddress();
		// get Profile
		m_Admin = ServiceTestUtils.getProfileByName(ServiceTestUtils.EProfile.SystemAdmin);
		m_SEProfile = ServiceTestUtils.getProfileByName(ServiceTestUtils.EProfile.TMBSegmentHead);
		m_BDMProfile = ServiceTestUtils.getProfileByName(ServiceTestUtils.EProfile.TMBBDM);
		// createUsers(integer startNo, Integer size,String FName,String LName,String Email,ID setProfileID,Boolean isSESegment,Boolean doInsert) {

		system.debug(':::: Profile Id for SE : ' + m_SEProfile.Id);

		m_SeUser = ServiceTestUtils.createUsers(1, 1, 'tmbbank1', 'TMBLName1', 'tmb@tmbbank.com', m_SEProfile.Id, true, true) [0];
		m_BDMUser = ServiceTestUtils.createUsers(3, 1, 'tmbbank2', 'TMBLName2', 'tmb@tmbbank.com', m_BDMProfile.Id, true, true) [0];

		System.debug('SE USER : ' + m_SeUser.Id);


		List<sObject> ls = Test.loadData(AcctPlan_Questionnaire_Template__c.sObjectType /*API Name*/, 'AcctPlanQuestionnaireTemplate' /*Static Resource Name*/);

		AccountPlanWalletTestUtilities.createStandardFee();
		AccountPlanWalletTestUtilities.createBanks();
		AccountPlanWalletTestUtilities.createCurrencies();
		AccountPlanTestUtilities.getAcctPlanMode();
	}



	@isTest
	private static void test_ConstructorSeWithGroup() {
		string year = System.today().year() + '';
		// Create Account 
		System.debug('Start : ' + m_SeUser.Id);
		List<Account> AccountList = AccountPlanTestUtilities.createAccounts(2, 'InitiateTest', 'Individual', m_SeUser.id, true, true);

		System.debug('Create account to group');
		// Bind to Group
		Group__c mastergroup = AccountPlanTestUtilities.createGroupMaster(1, 'Initiatetest', false, true).get(0);
		for (account acct : AccountList) {
			acct.Group__c = mastergroup.id;
		}
		update AccountList;
		// Set Target
		AccountPlanTestUtilities.createAccounts(1, 'InitiateNonGroupTest', 'Individual', m_SeUser.id, true, true);
		List<Target__c> TaragetList = AccountPlanTestUtilities.createTargetNI(5, m_SeUser.id, true);


		List<Account> acctForCompanyProfile = new List<Account> ();
		acctForCompanyProfile.add(AccountList.get(0));
		system.runAs(m_SeUser) {
			List<AcctPlanCompanyProfile__c> comprofileList = AccountPlanTestUtilities.createCompanyProfileByAccount(acctForCompanyProfile, true);
			AcctPlanCompanyProfile__c comprofile = comprofileList.get(0);
			List<group__c> mgroupList = new List<group__c> ();
			mgroupList.add(mastergroup);


			AcctPlanGroupProfile__c groupprofile = AccountPlanTestUtilities.createGroupProfilebyGroup(mgroupList, true).get(0);
			groupprofile.Year__c = year;
			update groupprofile;
			comprofile.AcctPlanGroup__c = groupprofile.id;

			update comprofile;




			AcctPlanPortfolio__c portfolio = AccountPlanTestUtilities.createAccountPlanPortfolio(m_SeUser.id, year, 10000000, true);
			AcctPlanWallet__c AcctPlanwallet = AccountPlanWalletTestUtilities.createAccountPlanWallet(comprofile.id, true);
			List<AcctPlanContribution__c> contributionlist = AccountPlanTestUtilities.createRenevueContribution(3, null, comprofile.id);

			AccountPlanWalletTestUtilities.createWalletCurrency(AcctPlanwallet.id);
			AccountPlanWalletTestUtilities.createQuestionnaireTemplate();
			AccountPlanWalletTestUtilities.createWalletDomainITables(AcctPlanwallet.id);
			Test.startTest();
			PageReference groupViewPage = Page.AccountPlanProductStrategy;
			groupViewPage.getParameters().put('GroupID', groupprofile.id);
			groupViewPage.getParameters().put('CompanyID', comprofile.id);
			groupViewPage.getParameters().put('walletID', AcctPlanwallet.id);
			Test.setCurrentPage(groupViewPage);
			ApexPages.StandardController sctrl = new ApexPages.StandardController(new AcctPlanProdStrategy__c());
			AccountPlanProductStrategyEx ctrl = new AccountPlanProductStrategyEx(sctrl);

			ctrl.InitData();
			ctrl.refresh();
			ctrl.EditPage();
			ctrl.cancel();
			AccountPlanProductStrategyService.getGroupCompaniesProductStrategyInfo(new Set<Id> { groupprofile.Id });
			AccountPlanProductStrategyService.AcctPlanProdStrategyCompanyInfo dto = AccountPlanProductStrategyService.getGroupCompaniesProductStrategyInfoV2(groupprofile.Id);
			decimal val1 = dto.TotalAccount;
			dto.getMapGroupWalletSizing.get(1.00);
			dto.getMapGroupActualNiRolling.get(1.00);
			decimal val2 = dto.GrandGroupAdjustedNI;
			dto.geMapGroupAdjustedNI.get(1.00);
			dto.getMapGroupAdjustedNIDebug.get(1.00);
			dto.getMapGroupExpectedIncrementalNIStep5.get(1.00);
			dto.geMapExistingSOWGroup.get(1.00);
			dto.getMapGroupExpectedIncrementalNI.get(1.00);
			dto.getMapGroupTotalCompanyNI.get(1.00);

			dto.getMapAspirationSOWStep4.get(1.00);
			dto.getMapAspirationSOWStep6.get(1.00);
			dto.getMapAspirationSOW.get(1.00);
			//
			dto.getMapCompareExpectedToAspirationSOW.get(1.00);
			decimal val3 = dto.GrandGroupWalletsizing;
			dto.getMapGrandGroupWalletSizingEachCompany.get(AccountList[0].Id);
			//
			decimal val4 = dto.GrandTotalGroupNI;
			decimal val5 = dto.GrandGroupExpectedIncrementalNIStep5;	
			decimal val6 = dto.GrandGroupExpectedIncrementalNI;
			decimal val7 = dto.GrandGroupActualNiRolling;
			Test.stopTest();           
			 
		}

	}


	@isTest
	private static void test_ConstructorNonSeNoneGroup() {

		// Create Account 
		System.debug('Start : ' + m_SeUser.Id);
		List<Account> AccountList = AccountPlanTestUtilities.createAccounts(2, 'InitiateTest', 'Individual', m_BDMUser.id, true, true);


		// Set Target
		AccountPlanTestUtilities.createAccounts(1, 'InitiateNonGroupTest', 'Individual', m_SeUser.id, true, true);
		List<Target__c> TaragetList = AccountPlanTestUtilities.createTargetNI(5, m_SeUser.id, true);


		List<Account> acctForCompanyProfile = new List<Account> ();
		acctForCompanyProfile.add(AccountList.get(0));
		system.runAs(m_BDMUser) {
			List<AcctPlanCompanyProfile__c> comprofileList = AccountPlanTestUtilities.createCompanyProfileByAccount(acctForCompanyProfile, true);
			AcctPlanCompanyProfile__c comprofile = comprofileList.get(0);
			comprofile.isMiniMode__c = false;
			update comprofile;
			string year = System.today().year() + '';

			AcctPlanPortfolio__c portfolio = AccountPlanTestUtilities.createAccountPlanPortfolio(m_SeUser.id, year, 10000000, true);


			AcctPlanWallet__c AcctPlanwallet = AccountPlanWalletTestUtilities.createAccountPlanWallet(comprofile.id, true);


			List<AcctPlanContribution__c> contributionlist = AccountPlanTestUtilities.createRenevueContribution(3, null, comprofile.id);

			AccountPlanWalletTestUtilities.createWalletCurrency(AcctPlanwallet.id);
			AccountPlanWalletTestUtilities.createQuestionnaireTemplate();
			AccountPlanWalletTestUtilities.createWalletDomainITables(AcctPlanwallet.id);

			Test.startTest();
			PageReference groupViewPage = Page.AccountPlanProductStrategy;
			groupViewPage.getParameters().put('GroupID', '');
			groupViewPage.getParameters().put('CompanyID', comprofile.id);
			groupViewPage.getParameters().put('walletID', AcctPlanwallet.id);
			Test.setCurrentPage(groupViewPage);
			ApexPages.StandardController sctrl = new ApexPages.StandardController(new AcctPlanProdStrategy__c());
			AccountPlanProductStrategyEx ctrl = new AccountPlanProductStrategyEx(sctrl);
			ctrl.InitData();
			ctrl.refresh();
			ctrl.save();


			AccountPlanProductStrategyService.getCompaniesProductStrategyInfo(new Set<Id> { comprofile.Id });



			Test.stopTest();



		}

	}







}