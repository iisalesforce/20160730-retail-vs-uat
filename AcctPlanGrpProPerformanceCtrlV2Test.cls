@isTest
public class AcctPlanGrpProPerformanceCtrlV2Test {

	public static Profile m_Admin;
	public static Profile m_SEProfile;
	public static Profile m_BDMProfile;
	public static User m_SeUser;
	public static User m_BDMUser;
	static {
		TestUtils.createAppConfig();
		TestUtils.createStatusCode();
		TestUtils.createDisqualifiedReason();
		TestUtils.CreateAddress();
		// get Profile
		m_Admin = ServiceTestUtils.getProfileByName(ServiceTestUtils.EProfile.SystemAdmin);
		m_SEProfile = ServiceTestUtils.getProfileByName(ServiceTestUtils.EProfile.TMBSegmentHead);
		m_BDMProfile = ServiceTestUtils.getProfileByName(ServiceTestUtils.EProfile.TMBBDM);
		// createUsers(integer startNo, Integer size,String FName,String LName,String Email,ID setProfileID,Boolean isSESegment,Boolean doInsert) {

		system.debug(':::: Profile Id for SE : ' + m_SEProfile.Id);

		m_SeUser = ServiceTestUtils.createUsers(1, 1, 'tmbbank1', 'TMBLName1', 'tmb@tmbbank.com', m_SEProfile.Id, true, true) [0];
		m_BDMUser = ServiceTestUtils.createUsers(3, 1, 'tmbbank2', 'TMBLName2', 'tmb@tmbbank.com', m_BDMProfile.Id, true, true) [0];

		System.debug('SE USER : ' + m_SeUser.Id);

		List<sObject> products = Test.loadData(Product2.sObjectType /*API Name*/, 'ProductMaster' /*Static Resource Name*/);
		List<sObject> ls = Test.loadData(AcctPlan_Questionnaire_Template__c.sObjectType /*API Name*/, 'AcctPlanQuestionnaireTemplate' /*Static Resource Name*/);
		List<sObject> sectionConfig = Test.loadData(Account_Plan_Deposit_Section__c.sObjectType /*API Name*/, 'DepositeSection' /*Static Resource Name*/);

		AccountPlanWalletTestUtilities.createStandardFee();
		AccountPlanWalletTestUtilities.createBanks();
		AccountPlanWalletTestUtilities.createCurrencies();
		AccountPlanTestUtilities.getAcctPlanMode();
	}
	@isTest
	private static void test_GroupPerformance() {
		IAccountPlanRefreshService mock = new AccountPlanRefreshServiceBatchMock();
		AccountPlanRefreshService.setMockservice(mock);



		IAcctPlanGroupWalletLockService mock2 = new AcctPlanGroupWalletLockServiceMock();
		AcctPlanGroupWalletLockService.setMockservice(mock2);





		//Test.setMock(WebServiceMock.class, new TMBAccountPlanServiceProxyMock());
		Map<Id, Id> accountsSet = new Map<Id, Id> ();
		string year = System.today().year() + '';
		// Create Account 
		System.debug('Start : ' + m_SeUser.Id);
		List<Account> AccountList = AccountPlanTestUtilities.createAccounts(2, 'InitiateTest', 'Individual', m_SeUser.id, true, true);

		System.debug('Create account to group');
		// Bind to Group
		Group__c mastergroup = AccountPlanTestUtilities.createGroupMaster(1, 'Initiatetest', false, true).get(0);
		for (account acct : AccountList) {
			acct.Group__c = mastergroup.id;
			accountsSet.put(acct.Id, acct.Id);
		}
		update AccountList;
		// Set Target
		AccountPlanTestUtilities.createAccounts(1, 'InitiateNonGroupTest', 'Individual', m_SeUser.id, true, true);
		List<Target__c> TaragetList = AccountPlanTestUtilities.createTargetNI(5, m_SeUser.id, true);


		List<Account> acctForCompanyProfile = new List<Account> ();
		acctForCompanyProfile.add(AccountList.get(0));
		system.runAs(m_SeUser) {
			List<AcctPlanCompanyProfile__c> comprofileList = AccountPlanTestUtilities.createCompanyProfileByAccount(acctForCompanyProfile, true);
			AcctPlanCompanyProfile__c comprofile = comprofileList.get(0);
			List<group__c> mgroupList = new List<group__c> ();
			mgroupList.add(mastergroup);


			AcctPlanGroupProfile__c groupprofile = AccountPlanTestUtilities.createGroupProfilebyGroup(mgroupList, true).get(0);
			groupprofile.Year__c = year;
			update groupprofile;
			comprofile.AcctPlanGroup__c = groupprofile.id;


			AcctPlanPortfolio__c portfolio = AccountPlanTestUtilities.createAccountPlanPortfolio(m_SeUser.id, year, 10000000, true);
			AcctPlanWallet__c AcctPlanwallet = AccountPlanWalletTestUtilities.createAccountPlanWallet(comprofile.id, true);
			List<AcctPlanContribution__c> contributionlist = AccountPlanTestUtilities.createRenevueContribution(3, null, comprofile.id);






			comprofile.Portfolio__c = portfolio.Id;
			AcctPlanCompanyPort__c comport = new AcctPlanCompanyPort__c();

			comport.Account__c = comprofile.Account__c;
			comport.Account_Plan_Portfolio__c = portfolio.Id;
			comport.Account_Name__c = 'ddd';
			insert comport;
			update comprofile;


			AccountPlanWalletTestUtilities.createWalletCurrency(AcctPlanwallet.id);
			AccountPlanWalletTestUtilities.createQuestionnaireTemplate();
			AccountPlanWalletTestUtilities.createWalletDomainITables(AcctPlanwallet.id);




			Test.startTest();

			AcctPlanCompanyProfile__c com1 = comprofileList[0];

			// Create Performance 
			AcctAnnualPerformance__c per = new AcctAnnualPerformance__c();

			per.Account__c = comprofile.Account__c;
			per.Transaction_Date__c = System.today().addMonths(- 3);
			per.CREDIT_FEE__c = 0.0;
			per.TMB_CUST_ID__c = 'TMBCUS00000001';
			insert per;
			AccountPlanRefreshService.initialStep0(com1.Account__c + '');
			AccountPlanRefreshService.RefreshProductStrategyPort(accountsSet.keySet(), year);
			AccountPlanRefreshService.RefreshNameProductStrategyPort(accountsSet.keySet(), year);

			AccountPlanProductStrategyService.createProductStrategyRecordToWallet(comprofile.Id, AcctPlanwallet.Id, groupprofile.id);

			AccountPlanRefreshService.RefreshWalletAndAnnualPerformanceRolling12Month(new Set<Id> { portfolio.Id });

			PageReference ViewPage = Page.AccountplanGroupPerformanceV2;
			ViewPage.getParameters().put('GroupID', groupprofile.id);
			ViewPage.getParameters().put('CompanyID', comprofile.id);
			ViewPage.getParameters().put('WalletID', AcctPlanwallet.id);
			Test.setCurrentPage(ViewPage);


			ApexPages.StandardController sctrl = new ApexPages.StandardController(new AcctPlanProdStrategy__c());
			AcctPlanGroupProjectedPerformanceCtrlV2 ctrl = new AcctPlanGroupProjectedPerformanceCtrlV2(sctrl);

			ctrl.Refresh();
			ctrl.initGroupWallet();
			ctrl.initGroupWalletV2(new List<AccountPlanProductStrategyService.AcctPlanProdStrategyInfo>());

			Test.stopTest();
		}

	}



















	public class AcctPlanGroupWalletLockServiceMock implements IAcctPlanGroupWalletLockService {
		public void Lock(Id groupId) { }
		public void Unlock(Id groupId) { }
		public boolean IsLock(id groupId) { return false; }
	}

	public class AccountPlanRefreshServiceBatchMock implements IAccountPlanRefreshService {
		public Boolean RefreshDepositInter(String acctPlanCompanyId, String acctPlanWallet, String acctPlanGruopId) {
			return true;
		}
		public Boolean RefreshDepositDomestic(String acctPlanCompanyId, String acctPlanWallet, String acctPlanGruopId) {
			return true;
		}
		public Boolean refreshWalletAndAnnualPerformanceRolling12Month(Set<ID> listPortfolioId) {
			return true;
		}

		public Boolean RefreshCompanyPerformanceProductStrategyForStep6(String companyProfileId) {
			return true;
		}
		public Boolean RefreshGroupPerformanceProductStrategyForStep6(String groupProfileId) { return true; }
		public Boolean RefreshPortfolioPerformanceProductStrategyForStep0(String portfolioId) { return true; }
		public List<AcctPlanProdStrategy__c> RefreshProductStrategyAndWalletByDomain(String acctPlanCompanyId, String acctPlanWallet, String acctPlanGruopId) {
			return new List<AcctPlanProdStrategy__c> ();

		}
		public void RefreshProductStrategyPort(Set<Id> accountIds, string year) { }
		public void RefreshProductStrategy(Set<Id> accountIds, string year, Id groupProfilesId) { }
		public void RefreshNameProductStrategyPort(Set<Id> accountIds, string year) { }
		public void RefreshGroupPerformance(Set<Id> accountId, string year) { }

		public List<AccountPlanRefreshService.CustomerWalletInfo> initialStep0(string accountIds) {
			return new List<AccountPlanRefreshService.CustomerWalletInfo> ();
		}

	}
}