public class RTL_TestUtility {
    public static final Id RTL_BRANCH_SALES_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB Retail Branch Sales Team' LIMIT 1].Id;    
    public static final String SUCCESS_CODE = '0000';
    public static final String QUERY_EXCEPTION_CODE = '1001';
    public static final String CALLOUT_EXCEPTION_CODE = '1002';
    public static final String FOUND_DUP_ALLOW_CREATE = '1010';
    public static final String FOUND_DUP_NOT_ALLOW_CREATE = '1011';
    public static final String WEBSERVICE_ERROR_CODE = '1003';
    public static final String DML_EXCEPTION_CODE = '1000';
    public static List<RTL_Channel_Referral__c> channelReferralList {
        get
        {
            if(channelReferralList == null){
                channelReferralList = new List<RTL_Channel_Referral__c>();
            }
            return channelReferralList;
        }set;
    }
    
    public static List<RTL_Interested_products_c__c> interestedProductList {
        get
        {
            if(interestedProductList == null){
                interestedProductList = new List<RTL_Interested_products_c__c>();
            }
            return interestedProductList;
        }set;
    }
    
   
    public static void createOpportunityRecordMap(){
        List<Opportunity_Stage_Mapping__c > OppStgMap = new List< Opportunity_Stage_Mapping__c >();
        Opportunity_Stage_Mapping__c  oppmap1 = new Opportunity_Stage_Mapping__c (Name='1',Application_status__c='Pre Work',Stage__c='CA-Prep',Opportunity_Type__c ='Credit');
        Opportunity_Stage_Mapping__c  oppmap2 = new Opportunity_Stage_Mapping__c (Name='2',Application_status__c='Rejected Appeal',Stage__c='CA-Prep',Opportunity_Type__c ='Credit');
        Opportunity_Stage_Mapping__c  oppmap3 = new Opportunity_Stage_Mapping__c (Name='3',Application_status__c='UW1',Stage__c='Approval Process',Opportunity_Type__c ='Credit');
        Opportunity_Stage_Mapping__c  oppmap4 = new Opportunity_Stage_Mapping__c (Name='4',Application_status__c='UW2',Stage__c='Approval Process',Opportunity_Type__c ='Credit');
        Opportunity_Stage_Mapping__c  oppmap5 = new Opportunity_Stage_Mapping__c (Name='5',Application_status__c='Cancelled',Stage__c='Cancelled',Opportunity_Type__c ='Non-credit');
        Opportunity_Stage_Mapping__c  oppmap6 = new Opportunity_Stage_Mapping__c (Name='6',Application_status__c='Completed',Stage__c='Issued by Centralized',Opportunity_Type__c ='Non-credit');
        oppStgMap.add(oppmap1);
        oppStgMap.add(oppmap2);
        oppStgMap.add(oppmap3);
        oppStgMap.add(oppmap4);
        oppStgMap.add(oppmap5);
        oppStgMap.add(oppmap6);
        insert oppStgMap;
    }
    
    public static void createRetailMasterProducts(Boolean doInsert){
        //List of retail master product records to be inserted 
        List<RTL_product_master__c> retailMasterProductsToInsert = new List<RTL_product_master__c>();
                                                                                  
        retailMasterProductsToInsert.add(new RTL_product_master__c ( 
                Active__c = TRUE,
                Product_Code__c = '0001',
                Product_Group__c = 'Deposit',
                Product_Sub_group__c = 'Deposit TestSubgroup'));
        
        retailMasterProductsToInsert.add(new RTL_product_master__c ( 
                Active__c = TRUE,
                Product_Code__c = '0002',
                Product_Group__c = 'Credit Card',
                Product_Sub_group__c = 'Credit Card TestSubgroup'));
        
        retailMasterProductsToInsert.add(new RTL_product_master__c ( 
                Active__c = TRUE,
                Product_Code__c = '0003',
                Product_Group__c = 'Bancassuarnce',
                Product_Sub_group__c = 'Bancassuarnce  TestSubgroup'));
        
        retailMasterProductsToInsert.add(new RTL_product_master__c ( 
                Active__c = TRUE,
                Product_Code__c = '0004',
                Product_Group__c = 'Investment',
                Product_Sub_group__c = 'Investment TestSubgroup'));
        
        retailMasterProductsToInsert.add(new RTL_product_master__c ( 
                Active__c = TRUE,
                Product_Code__c = '0005',
                Product_Group__c = 'Loans',
                Product_Sub_group__c = 'Loans TestSubgroup'));
        
        retailMasterProductsToInsert.add(new RTL_product_master__c ( 
                Active__c = TRUE,
                Product_Code__c = '0006',
                Product_Group__c = 'Retentions',
                Product_Sub_group__c = 'Retentions TestSubgroup'));
        
        retailMasterProductsToInsert.add(new RTL_product_master__c ( 
                Active__c = TRUE,
                Product_Code__c = '0007',
                Product_Group__c = 'Others',
                Product_Sub_group__c = 'Others TestSubgroup'));
        
        if(doInsert){
            insert retailMasterProductsToInsert;
        }
        System.debug('::::: insert retailMasterProductsTest is : ' + retailMasterProductsToInsert.size() +' row :::::');
    }
    
    public static List<RTL_Interested_products_c__c> createInterestedProducts(List<Lead> leads, Boolean doInsert){
        //List of interested product records to be inserted after associating with the lead
        List<RTL_Interested_products_c__c> interestedProductsToInsert = new List<RTL_Interested_products_c__c>();

        //select deposit product as master always
        RTL_product_master__c masterProduct = [Select Id, Product_Code__c, Product_Group__c, Product_Sub_group__c
                         from RTL_product_master__c 
                         where Product_Code__c = : '0001' and Active__c = : true limit 1];
        //select credit card as non primary ne always
        RTL_product_master__c masterNoPrimaryProduct = [Select Id, Product_Code__c, Product_Group__c, Product_Sub_group__c
                         from RTL_product_master__c 
                         where Product_Code__c = : '0002' and Active__c = : true limit 1];                              
         
        for (Lead lead:leads){
            //Create New PRIMARY interested product record and then associate with the Lead
            interestedProductsToInsert.add(new RTL_Interested_products_c__c ( 
                Branch_Code__c  = '0001',
                Co_borrower_1__c = 'Test Co Borrower1',
                Co_borrower_2__c = 'Test Co Borrower2',
                Collateral_Type__c = 'Test Collateral Type',
                Debt__c = 5000.23,
                Income_SE__c = 3000.23,
                Income_SL__c =3000.23,
                Is_Primary__c = TRUE,
                Lead__c = lead.Id,
                Loan_Amount__c = 3000.23,
                product_group__c = masterProduct.Product_Group__c,
                Product_Name__c = masterProduct.Id,
                Product_Sub_Group__c = masterProduct.Product_Sub_group__c ));
            
            //Create New NON-PRIMARY interested product record and then associate with the Lead
            interestedProductsToInsert.add(new RTL_Interested_products_c__c ( 
                Branch_Code__c  = '0002',
                Co_borrower_1__c = 'Test Co Borrower1',
                Co_borrower_2__c = 'Test Co Borrower2',
                Collateral_Type__c = 'Test Collateral Type',
                Debt__c = 5000.23,
                Income_SE__c = 3000.23,
                Income_SL__c = 3000.23,
                Is_Primary__c = FALSE,
                Lead__c = lead.Id,
                Loan_Amount__c = 3000.23,
                product_group__c = masterNoPrimaryProduct.Product_Group__c,
                Product_Name__c = masterNoPrimaryProduct.Id,
                Product_Sub_Group__c = masterNoPrimaryProduct.Product_Sub_group__c ));
        }
        interestedProductList = interestedProductsToInsert;
        
        if(doInsert){
            insert interestedProductList;
        }
        System.debug('::::: insert interestedProductsTest is : ' + interestedProductList.size() +' row :::::');
        return interestedProductList;
    }
    
    public static void createChannelReferralToInsert(List<Opportunity> opportunities, User user, Boolean doInsert){
        //List of channel records to be inserted
        List<RTL_Channel_Referral__c> channelReferralsToInsert = new List<RTL_Channel_Referral__c>();
                                                                                  
        for (Opportunity oppt:opportunities){
        //Create New Channel Referral record and then associate with the opportunity
            channelReferralsToInsert.add(new RTL_Channel_Referral__c ( RTL_Branch_Code__c = user.RTL_Branch_Code__c,
                RTL_Opportunity__c = oppt.Id,
                RTL_Start_Date__c =DateTime.now(),
                RTL_Owner__c = user.Id,
                Name = user.RTL_Channel__c));
        }
        channelReferralList = channelReferralsToInsert;
        
        if(doInsert){
            insert channelReferralsToInsert;
        }
        System.debug('::::: insert channelreferralTest is : ' + channelReferralList.size() +' row :::::');
        //return channelReferralsToInsert;
    }
    
    /* This test data is used for lead queue auto assignment */
    public static Map<String, Group> createLeadQueues(Boolean doInsert) {
        //Keep the list of the existing lead queues
        Map<String, Group> queueMap = new Map<String, Group>();
        for(Group queue : [Select Id, Name, DeveloperName from Group where Type = 'Queue'])
            queueMap.put(queue.DeveloperName, queue);
                    
        /* Create two lead queues with name RTLQ_001 & RTLQ_Outbound */
        List<Group> leadQueues = new List<Group>();
        Group queue = new Group(Name='RTLQ_001', DeveloperName='RTLQ_001', Type='Queue');
        if (queueMap.get(queue.DeveloperName) == null) leadQueues.add(queue);
        queue = new Group(Name='RTLQ_Outbound', DeveloperName='RTLQ_Outbound', Type='Queue');
        if (queueMap.get(queue.DeveloperName) == null) leadQueues.add(queue);
        insert leadQueues;

        List<QueuesObject> leadQOs = new List<QueuesObject>();
        QueuesObject qso = null;
        for (Group q: leadQueues) {
            qso = new QueueSObject(QueueID = q.id, SobjectType = 'Lead');
            leadQOs.add(qso);
            queueMap.put(q.DeveloperName, q);
        }
        if(doInsert) insert leadQOs;
        return queueMap;
    }
    
    /* This test data is used for lead queue lookup for branch code from branch_zone */
    public static List<Branch_and_Zone__c> createBranchZone(Integer size, Boolean doInsert) {
        List<Branch_and_Zone__c> branchList = new List<Branch_and_Zone__c>();
        
        /* Create branch zone mapping branch name and branch code */
        for (Integer i=0; i<size; i++) {
            branchList.add(new Branch_and_Zone__c(Name='RTLQ_00'+(i+1)+' (00'+(i+1)+')', Branch_Name__c='RTLQ_00'+(i+1), Branch_Code__c='00'+(i+1)));
        }
        if(doInsert) insert branchList;
        return branchList;
    }   
    
    /* This test data is used for lead interested product */
    public static List<RTL_product_master__c> createRetailProducts(Boolean doInsert) {
        List<RTL_product_master__c> productList = new List<RTL_product_master__c>();
        
        RTL_product_master__c product = new RTL_product_master__c(Name='So Fast Test', Product_Group__c='Credit Card', Product_Sub_group__c='Credit Card');
        productList.add(product);
        product = new RTL_product_master__c(Name='So Smart Test', Product_Group__c='Credit Card', Product_Sub_group__c='Credit Card');
        productList.add(product);
        product = new RTL_product_master__c(Name='Home Loan Test', Product_Group__c='Lending', Product_Sub_group__c='Secured Lending');
        productList.add(product);
        if (doInsert)   insert productList;
        return productList;
    }
        
    /* This test data is used for web-to-lead assignment criterias */
    public static void createLeadAssignCriterias(Boolean doInsert) {
        List<RTL_Assignment_Criterias__c> criteriaList = new List<RTL_Assignment_Criterias__c>();
        List<RTL_product_master__c> productList = createRetailProducts(true);
        RTL_Assignment_Criterias__c assignCriteria = null;
        for (RTL_product_master__c product: productList) {
            if(product.Name == 'So Fast Test') {
                assignCriteria = new RTL_Assignment_Criterias__c(Product_Name__c=product.Id, Destination_BKK__c='Outbound', Destination_UPC__c='Branch');
            } else if (product.Name == 'So Smart Test') {
                assignCriteria = new RTL_Assignment_Criterias__c(Product_Name__c=product.Id, Destination_BKK__c='Branch', Destination_UPC__c='Branch');
            } else if (product.Name == 'Home Loan Test') {
                assignCriteria = new RTL_Assignment_Criterias__c(Product_Name__c=product.Id, Destination_BKK__c='Outbound', Destination_UPC__c='Outbound');
            }
            criteriaList.add(assignCriteria);
        }
        if (doInsert) insert criteriaList;
    }    
    
    public static User createRetailTestUser(Boolean doInsert) {
        User retailUser = new User(FirstName='retail', LastName='test user', 
                                UserName='rtl_testuser@tmb.com',
                                Email='rtl_testuser@tmb.com', Alias='RTLUser',
                                LocaleSidKey='en_US', LanguageLocaleKey='en_US', EmailEncodingKey='ISO-8859-1', 
                                ProfileId = RTL_BRANCH_SALES_PROFILE_ID, TimeZoneSidKey='America/New_York',isActive = true,
                                RTL_Branch_Code__c='001',
                                Employee_ID__c='RTL01', RTL_Branch__c='RTLQ_001 (001)');

        
        if(doInsert) insert retailUser;
        return retailUser;
    }
    
    public static User createRetailTestUserWithBranch(Boolean doInsert) {
        User retailUser = new User(FirstName='retail', LastName='test user2', 
                                UserName='rtl_testuser2@tmb.com',
                                Email='rtl_testuser2@tmb.com', Alias='RTLUser2',
                                LocaleSidKey='en_US', LanguageLocaleKey='en_US', EmailEncodingKey='ISO-8859-1', 
                                ProfileId = RTL_BRANCH_SALES_PROFILE_ID, TimeZoneSidKey='America/New_York',isActive = true,
                                RTL_Branch_Code__c='002',
                                Employee_ID__c='RTL02', RTL_Branch__c='RTLQ_002 (002)');

        
        if(doInsert) insert retailUser;
        return retailUser;
    }       
    
    public static User createRetailTestUserWithoutBranch(Boolean doInsert) {
        User retailUser = new User(FirstName='retail', LastName='test user3', 
                                UserName='rtl_testuser3@tmb.com',
                                Email='rtl_testuser3@tmb.com', Alias='RTLUser3',
                                LocaleSidKey='en_US', LanguageLocaleKey='en_US', EmailEncodingKey='ISO-8859-1', 
                                ProfileId = RTL_BRANCH_SALES_PROFILE_ID, TimeZoneSidKey='America/New_York',isActive = true,
                                Employee_ID__c='RTL03');

        
        if(doInsert) insert retailUser;
        return retailUser;
    }       
    
    public static List<User> createRetailTestUserOppt(Boolean doInsert) {
        List<User> users = new List<User>();
        User retailUser1 = new User(Alias = 'RTL1', Email='standarduser-ii@rtl1.com.sit', 
                          EmailEncodingKey='UTF-8', LastName='rtl1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = RTL_BRANCH_SALES_PROFILE_ID,Employee_ID__c='RTL04',
                          Zone_Code__c='HHHH',Segment__c='SS',Zone__c = 'TEST',
                          TimeZoneSidKey='America/Los_Angeles', UserName='rtl1@testorg.com',
                          RTL_Channel__c ='Outbound');
        users.add(retailUser1);
        User retailUser2 = new User(Alias = 'RTL2', Email='standarduser-ii2@rtl2.com.sit', 
                           EmailEncodingKey='UTF-8', LastName='rtl2', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = RTL_BRANCH_SALES_PROFILE_ID,Employee_ID__c='RTL05',
                           Zone_Code__c='HHHH',Segment__c='SS',Zone__c = 'TEST',
                           TimeZoneSidKey='America/Los_Angeles', UserName='rtl2@testorg.com.sit',
                           RTL_Channel__c ='Outbound'); 
        users.add(retailUser2);
        User retailUser3 = new User(Alias = 'RTL3', Email='standarduser-ii2@rtl3.com.sit', 
                           EmailEncodingKey='UTF-8', LastName='rtl2', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = RTL_BRANCH_SALES_PROFILE_ID,Employee_ID__c='RTL06',
                           Zone_Code__c='HHHH',Segment__c='SS',Zone__c = 'TEST',
                           TimeZoneSidKey='America/Los_Angeles', UserName='rtl3@testorg.com.sit',
                           RTL_Branch_Code__c ='0001', RTL_Channel__c ='Branch'); 
        users.add(retailUser3);         
        
        if(doInsert) insert users;
        return users;
    }    
    
    public static void createLeadTaskAndEvent(List<Lead> leads, Boolean doInsert) {
        List<Task> taskList = new List<Task>();
        List<Event> eventList = new List<Event>();
        
        for (Lead lead:leads){
            taskList.add(new Task(Subject = 'test lead task', WhoID = lead.Id));
            eventList.add(new Event(Subject = 'test lead event', WhoID = lead.Id, DurationInMinutes=6000, ActivityDateTime=datetime.newInstance(2017, 05,05))); 
        }
            
        if(doInsert) {
            insert taskList;
            //insert eventList;
        }
    }    
    
    public static void enableWebToLeadAutoAssign(Boolean doInsert) {
        RTL_Lead_Assignment__c leadAssign = new RTL_Lead_Assignment__c();
        leadAssign.Name = 'Auto Assign';
        leadAssign.Enable__c = true;
        
        if(doInsert) insert leadAssign;
    }     
    
     public static void disableWebToLeadAutoAssign(Boolean doInsert) {
        RTL_Lead_Assignment__c leadAssign = new RTL_Lead_Assignment__c();
        leadAssign.Name = 'Auto Assign';
        leadAssign.Enable__c = false;
        
        if(doInsert) insert leadAssign;
    }     
    
    public static void setOpptRecordTypeMapping(Boolean doInsert) {
        Opportunity_Recordtype_Mapping__c opptMapping = new Opportunity_Recordtype_Mapping__c();
        opptMapping.Name = 'RetailCardandRDCStage';
        opptMapping.Product_Group__c = 'Deposit';
        opptMapping.Record_Type__c = 'Retail Deposit';
        opptMapping.Stage__c = 'Prospect (Deposit)';
    }
    
    public static List<Account> createAccounts(Integer size , Boolean doInsert)
    {
        List<Account> accountsToCreate = new List<Account>();
        RecordType recordType = [select Id from RecordType where developername='Retail_Prospect' and SobjectType = 'Account' and IsActive = true limit 1];
        for(Integer i =1 ; i<=size ;i++)
        {   
            Account acct = new Account(); 
            acct.Name = 'Test RTL Account '+i;
            acct.Phone ='1111111111';
            acct.Mobile_Number_PE__c  = '1111111111';
            acct.Company_Name_Temp__c  =  'TestRTLCompany'+i;
            acct.ID_Type_PE__c = 'Passport ID';
            acct.ID_Number_PE__c = '11111' + i;
            acct.RTL_NID__c = '1111' + i;
            acct.RecordType = recordType;
            
            accountsToCreate.add(acct);    
        }
        
        if(doInsert){
                insert accountsToCreate;
        }
        
        return accountsToCreate;
    }    
    
    public static Lead createLeadWithDupID(Boolean doInsert)
    {
        Lead lead = new Lead();
        RecordType recordType = [select Id from RecordType where developername='Retail_Banking' and IsActive = true limit 1];       
        lead.Title = 'TH';
        lead.Company = 'X';
        lead.FirstName = 'Test';
        lead.LastName = 'TestDupLead';
        lead.Email = 'testduplead@salesforce.com';
        lead.RTL_Mobile_Number__c = '1111111111';
        lead.RTL_Phone_Number__c = '222222222';
        lead.RTL_Office_Number__c = '333333333';
        lead.Status = 'New';
        lead.Street = 'aaa';
        lead.City = 'bbb';
        lead.State = 'CC';
        lead.PostalCode = '1111111';
        lead.Country = 'DD';
        lead.RecordType = recordType;
        lead.RTL_ID_Type__c = 'Passport ID';
        lead.RTL_Citizen_Id__c = '111111';
        
        if(doInsert){
            insert lead;
        }
        return lead;
    } 
    
    public static List<Lead> createLeads(Integer size , Boolean doInsert)
    {
        List<Lead> leads = new List<Lead>();
        Lead lead = null;
        RecordType recordType = [select Id from RecordType where developername='Retail_Banking' and IsActive = true limit 1];
        for(Integer i=0 ; i<size ;i++) {        
            lead = new Lead();
            lead.Title = 'TH';
            lead.Company = 'X';
            lead.FirstName = 'Test'+i;
            lead.LastName = 'TestLead';
            lead.Email = 'test'+i+'@salesforce.com';
            lead.RTL_Mobile_Number__c = '1111111111';
            lead.RTL_Phone_Number__c = '222222222';
            lead.RTL_Office_Number__c = '333333333';
            lead.Status = 'New';
            lead.Street = 'aaa';
            lead.City = 'bbb';
            lead.State = 'CC';
            lead.PostalCode = '1111111';
            lead.Country = 'DD';
            lead.RecordType = recordType;
            lead.RTL_ID_Type__c = 'Passport ID';
            lead.RTL_Citizen_Id__c = 'aaaaaa';
            leads.add(lead);
        }
        
        if(doInsert){
            insert leads;
        }
        return leads;
    }
    
    /* This test data is used for positive test of web-to-lead FR-003 and FR-011 */
    public static void createPositiveWebToLead()
    {
        List<Lead> webToLeads = new List<Lead>();
        RecordType recordType = [select Id from RecordType where developername='Retail_Banking' and IsActive = true limit 1];   
        /* 1st web-to-lead is within region BKK, single product 'So Fast Test'
        * expected result is: 
        * 1. lead is created
        * 2. auto assign lead queue to Outbound queue RTLQ_Outbound (FR-011)
        * 3. interested product is added as primary (FR-003)
        */
        Lead lead = new Lead();
        lead.Company = 'X';
        lead.RTL_TMB_Campaign_Source__c = 'Web';
        lead.FirstName = 'Test1';
        lead.LastName = 'WebToLead';
        lead.Email = 'test1@salesforce.com';
        lead.City = 'Bangkok';
        lead.RTL_Branch_Name__c = '001';
        lead.RTL_Product_Name__c = 'So Fast Test';
        lead.Status = 'New';
        lead.RTL_ID_Type__c = 'Passport ID';
        lead.RTL_Citizen_Id__c = '11111';
        lead.RTL_Address_Line1__c = 'address1';     
        lead.RTL_Address_Line4__c = 'address4'; 
        lead.RecordType = recordType;
        webToLeads.add(lead);
        /* 2nd web-to-lead is outside region BKK, single product 'So Fast Test'
        * expected result is: 
        * 1. lead is created        
        * 2. auto assign lead queue to Branch queue same as branch name: RTLQ_001 (FR-011)
        * 3. interested product is added as primary (FR-003)
        */
        lead = new Lead(); 
        lead.Company = 'X';
        lead.RTL_TMB_Campaign_Source__c = 'Web';
        lead.FirstName = 'Test2';
        lead.LastName = 'WebToLead';
        lead.Email = 'test2@salesforce.com';
        lead.City = 'PhuketIsland';
        lead.RTL_Branch_Name__c = '001';
        lead.RTL_Product_Name__c = 'So Fast Test';
        lead.Status = 'New';
        lead.RTL_ID_Type__c = 'Passport ID';
        lead.RTL_Citizen_Id__c = '22222';   
        lead.RTL_Address_Line1__c = 'address1';     
        lead.RTL_Address_Line4__c = 'address4'; 
        lead.RecordType = recordType;       
        webToLeads.add(lead);
        /* 3rd web-to-lead is within region BKK, multiple products 'So Fast Test;So Smart Test'
        * expected result is: 
        * 1. lead is created        
        * 2. for multiple products, auto assign lead queue to Branch queue, e.g. RTLQ_001 (FR-011)
        * 3. two interested products are added, set 'So Fast Test' as primary (FR-003)
        */
        lead = new Lead(); 
        lead.Company = 'X';
        lead.RTL_TMB_Campaign_Source__c = 'Web';
        lead.FirstName = 'Test3';
        lead.LastName = 'WebToLead';
        lead.Email = 'test3@salesforce.com';
        lead.City = 'Bangkok';
        lead.RTL_Multiple_Interested_Product_s__c = true;
        lead.RTL_Branch_Name__c = '001';
        lead.RTL_Product_Name__c = 'So Fast Test;So Smart Test';
        lead.Status = 'New';    
        lead.RTL_ID_Type__c = 'Passport ID';
        lead.RTL_Citizen_Id__c = '33333';   
        lead.RTL_Address_Line1__c = 'address1';     
        lead.RTL_Address_Line4__c = 'address4';     
        lead.RecordType = recordType;
        webToLeads.add(lead);
        /* 4th web-to-lead is outside region BKK, single product 'Home Loan Test'
        * expected result is: 
        * 1. lead is created
        * 2. auto assign lead queue to Outbound queue RTLQ_Outbound (FR-011)
        * 3. interested product is added as primary (FR-003)
        */
        lead = new Lead();
        lead.Company = 'X';
        lead.RTL_TMB_Campaign_Source__c = 'Web';
        lead.FirstName = 'Test4';
        lead.LastName = 'WebToLead';
        lead.Email = 'test4@salesforce.com';
        lead.City = 'PhuketIsland';
        lead.RTL_Branch_Name__c = '001';
        lead.RTL_Product_Name__c = 'Home Loan Test';
        lead.Status = 'New';
        lead.RTL_ID_Type__c = 'Passport ID';
        lead.RTL_Citizen_Id__c = '44444';   
        lead.RTL_Address_Line1__c = 'address1';     
        lead.RTL_Address_Line4__c = 'address4'; 
        lead.RecordType = recordType;
        webToLeads.add(lead);       
        insert webToLeads;  
    }
    
    /* This test data is used for negative test of web-to-lead FR-003 and FR-011 */
    public static void createNegativeWebToLead()
    {
        List<Lead> webToLeads = new List<Lead>();
        RecordType recordType = [select Id from RecordType where developername='Retail_Banking' and IsActive = true limit 1];
        /* 1st web-to-lead is within region BKK, single product 'Bad Product'
        * expected result is: 
        * 1. lead is created
        * 2. lead queue is not assigned, Lead Owner is running user (FR-011)
        * 3. interested product is not added (FR-003)
        * 4. lead description is added with some explanation contains 'Bad Product'
        */
        Lead lead = new Lead(); 
        lead.Company = 'X';
        lead.RTL_TMB_Campaign_Source__c = 'Web';
        lead.FirstName = 'Test1';
        lead.LastName = 'NegativeWebToLead';
        lead.Email = 'negtest1@salesforce.com';
        lead.City = 'Bangkok';
        lead.RTL_Branch_Name__c = '001';
        lead.RTL_Product_Name__c = 'Bad Product';
        lead.RTL_Description__c = 'Bad Product';
        lead.Status = 'New';    
        lead.RTL_ID_Type__c = 'Passport ID';
        lead.RTL_Citizen_Id__c = '55555';
        lead.RTL_Address_Line1__c = 'address1';     
        lead.RTL_Address_Line4__c = 'address4';         
        lead.RecordType = recordType;       
        webToLeads.add(lead);
        /* 2nd web-to-lead is within region BKK, multiple products 'So Fast Test;Bad Product'
        * expected result is: 
        * 1. lead is created
        * 2. for multiple products, auto assign lead queue to Branch queue, e.g. RTLQ_001 (FR-011)
        * 3. only one interested product 'So Fast Test' is added as primary (FR-003)
        * 4. lead description is added with some explanation contains 'Bad Product'
        */
        lead = new Lead(); 
        lead.Company = 'X';
        lead.RTL_TMB_Campaign_Source__c = 'Web';
        lead.FirstName = 'Test2';
        lead.LastName = 'NegativeWebToLead';
        lead.Email = 'negtest2@salesforce.com';
        lead.City = 'Bangkok';
        lead.RTL_Multiple_Interested_Product_s__c = true;
        lead.RTL_Branch_Name__c = '001';
        lead.RTL_Product_Name__c = 'So Fast Test;Bad Product';
        lead.RTL_Description__c = 'Bad Product';
        lead.Status = 'New';    
        lead.RTL_ID_Type__c = 'Passport ID';
        lead.RTL_Citizen_Id__c = '66666';   
        lead.RTL_Address_Line1__c = 'address1';     
        lead.RTL_Address_Line4__c = 'address4';     
        lead.RecordType = recordType;       
        webToLeads.add(lead);
        /* 3rd web-to-lead is within region BKK, multiple products 'Bad Product;So Fast Test'
        * expected result is: 
        * 1. lead is created
        * 2. for multiple products, auto assign lead queue to Branch queue, e.g. RTLQ_001 (FR-011)
        * 3. only one interested product 'So Fast Test' is added as primary (FR-003)
        * 4. lead description is added with some explanation contains 'Bad Product'
        */
        lead = new Lead(); 
        lead.Company = 'X';
        lead.RTL_TMB_Campaign_Source__c = 'Web';
        lead.FirstName = 'Test3';
        lead.LastName = 'NegativeWebToLead';
        lead.Email = 'negtest3@salesforce.com';
        lead.City = 'Bangkok';
        lead.RTL_Multiple_Interested_Product_s__c = true;
        lead.RTL_Branch_Name__c = '001';
        lead.RTL_Product_Name__c = 'Bad Product;So Fast Test';
        lead.RTL_Description__c = 'Bad Product';
        lead.Status = 'New';
        lead.RTL_ID_Type__c = 'Passport ID';
        lead.RTL_Citizen_Id__c = '77777';   
        lead.RTL_Address_Line1__c = 'address1'; 
        lead.RTL_Address_Line4__c = 'address4';             
        lead.RecordType = recordType;   
        webToLeads.add(lead);
        /* 4th web-to-lead is within region BKK, multiple products 'Bad Product1;Bad Product2'
        * expected result is: 
        * 1. lead is created
        * 2. for multiple products, auto assign lead queue to Branch queue, e.g. RTLQ_001 (FR-011)
        * 3. no interested product is added (FR-003)
        * 4. lead description is added with some explanation contains 'Bad Productx'
        */
        lead = new Lead(); 
        lead.Company = 'X';
        lead.RTL_TMB_Campaign_Source__c = 'Web';
        lead.FirstName = 'Test4';
        lead.LastName = 'NegativeWebToLead';
        lead.Email = 'negtest4@salesforce.com';
        lead.City = 'Bangkok';
        lead.RTL_Multiple_Interested_Product_s__c = true;
        lead.RTL_Branch_Name__c = '001';
        lead.RTL_Product_Name__c = 'Bad Product1;Bad Product2';
        lead.RTL_Description__c = 'Bad Product2';
        lead.Status = 'New';    
        lead.RTL_ID_Type__c = 'Passport ID';
        lead.RTL_Citizen_Id__c = '88888';   
        lead.RTL_Address_Line1__c = 'address1';
        lead.RTL_Address_Line4__c = 'address4';         
        lead.RecordType = recordType;       
        webToLeads.add(lead);
        insert webToLeads;              
    }   
    
    public static List<Opportunity> createOpportunity(List<Account> listAcc, Boolean doInsert) {
        List<Opportunity> opptList = new List<Opportunity>();
        Opportunity o;
        RecordType recordType = [select Id from RecordType where developername='Retail_Deposit' and SobjectType = 'Opportunity' and IsActive = true limit 1];
        for( Integer i = 0 ; i < listAcc.size() ; i++ ){
            o = new Opportunity(Name='II-OPP-'+i,
                                StageName = 'Open',
                                CloseDate = Date.today(),
                                AccountId = listAcc.get(i).Id,
                                Probability = 10,
                                Amount = 0,
                                Description = 'desc'+i,
                                CA_Prep_Start_Date__c = Date.today(),
                                Approval_Process_Date__c = Date.today(),
                                Post_Approval_Date__c = Date.today(),
                                Complete_Date__c = Date.today(),
                                Trigger_flag__c = true,
                                RecordType=recordType
                               );
            opptList.add(o);
            
        }
        if(doInsert){
                insert opptList;
        }
        return opptList;
    }         
}