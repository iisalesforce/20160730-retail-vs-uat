trigger LeadLogValidHeaderTrigger on LeadLogValidHeader__c (after update) {

    if(Trigger.isAfter && Trigger.isUpdate) 
    {
        UploadleadPagingController.delLogfromtrigger(Trigger.new);
    }
}