public class UploadLeadService{
    public Blob csvFileBody           			{get;set;}
    public string csvAsString                   {get;set;}
    public String[] csvFileLines                {get;set;}
    public string validheaderId                 {get;set;}
    public integer rows                         {get;set;}
    
    
    List<ValidWarpper> validlogs {get;set;}
    Public Integer size{get;set;} 
    Public Integer noOfRecords{get; set;} 
    public List<SelectOption> paginationSizeOptions{get;set;}
    
    public List<LeadLogValidDetail__c> ValidDetail   {get; set;}
    
    public List<UploadLead> RowList             {get; set;}
    public List<UploadLead> RowListTrue         {get; set;}
    public List<UploadLead> RowListTrueDup      {get; set;}
    public List<UploadLead> RowTruebeforeinsert {get; set;}
    public List<UploadLead> RowListFalse        {get; set;}
    public List<UploadLead> Leadafterinsert     {get; set;}
    public List<UploadLead> allLeadlog          {get; set;}
    
    public List<UploadLead> fileDuptrue         {get; set;}
    public List<LeadLogHeader__c> loghead       {get; set;}
    public List<LeadLogHeader__c> logheadFalse  {get; set;}
    
    public String messagefiletype 			{get; set;}
    
    Map<Integer,UploadLead> leadmap         = new map<Integer,UploadLead>();
    Map<Integer,UploadLead> leadmapCheck    = new map<Integer,UploadLead>();
    Map<Integer,UploadLead> leadmapinsert   = new map<Integer,UploadLead>();
    Map<Integer,DisqualifiedProcessBuilder.LeadWrapperSOAP> mapleadwarpper = new map<Integer,DisqualifiedProcessBuilder.LeadWrapperSOAP>();
    
    public UploadLeadService(){
        
        csvFileLines = new String[]{};
        messagefiletype = status_code__c.getValues('5101').status_message__c;   
        set<id> headset = new set<id>();
        set<id> logfalseset = new set<id>();
        set<id> logtrueset = new set<id>();
        loghead = new List<LeadLogHeader__c>();
        logheadFalse = new List<LeadLogHeader__c>();
        //loghead = [Select Id,OwnerID,name,Createddate,Row__c from LeadLogHeader__c where OwnerID =: UserInfo.getUserId() order by Createddate desc];
        List<LeadLogHeader__c> temphead = [Select Id,OwnerID,name,Createddate,Row__c from LeadLogHeader__c where OwnerID =: UserInfo.getUserId() order by Createddate desc];
        for(LeadLogHeader__c sh : temphead) {
            headset.add(sh.Id);
        }
        
        List<LeadLogDetail__c> templogFalse = [Select id,parrent__c,name,Lead_Valid__c from LeadLogDetail__c where parrent__c IN: headset and Lead_Valid__c =: True];
        if(templogFalse.size() > 0) {
            for(LeadLogDetail__c sf : templogFalse) {
                logfalseset.add(sf.parrent__c);
            }
        }
        
        /*List<LeadLogDetail__c> templogTrue = [Select id,parrent__c,name,Lead_Valid__c from LeadLogDetail__c where parrent__c IN: headset and Lead_Valid__c =: False];
        if(templogTrue.size() > 0) {
            for(LeadLogDetail__c sf : templogTrue) {
                logtrueset.add(sf.parrent__c);
            }
        }*/
        
        
		loghead = [Select Id,OwnerID,name,Createddate,Row__c from LeadLogHeader__c where OwnerID =: UserInfo.getUserId() order by Createddate desc];       
        if(loghead.size() > 0)
        {
            Integer newrow = 1;
            for(LeadLogHeader__c listrow : loghead)
            {
                listrow.Row__c = newrow;
                newrow++;
            }
        }
        
        logheadFalse = [Select Id,OwnerID,name,Createddate,Row__c from LeadLogHeader__c where OwnerID =: UserInfo.getUserId() and Id IN:logfalseset order by Createddate desc];       
        if(logheadFalse.size() > 0)
        {
            Integer newrow = 1;
            for(LeadLogHeader__c listrow : logheadFalse)
            {
                listrow.Row__c = newrow;
                newrow++;
            }
        }
        //system.debug('headset = '+headset);
        //system.debug('templogFalse = '+templogFalse);
    }
    
    public PageReference importCSVFile(){
        	Boolean isUTF8 = true;
        	string csvdetail;
            try{
             	csvdetail = csvFileBody.tostring() ;
            }catch(Exception e){
                isUTF8 = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, status_code__c.getValues('5108').status_message__c));
				return null;                
            }
        if(isUTF8){
            Integer size = csvFileBody.size();
            csvdetail = csvFileBody.tostring();
            UploadLeadUtilityRowIterator r = New UploadLeadUtilityRowIterator(csvdetail,'\r\n'); //Replace \n with whatever delineates your row
            String firstRow;
            if(r.hasNext()) {
                firstRow = r.next();
            }
            List<String> csvdetail2 = new list<String>();
            while(r.hasNext())
            {
                // Read line
                String line = r.next();
                csvdetail2.add(line);
            }
            //system.debug('csvdetail size = '+csvdetail2.size());
            RowListTrue     = new List<UploadLead>();
            RowListFalse    = new List<UploadLead>();
            Leadafterinsert = new List<UploadLead>();
            RowListTrueDup  = new List<Uploadlead>();
            RowList         = new List<UploadLead>();
            allLeadlog      = new list<UploadLead>();
            RowTruebeforeinsert = new List<UploadLead>();
            fileDuptrue     = new list<UploadLead>();
            
            List<String> csvRecordData = new List<String>();
            
            UploadLead tr;
            Integer row1 = 1;
            //Tinnakrit Comment: - <=2001 is means 2001 is acceptable so it should be <2001 or <=2000
            if(csvdetail2.size() <= 2001){
                for(Integer i=0;i<csvdetail2.size();i++){
                    tr = new UploadLead();
                    if(csvdetail2[i].length() > 37) {
                        csvRecordData = csvdetail2[i].trim().split(',',-1);
                        tr.row                      = row1;
                        tr.leadOwner                = csvRecordData[0];
                        tr.leadOwnername            = csvRecordData[0];
                        tr.PrimaryCampaign          = csvRecordData[1];
                        tr.PrimaryCampaignName      = csvRecordData[1];
                        tr.LeadOwnerExceptionFlag   = csvRecordData[2];
                        tr.LinktoCustomer           = csvRecordData[3];
                        tr.LinktoCustomerName       = csvRecordData[3];
                        tr.CustomerType             = csvRecordData[4];
                        tr.CustomerName             = csvRecordData[5];
                        tr.CustomerNameEN           = csvRecordData[6];
                        tr.IDType                   = csvRecordData[7];
                        tr.IDNumber                 = csvRecordData[8];
                        tr.ContactTitle             = csvRecordData[9];
                        tr.ContactFirstname         = csvRecordData[10];
                        tr.ContactLastname          = csvRecordData[11];
                        tr.ContactPosition          = csvRecordData[12];
                        tr.DecisionMap              = csvRecordData[13];
                        tr.ContactPhoneNumber       = csvRecordData[14];
                        tr.Industry                 = csvRecordData[15];
                        tr.IndustryName             = csvRecordData[15];
                        tr.Groupname                = csvRecordData[16];
                        tr.Groupname2               = csvRecordData[16];
                        tr.NoOfyears                = csvRecordData[17];
                        tr.SalesAmountperyear       = csvRecordData[18];
                        tr.LeadSource               = csvRecordData[19];
                        tr.OtherSource              = csvRecordData[20];
                        tr.BranchedReferred         = csvRecordData[21];
                        tr.BranchedReferredName     = csvRecordData[21];
                        tr.ReferralStaffID          = csvRecordData[22];
                        tr.ReferralStaffName        = csvRecordData[23];
                        tr.TotalExpectedRevenue     = csvRecordData[24];
                        tr.Address                  = csvRecordData[25];
                        tr.SubDistrict              = csvRecordData[26];
                        tr.District                 = csvRecordData[27];
                        tr.Province                 = csvRecordData[28];
                        tr.ZipCode                  = csvRecordData[29];
                        tr.Country                  = csvRecordData[30];
                        tr.MobileNo                 = csvRecordData[31];
                        tr.OfficeNo                 = csvRecordData[32];
                        tr.Ext                      = csvRecordData[33];
                        tr.Email                    = csvRecordData[34];
                        tr.Rating                   = csvRecordData[35];
                        tr.prescreen				= csvRecordData[36].tolowercase().trim();//CR Edit 11/06/2016
                        if(csvRecordData.size() > 37) {
                            tr.Remark                   = csvRecordData[37];
                        }
                        leadmap.put(row1, tr);
                        row1++; 
                        RowList.add(tr);   
                    }
                    
                }
                string test = string.join(RowList, ','); 
            }else{
                ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,status_code__c.getValues('5001').status_message__c);
                ApexPages.addmessage(errorMessage);
                return null;
            }
            
            
            
            Map<String,string> mapdupfile = new Map<String,string>();
            set<String> setidnumber = new set<string>();
            
            set<String> s1 = new set<String>();
            set<Integer> s2 = new set<Integer>();
            /*for(uploadlead checkeddup : RowList)
{
if(checkeddup.idtype != null && checkeddup.IDNumber != null && checkeddup.idtype != '' && checkeddup.IDNumber != '')
{

//Tinnakrit Comment - Change To Map<String,String> do not compare with IDTYPE+IDNUMBER because ID TYPE has whitespace
//and make sure that program must be ignored Case Sensitives
if (s1.contains(checkeddup.idtype+checkeddup.IDNumber))
{
s2.add(checkeddup.row);
}else{

s1.add(checkeddup.idtype+checkeddup.IDNumber);
if(checkeddup.IDType != null)
{
//mapdupfile.put(checkeddup.IDType,checkeddup.IDNumber);
}
}
}

}*/
            boolean checkflage;
            for(uploadlead dupleadfile : RowList)
            {
                /*dupleadfile.Errormessage = '';
                if(s2.contains(dupleadfile.row))
                {
                dupleadfile.Errormessage += 'Duplicate in file \r\n';
                dupleadfile.flag = false;
                //fileDuptrue.add(dupleadfile);
                }else{*/
                                /*
                if(dupleadfile.LinktoCustomer != null && dupleadfile.LinktoCustomer != '') {
                for(integer i = 0; i <= 25; i++) {
                
                }
                }
                */                
                //Tinnakrit Comment - Change it to REGEX, try to not hard code here
                if(dupleadfile.LinktoCustomer != null && dupleadfile.LinktoCustomer != '' && dupleadfile.LinktoCustomer.isNumeric()) { 
                    if(dupleadfile.LinktoCustomer.length() == 1){
                        dupleadfile.LinktoCustomer = '00110000000000000000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '00110000000000000000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 2){
                        dupleadfile.LinktoCustomer = '0011000000000000000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '0011000000000000000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 3){
                        dupleadfile.LinktoCustomer = '001100000000000000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '001100000000000000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 4){
                        dupleadfile.LinktoCustomer = '00110000000000000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '00110000000000000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 5){
                        dupleadfile.LinktoCustomer = '0011000000000000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '0011000000000000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 6){
                        dupleadfile.LinktoCustomer = '001100000000000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '001100000000000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 7){
                        dupleadfile.LinktoCustomer = '00110000000000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '00110000000000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 8){
                        dupleadfile.LinktoCustomer = '0011000000000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '0011000000000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 9){
                        dupleadfile.LinktoCustomer = '001100000000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '001100000000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 10){
                        dupleadfile.LinktoCustomer = '00110000000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '00110000000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 11){
                        dupleadfile.LinktoCustomer = '0011000000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '0011000000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 12){
                        dupleadfile.LinktoCustomer = '001100000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '001100000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 13){
                        dupleadfile.LinktoCustomer = '00110000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '00110000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 14){
                        dupleadfile.LinktoCustomer = '0011000000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '0011000000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 15){
                        dupleadfile.LinktoCustomer = '001100000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '001100000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 16){
                        dupleadfile.LinktoCustomer = '00110000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '00110000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 17){
                        dupleadfile.LinktoCustomer = '0011000000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '0011000000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 18){
                        dupleadfile.LinktoCustomer = '001100000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '001100000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 19){
                        dupleadfile.LinktoCustomer = '00110000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '00110000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 20){
                        dupleadfile.LinktoCustomer = '0011000000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '0011000000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 21){
                        dupleadfile.LinktoCustomer = '001100000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '001100000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 22){
                        dupleadfile.LinktoCustomer = '00110000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '00110000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 23){
                        dupleadfile.LinktoCustomer = '0011000'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '0011000'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 24){
                        dupleadfile.LinktoCustomer = '001100'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '001100'+dupleadfile.LinktoCustomer;
                    }else if(dupleadfile.LinktoCustomer.length() == 25){
                        dupleadfile.LinktoCustomer = '00110'+dupleadfile.LinktoCustomer;
                        dupleadfile.LinktoCustomerName = '00110'+dupleadfile.LinktoCustomer;
                    }
                }
                if(dupleadfile.LeadOwnerExceptionFlag == null || dupleadfile.LeadOwnerExceptionFlag == '')
                {
                    dupleadfile.LeadOwnerExceptionFlag = 'No';
                }
                dupleadfile.flag = true;
                //}
                fileDuptrue.add(dupleadfile);
            }
            
            
            fileDuptrue = LeadValidation.Leadvalid(fileDuptrue,null);
            for(uploadlead row : fileDuptrue)//RowList
            {
                if(!string.isBlank(row.Errormessage)){
                   string err = row.Errormessage.substring(0,row.Errormessage.length() -1 );
                   system.debug('err : '+err);
                }
                allLeadlog.add(row);
            }
            //System.debug('##RowListTrue = ' + RowListTrue);
            
        }            
        	
        return validfiles();
		
    }

    
    
    /***********************************************************************/
    /*************************** validfiles ********************************/
    /***********************************************************************/
    Public PageReference validfiles(){
    	LeadLogValidHeader__c  newvalidheader = new LeadLogValidHeader__c();
        newvalidheader.name = csvAsString;
    	insert newvalidheader;   
        
        List<LeadLogValidDetail__c> Inligvalid = new List<LeadLogValidDetail__c>();
        
        for(Uploadlead validlog : fileDuptrue){//allLeadlog){
            LeadLogValidDetail__c val = new LeadLogValidDetail__c();
            val.LeadLogValidHeader__c = newvalidheader.id;
            val.leadOwnerName__c  = validlog.leadOwnername;
            val.PrimaryCampaignName__c = validlog.PrimaryCampaignName;
            val.LeadRecordtypeName__c = validlog.LeadRecordtypeName;
            val.LeadRecordtypeId__c = validlog.LeadRecordtypeId;
            val.IndustryName__c = validlog.IndustryName;
            val.Groupname2__c = validlog.Groupname2;
            val.row__c = validlog.row;
            val.leadOwner__c = validlog.leadOwner;
            val.LinktoCustomerName__c = validlog.LinktoCustomerName;
            val.PrimaryCampaign__c = validlog.PrimaryCampaign;
            val.LeadOwnerExceptionFlag__c = validlog.LeadOwnerExceptionFlag;
            val.LinktoCustomer__c = validlog.LinktoCustomer;
            val.CustomerType__c = validlog.CustomerType;
            if(validlog.CustomerName.length() > 255) {
            	val.CustomerName__c = validlog.CustomerName.subString(0,255);
            }else {
            	val.CustomerName__c = validlog.CustomerName;    
            }
            if(validlog.CustomerNameEN.length() > 100) {
                val.CustomerNameEN__c = validlog.CustomerNameEN.subString(0,100);
            } else {
            	val.CustomerNameEN__c = validlog.CustomerNameEN;    
            }
            val.DecisionMap__c = validlog.DecisionMap;
            val.IDType__c = validlog.IDType;
            if(validlog.IDNumber.length() > 255) {
            	val.IDNumber__c = validlog.IDNumber.subString(0,255);
            }else {
            	val.IDNumber__c = validlog.IDNumber;   
            }
            val.ContactTitle__c = validlog.ContactTitle;
            if(validlog.ContactFirstname.length() > 40) {
            	val.ContactFirstname__c = validlog.ContactFirstname.subString(0,40);
            }else {
            	val.ContactFirstname__c = validlog.ContactFirstname;   
            }
            if(validlog.ContactLastname.length() > 80) {
            	val.ContactLastname__c = validlog.ContactLastname.subString(0,80);
            }else {
                val.ContactLastname__c = validlog.ContactLastname;
            }
            if(validlog.ContactPosition.length() > 128) {
            	val.ContactPosition__c = validlog.ContactPosition.subString(0,128);
            }else {
                val.ContactPosition__c = validlog.ContactPosition;
            }
            if(validlog.ContactPhoneNumber.length() > 40) {
            	val.ContactPhoneNumber__c = validlog.ContactPhoneNumber.subString(0,40);
            } else {
            	val.ContactPhoneNumber__c = validlog.ContactPhoneNumber;    
            }
            val.Industry__c = validlog.Industry;
            val.Groupname__c = validlog.Groupname;
            if(validlog.NoOfyears.length() > 5) {
            	val.NoOfyears__c = validlog.NoOfyears.subString(0,5);
            }else {
                val.NoOfyears__c = validlog.NoOfyears;
            }
            if(validlog.SalesAmountperyear.length() > 19) {
            	val.SalesAmountperyear__c = validlog.SalesAmountperyear.subString(0,19);
            }else {
                val.SalesAmountperyear__c = validlog.SalesAmountperyear;
            }
            val.LeadSource__c = validlog.LeadSource;
            if(validlog.OtherSource.length() > 25) {
            	val.OtherSource__c = validlog.OtherSource.subString(0,25);
            }else {
                val.OtherSource__c = validlog.OtherSource;
            }
            val.BranchedReferred__c = validlog.BranchedReferred;
            val.BranchedReferredName__c = validlog.BranchedReferredName;
            if(validlog.ReferralStaffID.length() > 255) {
            	val.ReferralStaffID__c = validlog.ReferralStaffID.subString(0,255);    
            } else {
            	val.ReferralStaffID__c = validlog.ReferralStaffID;
            }
            if(validlog.ReferralStaffName.length() > 255) {
            	val.ReferralStaffName__c = validlog.ReferralStaffName.subString(0,255);
            } else {
            	val.ReferralStaffName__c = validlog.ReferralStaffName;
            }
            if(validlog.TotalExpectedRevenue.length() > 19) {
                val.TotalExpectedRevenue__c = validlog.TotalExpectedRevenue.subString(0,19);
            }else {
                val.TotalExpectedRevenue__c = validlog.TotalExpectedRevenue;
            }
            if(validlog.Address.length() > 255) {
            	val.Address__c = validlog.Address.subString(0,255);    
            } else {
            	val.Address__c = validlog.Address;
            }
            if(validlog.SubDistrict.length() > 255) {
            	val.SubDistrict__c = validlog.SubDistrict.subString(0,255);
            } else {
            	val.SubDistrict__c = validlog.SubDistrict;    
            }
            if(validlog.District.length() > 255) {
            	val.District__c = validlog.District.subString(0,255);    
            } else {
            	val.District__c = validlog.District;
            }
            if(validlog.Province.length() > 255) {
            	val.Province__c = validlog.Province.subString(0,255);
            } else {
            	val.Province__c = validlog.Province;
            }
            if(validlog.ZipCode.length() > 255) {
            	val.ZipCode__c = validlog.ZipCode.subString(0,255);
            } else {
            	val.ZipCode__c = validlog.ZipCode;    
            }
            	val.Country__c = validlog.Country;
            if(validlog.MobileNo.length() > 40) {
                val.MobileNo__c = validlog.MobileNo.subString(0,40);
            } else {
                val.MobileNo__c = validlog.MobileNo; 
            }
            if(validlog.OfficeNo.length() > 40) {
                val.OfficeNo__c = validlog.OfficeNo.subString(0,40);
            } else {
                val.OfficeNo__c = validlog.OfficeNo;
            }
            
            if(validlog.Ext.length() > 255) {
            	val.Ext__c = validlog.Ext.subString(0,255);
            } else {
            	val.Ext__c = validlog.Ext;    
            }
            if(validlog.Email.length() > 80) {
            	val.Email__c = validlog.Email.subString(0,80);
            } else {
            	val.Email__c = validlog.Email;
            }
            val.Rating__c = validlog.Rating;
            if(validlog.Remark.length() > 255) {
                val.Remark__c = validlog.Remark.subString(0,255);
            }else {
                val.Remark__c = validlog.Remark; 
            }
            val.Prescreen__c  = validlog.prescreen;
            val.Errormessage__c = validlog.Errormessage;
            val.valid__c = validlog.flag; 
            Inligvalid.add(val);
        }
        if(Inligvalid.size() > 0){
            insert Inligvalid;
        }
        validheaderId = newvalidheader.id;
        allLeadlog.clear();
        PageReference redirectPage = Page.Uploadleadvalid;
        redirectPage.setRedirect(true);
        redirectPage.getParameters().put('id',newvalidheader.id);
        redirectPage.getParameters().put('name',csvAsString);
        return redirectPage;
    }
    
    
    /****************************************************************************
    **********************************Log files**********************************
    ****************************************************************************/
    
    Public void  logfiles()
    {
        set<id> headset = new set<id>();
        set<id> logfalseset = new set<id>();
        set<id> logtrueset = new set<id>();
        loghead = new List<LeadLogHeader__c>();
        logheadFalse = new List<LeadLogHeader__c>();
        //loghead = [Select Id,OwnerID,name,Createddate,Row__c from LeadLogHeader__c where OwnerID =: UserInfo.getUserId() order by Createddate desc];
        List<LeadLogHeader__c> temphead = [Select Id,OwnerID,name,Createddate,Row__c from LeadLogHeader__c where OwnerID =: UserInfo.getUserId() order by Createddate desc];
        for(LeadLogHeader__c sh : temphead) {
            headset.add(sh.Id);
        }
        
        List<LeadLogDetail__c> templogFalse = [Select id,parrent__c,name,Lead_Valid__c from LeadLogDetail__c where parrent__c IN: headset and Lead_Valid__c =: True];
        if(templogFalse.size() > 0) {
            for(LeadLogDetail__c sf : templogFalse) {
                logfalseset.add(sf.parrent__c);
            }
        }
        
        /*List<LeadLogDetail__c> templogTrue = [Select id,parrent__c,name,Lead_Valid__c from LeadLogDetail__c where parrent__c IN: headset and Lead_Valid__c =: False];
        if(templogTrue.size() > 0) {
            for(LeadLogDetail__c sf : templogTrue) {
                logtrueset.add(sf.parrent__c);
            }
        }*/
        
        
		loghead = [Select Id,OwnerID,name,Createddate,Row__c from LeadLogHeader__c where OwnerID =: UserInfo.getUserId() order by Createddate desc];       
        if(loghead.size() > 0)
        {
            Integer newrow = 1;
            for(LeadLogHeader__c listrow : loghead)
            {
                listrow.Row__c = newrow;
                newrow++;
            }
        }
        
        logheadFalse = [Select Id,OwnerID,name,Createddate,Row__c from LeadLogHeader__c where OwnerID =: UserInfo.getUserId() and Id IN:logfalseset order by Createddate desc];       
        if(logheadFalse.size() > 0)
        {
            Integer newrow = 1;
            for(LeadLogHeader__c listrow : logheadFalse)
            {
                listrow.Row__c = newrow;
                newrow++;
            }
        }
       // system.debug('headset = '+headset);
    }
   
    
    
}