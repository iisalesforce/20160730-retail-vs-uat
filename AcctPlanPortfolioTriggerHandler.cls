public class AcctPlanPortfolioTriggerHandler {
    static Boolean detectError = false;
    static String errorException = '';
    static String STR_INSERT = 'insert';
    static String STR_UPDATE = 'update';
    static String STR_DELETE = 'delete';
    

        
    public static void handlerAfterInsert(List<AcctPlanPortfolio__c> portNew){
        List<AcctPlanPortfolio__c> listNew = checkConditionApprove(portNew,null);
        if( listNew.size() > 0 ){
            System.debug(':::: afterInsert Start ::::');
            Trigger_CSV(listNew,null,STR_INSERT);    
            System.debug(':::: afterInsert End ::::');
        }        
    }
    
    public static void handlerAfterUpdate(List<AcctPlanPortfolio__c> portNew,List<AcctPlanPortfolio__c> portOld){
        List<AcctPlanPortfolio__c> listNew = checkConditionApprove(portNew,portOld);
        if( listNew.size() > 0 ){
            System.debug(':::: afterUpdate Start ::::');
            Trigger_CSV(listNew,portOld,STR_UPDATE);    
            System.debug(':::: afterUpdate End ::::');
        }      
    }
    
    public static List<AcctPlanPortfolio__c> checkConditionApprove(List<AcctPlanPortfolio__c> portNew,List<AcctPlanPortfolio__c> portOld){
        List<AcctPlanPortfolio__c> listReturn = new List<AcctPlanPortfolio__c>();
        Map<Id,AcctPlanPortfolio__c> mapPortOld = new Map<Id,AcctPlanPortfolio__c>();
        if( portOld != null && portOld.size() > 0 ){
            mapPortOld.putAll(portOld);
        }
        
        for(AcctPlanPortfolio__c port : portNew){
            if(port.Status__c.indexOf('Approved') >= 0 ){
                listReturn.add(port);
            }
        }
        
        return listReturn;
    }
    
    
    
    
    
    public static void Trigger_CSV(List<AcctPlanPortfolio__c> portNew,List<AcctPlanPortfolio__c> portOld,String eventMode){
        System.debug(':::: Trigger_CSV Start ::::');
        Map<Id,AcctPlanPortfolio__c> listPortOld = new Map<Id,AcctPlanPortfolio__c>();
        if(eventMode == STR_UPDATE){
            listPortOld.putAll(portOld);
        }
        
        
        
        
    }
    
    
    }