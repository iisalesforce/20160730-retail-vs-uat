public class OpportunityTriggerHandler {
    static Boolean detectError = false;
    static String errorException = '';
    static String STR_INSERT = 'insert';
    static String STR_UPDATE = 'update';
    static String STR_DELETE = 'delete';
    
    static String Stage_CaPrep = 'CA-Prep';
    static String Stage_ApprovalProcess = 'Approval Process';
    static String Stage_PostApproval = 'Post Approval';
    static String Stage_LimitSetup = 'Limit Setup';
    
    
    public static Map<String,Pricebook2> pricebookmap{get
    {
        if(pricebookmap ==null){
            pricebookmap= new Map<String,Pricebook2>();
            for(Pricebook2 R : [SELECT Name FROM Pricebook2 WHERE IsActive = true ORDER BY Name ASC]){
                   pricebookmap.put(r.Name,r);
                }
        }
        return pricebookmap;
        
    }set;}
                
    public static User currentUser {get{
        if(currentUser==null){
            currentUser =[ select Id,Region__c,Zone__c,Segment__c from User where Id = :UserInfo.getUserId() ];
    
        }
        
        return currentUser;
        
    }set;}
    
    
    public static List<RecordType> recordTypeList {get{
        if(recordTypeList==null){
           recordTypeList = [ select Id,Name 
                                           from RecordType 
                                           where Name IN ('SE Credit Product','WBG Credit Product','SE Credit Product2','WBG Credit Product2'
                                                          ,'Non-credit Product','Non-credit Product2')
                                          and SobjectType = 'Opportunity'];
        }
        
        return recordTypeList;
        
    }set;}  
    
    
    public static List<Opportunity_Stage_Mapping__c> OppstageList {get
    {
        if(OppstageList ==null){
           OppstageList = [SELECT Name,Application_status__c,Opportunity_Type__c,Stage__c  
                                                   FROM Opportunity_Stage_Mapping__c];
        }
        return OppstageList;
    }
                                                                   set;}
    
    public static void handleBeforeInsert(List<Opportunity> oppsNew){
        CAID.checkGenerateCAID(oppsNew,null,STR_INSERT);    
        conditionCreateOpportunity(oppsNew,null,STR_INSERT);
        
    }
    
    public static void handleBeforeUpdate(List<Opportunity> oppsNew,List<Opportunity> oppsOld){
         stageMappingstageChange(oppsNew,oppsOld,STR_UPDATE);
        List<Opportunity> listNew = checkConditionStageName(oppsNew,oppsOld,Stage_CaPrep);
        if( listNew.size() > 0 ){
            System.debug(':::: updateOpportunityData Start ::::');
            updateOpportunityData(listNew, oppsOld, STR_UPDATE,Stage_CaPrep);
            System.debug(':::: updateOpportunityData End ::::');  
        }
        /*
        listNew = checkConditionChangeOwner(oppsNew,oppsOld);
        if( listNew.size() > 0 ){
            changeOwnerOpportunity(listNew,oppsOld,STR_UPDATE);
        }*/
        listNew = checkConditionStageChange(oppsNew,oppsOld);
        if( listNew.size() > 0 ){
            stageChange(listNew,oppsOld,STR_UPDATE);
        }
        
        
    }
    
    public static void handleAfterUpdate(List<Opportunity> oppsNew,List<Opportunity> oppsOld){
        Trigger_T07(oppsNew,oppsOld,STR_UPDATE);
        Trigger_T08(oppsNew,oppsOld,STR_UPDATE);
        Trigger_T09(oppsNew,oppsOld,STR_UPDATE);
        Trigger_T10(oppsNew,oppsOld,STR_UPDATE);
    }
    /*
    public static List<Opportunity> checkConditionChangeOwner(List<Opportunity> oppsNew,List<Opportunity> oppsOld){
        List<Opportunity> listReturn = new List<Opportunity>();
        Map<Id,Opportunity> mapOppsOld = new Map<Id,Opportunity>();
        if( oppsOld != null && oppsOld.size() > 0 ){
            mapOppsOld.putAll(oppsOld);
        }
        for(Opportunity opp : oppsNew){
            if( opp.OwnerId != mapOppsOld.get(opp.Id).OwnerId ){//&& opp.Trigger_Flag__c                
                listReturn.add(opp);
            }else{
                //opp.Trigger_Flag__c = false;
            }
        }
        return listReturn;
    }*/
    
     public static List<Opportunity> checkConditionStageChange(List<Opportunity> oppsNew,List<Opportunity> oppsOld){
        List<Opportunity> listReturn = new List<Opportunity>();
        Map<Id,Opportunity> mapOppsOld = new Map<Id,Opportunity>();
        if( oppsOld != null && oppsOld.size() > 0 ){
            mapOppsOld.putAll(oppsOld);
        }
        for(Opportunity opp : oppsNew){
            if( opp.StageName != mapOppsOld.get(opp.Id).StageName ){               
                listReturn.add(opp);
            }
        }
        return listReturn;
    }
    
    public static List<Opportunity> checkConditionStageName(List<Opportunity> oppsNew,List<Opportunity> oppsOld,String StageName){
        List<Opportunity> listReturn = new List<Opportunity>();
        Map<Id,Opportunity> mapOppsOld = new Map<Id,Opportunity>();
        if( oppsOld != null && oppsOld.size() > 0 ){
            mapOppsOld.putAll(oppsOld);
        }
        
        for(Opportunity opp : oppsNew){
            Boolean checkCondition = false;
            if( mapOppsOld.get(opp.Id).StageName != opp.StageName 
               && opp.StageName == StageName && StageName != Stage_LimitSetup ){
                   checkCondition = true;
            }else if( StageName == Stage_LimitSetup  
                        && mapOppsOld.get(opp.Id).StageName != opp.StageName 
                        && opp.StageName == StageName 
                        && mapOppsOld.get(opp.Id).StageName == Stage_PostApproval){
                            checkCondition = true;
            }           
            
            if(checkCondition){
                listReturn.add(opp);
            }
        }
        return listReturn;
    }
    
    public static void Trigger_T07(List<Opportunity> oppsNew,List<Opportunity> oppsOld,String eventMode){
        List<Opportunity> listNew = checkConditionStageName(oppsNew,oppsOld,Stage_CaPrep);
        if( listNew.size() > 0 ){
            System.debug(':::: Trigger_T07 Start ::::');
            //System.debug('mix old : '+oppsOld);
            //System.debug('mix new : '+oppsNew);
            updateTargetByOppStatus(listNew, oppsOld, eventMode,Stage_CaPrep);
            System.debug(':::: Trigger_T07 End ::::');  
        }
    }
    
    public static void Trigger_T08(List<Opportunity> oppsNew,List<Opportunity> oppsOld,String eventMode){
        List<Opportunity> listNew = checkConditionStageName(oppsNew,oppsOld,Stage_ApprovalProcess);
        if( listNew.size() > 0 ){
            System.debug(':::: Trigger_T08 Start ::::');
            updateTargetByOppStatus(listNew, oppsOld, eventMode,Stage_ApprovalProcess);
            System.debug(':::: Trigger_T08 End ::::');  
        }
    }
    
    public static void Trigger_T09(List<Opportunity> oppsNew,List<Opportunity> oppsOld,String eventMode){
        List<Opportunity> listNew = checkConditionStageName(oppsNew,oppsOld,Stage_PostApproval);
        if( listNew.size() > 0 ){
            System.debug(':::: Trigger_T09 Start ::::');
            updateTargetByOppStatus(listNew, oppsOld, eventMode,Stage_PostApproval);
            System.debug(':::: Trigger_T09 End ::::');  
        }
    }
    
    public static void Trigger_T10(List<Opportunity> oppsNew,List<Opportunity> oppsOld,String eventMode){
        List<Opportunity> listNew = checkConditionStageName(oppsNew,oppsOld,Stage_LimitSetup);
        if( listNew.size() > 0 ){
            System.debug(':::: Trigger_T10 Start ::::');
            updateTargetByOppStatus(listNew, oppsOld, eventMode,Stage_LimitSetup);
            System.debug(':::: Trigger_T10 End ::::');  
        }
    }
    
    public static void updateTargetByOppStatus(List<Opportunity> oppsNew,List<Opportunity> oppsOld,String eventMode,String stageName){
        Map<Id,Opportunity> listOppsOld = new Map<Id,Opportunity>();
        if(eventMode == STR_UPDATE){
            listOppsOld.putAll(oppsOld);
        }
        
        List<Id> ids = new List<Id>();
        List<Id> listOwnerId = new List<Id>();
        Map<Id,Target__c> mapTargetForUpdate = new Map<Id,Target__c>();
        List<RecordType> listRecordType = [select Id from RecordType where name = 'Activities Target'];
        // '012N0000000D5FHIA0'; // id for Activities Target
        Id recordTypeId = listRecordType[0].Id; 
        String strMonthlyActual = '';
        String strMonthlyAmount = '';
        List<String> listZone = new List<String>();
        List<Integer> listMonth = new List<Integer>();
        List<Integer> listYear = new List<Integer>();
        
        for(Opportunity opp : oppsNew){
            if( listOppsOld.get(opp.Id).StageName != opp.StageName 
               && opp.StageName == StageName){
                   if(opp.Id != null){
                       ids.add(opp.Id);
                   }
                   if(opp.OwnerId != null){
                       listOwnerId.add(opp.OwnerId);    
                   }
                   
                   if( stageName == Stage_CaPrep ){
                       strMonthlyActual = 'Monthly_Close_Sales_Actual__c';
                       strMonthlyAmount = 'Monthly_Close_Sales_Amount__c';
                       if( opp.CA_Prep_Start_Date__c != null ){
                           listMonth.add( Integer.valueOf(String.valueOf(opp.CA_Prep_Start_Date__c).substring(5,7)) );
                           listYear.add( Integer.valueOf(String.valueOf(opp.CA_Prep_Start_Date__c).substring(0,4)) );
                       }
                   }else if( stageName == Stage_ApprovalProcess ){           
                       strMonthlyActual = 'Monthly_App_In_Actual__c';
                       strMonthlyAmount = 'Monthly_App_In_Amount__c';
                       if( opp.Approval_Process_Date__c != null ){
                           listMonth.add( Integer.valueOf(String.valueOf(opp.Approval_Process_Date__c).substring(5,7)) );
                           listYear.add( Integer.valueOf(String.valueOf(opp.Approval_Process_Date__c).substring(0,4)) );
                       }
                   }else if( stageName == Stage_PostApproval ){           
                       strMonthlyActual = 'Monthly_Approved_Actual__c';
                       strMonthlyAmount = 'Monthly_Approved_Amount__c';
                       if( opp.Post_Approval_Date__c != null ){
                           listMonth.add( Integer.valueOf(String.valueOf(opp.Post_Approval_Date__c).substring(5,7)) );
                           listYear.add( Integer.valueOf(String.valueOf(opp.Post_Approval_Date__c).substring(0,4)) );
                       }
                   }else if( stageName == Stage_LimitSetup ){           
                       strMonthlyActual = 'Monthly_Set_Up_Actual__c';
                       strMonthlyAmount = 'Monthly_Set_Up_Amount__c';
                       if( opp.Complete_Date__c != null ){
                           listMonth.add( Integer.valueOf(String.valueOf(opp.Complete_Date__c).substring(5,7)) );
                           listYear.add( Integer.valueOf(String.valueOf(opp.Complete_Date__c).substring(0,4)) );
                       }
                   }
                   
               }    
        }
        
        Map<Id,User> listUser = new Map<Id,User>([select Id,Name,Zone__c  from User where Id IN :listOwnerId]);
        for(user u : listUser.values() ){
            if(u.Zone__c != null){
                listZone.add(u.Zone__c);    
            }
        }
        
        Map<Id,AggregateResult> listOppHis = new Map<Id,AggregateResult>([select count(id) countOpp,OpportunityId Id
                                                                          from OpportunityHistory 
                                                                          where OpportunityId IN :ids 
                                                                          and StageName = :stageName
                                                                          group by OpportunityId]);
        System.debug('::>'+listOppHis);
        //list for get record target
        List<Target__c> listTarget = new List<Target__c>();
        
        String queryTarget = 'select Zone__c,OwnerId,'+strMonthlyActual+','+strMonthlyAmount +' '+
            'from Target__c where CALENDAR_MONTH(Monthly_Target_date__c) IN :listMonth '+
            'and CALENDAR_YEAR(Monthly_Target_date__c) IN :listYear and OwnerId IN :listOwnerId '+
            'and Zone__c IN :listZone '+
            'and RecordTypeId = :recordTypeId';
        
        //System.debug(queryTarget);
        
        listTarget = Database.query(queryTarget);
        
        Map<String,Target__c> mapTarget = new Map<String,Target__c>();
        for( Target__c t : listTarget ){
            mapTarget.put(t.OwnerId,t);
        }
        System.debug('listtarget : '+listTarget);
        
        for(Opportunity opp : oppsNew){
          //System.debug('Opp : ' + opp);    
            Boolean checkCondition = false;
            if( listOppsOld.get(opp.Id).StageName != opp.StageName 
               && opp.StageName == StageName && StageName != Stage_LimitSetup ){
                   checkCondition = true;
            }else if( StageName == Stage_LimitSetup  
                        && listOppsOld.get(opp.Id).StageName != opp.StageName 
                        && opp.StageName == StageName 
                        && listOppsOld.get(opp.Id).StageName == Stage_PostApproval){
                            checkCondition = true;
            }
            if( checkCondition ){
                   Integer countOpp = 0;
                   if( listOppHis.containsKey(opp.Id) ){
                       countOpp = (Integer)listOppHis.get(opp.Id).get('countOpp');
                   }else{
                       System.debug('::::: OwnerId : '+opp.OwnerId+' error containsKey in OppHistory');
                   }
                   
                   System.debug('::::: countOpp size is : '+countOpp +' :::::');
                   if( countOpp == 1 ){
                       if( mapTarget.containsKey(opp.OwnerId) && !mapTargetForUpdate.containsKey(opp.OwnerId) ){
                           Decimal AppAmount = (opp.Approved_Amount__c == null) ? 0 : opp.Approved_Amount__c;
                           Decimal MonActual = (mapTarget.get(opp.OwnerId).get(strMonthlyActual) == null) ? 
                               0 : (Decimal)mapTarget.get(opp.OwnerId).get(strMonthlyActual);
                           Decimal MonAmount = (mapTarget.get(opp.OwnerId).get(strMonthlyAmount) == null) ?
                               0 : (Decimal)mapTarget.get(opp.OwnerId).get(strMonthlyAmount);
                           
                           MonActual += 1;
                           MonAmount += AppAmount;
                           
                           if( stageName == Stage_CaPrep ){
                               mapTarget.get(opp.OwnerId).Monthly_Close_Sales_Actual__c = MonActual;
                               mapTarget.get(opp.OwnerId).Monthly_Close_Sales_Amount__c = MonAmount;
                           }else if( stageName == Stage_ApprovalProcess ){           
                               mapTarget.get(opp.OwnerId).Monthly_App_In_Actual__c = MonActual;
                               mapTarget.get(opp.OwnerId).Monthly_App_In_Amount__c = MonAmount;
                           }else if( stageName == Stage_PostApproval ){           
                               mapTarget.get(opp.OwnerId).Monthly_Approved_Actual__c = MonActual;
                               mapTarget.get(opp.OwnerId).Monthly_Approved_Amount__c = MonAmount;
                           }else if( stageName == Stage_LimitSetup ){           
                               mapTarget.get(opp.OwnerId).Monthly_Set_Up_Actual__c = MonActual;
                               mapTarget.get(opp.OwnerId).Monthly_Set_Up_Amount__c = MonAmount;
                           }

                           mapTargetForUpdate.put( opp.OwnerId,mapTarget.get(opp.OwnerId) ); 
                           System.debug('::::: OwnerId : '+opp.OwnerId+' '+strMonthlyActual+' : '+ mapTarget.get(opp.OwnerId).get(strMonthlyActual)+' :::::');
                           System.debug('::::: OwnerId : '+opp.OwnerId+' '+strMonthlyAmount+' : '+ mapTarget.get(opp.OwnerId).get(strMonthlyAmount)+' :::::');
                       }else{
                           System.debug('::::: OwnerId : '+opp.OwnerId+' error containsKey in Target Or mapTargetForUpdate is Duplicated');
                       }
                       
                   }
               }    
        }
        if(mapTargetForUpdate.size() > 0){
            try{
                update mapTargetForUpdate.values();
            }catch (DmlException e){
                System.debug('error : '+e.getMessage());
            }
        }
        System.debug('::::: List for update '+mapTargetForUpdate.size()+' row :::::');
    }
    
    public static void conditionCreateOpportunity(List<Opportunity> oppsNew,List<Opportunity> oppsOld,String eventMode){
        System.debug(':::: conditionCreateOpportunity Start ::::');
        
        Set<Id> idAccountList = new Set<Id>();
        Set<Id> idUserList = new Set<Id>();        
        
        for( Opportunity eachOpp : oppsNew ){
            System.debug('Pricebook2 : '+eachopp.Pricebook2Id);
            if( eachOpp.AccountId != null ){
                idAccountList.add(eachOpp.AccountId);
            }
            
            idUserList.add(eachOpp.OwnerId);
        }
        
        Map<Id,Account> queryAccount = new Map<Id,Account>([ select Id,OwnerId,Owner.Segment__c
                                                            from Account 
                                                            where Id IN :idAccountList ]);
        
        List<AccountTeamMember> queryAccountTeamMember = [ select accountId,userId
                                                                                      from AccountTeamMember 
                                                                                      where accountId IN :idAccountList 
                                                                                      and userId IN :idUserList];
        
        Map<Id,User> queryUser = new Map<Id,User>([select Id,UserRole.Name,Segment__c from user where Id IN :idUserList]);
        
        Pricebook2 standardPricebook = new Pricebook2(); 
        if( Test.isRunningTest() ){
            standardPricebook.Id = Test.getStandardPricebookId();
        }else{
            try{
          
               
          //User us = [SELECT ID,Segment__c FROM User 
           //                                        WHERE ID=:UserInfo.getUserId() LIMIT 1];
     // System.debug('user : '+us);
          
                if(currentUser.Segment__c != null){
                   //standardPricebook.id = pricebookmap.get('SE Price Book').id;
           standardPricebook.id = pricebookmap.get(Price_book_access__c.GetValues(currentUser.Segment__c).PricebookItem__c).id;
                  
                }else{ 
                    standardPricebook.id = pricebookmap.get(Price_book_access__c.GetValues('Default').PricebookItem__c).id;
                }
                
               
              System.debug('Picked : '+standardPricebook);
                
            }catch(QueryException e){
                System.debug('error select std pricebook : '+e);
            }               
        }
        

    for( Opportunity eachOpp : oppsNew ){
            Boolean checkCreate = false;           
            if( eachOpp.Pricebook2Id == null ){
                eachOpp.Pricebook2Id = standardPricebook.Id;
            }
            System.debug('standardPricebook : '+standardPricebook);
            System.debug('eachOpp.Pricebook2Id : '+eachOpp.Pricebook2Id);
            System.debug('eachOpp : '+eachOpp);
            if( queryAccount.containsKey(eachOpp.AccountId) ){                
                if( queryAccount.get(eachOpp.AccountId).OwnerId == eachOpp.OwnerId ){
                    checkCreate = true;
                }
                
                if(!checkCreate){
                    for(AccountTeamMember eachAccTeam : queryAccountTeamMember ){
                        if( eachOpp.AccountId == eachAccTeam.AccountId && eachOpp.OwnerId ==  eachAccTeam.UserId ){
                            checkCreate = true;
                            break;
                        }
                    }  
                }
            }
            
            if(!checkCreate){
                
                List<Account__c> privilegeList = Account__c.getall().values();
                
                for( Account__c eachPrivilege : privilegeList ){
                    if( queryUser.containsKey(eachOpp.OwnerId) && queryAccount.containsKey(eachOpp.AccountId)){
                        if(queryUser.get(eachOpp.OwnerId).UserRole.Name == eachPrivilege.Role__c 
                          && queryAccount.get(eachOpp.AccountId).Owner.Segment__c == eachPrivilege.Segment__c){
                            checkCreate = true;
                        } 
                    }                    
                }
                
                /*if( queryUser.get(eachOpp.OwnerId).UserRole.Name == Account__c.getValues('Privilege_RMC').Role__c &&
                  queryAccount.get(eachOpp.AccountId).Owner.Segment__c == Account__c.getValues('Privilege_RMC').Segment__c ){
                    checkCreate = true;
                } */               
            }
            
            if(!checkCreate){
                eachOpp.addError( Trigger_Msg__c.getValues('Permission_Create_Opportunity').Description__c  ,false);    
            }
            System.debug('::::: checkCreate : '+checkCreate+' || '+eachOpp.Name+' :::::');
        }        
        
        
        System.debug(':::: conditionCreateOpportunity End ::::');
    }
    
    public static void updateOpportunityData(List<Opportunity> oppsNew,List<Opportunity> oppsOld,String eventMode,String stageName){
        //User queryUser = [ select Id,Region__c,Zone__c,Segment__c from User where Id = :UserInfo.getUserId() ];
        
        
        Id recordIdSECreditProduct;
        Id recordIdWBGCreditProduct;
        Id recordIdSECreditProduct2;
        Id recordIdWBGCreditProduct2;
        
        for( RecordType eachRecordType : recordTypeList ){
            if( eachRecordType.Name == 'SE Credit Product' ){
                recordIdSECreditProduct = eachRecordType.Id;
            }else if( eachRecordType.Name == 'WBG Credit Product' ){
                recordIdWBGCreditProduct = eachRecordType.Id;
            }else if( eachRecordType.Name == 'SE Credit Product2' ){
                recordIdSECreditProduct2 = eachRecordType.Id;
            }else if( eachRecordType.Name == 'WBG Credit Product2' ){
                recordIdWBGCreditProduct2 = eachRecordType.Id;
            }
        }
        
        for(Opportunity eachOpp : oppsNew ){
            if( eachOpp.StageName == Stage_CaPrep ){
                eachOpp.Check_CA_Prep__c = false;
                eachOpp.Submitted_Region__c = currentUser.Region__c ;
                eachOpp.Submitted_Segment__c = currentUser.Zone__c;
                eachOpp.Submitted_Zone__c = currentUser.Segment__c;
                if( eachOpp.RecordTypeId == recordIdSECreditProduct ){
                    eachOpp.RecordTypeId = recordIdSECreditProduct2;
                }else if( eachOpp.RecordTypeId == recordIdWBGCreditProduct ){
                    eachOpp.RecordTypeId = recordIdWBGCreditProduct2;
                }
            }
        }
        System.debug('updateOpportunityData : '+oppsNew);        
    }
    /*
    public static void changeOwnerOpportunity(List<Opportunity> oppsNew,List<Opportunity> oppsOld,String eventMode){
        System.debug('::::: changeOwnerOpportunity Start :::::');
        Map<Id,Opportunity> mapOppsOld = new Map<Id,Opportunity>();
        if( oppsOld.size() > 0 && oppsOld != null ){
            mapOppsOld.putAll(oppsOld);
        }
        for( Opportunity eachOpp : oppsNew ){
            if( eachOpp.StageName == 'Open' ){
                //eachOpp.PrevOwnerId__c = eachOpp.OwnerId;
            }else{
                //eachOpp.OwnerId = eachOpp.PrevOwnerId__c;
            }
            //eachOpp.OwnerId = mapOppsOld.get(eachOpp.Id).OwnerId;
            //eachOpp.Trigger_flag__c = false;
        }
        System.debug(':::: Opportunity Reverse Owner size of '+oppsNew.size()+' row ::::');
        System.debug('::::: changeOwnerOpportunity End :::::');
    }   */
    
    public static void stageChange(List<Opportunity> oppsNew,List<Opportunity> oppsOld,String eventMode){
        System.debug('::::: stageChange Start :::::');
        Map<Id,Opportunity> mapOppsOld = new Map<Id,Opportunity>();
        if( oppsOld.size() > 0 && oppsOld != null ){
            mapOppsOld.putAll(oppsOld);
        }
        for( Opportunity eachOpp : oppsNew ){
            eachOpp.PrevOwnerId__c = eachOpp.OwnerId;
        }
        System.debug('::::: stageChange End :::::');
    }
    
    public static void stageMappingstageChange(List<Opportunity> oppsNew,List<Opportunity> oppsOld,String eventMode){
        Map<String,String> CreditMapping = new Map<String,String>();
        Map<String,String> NonCreditMapping = new Map<String,String>();
        for(Opportunity_Stage_Mapping__c  oppmap : OppstageList){
                                                       String LowerApplicationStatus = oppmap.Application_status__c;
                                                       if(oppmap.Opportunity_Type__c =='Credit'){
                                                           CreditMapping.put(LowerApplicationStatus.toLowerCase(),oppmap.Stage__c );
                                                       }else if(oppmap.Opportunity_Type__c =='Non-Credit'){
                                                           NonCreditMapping.put(LowerApplicationStatus.toLowerCase(),oppmap.Stage__c );
                                                       }             
        }
        Map<ID,Opportunity> oldOppMap = new Map<ID,Opportunity>();
        
        for(Opportunity eachOldOpp : oppsOld){
            
            oldOppMap.put(eachOldOpp.id,eachOldOpp);
        }
        
        map<string,string> mapRecordType = new map<string,string>();
        for (recordtype r : recordTypeList)
        {
            mapRecordType.put(r.id,r.name);
        }
        
        for( Opportunity eachOpp : oppsNew )
        {
        
            if (eachOpp.recordtypeid == oldOppMap.get(eachOpp.id).recordtypeid)
            {
        
               if(mapRecordType.get(eachOpp.recordtypeid) == 'SE Credit Product2' ||
                  mapRecordType.get(eachOpp.recordtypeid) == 'WBG Credit Product2' ||
                  mapRecordType.get(eachOpp.recordtypeid) == 'Non-credit Product2' )
               {
                    if(eachOpp.Application_Status__c != oldOppMap.get(eachOpp.id).Application_Status__c)
                    {
                    //Start Mapping
                    //
                        if(eachOpp.Application_Status__c !=null && eachOpp.Application_Status__c !=''){
                            String LowerApplicationStatus = eachOpp.Application_Status__c.toLowerCase();
                            if(eachOpp.Opportunity_Type_Formula__c =='Credit' && CreditMapping.containsKey(LowerApplicationStatus)){
                                eachOpp.StageName = CreditMapping.get(LowerApplicationStatus);
                            }else if(eachOpp.Opportunity_Type_Formula__c =='Non-credit' && NonCreditMapping.containsKey(LowerApplicationStatus)){
                                eachOpp.StageName = NonCreditMapping.get(LowerApplicationStatus);
                            }else{
                                eachOpp.addError('This application status does not match in the system. Please select another value.');
                            }
                        
                        }else{
                            if (eachOpp.stagename != 'Close Lost')
                                eachOpp.addError('Application status cannot be null.');
                        }
                    //
                    //Stop Mapping
                     
                    }
                    
                }else{
                    //eachOpp.addError('Cannot update application status unless you have submitted');
                }
                
            }
            
        }
        
        
    }
}