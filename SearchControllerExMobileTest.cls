@isTest
global  class SearchControllerExMobileTest {
    // This method will Run First
    static{
        // Initial Data Custom config
        TestUtils.createAppConfig();
        TestUtils.createStatusCode();        
        TestUtils.createIdType();
    }   
   
    
    static testmethod void  N_NullAllValue(){
        TestUtils.createUsers(1,'MockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'123TEST','Individual', true); 
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectEx;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExMobile searchEx = new SearchControllerExMobile(sc);   
        // Search Individual By Full Name
        searchEx.hdCusType ='Individual';
        acc.First_name__c ='';
        acc.Last_name__c ='';
        acc.Mobile_Number_Temp__c = '';
        acc.Office_Number_Temp__c = '';
        acc.TMB_Customer_ID_Temp__c = '';
        acc.ID_Number_Temp__c='';
        acc.ID_Type__c ='';
        System.debug(SearchEx.isIDValid);
        System.debug(SearchEx.isInformation);
		System.debug(SearchEx.isCusonly);
        System.debug(SearchEx.isLasNameDisabled);
		System.debug(SearchEx.CustTypeOptionList);
        System.debug(SearchEx.IDTypeOptionList);
		System.debug(SearchEx.IDNumberstr);
        System.debug(SearchEx.CustTypestr);
        System.debug(SearchEx.TMBCustStr);
        searchEx.hdIdType = null;
        acc.ID_Type__c = null;        
        Test.startTest();
        // Setup  Mock Callout
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchPartialMock());        
        searchEx.search();
        
        Test.stopTest();
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();          
        system.assertNotEquals(0, msgs.size()); 
        System.debug('Error Cust type null : '+ErrorHandler.statusMessage);

    }
     
    static testmethod void  P_NormalTestFoundDuplicateByFullNameAndMobileAndCanCreate(){
        // Initial Data
        TestUtils.createUsers(1,'MockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'123TEST','Individual', true);        
        
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectEx;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExMobile searchEx = new SearchControllerExMobile(sc);   
        // Search Individual By Full Name
        searchEx.hdCusType ='Individual';
        acc.First_name__c =null;
        acc.Last_name__c =null;
        //acc.Mobile_Number_Temp__c = '0899226677';
        //acc.Office_Number_Temp__c = '027777733';
        
        searchEx.hdIdType = null;
        acc.TMB_Customer_ID_Temp__c ='123123';     
        Test.startTest();
        // Setup  Mock Callout
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchPartialMock());        
        searchEx.search();
        // Found Duplicate But Can Go next step
              
        Test.stopTest();    
        
        // Bind Account that return from service to UI
        searchEx.getAccounts();
        searchEx.ownerId = TestUtils.userList.get(0).id;        
        searchEx.viewOwner();         
        searchEx.acctId = TestUtils.accountList.get(0).id;
        searchEx.viewProspect();
        searchEx.resetNextButton();
    }    
    
    static testmethod void  P_NormalTestFoundDuplicateByFullNameAndCanCreate(){
        // Initial Data
        TestUtils.createUsers(1,'MockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'123TEST','Individual', true);        
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectEx;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExMobile searchEx = new SearchControllerExMobile(sc);   
        // Search Individual By Full Name
        searchEx.hdCusType ='Individual';
        acc.First_name__c ='pom';
        acc.Last_name__c ='pom';
        searchEx.hdIdType = null;
        acc.ID_Type__c = null;        
        Test.startTest();
        // Setup  Mock Callout
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchPartialMock());        
        searchEx.search();
        // Found Duplicate But Can Go next step
        System.assertEquals('Found duplication but can create',ErrorHandler.statusMessage);        
        Test.stopTest();    
        
        // Bind Account that return from service to UI
        searchEx.getAccounts();
        searchEx.ownerId = TestUtils.userList.get(0).id;
        
        searchEx.viewOwner(); 
        
        searchEx.acctId = TestUtils.accountList.get(0).id;
        searchEx.viewProspect();
        searchEx.resetNextButton();
        
        // Go to next Page
        PageReference nextPage =  searchEx.next(); 
        
        PageReference accountCreationPage = Page.CreateProspectMobileLayout;       
        accountCreationPage.getParameters().put('customer_type',acc.Customer_Type_Temp__c);
        accountCreationPage.getParameters().put('company_name',acc.First_name__c); //First Name / Company name
        accountCreationPage.getParameters().put('first_name',acc.First_name__c);
        accountCreationPage.getParameters().put('last_name',acc.Last_name__c);
        accountCreationPage.getParameters().put('mobile_number',acc.Mobile_Number_Temp__c);
        accountCreationPage.getParameters().put('office_number',acc.Office_Number_Temp__c);
        accountCreationPage.getParameters().put('id_type',acc.ID_Type__c);
        accountCreationPage.getParameters().put('id_number',acc.ID_Number_Temp__c);
        accountCreationPage.getParameters().put('tmb_cust_id',acc.TMB_Customer_ID_Temp__c);
        
        system.assertEquals(nextPage.getUrl(), accountCreationPage.getUrl());
        // System(nextPage,accountCreationPage); 
        
        
        
        
    }
   /* 
    static testmethod void N_CalloutReturnMoreThan30(){
        // Initial Data
        TestUtils.createUsers(1,'RespondMockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'RespondTEST','Individual', true);   
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectEx;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExMobile searchEx = new SearchControllerExMobile(sc);   
        // Search Individual By Full Name
        searchEx.hdCusType ='Individual';
        acc.First_name__c ='pom';
        acc.Last_name__c ='pom';
        searchEx.hdIdType = null;
        acc.ID_Type__c = null;        
        
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchResponseIsErrorMock());
        Test.startTest();            
        searchEx.search();    
        Test.stopTest();  
        
    }  */
    
    static testmethod void N_SearchFoundAndViewOwnerError(){
        // Initial Data
        TestUtils.createUsers(1,'MockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'123TEST','Individual', true);        
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectEx;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExMobile searchEx = new SearchControllerExMobile(sc);   
        // Search Individual By Full Name
        searchEx.hdCusType ='Individual';
        acc.First_name__c ='pom';
        acc.Last_name__c ='pom';
        searchEx.hdIdType = null;
        acc.ID_Type__c = null;        
        Test.startTest();
        // Setup  Mock Callout
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchPartialMock());        
        searchEx.search();
        // Found Duplicate But Can Go next step
        System.assertEquals('Found duplication but can create',ErrorHandler.statusMessage);        
        Test.stopTest();    
        
        // Bind Account that return from service to UI
        searchEx.getAccounts();
        searchEx.ownerId = '005900000038TdhAAE';        
        searchEx.viewOwner();        
        List<Apexpages.Message> msgs = ApexPages.getMessages();          
        system.assertNotEquals(0, msgs.size());        
    }    
    
    static testmethod void N_SearchFoundAndViewAccountError(){
        // Initial Data
        TestUtils.createUsers(1,'MockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'123TEST','Individual', true);        
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectEx;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExMobile searchEx = new SearchControllerExMobile(sc);   
        // Search Individual By Full Name
        searchEx.hdCusType ='Individual';
        acc.First_name__c ='pom';
        acc.Last_name__c ='pom';
        searchEx.hdIdType = null;
        acc.ID_Type__c = null;        
        Test.startTest();
        // Setup  Mock Callout
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchPartialMock());        
        searchEx.search();
        // Found Duplicate But Can Go next step
        System.assertEquals('Found duplication but can create',ErrorHandler.statusMessage);        
        Test.stopTest();    
        
        // Bind Account that return from service to UI
        searchEx.getAccounts();
        searchEx.acctId = '001O000000c9ajLIAQ';
        searchEx.viewProspect();      
        List<Apexpages.Message> msgs = ApexPages.getMessages();          
        system.assertNotEquals(0, msgs.size());        
    }
    
    static testmethod void P_TestBaseClass(){
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectEx;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExMobile searchEx = new SearchControllerExMobile(sc);  
        
        string dd =  AccountExtensionBase.SUCCESS_CODE ;
        dd = AccountExtensionBase.QUERY_EXCEPTION_CODE;
        dd = AccountExtensionBase.CALLOUT_EXCEPTION_CODE ;
        dd = AccountExtensionBase.FOUND_DUP_ALLOW_CREATE ;
        dd =  AccountExtensionBase.FOUND_DUP_NOT_ALLOW_CREATE;
        dd =  AccountExtensionBase.FOUND_DUP_MORE_THAN_30_NOT_ALLOW_CREATE;
        
        
    }    
    
        static testmethod void  N_NullhdCusType(){
        TestUtils.createUsers(1,'MockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'123TEST','Individual', true); 
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectEx;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExMobile searchEx = new SearchControllerExMobile(sc);   
        // Search Individual By Full Name
        searchEx.hdCusType ='';
        acc.First_name__c ='';
        acc.Last_name__c ='';
        acc.Mobile_Number_Temp__c = '';
        acc.Office_Number_Temp__c = '';
        acc.TMB_Customer_ID_Temp__c = '';
        acc.ID_Number_Temp__c='';
        acc.ID_Type__c ='';
        System.debug(SearchEx.isIDValid);
        System.debug(SearchEx.isInformation);
		System.debug(SearchEx.isCusonly);
        System.debug(SearchEx.isLasNameDisabled);
		System.debug(SearchEx.CustTypeOptionList);
        System.debug(SearchEx.IDTypeOptionList);
		System.debug(SearchEx.IDNumberstr);
        System.debug(SearchEx.CustTypestr);
        System.debug(SearchEx.TMBCustStr);
        searchEx.hdIdType = null;
        acc.ID_Type__c = null;        
        Test.startTest();
        // Setup  Mock Callout
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchPartialMock());        
        searchEx.search();
        
        Test.stopTest();
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();          
        system.assertNotEquals(0, msgs.size()); 
        System.debug('Error Cust type null : '+ErrorHandler.statusMessage);

    }
    
            static testmethod void CheckboxModecust(){
        // Initial Data
        TestUtils.createUsers(1,'MockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'123TEST','Individual', true);        
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectEx;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExMobile searchEx = new SearchControllerExMobile(sc);   
        // Search Individual By Citizen ID
        searchEx.hdCusType ='Individual';
        searchEx.hdCheckBoxMode='cust';
        acc.First_name__c ='pom';
        acc.Last_name__c ='pom';
        acc.ID_Type__c ='Citizen ID';
        searchEx.account.ID_Type__c ='Citizen ID';
       // searchEx.acctId='';
       // searchEx.ownerId='';

       // acc.ID_Type__c = null;        
        Test.startTest();
        // Setup  Mock Callout
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchPartialMock());        
        searchEx.search();
        //Found Error
        //System.assertEquals('TMB Customer ID is required',ErrorHandler.statusMessage);        
        Test.stopTest();    
        List<Apexpages.Message> msgs = ApexPages.getMessages();          
        system.assertNotEquals(0, msgs.size()); 
 
    }
    
            static testmethod void CheckidTypeidNumNull(){
        // Initial Data
        TestUtils.createUsers(1,'MockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'123TEST','Individual', true);        
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectEx;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExMobile searchEx = new SearchControllerExMobile(sc);   
        // Search Individual By Citizen ID
        searchEx.hdCusType ='Individual';
        searchEx.hdCheckBoxMode='info';
        acc.First_name__c ='rat';
        acc.ID_Number_Temp__c='2123431';     
        acc.ID_Type__c =null;
        searchEx.account.ID_Type__c =null;
        searchEx.account.ID_Number_Temp__c='2123431';  
       // searchEx.acctId='';
       // searchEx.ownerId='';
       // acc.ID_Type__c = null;        
        Test.startTest();
        // Setup  Mock Callout
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchPartialMock());        
        searchEx.search();
        // Found Error
        //System.assertEquals('ID Type cannot be None when searching ID Number',ErrorHandler.statusMessage);        
        Test.stopTest();    
		List<Apexpages.Message> msgs = ApexPages.getMessages();          
        system.assertNotEquals(0, msgs.size()); 
        
    }
    
    
}