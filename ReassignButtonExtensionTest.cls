@IsTest(SeeAllData=false)
public class ReassignButtonExtensionTest {
    
            static {
        TestUtils.createIdType();
        TestUtils.createAppConfig();
        TestUtils.createStatusCode();
        TestUtils.createDisqualifiedReason();    
      
    }
    public static testmethod void positiveTest(){        
        System.debug(':::: positiveTest Start ::::');
        TestInit.createUser(false);
        User u = TestInit.us;
        System.runAs(u) {
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
            System.debug('Current User Id: ' + UserInfo.getUserId()); 
            TestInit.createAccount(1);
            TestInit.createOpportunity(TestInit.accountList.values());            
            List<Account> accountList = TestInit.accountList.values();
            accountList.get(0).Re_assign_prospect__c = 'yes';
            accountList.get(0).Account_Type__c = 'Qualified Prospect';
            accountList.get(0).Suggested_Sub_Segment__c = 'TEST';
            update accountList.get(0);
            ApexPages.StandardController sc = new ApexPages.StandardController(accountList.get(0));
            ReassignButtonExtension objEx = new ReassignButtonExtension(sc);
            Test.startTest();
            objEx.Trigger_T01();
            accountList.get(0).Re_assign_prospect__c = 'yes';
            accountList.get(0).Account_Type__c = 'Qualified Prospect';
            accountList.get(0).Suggested_Sub_Segment__c = 'ABC';
            update accountList.get(0);
            sc = new ApexPages.StandardController(accountList.get(0));
            objEx = new ReassignButtonExtension(sc);
            objEx.Trigger_T01();
            Test.stopTest();           
        }
        System.debug(':::: positiveTest End ::::');
    }
    
}