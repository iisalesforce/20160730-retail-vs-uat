public without sharing class RTL_LeadTriggerHandler extends TriggerHandler {//Use without sharing to allow lead conversion from non-owner
/*------------------------------------------------------------------------
Author:        Debi Prasad Baral
Company:       Salesforce
Description:   A class created to manage trigger actions from the Lead object 
               Responsible for:
               1 - Validating that the Lead Status is qualified and the at least one associated interested product is primary
               2 - Updating the opportunity created during lead conversion according to the correct record type.
History
<Date>            <Authors Name>    <Brief Description of Change> 
--------------------------------------------------------------------------*/
      
    List<Account> updateAccounts = new List<Account>(); 
    List<Opportunity> updateOpportunities = new List<Opportunity>(); 
    List<RTL_Interested_products_c__c> updateInterestedProducts = new List<RTL_Interested_products_c__c>();
    
    public RTL_LeadTriggerHandler(){
        if(Test.isRunningTest()){
            this.setMaxLoopCount(10);
        }
        else{
            this.setMaxLoopCount(2);
        }
     
        System.Debug( 'TMB: -> LeadTriggerHandler Invoked' );
    }
            
    //take the first and last 3 characters of ID number as NID of customer
    public String getNID (String idNumber) {
        String nid;
        if (idNumber != null && idNumber.length() > 0) {
            nid = (idNumber.length() <= 5) ?  idNumber: idNumber.substring(0,1)+idNumber.substring(idNumber.length()-4); 
        }
        return nid;
    }
    
    protected override void beforeUpdate(map<id,sObject> oldMap, map<id,sObject> newMap) {
        System.Debug('TMB: -> LeadTriggerHandler start of beforeUpdate');      

        User user = null;
        Group queue = null;
        Branch_and_Zone__c branch = null;
        //Map<String, Branch_and_Zone__c> branchNameMap = new Map<String, Branch_and_Zone__c>();
        Map<String, Branch_and_Zone__c> branchCodeMap = new Map<String, Branch_and_Zone__c>();
        for(Branch_and_Zone__c branchzone : [Select Name, Branch_Code__c, RTL_Region_Code__c, RTL_Zone_Code__c from Branch_and_Zone__c]) {
            //branchNameMap.put(branchzone.Name, branchzone);
            branchCodeMap.put(branchzone.Branch_Code__c, branchzone);
        }   
        
        //Store lead owners to map to user object
        Set<Id> leadOwnerIds = new Set<Id>();
        //Store the last 3 characters of the lead ID number as NID number to compare against account object
        Set<String> leadNIDNumbers = new Set<String>();
        String leadIDNumber = null;
         // Add filter status check for lead conversion
        String qStatus = 'Qualified';
        String cStatus = 'Closed Converted';
        for (Id lId:newMap.keySet()){
            leadOwnerIds.add(((lead)newMap.get(lId)).OwnerId);
            leadIDNumber = ((lead)newMap.get(lId)).RTL_Citizen_Id__c;
            boolean currentStatus = (((lead)oldMap.get(lId)).Status == qStatus);
            boolean targetStatus = (((lead)newMap.get(lId)).Status == cStatus);
            if (leadIdNumber != null && currentStatus && targetStatus)  
                leadNIDNumbers.add(getNID(leadIDNumber));
        }
        //Keep the list of the lead owner as users
        Map<Id, User> userMap = new Map<Id, User>();
        for(User u: [Select Id, RTL_Branch__c, RTL_Branch_Code__c, Region_Code__c, Zone_Code__c, RTL_Channel__c from User where id in :leadOwnerIds])
            userMap.put(u.Id, u);
        //Keep the list of the lead owner as queues
        Map<Id, Group> queueMap = new Map<Id, Group>();
        for(Group q : [Select Id, DeveloperName from Group where Type = 'Queue' and id in :leadOwnerIds])
            queueMap.put(q.Id, q);
           
        //Store account ID type + ID number combination as set for dudup check of customer before lead conversion validation
        Set<String> accountSet = new Set<String>();
        for(Account acct : [Select ID_Type_PE__c, ID_Number_PE__c, RTL_NID__c from Account where RTL_NID__c in :leadNIDNumbers])
            accountSet.add(acct.ID_Type_PE__c+acct.ID_Number_PE__c);
        
        String branchCode = null;
        for (Id lId:newMap.keySet()){
            Lead leadObj = (lead)newMap.get(lId);
            Lead oldLeadObj = (lead)oldMap.get(lId);
            
            if (RTL_Utility.getObjectRecordTypeIdsByDevNamePrefix(Lead.SObjectType, 'Retail').contains(leadObj.RecordTypeId)) {//only continue if it's retail record type
                //lead conversion validation
                if(leadObj.IsConverted){
                    //FR-008: make sure the lead status is Qualified with only 1 primary product before lead conversion
                    validateLead(leadObj, oldLeadObj, accountSet);
                }
                //update branch name after lead owner is changed
                if(leadObj.OwnerId != oldLeadObj.OwnerId) {
                    //the lead owner chould be either a user or a queue
                    user = userMap.get(leadObj.OwnerId);
                    if (user != null) {
                        if (user.RTL_Branch_Code__c != null && branchCodeMap.get(user.RTL_Branch_Code__c) != null) {
                            leadObj.RTL_Branch_and_Zone__c = branchCodeMap.get(user.RTL_Branch_Code__c).Id;
                        } else {
                            leadObj.RTL_Branch_and_Zone__c = null;
                        }
                        leadObj.RTL_Branch_Team_Name_Code_Rpt__c = user.RTL_Branch__c;
                        leadObj.RTL_Branch_Code_Rpt__c = user.RTL_Branch_Code__c;
                        leadObj.RTL_Region_Code_Rpt__c = user.Region_Code__c;
                        leadObj.RTL_Zone_Code_Rpt__c = user.Zone_Code__c;
                        leadObj.RTL_Lead_Channel__c = user.RTL_Channel__c;
                    } else {
                        //the lead owner could be a branch queue or outbound queue
                        queue = queueMap.get(leadObj.OwnerId);
                        branchCode = queue.DeveloperName;
                        if (branchCode.indexOf('_') != -1) {
                            branchCode = branchCode.substring(branchCode.indexOf('_') + 1, branchCode.length());
                        }
                        branch = branchCodeMap.get(branchCode);
                        if (branch != null) {//branch queue
                            leadObj.RTL_Branch_and_Zone__c = branch.Id;      
                            leadObj.RTL_Branch_Team_Name_Code_Rpt__c = branch.Name;                     
                            leadObj.RTL_Branch_Code_Rpt__c = branch.Branch_Code__c;
                            leadObj.RTL_Region_Code_Rpt__c = branch.RTL_Region_Code__c;
                            leadObj.RTL_Zone_Code_Rpt__c = branch.RTL_Zone_Code__c;
                            leadObj.RTL_Lead_Channel__c = null;                         
                        } else {//outbound queue
                            leadObj.RTL_Branch_and_Zone__c = null;
                            leadObj.RTL_Branch_Team_Name_Code_Rpt__c = null;
                            leadObj.RTL_Branch_Code_Rpt__c = null;
                            leadObj.RTL_Region_Code_Rpt__c = null;
                            leadObj.RTL_Zone_Code_Rpt__c = null; 
                            leadObj.RTL_Lead_Channel__c = null;                                   
                        }
                    }
               }
            }          
        }
        System.Debug('TMB: -> LeadTriggerHandler end of beforeUpdate');  
    }
    
    protected override void afterUpdate(map<id,sObject> oldMap, map<id,sObject> newMap) {        
        System.Debug('TMB: -> LeadTriggerHandler start of afterUpdate');  
        
        /*
         * Loop through the leads submitted through this
         * trigger.  Populate the appropriate sets of Ids
         * for each lead with populated values.
         */
        for (Id lId:newMap.keySet()){
            Lead leadObj = (lead)newMap.get(lId);
            Lead oldLeadObj = (lead)oldMap.get(lId);
            
            if (RTL_Utility.getObjectRecordTypeIdsByDevNamePrefix(Lead.SObjectType, 'Retail').contains(leadObj.RecordTypeId)) {//only continue if it's retail record type            
                //post processing of lead conversion
                if(leadObj.IsConverted){ 
                    if (leadObj.convertedAccountId != null) {
                        //Call method to create a list of account to be updated
                        createAccountToUpdate(leadObj);
                        //Call method to create a list of opportunities to be updated 
                        createOpportunitiesToUpdate(leadObj);
                        //Call Method to Update Interested Product Reference if Account was generated from Lead        
                        insertInterestedProductsForConvertedAccounts(leadObj); 
                    }
                }  
            }    
        }
      
        //Update the records for any update list populated with records.
        if(updateAccounts.size() > 0){
            // Update the converted account records
            Database.SaveResult[] lsr = Database.update(updateAccounts, false);
            // Iterate through each returned result
            for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated lead converted account.');
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(logginglevel.ERROR, 'There is error updating lead converted account. Error Message is: ' + err.getMessage());
                    }
                }
            }            
        }
        
        if(updateOpportunities.size() > 0){
            // Update the converted opportunities records
            Database.SaveResult[] lsr = Database.update(updateOpportunities, false);
            // Iterate through each returned result
            for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated lead converted opportunity.');
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(logginglevel.ERROR, 'There is error updating lead converted opportunity. Error Message is: ' + err.getMessage());
                    }
                }
            }             
        }
                
        if(updateInterestedProducts.size() > 0){
            // Update the account interested product
            Database.SaveResult[] lsr = Database.update(updateInterestedProducts, false);
            // Iterate through each returned result
            for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated lead converted interested product.');
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(logginglevel.ERROR, 'There is error updating lead converted interested product. Error Message is: ' + err.getMessage());
                    }
                }
            }           
        }     
    }
    
    protected override void beforeInsert(List<SObject> leads) {        
        System.Debug('TMB: -> RTL_LeadTriggerHandler start of beforeInsert');
        
        //FR-011: Only auto assign the web-to-lead to different lead queue based on Assignment Criteria
        //A Lead Assignment Rule named "WebToLeadNoOwner" is created with "Do Not Reassign Owner" for web-to-lead 
        //this is to make sure the default lead owner (set in lead settings) won't overwrite the value set in beforeInsert trigger
        prepareLeadInfo(leads);
    }
    
    protected override void afterInsert(map<id,SObject> newMap) {       
        System.Debug('TMB: -> RTL_LeadTriggerHandler start of afterInsert');
        
        //FR-003: Add lead interested product from web-to-lead
        addInterestedProducts(newMap);
    }      

    /*end trigger handler overrides*/
    //====================================================================
    
    /* private methods */
    //====================================================================
    /**
     * @desc This method would validate rules before the lead is converted: 
     * Lead is qualified and it has only one primary product
     * @param [Lead leadObj Lead oldLeadObj Set accountSet]
     */
    private void validateLead(Lead leadObj, Lead oldLeadObj, Set<String> accountSet){  
        if (oldLeadObj.Status != 'Qualified'){//check if the lead status was Qualified
            leadObj.addError(System.Label.LeadError_QualifiedBeforeConversion);
        }else if (accountSet.contains(leadObj.RTL_ID_Type__c + leadObj.RTL_Citizen_Id__c)) {//check if customer is duplicated with combination of ID type and ID number
            leadObj.addError(System.Label.LeadError_DupCustomerBeforeConversion);
        } else {
            //Get the count of primary product of lead interested products
            Integer primaryProductCount = [SELECT Count() FROM RTL_Interested_products_c__c 
                          WHERE Lead__c = :leadObj.Id AND Is_Primary__c = true];          
            if(primaryProductCount != 1){
                leadObj.addError(System.Label.LeadError_OnePrimaryProductBeforeConversion);
            }
        }
    }
    /* private methods */
    //====================================================================
    /**
     * @desc This method would create a list of account records which were associated with lead being converted
     * and will be updated
     * @param [Lead leadObj]
     */
    private void createAccountToUpdate(Lead leadObj){
        Account account = [SELECT Id, RecordTypeId, Account_Type__c, ID_Number_PE__c FROM Account WHERE Id =:leadObj.convertedAccountId];
        // Make appropriate updates here.
        account.RecordTypeId = [Select Id from RecordType 
                                        where Name = :'Retail Prospect' and SobjectType = 'Account' 
                                        and IsActive = true limit 1].Id;
        account.Account_Type__c = 'Retail Prospect';
        account.First_name_PE__c = leadObj.FirstName;
        account.Last_name_PE__c = leadObj.LastName;
        account.RTL_Customer_Title__c = leadObj.Salutation; //Customer Title Name (TH)
        //account.Mobile_Number_PE__c = leadObj.MobilePhone; //Mobile Phone Number
        //account.C_Home_phone_PE__c = leadObj.Phone; //Home Phone Number
        //account.Phone = leadObj.RTL_Office_Number__c; //Office Phone Number                
        account.Mobile_Number_PE__c = leadObj.RTL_Mobile_Number__c; //Mobile Phone Number
        account.C_Home_phone_PE__c = leadObj.RTL_Phone_Number__c; //Home Phone Number
        account.RTL_Office_Phone_Number__c = leadObj.RTL_Office_Number__c; //Office Phone Number
        account.Email_Address_PE__c = leadObj.RTL_Email_Address__c; //Email 1
        account.RTL_NID__c = getNID(account.ID_Number_PE__c);
        if(leadObj.Street != null){
            account.Primary_Address_Line_1_PE__c = leadObj.Street; //Primary Address No./Moo/Soi/Street
        }
        if(leadObj.City != null){
            account.Primary_Address_Line_3_PE__c = leadObj.City; //Primary District
        }    
        if(leadObj.State != null){
            account.Province_Primary_PE__c = leadObj.State; //Primary Province
        }      
        if(leadObj.PostalCode != null){
            account.Zip_Code_Primary_PE__c = leadObj.PostalCode; //Primary Zip code
        }
        if(leadObj.Country != null){
            account.Country_Primary_PE__c = leadObj.Country; //Primary Country
        }
                
        updateAccounts.add(account);
    }
    
    /**
     * @desc This method would create a list of opportunity records which were associated with lead being converted
     * and will be updated
     * @param [Lead leadObj]
     */
    private void createOpportunitiesToUpdate(Lead leadObj){
        if (leadObj.convertedOpportunityId != null) {
            //opportunity will only be created during lead conversion
            //1. if "Do not create a new opportunity upon conversion" is not checked and opportunity detail is provided in browser
            //2. opportunity detail is provided in SF1 UI
            Opportunity opportunity = [SELECT Id, Name, RTL_Product_Name__c FROM Opportunity WHERE Id =: leadObj.convertedOpportunityId];
            //get interested product fields to pass on to oppertunity
            RTL_Interested_products_c__c intProduct = new RTL_Interested_products_c__c();
            intProduct = [Select Id, Product_Name__c, product_group__c, Product_Sub_Group__c, 
                                      Income_SL__c, Income_SE__c, Debt__c, Collateral_Type__c, Loan_Amount__c,
                                      Co_borrower_1__c, Co_borrower_2__c from RTL_Interested_products_c__c 
                                                       where Is_Primary__c = : true and Lead__c = : leadObj.Id limit 1];
                        
            //get the custom settings values 
            List<Opportunity_Recordtype_Mapping__c> stageMappingCodes = Opportunity_Recordtype_Mapping__c.getAll().values(); 
            
            for (Opportunity_Recordtype_Mapping__c stagemappingCode : stageMappingCodes){
                if (intProduct.product_group__c == stagemappingCode.Product_Group__c){
                    opportunity.RecordTypeId = [Select Id from RecordType 
                                            where Name = : stagemappingCode.Record_Type__c and SobjectType = 'Opportunity' 
                                            and IsActive = true limit 1].Id;
                    opportunity.StageName = stagemappingCode.Stage__c;         
                }
            }
                       
            //make appropriate updates here.
            opportunity.RTL_Product_Group__c = intProduct.product_group__c;
            opportunity.RTL_Product_Name__c = intProduct.Product_Name__c; //Product Name
            opportunity.RTL_Prod_SubGrp__c = intProduct.Product_Sub_Group__c; //Product Sub Group
            opportunity.RTL_Income_SL__c = intProduct.Income_SL__c; //Income - SL
            opportunity.RTL_Income_SE__c = intProduct.Income_SE__c; //Income - SE
            opportunity.RTL_Debt__c = intProduct.Debt__c; // Debt
            opportunity.RTL_Collateral_Type__c = intProduct.Collateral_Type__c; //Collateral Type 
            opportunity.Amount = intProduct.Loan_Amount__c; //Loan amount
            opportunity.RTL_Co_borrower_1__c = intProduct.Co_borrower_1__c; //Co-borrower 1
            opportunity.RTL_Co_borrower_2__c = intProduct.Co_borrower_2__c; //Co-borrower 2 

            updateOpportunities.add(opportunity);
        }   
    }
    
    /**
     * @desc This method would associate interested product with account records which were associated with lead being converted
     * @param [Lead leadObj]
     */
    private void insertInterestedProductsForConvertedAccounts(Lead leadObj){
        //associate lead interested product to customer
       for(RTL_Interested_products_c__c relatedInterestedProduct : [Select Id, Lead__c, Customer__c FROM RTL_Interested_products_c__c 
                               WHERE Lead__c =:leadObj.Id]) {                   
           relatedInterestedProduct.Customer__c = leadObj.convertedAccountId;
           updateInterestedProducts.add(relatedInterestedProduct);
       }
    }
    
    /**
     * This is the method to auto assign lead queue (FR-011)
     **/
    private static void prepareLeadInfo(List<Lead> leads) {      
        //Keep the list of the lead queues, where queue Id will be assigned as lead OwnerId
        Map<String, Group> queueMap = new Map<String, Group>();
        String branchCode = null;
        for(Group queue : [Select Id, DeveloperName from Group where Type = 'Queue']) {
            branchCode = queue.DeveloperName;//e.g. DeveloperName=RTLQ_001, branchcode=001
            if (branchCode.indexOf('_') != -1) {
                branchCode = branchCode.substring(branchCode.indexOf('_')+1, branchCode.length());
            }
            queueMap.put(branchCode, queue);
        }                         
        
        //Keep the list of the lead assignment criteria in map, which will help to determine the lead queue
        Map<String, RTL_Assignment_Criterias__c> ruleMap = new Map<String, RTL_Assignment_Criterias__c>();
        for(RTL_Assignment_Criterias__c rule : [Select Product_Name__r.Name, Destination_BKK__c, Destination_UPC__c from RTL_Assignment_Criterias__c])
            ruleMap.put(rule.Product_Name__r.Name, rule);
            
        Set<Id> leadOwnerIds = new Set<Id>();
        for (Lead lead: leads){
            leadOwnerIds.add(lead.OwnerId);
        }
        //Keep the list of the lead owner as users
        Map<Id, User> userMap = new Map<Id, User>();
        for(User u: [Select Id, RTL_Branch__c, RTL_Branch_Code__c, Region_Code__c, Zone_Code__c, RTL_Channel__c from User where id in :leadOwnerIds])
            userMap.put(u.Id, u);
                   
        //Keep the branch Name into list for lead owner to lookup
        Map<String, Branch_and_Zone__c> branchCodeMap = new Map<String, Branch_and_Zone__c>();
        for(Branch_and_Zone__c branchzone : [Select Name, Branch_Name__c, Branch_Code__c, RTL_Region_Code__c, RTL_Zone_Code__c from Branch_and_Zone__c]) {
            branchCodeMap.put(branchzone.Branch_Code__c, branchzone);
        }
        
        String queueName, w2lBranchCode, productName, failProducts = null;
        Set<String> productNameList = null;
        RTL_Assignment_Criterias__c assignRule = null;
        
        for (Lead leadObj:leads){
            //if (RTL_Utility.getObjectRecordTypeIdsByDevNamePrefix(Lead.SObjectType, 'Retail').contains(leadObj.RecordTypeId)) {//only continue if it's retail record type 
                //Check if it's web-to-lead
                if (leadObj.RTL_TMB_Campaign_Source__c == 'Web') {
                    //Get Product Name from lead object, which may contains multiple products delimited by ";"
                    productName = leadObj.RTL_Product_Name__c;
                    productNameList = new Set<String>();
                    if (productName != null) productNameList.addAll(productName.split(';'));
                    failProducts = ''; 
                    for (String name: productNameList) {
                        RTL_Assignment_Criterias__c productDetail = ruleMap.get(name);
                        if (productDetail == null) {
                            //Handle exception in case the product name doesn't exist
                            failProducts += name + ';';
                        }
                    }
                    //handle product name validation here in order to use the same transaction to update lead object
                    if (failProducts.length() > 0) {// Add unsupported product name to lead description
                        failProducts = failProducts.substring(0, failProducts.length()-1);//remove the last ';' at the end
                        leadObj.RTL_Description__c = System.Label.LeadDesc_ProductNameNotFoundAsInterestedProduct + failProducts;
                    }
                    //only auto assign to lead queue if custom settings Lead Assignment/Auto Assign/Enable__c = true
                    if (RTL_Lead_Assignment__c.getValues('Auto Assign') != null && RTL_Lead_Assignment__c.getValues('Auto Assign').Enable__c) {
                        w2lBranchCode = leadObj.RTL_Branch_Name__c;
                        queueName = null;
                        if (leadObj.RTL_Multiple_Interested_Product_s__c) {
                            //If the lead has multi products, auto assign to outbound or branch queue (to be determined)
                            //queueName = 'Outbound';
                            queueName = (branchCodeMap.get(w2lBranchCode) == null) ? null:w2lBranchCode;
                        } else {
                            //If the lead has only one product, assign the lead queue based on assignment criteria
                            assignRule = ruleMap.get(productName);
                            // Handle exception in case the product name doesn't exist in RTL_Assignment_Criterias__c
                            if (assignRule != null) {
                                //Determine the lead queue Id based on Lead Region and lookup the destination from Assignment Criteria
                                if (leadObj.RTL_Region__c == 'BKK') {
                                    if (assignRule.Destination_BKK__c == 'Branch') queueName = (branchCodeMap.get(w2lBranchCode) == null) ? null:w2lBranchCode;
                                    else queueName = 'Outbound';
                                } else {
                                    if (assignRule.Destination_UPC__c == 'Branch') queueName = (branchCodeMap.get(w2lBranchCode) == null) ? null:w2lBranchCode;
                                    else queueName = 'Outbound';
                                }
                            }
                        }
                        //Assign queue Id to lead OwnerId
                        if (queueName != null) { //double check if queue name is null in case the product name doesn't exist
                            if (queueMap.get(queueName) != null) //double check if queue name exist before assigning to lead owner
                                leadObj.OwnerId = ((Group)queueMap.get(queueName)).Id;
                            if (queueName != 'Outbound' && branchCodeMap.get(w2lBranchCode) != null) {//branch queue
                                Branch_and_Zone__c branch = branchCodeMap.get(w2lBranchCode);
                                leadObj.RTL_Branch_and_Zone__c = branch.Id;
                                leadObj.RTL_Branch_Team_Name_Code_Rpt__c = branch.Name;
                                leadObj.RTL_Branch_Code_Rpt__c = branch.Branch_Code__c;
                                leadObj.RTL_Region_Code_Rpt__c = branch.RTL_Region_Code__c;
                                leadObj.RTL_Zone_Code_Rpt__c = branch.RTL_Zone_Code__c;                                  
                            } else {//outbound queue or branch is not defined in branch_and_zone
                                leadObj.RTL_Branch_and_Zone__c = null;                                  
                            }
                        }
                    } else {
                        //if not custom setting is not enabled, the lead owner will be user
                        //assignLeadOwnerBranch(leadObj, userMap, branchNameMap);
                        assignLeadOwnerBranch(leadObj, userMap, branchCodeMap);
                    }
                } else {
                    //if not web-to-lead, the lead owner will be user
                    //assignLeadOwnerBranch(leadObj, userMap, branchNameMap);
                    assignLeadOwnerBranch(leadObj, userMap, branchCodeMap);    
                }
            //}
        }     
    }    
    
    /**
    * This is the method to assign lead owner's branch information
    **/
    //private static void assignLeadOwnerBranch(Lead leadObj, Map<Id, User> userMap, Map<String, Branch_and_Zone__c> branchNameMap) {
    private static void assignLeadOwnerBranch(Lead leadObj, Map<Id, User> userMap, Map<String, Branch_and_Zone__c> branchCodeMap) {
        User user = userMap.get(leadObj.OwnerId);
        if (user != null) {
            if (user.RTL_Branch_Code__c != null && branchCodeMap.get(user.RTL_Branch_Code__c) != null) {
                leadObj.RTL_Branch_and_Zone__c = branchCodeMap.get(user.RTL_Branch_Code__c).Id;   
            } else {
                leadObj.RTL_Branch_and_Zone__c = null;
            }
           leadObj.RTL_Branch_Team_Name_Code_Rpt__c = user.RTL_Branch__c;
           leadObj.RTL_Branch_Code_Rpt__c = user.RTL_Branch_Code__c;
           leadObj.RTL_Region_Code_Rpt__c = user.Region_Code__c;
           leadObj.RTL_Zone_Code_Rpt__c = user.Zone_Code__c;   
           leadObj.RTL_Lead_Channel__c = user.RTL_Channel__c;
        }       
    }
    
    /**
     * This is the method to persist interested products from web-to-lead (FR-003)
     **/
    private static void addInterestedProducts(map<id,sObject> leadMap) {   
        // List of lead interested products to be inserted
        List<RTL_Interested_products_c__c> interestProductList = new List<RTL_Interested_products_c__c>();
        List<Lead> leads = new List<Lead>();
        
        String productName = null;
        Set<String> productNameList = null;
        Integer i = 0;
        RTL_Interested_products_c__c interestProduct = null;
        
        //Put all retail product master to map for later retrieval
        Map<String, RTL_product_master__c> productMap = new Map<String, RTL_product_master__c>();
        for(RTL_product_master__c product : [Select Id, Name, Product_Group__c, Product_Sub_group__c 
                                                         from RTL_product_master__c order by Name, Product_Group__c desc]) {
            if (product.Product_Group__c != 'Loan Retention') productMap.put(product.Name, product);                                                         	
		}
        
        for (Id lId:leadMap.keySet()){
            Lead leadObj = (lead)leadMap.get(lId);
            //if (RTL_Utility.getObjectRecordTypeIdsByDevNamePrefix(Lead.SObjectType, 'Retail').contains(leadObj.RecordTypeId)) {//only continue if it's retail record type 
                // Check if it's web-to-lead
                if (leadObj.RTL_TMB_Campaign_Source__c == 'Web') {
                    // Get Product Name from lead object, which may contains multiple products delimited by ";"
                    productName = leadObj.RTL_Product_Name__c;
                    if (productName != null) {
                        productNameList = new Set<String>();
                        productNameList.addAll(productName.split(';'));
                        i = 0;
                        for (String name: productNameList) {
                            RTL_product_master__c productDetail = productMap.get(name);
                            if (productDetail != null) {
                                interestProduct = new RTL_Interested_products_c__c();
                                // Only set the first interested product as primary
                                if (i == 0) {
                                    interestProduct.Is_Primary__c = true;
                                    i++;
                                }
                                interestProduct.Product_Name__c = productDetail.Id;
                                interestProduct.product_group__c = productDetail.Product_Group__c;
                                interestProduct.Product_Sub_Group__c = productDetail.Product_Sub_group__c;
                                interestProduct.Lead__c = leadObj.Id;
                                interestProductList.add(interestProduct);
                            }
                        }
                    }
                }
            //}
        }
        // insert lead interested product
        if (interestProductList.size() > 0) {
            Database.SaveResult[] lsr = Database.insert(interestProductList, false);
            // Iterate through each returned result
            for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted lead interested product.');
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(logginglevel.ERROR, 'There is error inserting lead interested product. Error Message is: ' + err.getMessage());
                    }
                }
            } 
        }
    } 
      /* end private methods */
    //====================================================================

     // exception class
    public class LeadTriggerHandlerException extends Exception {} 

}