public class ContactTriggerHandler {
    
    static Boolean detectError = false;
    static String errorException = '';
    static String STR_INSERT = 'insert';
    static String STR_UPDATE = 'update';
    static String STR_DELETE = 'delete';
    
    public static void handlerBeforeInsert(List<Contact> contactsNew){
        conditionCreateContact(contactsNew,null,STR_INSERT);
    }
    
    public static void conditionCreateContact(List<Contact> contactsNew,List<Contact> contactsOld,String eventMode){
        System.debug(':::: conditionCreateVisitReport Start ::::');
        
        List<Id> idAccountList = new List<Id>();
        List<Id> idUserList = new List<Id>();        
        
        for( Contact eachCont : contactsNew ){
            if( eachCont.AccountId != null ){
                idAccountList.add(eachCont.AccountId);
            }            
            idUserList.add(eachCont.OwnerId);
        }
        
        Map<Id,Account> queryAccount = new Map<Id,Account>([ select Id,OwnerId,Owner.Segment__c
                                                            from Account 
                                                            where Id IN :idAccountList ]);
        
        List<AccountTeamMember> queryAccountTeamMember = [ select accountId,userId
                                                          from AccountTeamMember 
                                                          where accountId IN :idAccountList 
                                                          and userId IN :idUserList];
        
        Map<Id,User> queryUser = new Map<Id,User>([select Id,UserRole.Name,Segment__c from user where Id IN :idUserList]);
		System.debug('queryAccount : '+queryAccount);
        System.debug('contactsNew : '+contactsNew);        
        for( Contact eachCont : contactsNew ){
            Boolean checkCreate = false;
            if( queryAccount.containsKey(eachCont.AccountId) ){                
                if( queryAccount.get(eachCont.AccountId).OwnerId == eachCont.OwnerId ){
                    checkCreate = true;
                }
                
                if(!checkCreate){
                    for(AccountTeamMember eachAccTeam : queryAccountTeamMember ){
                        if( eachCont.AccountId == eachAccTeam.AccountId && eachCont.OwnerId ==  eachAccTeam.UserId ){
                            checkCreate = true;
                            break;
                        }
                    }  
                }
            }
            
            if(!checkCreate){
                
                List<Account__c> privilegeList = Account__c.getall().values();
                
                for( Account__c eachPrivilege : privilegeList ){
                    if(queryUser.get(eachCont.OwnerId).UserRole.Name == eachPrivilege.Role__c 
                       && queryAccount.get(eachCont.AccountId).Owner.Segment__c == eachPrivilege.Segment__c){
                           checkCreate = true;
                       }                    
                }                            
            }
            
            if(!checkCreate){
                eachCont.addError( Trigger_Msg__c.getValues('Permission_Create_Opportunity').Description__c  ,false);    
            }
            System.debug('::::: checkCreate : '+checkCreate+' || '+eachCont.Name+' :::::');
        }   
        
        System.debug(':::: conditionCreateContact End ::::');
    }
    
    
}