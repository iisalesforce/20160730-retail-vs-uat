public class RetailLeadExtension {
    private Lead lead;
    private String action;    
    private String recordtypeId;
    private String saveAndNew {get;set;}
    //public String displayS1 {get;set;}
    
    private static final String STR_INSERT = 'insert';
    private static final String STR_UPDATE = 'update';
    
    public Integer getDisplayS1() {
    	return (UserInfo.getUiThemeDisplayed() == 'Theme4t'? 1:2);
    }
    public RetailLeadExtension(ApexPages.StandardController std){
    	//displayS1 = getDisplayS1();
        this.lead = (Lead)std.getRecord();
        recordtypeId = ApexPages.currentPage().getParameters().get('RecordType');
        recordtypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Retail Banking').getRecordTypeId();
        lead.RecordTypeId = recordtypeId;
        
        action = STR_INSERT;  
        System.debug(lead);
        //edit mode
        if(null != lead.id)
        {
            action = STR_UPDATE;
            
        }
        else/*create mode*/
        {
            lead.OwnerId = System.UserInfo.getUserId();
        }
    }
    public PageReference save(){
        try
        {
            System.debug('String Action :: '+action);
            lead.Company = checkNull(lead.FirstName) + ' '+ checkNull(lead.LastName); 
        	if(action == STR_INSERT)
            {
        		insert lead;
        	}
            else
            {
            	update lead;
        	}    
            
        }  
        catch(DMLException e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getDMLMessage(0)));
            System.debug(e.getDmlMessage(0));
            return null;
        }catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            return null;
        }

        SaveAndNew = ApexPages.currentPage().getParameters().get('saveAndNew');
        //check if user click on save or save & new button       
        System.debug('Save And New ???? '+SaveAndNew);
        if(saveAndNew == 'saveAndNew'){
            PageReference newLead = new PageReference('/apex/RetailLeadCreation');
        	newLead.setRedirect(true);
			return newLead;
        }
        
		return new ApexPages.StandardController(lead).view();
    }
    
    public PageReference cancelLead(){
    	PageReference leadHome = new PageReference('/00Q/o');
    	if(null != lead.id) {
    		leadHome = new PageReference('/'+lead.id);
    	}
    	leadHome.setRedirect(true);
    	return leadHome;
    }
    
    public Boolean getisCreateMode(){
        return action == STR_INSERT?true:false;
    }
    
    public string checkNull(string input){
        if(null == input)
            return '';
        return input;
    }
}