public class CampaignTriggerHandler {

    public CampaignTriggerHandler()
    {
    
    }
    
    public static void checkDuplicateName(list<campaign> listNewCampaign, list<campaign> listOldCampaign)
    {
        boolean error = false;
        
        set<string> listCheckCampaignNameDuplicate = new set<string>();
        for (campaign c : listNewCampaign) // new list campaign name duplicate
        {
            if (listCheckCampaignNameDuplicate.contains(c.name))
                error = true;
            listCheckCampaignNameDuplicate.add(c.name);
        }
        
        set<string> listCampaignName = new set<string>();
        map<string,string> mapOldCampaignName = new map<string,string>();
        for(campaign c : listOldCampaign)
        {
            mapOldCampaignName.put(c.id,c.name);
        }
        for(campaign c : listNewCampaign)
        {
            if (c.name != mapOldCampaignName.get(c.id))
                listCampaignName.add(c.name);
        }

        integer found = [select count() from campaign where name in : listCampaignName];
        if (found > 0) error = true;
      
        if (error)
            listNewCampaign.get(0).name.addError(status_code__c.getValues('8002').status_message__c);
    }
    
    

}