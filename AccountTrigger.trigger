trigger AccountTrigger on Account (before insert, after insert,before update, after update, after delete, after undelete) {    
    //////////////////////////////////////
    // Create By : Thanakorn Haewphet
    // Email : tnh@ii.co.th
    // Create Date : 2014-10-20 
    // Summary : 
    // NOte : https://www.salesforce.com/us/developer/docs/apexcode/Content/apex_triggers_context_variables_considerations.htm
    //        ***  All Trigger.New   You can use an object to change its own field values using trigger.new, 
    //        but only in before triggers. In all after triggers, trigger.new is not saved, so a runtime exception is thrown.
    //        
    //  Change 01: By Ktc               
    //  Change Date: 23-10-2557
    //  Change Description : Move Update Account Process to Before trigger !!!
    //      
    //////////////////////////////////////
    System.debug('::: AccountTrigger Start ::::');
    // Use App Configuration to control =>  go to  App Setup > Develop > Custom Settings > AppConfig > Manage > runtrigger and then change the value
    Boolean RunTrigger = AppConfig__c.getValues('runtrigger').Value__c == 'true' ; 
    System.debug('RUNTRIGGER : '+RunTrigger +' : '+Test.isRunningTest());
    //Boolean RunTrigger = false;
    // ********   BEFORE INSERT TRIGGER RUN HERE   ********* 
    if(Trigger.isBefore && Trigger.isInsert) 
    {        
        if(RunTrigger || Test.isRunningTest() ){
            AccountTriggerHandler.handleBeforeInsert(Trigger.new);
        }
    }     
    // ********   BEFORE UPDATE TRIGGER RUN HERE   ********* 
    else if(Trigger.isBefore && Trigger.isUpdate) 
    {        
        if(RunTrigger || Test.isRunningTest()){
            AccountTriggerHandler.handleBeforeUpdate(Trigger.new,Trigger.old);
        } 
    } 
    // ********   BEFORE DELETE TRIGGER RUN HERE   ********* 
 /*   else if(Trigger.isBefore && Trigger.isDelete) 
    {        
        
    } 
    // ********   AFTER INSERT TRIGGER RUN HERE   ********* 
    else if(Trigger.isAfter && Trigger.isInsert) 
    {
        
    } */
    // ********   BEFORE UPDATE TRIGGER RUN HERE   ********* 
    else if(Trigger.isAfter && Trigger.isUpdate) 
    {        
        if(RunTrigger || Test.isRunningTest()){
            AccountTriggerHandler.handleAfterUpdate(Trigger.new,Trigger.old);
        } 
    }
    // ********   AFTER DELETE TRIGGER RUN HERE   *********  
 /*   else if(Trigger.isAfter && Trigger.isDelete) 
    {        
        
    } */
    System.debug('::: AccountTrigger End ::::');
}