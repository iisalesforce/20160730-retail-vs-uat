/*****************************************************************************************
*  NOTE : GroupWalletRefreshEntranceBatch 
*         สำหรับ Refresh เคสที่ยังไม่มี Account Plan
*
******************************************************************************************/

global class GroupWalletRefreshEntranceBatch implements Database.Batchable<sObject> ,Database.AllowsCallouts{
    
    // Account Plan Year
    public string m_year {get;set;}
    //  Support
    public Set<Id> m_accountWithAccountPlan    {get;set;}
    public Set<Id> m_accountWithoutAccountPlan {get;set;} 
    
    public id m_groupId {get;set;} 
    public id m_groupProfileId {get;set;}
    
    global GroupWalletRefreshEntranceBatch (Set<Id> accountWithAccountPlan,Set<Id> accountWithoutAccountPlan,Id groupId,string year,id groupProfileId){
        
        m_accountWithAccountPlan    =  accountWithAccountPlan ;
        m_accountWithoutAccountPlan =  accountWithoutAccountPlan;
        m_year = year;
        m_groupId = groupId;
        m_groupProfileId = groupProfileId;
        
    }
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){  
        
        //Get Account to Process
        Set<Id> accountIds = m_accountWithoutAccountPlan ;
        string year = m_year ;
        return Database.getQueryLocator([
            SELECT Id, Name ,Account_Plan_Flag__c,  Group__c,Group__r.Name ,Owner.Segment__c   
            FROM Account 
            WHERE    Id IN : accountIds 
        ]);
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        Set<id> accountIds = (new Map<Id,SObject>(scope)).keySet();
        // Logic to be Executed batch wise      
        AccountPlanRefreshService.RefreshProductStrategyPort(accountIds,this.m_year);
    }
     
    global void finish(Database.BatchableContext BC){
        
       /* if(m_accountWithAccountPlan.size() > 0){
        	 GroupWalletRefreshSecondBatch batch = 
        	 new GroupWalletRefreshSecondBatch( m_accountWithAccountPlan, m_accountWithoutAccountPlan,m_groupId, m_year,m_groupProfileId);  
             Database.executeBatch(batch ,25);
        }
        else{   */

		/*
	* Here's how you can query your current Salesforce.com instance from Apex code:
	*
	* String s = System.URL.getSalesforceBaseUrl().getHost();   OR System.URL.getSalesforceBaseURL().toExternalForm()
	* system.debug(s);
	*
	* Example - email link with absolute URL from Apex Code:
	*
	* mail.setHtmlBody('Your case:<b> ' + case.Id +' </b>has been created<p>'+
	* ' View case <a href=https://' + System.URL.getSalesforceBaseUrl().getHost() + '/'+case.Id+'>click here</a>');	
	*/
	
    		 GroupWalletRefreshThirdBatch batch =     
    		 new GroupWalletRefreshThirdBatch( m_accountWithAccountPlan, m_accountWithoutAccountPlan,m_groupId, m_year,m_groupProfileId); 
             Database.executeBatch(batch ,25);
			 string mainUrl =  'https://' +  System.URL.getSalesforceBaseUrl().getHost() + '/'+'apex/AccountPlanGroupWallet?walletID=&CompanyID=&GroupID='+m_groupProfileId;

			 	 string htmlMsg =  'Refresh Group Wallet batch processing is completed'
			     			  +'<br />Please click below url to view group wallet '
							  +'<br />View <a href="'+mainUrl+'"> click here</a>';
			 

    		 BatchEmailService.SendEmail(BC.getJobId(),'Batch Processing','Refresh Group Wallet batch processing is completed',htmlMsg);

		//}
        
    }
}