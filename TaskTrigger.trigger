trigger TaskTrigger on Task (before insert, after insert,before update, after update, after delete, after undelete) {    
    //////////////////////////////////////
    // Create By : Thanakorn Haewphet
    // Email : tnh@ii.co.th
    // Create Date : 2014-10-20 
    // Summary : -
    //////////////////////////////////////
    System.debug('::: TaskTrigger Start ::::');
    Boolean RunTrigger = AppConfig__c.getValues('runtrigger').Value__c == 'true' ; 
    //Boolean RunTrigger = false;
    // ********   BEFORE INSERT TRIGGER RUN HERE   ********* 
    if(Trigger.isBefore && Trigger.isInsert) 
    {        
        
    }     
    // ********   BEFORE UPDATE TRIGGER RUN HERE   ********* 
    else if(Trigger.isBefore && Trigger.isUpdate) 
    {        
    } 
    // ********   BEFORE DELETE TRIGGER RUN HERE   ********* 
    else if(Trigger.isBefore && Trigger.isDelete) 
    {        
        
    } 
    // ********   AFTER INSERT TRIGGER RUN HERE   ********* 
    else if(Trigger.isAfter && Trigger.isInsert) 
    {
        if(RunTrigger || Test.isRunningTest()){
            TaskTriggerHandler.handlerAfterInsert(Trigger.new);
        }
    } 
    // ********   BEFORE UPDATE TRIGGER RUN HERE   ********* 
/*    else if(Trigger.isAfter && Trigger.isUpdate) 
    {        
     
    } */
    // ********   AFTER DELETE TRIGGER RUN HERE   *********  
/*    else if(Trigger.isAfter && Trigger.isDelete) 
    {        
        
    } */
    System.debug('::: TaskTrigger End ::::');
}