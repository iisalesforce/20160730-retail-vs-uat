global without sharing  class CompanyProfileEmbedEx {
	public ApexPages.StandardController ctrl;
    public Account acct {get;set;}
    public String ownerId {get;set;}
    public List <AcctPlanCompanyProfile__c> companyList {get;set;} 
    public CompanyProfileEmbedEx(ApexPages.StandardController controller){
          ctrl = controller;
     	  acct= (Account)controller.getRecord();
        
        companyList = [SELECT ID,AccountName__c,Account__c,AcctPlanGroup__c,Name,AcctPlanGroup__r.Name,LastModifiedDate,
                       Year__c,Status__c,OwnerID,Owner.Name,LastModifiedByID,LastModifiedBy.name  FROM AcctPlanCompanyProfile__c
                      WHERE Account__c =:acct.id];
    }
    
}