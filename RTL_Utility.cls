public with sharing class RTL_Utility {
	/*
	*This method is used to retrieve a list of record type Ids based on the SObject and record type Developer Name prefix
	*/
	public static Set<Id> getObjectRecordTypeIdsByDevNamePrefix(SObjectType sObjectType, String recordTypeDevNamePrefix) {
		Set<Id> recordTypeIds = new Set<Id>();
		
        //Generate a map of tokens for all the Record Types for the desired object
        Map<String,Schema.RecordTypeInfo> rtMapByName = sObjectType.getDescribe().getRecordTypeInfosByName();

		for (String rtName : rtMapByName.keySet()){
			if (rtName.startsWith(recordTypeDevNamePrefix)) {
				recordTypeIds.add(rtMapByName.get(rtName).getRecordTypeId());
			}
		}

        //Retrieve the record type id by name
        return recordTypeIds;
	} 
}