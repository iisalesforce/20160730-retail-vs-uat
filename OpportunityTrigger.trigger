trigger OpportunityTrigger on Opportunity (before insert, after insert,before update, after update, after delete, after undelete) {
    ///////////////////////////////////////////
    // Created   :  2014-09-05 
    // Create By :  Keattisak Chinburarat
    // Modified  :  2014-11-04
    // Last Modifed : Thanakorn Haewphet
    ///////////////////////////////////////////


    /***---------------        Check opportunity is not retail        ---------------***/   
    list<Opportunity> listNewOpp = new list<Opportunity>();
    list<Opportunity> listOldOpp = new list<Opportunity>();
    set<string> setRecordTypeId = new set<string>();
    for (recordtype r : [select id from recordtype where SobjectType = 'Opportunity' AND (name like '%Credit%' or name like '%Non-credit%') ]){
        setRecordTypeId.add(r.id);
    }
    if (Trigger.new != null){
        for (Opportunity o : Trigger.new){
            if (setRecordTypeId.contains(o.recordtypeId))
                listNewOpp.add(o);
        }
    }
    if (Trigger.old != null){
        for (Opportunity o : Trigger.old){
            if (setRecordTypeId.contains(o.recordtypeId))
                listOldOpp.add(o);
        }
    }
    /***---------------        Check opportunity is not retail        ---------------***/


    
    Date currentDate = Date.today();
    Integer currentyear = currentDate.year();
    Integer currentmonth = currentDate.month();
    string strCurrentyear = string.valueOf(currentyear).right(2);
    
    System.debug(':::: OpportunityTrigger Start'); 
    Boolean RunTrigger = AppConfig__c.getValues('runtrigger').Value__c == 'true' ; 
    //Boolean RunTrigger = false;
    
    //-------------   BEFORE INSERT TRIGGER RUN HERE    -------------
    if(Trigger.isBefore && Trigger.isInsert) 
    {                 
        /*Opportunity[] Opps = Trigger.new;
         //  List<Id> userIds = new  List<Id>();   
        Decimal tgcount = Opps.size();        
        RunningUtility util  = new RunningUtility('Opportunity',currentyear,currentmonth,'');
        Running__c running= (Running__c)util.GetCurrentNumber();            
        Decimal start = running.No__c;            
        running.No__c = running.No__c+ 1 ;
        System.debug(':::: Running__c.No__c => ' + running.No__c); */
        if( RunTrigger || Test.isRunningTest() ){
            OpportunityTriggerHandler.handleBeforeInsert(listNewOpp);
        }
        
    }
    
    // -------------   BEFORE UPDATE TRIGGER RUN HERE   ------------- 
    else if(Trigger.isBefore && Trigger.isUpdate) 
    {  
        if( RunTrigger || Test.isRunningTest() ){
            OpportunityTriggerHandler.handleBeforeUpdate(listNewOpp,listOldOpp);
        }        
    }
    // -------------   BEFORE DELETE TRIGGER RUN HERE   ------------- 
/*    else if(Trigger.isBefore && Trigger.isDelete) 
    {
    } 
*/
    // -------------   AFTER INSERT TRIGGER RUN HERE    ------------- 
/*
    else if(Trigger.isAfter && Trigger.isInsert) 
    {
  
    }  
*/
    // -------------   BEFORE AFTER TRIGGER RUN HERE    ------------- 
    else if(Trigger.isAfter && Trigger.isUpdate) 
    {
        if(RunTrigger || Test.isRunningTest()){
            OpportunityTriggerHandler.handleAfterUpdate(listNewOpp,listOldOpp);
        }
        
    }    
    // -------------   AFTER DELETE TRIGGER RUN HERE    -------------  
  /*  else if(Trigger.isAfter && Trigger.isDelete) 
    {       
    } */
    
    System.debug(':::: OpportunityTrigger End'); 
}