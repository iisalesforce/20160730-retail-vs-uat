public class AccountConsoleExtension {

    public Account acct {get;set;}
    public double NiicForPie {get;set;}
    public double NiidForPie {get;set;}
    public double FeeForPie {get;set;}
    public double NiicActForPie {get;set;}
    public double NiidActForPie {get;set;}
    public double FeeActForPie {get;set;}
    public decimal totalRevenue {get;set;}
    public decimal grossProfit {get;set;}
    public decimal netProfit {get;set;}
    public decimal niPlan {get;set;}
    public decimal niicPlan {get;set;}
    public decimal niidPlan {get;set;}
    public decimal feePlan {get;set;}
    public decimal niWallet {get;set;}
    public decimal niicWallet {get;set;}
    public decimal niidWallet {get;set;}
    public decimal feeWallet {get;set;}
    public decimal niAct {get;set;}
    public decimal niicAct {get;set;}
    public decimal niidAct {get;set;}
    public decimal feeAct {get;set;}
    public decimal niSow {get;set;}
    public decimal niicSow {get;set;}
    public decimal niidSow {get;set;}
    public decimal feeSow {get;set;}
    public decimal supplyActual {get;set;} 
    //public integer openTask {get;set;}
   //public integer openVisitplan {get;set;}
    public date activityDate {get;set;}
    //public date visitDate {get;set;}
    public integer SumTransBank {get;set;}
    public integer SumDepInvest {get;set;}
    public integer SumFundBorrow {get;set;}
    public integer SumRiskProtect {get;set;}
    public double TotalTransBank {get;set;}
    public double TotalDepInvest {get;set;}
    public double TotalFundBorrow {get;set;}
    public double TotalRiskProtect {get;set;}
    public double TotalLimitTransBank {get;set;}
    public double TotalLimitRiskProtect {get;set;}
    public integer SumActTransBank {get;set;}
    public integer SumActDepInvest {get;set;}
    public integer SumActFundBorrow {get;set;}
    public integer SumActRiskProtect {get;set;}
    public id TransBankReportId {get;set;}
    public id DepInvestReportId {get;set;}
    public id FundBorrowReportId {get;set;}
    public id RiskProtectReportId {get;set;}
    public id CreditPipeReportId {get;set;}
    public id NonCreditPipeReportId {get;set;}
    public integer SumOpenTask {get;set;}
    public integer SumOpenVisit {get;set;}
    public integer SumOpenOpt {get;set;}
    public date LastVisitDate {get;set;}
    public String AsOfDate {get;set;}
    //public date LastActivityDate {get;set;}
    
    public AccountConsoleExtension(ApexPages.StandardController std){
    
        acct = (Account)std.getRecord();
      
        //getReportId();
        //getOnhand();
        //ActivitySummary();
    }
    
        public void getWallet()
    {
        try{
        Account tmpAcct = [select Loan_Classification__c, SCF_Score__c
                           ,NI_Plan__c,NIIc_Plan__c,NIId_Plan__c,Fee_Plan__c
                           , NI_Wallet__c, NIIc_Wallet__c, NIId_Wallet__c, Fee_Wallet__c
                           ,NI_Actual__c, NIIc_Actual__c, NIId_Actual__c, Fee_Actual__c
                           , NI_SoW__c, NIIc_SoW__c, NIId_SoW__c, Fee_SoW__c
                           , Total_Revenue_Baht__c, Gross_Profit__c, Net_Profit_Baht__c
                           , Last_Activity_Date__c,Supply_Chain_Actual__c,As_of_date_Wallet__c
                 /*          , Open_Tasks_tmp__c, Open_Visit_Plan_tmp__c
                           , Last_Visit_Date__c */
                           from Account where id =: acct.id]; 
        acct.Loan_Classification__c = tmpAcct.Loan_Classification__c;
        acct.SCF_Score__c = tmpAcct.SCF_Score__c;
        //acct.Service_class__c = tmpAcct.Service_class__c;
        
        NiicForPie = double.valueof(tmpAcct.NIIc_Wallet__c);
        NiidForPie = double.valueof(tmpAcct.NIId_Wallet__c);
        FeeForPie = double.valueof(tmpAcct.Fee_Wallet__c);
            
        NiicActForPie = double.valueof(tmpAcct.NIIc_Actual__c);
        NiidActForPie = double.valueof(tmpAcct.NIId_Actual__c);
        FeeActForPie = double.valueof(tmpAcct.Fee_Actual__c);   

        totalRevenue = (tmpAcct.Total_Revenue_Baht__c != null) ? tmpAcct.Total_Revenue_Baht__c : 0.00;
        grossProfit = (tmpAcct.Gross_Profit__c != null) ? tmpAcct.Gross_Profit__c : 0.00;
        netProfit = (tmpAcct.Net_Profit_Baht__c != null) ? tmpAcct.Net_Profit_Baht__c : 0.00;
        
        niPlan = (tmpAcct.NI_Plan__c!= null) ? tmpAcct.NI_Plan__c: 0.00;
        niicPlan = (tmpAcct.NIIc_Plan__c!= null) ? tmpAcct.NIIc_Plan__c: 0.00;
        niidPlan = (tmpAcct.NIId_Plan__c!= null) ? tmpAcct.NIId_Plan__c: 0.00;
        feePlan = (tmpAcct.Fee_Plan__c!= null) ? tmpAcct.Fee_Plan__c: 0.00;
        
        niWallet = (tmpAcct.NI_Wallet__c != null) ? tmpAcct.NI_Wallet__c : 0.00;
        niicWallet = (tmpAcct.NIIc_Wallet__c != null) ? tmpAcct.NIIc_Wallet__c : 0.00;
        niidWallet = (tmpAcct.NIId_Wallet__c != null) ? tmpAcct.NIId_Wallet__c : 0.00;
        feeWallet = (tmpAcct.Fee_Wallet__c != null) ? tmpAcct.Fee_Wallet__c : 0.00;
 
        niAct = (tmpAcct.NI_Actual__c != null) ? tmpAcct.NI_Actual__c : 0.00;
        niicAct = (tmpAcct.NIIc_Actual__c != null) ? tmpAcct.NIIc_Actual__c : 0.00;
        niidAct = (tmpAcct.NIId_Actual__c != null) ? tmpAcct.NIId_Actual__c : 0.00;
        feeAct = (tmpAcct.Fee_Actual__c != null) ? tmpAcct.Fee_Actual__c : 0.00;
        
        niSow= (tmpAcct.NI_SoW__c != null) ? tmpAcct.NI_SoW__c : 0.00;
        niicSow = (tmpAcct.NIIc_SoW__c != null) ? tmpAcct.NIIc_SoW__c : 0.00;
        niidSow = (tmpAcct.NIId_SoW__c != null) ? tmpAcct.NIId_SoW__c : 0.00;
        feeSow = (tmpAcct.Fee_SoW__c != null) ? tmpAcct.Fee_SoW__c : 0.00;
        
        supplyActual = (tmpAcct.Supply_Chain_Actual__c != null) ? tmpAcct.Supply_Chain_Actual__c : 0.00;   
        AsOfDate = tmpAcct.As_of_date_Wallet__c ;      
        //openTask = integer.valueof(tmpAcct.Open_Tasks_tmp__c);
        //openVisitplan = integer.valueof(tmpAcct.Open_Visit_Plan_tmp__c);
        
        //activityDate = tmpAcct.Last_Activity_Date__c;
        //visitDate = tmpAcct.Last_Visit_Date__c;   
        //system.debug(acct);

        }catch(Exception Ex){
                system.debug('::::::Exception getWallet::::::');
        }
        
    }
       
    /*
    public String getLoanClassification(){
        if (acct.Loan_Classification__c != null)
            return acct.Loan_Classification__c;
        else return 'NPL';

    }
    
    public String getSCFScore(){
    
        Integer returnData = 0;
        if (acct.SCF_Score__c != null){
            if (acct.SCF_Score__c.isNumeric()){
            
                if (integer.valueof(acct.SCF_Score__c) > 0 && integer.valueof(acct.SCF_Score__c) <= 100)
                    returnData = (integer.valueof(acct.SCF_Score__c) * 2) - 205;
            
            }        
        } 
        
        return string.valueof(returnData)+'px';
    
    } */
    
    /*public string getServiceClass(){
        
        String returnData = 'bronze';
        if (acct.Service_class__c != null){
            if (acct.Service_class__c.toLowerCase() == 'silver' || 
                acct.Service_class__c.toLowerCase() == 'gold' || 
                acct.Service_class__c.toLowerCase() == 'platinum')
            returnData = acct.Service_class__c.toLowerCase();
        }
        
        return returnData;
        
    }*/
    
    public List<PieWedgeData> getPieData() {
        List<PieWedgeData> data = new List<PieWedgeData>();
        
        if (NiicForPie != null) data.add(new PieWedgeData('NIIc', NiicForPie,NiicActForPie));
        if (NiidForPie != null) data.add(new PieWedgeData('NIId', NiidForPie, NiidActForPie));
        if (FeeForPie != null) data.add(new PieWedgeData('Fee', FeeForPie, FeeActForPie));
        system.debug('NiicForPie '+NiicForPie);
        system.debug('NiidForPie '+NiidForPie);
        system.debug('FeeForPie '+FeeForPie);
        
        return data;
    }

    public class PieWedgeData {

        public String name { get; set; }
        public double Wallet { get; set; }
        public double Actual { get; set; }

        public PieWedgeData(String name, double Wallet,double Actual) {
            this.name = name;
            this.Wallet = Wallet;
            this.Actual = Actual;
        }
    }
    
         public void ActivitySummary()
    {    
         if(SumOpenTask==null)SumOpenTask=0;
         if(SumOpenVisit==null)SumOpenVisit=0;
         //if(SumOpenOpt==null)SumOpenOpt=0;
        
        try{
        //Status picklist have 5 values (Not Started,In Progress,Completed,Waiting on someone else,Deferred)
        SumOpenTask = [select COUNT() from Task where AccountId = :acct.id  AND IsClosed =: false AND IsRecurrence =: false ]; 
        
        //SumOpenTask = [select COUNT() from Task where AccountId = :acct.id  AND Status != 'Completed' AND IsRecurrence =: false ];
        //acct.Open_Tasks_tmp__c = SumOpenTask;  
        //system.debug('SumOpenTask '+SumOpenTask);

        //Status picklist have 3 values (1 - Open,2 - Completed,3 - Cancel) 
        SumOpenVisit = [select COUNT() from Call_Report__c where Customer_name__c = :acct.id  AND Status__c = '1 - Open'];
        //acct.Open_Visit_Plan_tmp__c = SumOpenVisit;
     //system.debug('SumOpenVisit '+SumOpenVisit);
        List<Call_Report__c> ActualVisitDate = [select Actual_Visit_Date__c from Call_Report__c where Customer_name__c = :acct.id  AND Status__c = '2 - Completed'];
            for (Call_Report__c v1 : ActualVisitDate){

            if (LastVisitDate==null){
            LastVisitDate = Date.valueOf(v1.Actual_Visit_Date__c);
            }
            if(LastVisitDate<Date.valueOf(v1.Actual_Visit_Date__c)){
                LastVisitDate = Date.valueOf(v1.Actual_Visit_Date__c);
            }
           
        }
            
            Account tmpAcct = [select Last_Activity_Date__c,No_of_Open_Opportunities__c from Account where id =: acct.id];
            activityDate = tmpAcct.Last_Activity_Date__c;
            SumOpenOpt = Integer.valueOf(tmpAcct.No_of_Open_Opportunities__c) ;
                //acct.Last_Visit_Date__c = LastVisitDate;
            //system.debug('LastVisitDate '+LastVisitDate); 
/*     
        List<Task> ActivityDate =  [select ActivityDate from Task where AccountId = :acct.id  AND Status != 'Completed'];
        for (Task t1 : ActivityDate){

            if (LastActivityDate==null){
            LastActivityDate = Date.valueOf(t1.ActivityDate);
            }
            if(LastActivityDate<Date.valueOf(t1.ActivityDate)){
                LastActivityDate = Date.valueOf(t1.ActivityDate);
            }
           
        }
            acct.LastActivityDate = LastActivityDate;
     */
            //update  acct ;
        }catch(Exception Ex){ system.debug('::::::Exception ActivitySummary::::::');}
    }
    
     public void getReportId()
    {
        system.debug('getReportId ');
        
  /*      try{
        CustSingleView__c CustSingleView1 = CustSingleView__c.GetValues('CreditPipeline');
        String CreditPipeline = CustSingleView1.ReportName__c;
        CreditPipeReportId = [SELECT id FROM report where name = : CreditPipeline ].Id;
        //system.debug('CreditPipeline '+CreditPipeline);
        }catch(Exception Ex){
            system.debug('::::::Exception GetCreditPipelineId::::::');
        }
        
        try{
        CustSingleView__c CustSingleView2 = CustSingleView__c.GetValues('NonCreditPipeline');
        String NonCreditPipeline = CustSingleView2.ReportName__c;
        NonCreditPipeReportId = [SELECT id FROM report where name = : NonCreditPipeline ].Id;
        //system.debug('NonCreditPipeline '+NonCreditPipeline);
        }catch(Exception Ex){
            system.debug('::::::Exception GetNonCreditPipelineId::::::');
        }  */
        try{
        CustSingleView__c CustSingleView3 = CustSingleView__c.GetValues('TransactionBank');
        String TransactionBank = CustSingleView3.ReportName__c;
        TransBankReportId = [SELECT id FROM report where name = : TransactionBank ].Id;
        //system.debug('TransactionBank '+TransactionBank);
          }catch(Exception Ex){
            system.debug('::::::Exception Domain1Id::::::');
        }
        
        try{
        CustSingleView__c CustSingleView4 = CustSingleView__c.GetValues('DepositInvest');
        String DepositInvest = CustSingleView4.ReportName__c;
        DepInvestReportId = [SELECT id FROM report where name = : DepositInvest ].Id;
        //system.debug('DepositInvest '+DepositInvest);
          }catch(Exception Ex){
           system.debug('::::::Exception Domain2Id::::::');
        }
        
        try{
        CustSingleView__c CustSingleView5 = CustSingleView__c.GetValues('FundBorrow');
        String FundBorrow = CustSingleView5.ReportName__c;
        FundBorrowReportId = [SELECT id FROM report where name = : FundBorrow ].Id;
        //system.debug('FundBorrow '+FundBorrow);
        //system.debug('FundBorrowReportId '+FundBorrowReportId);
          }catch(Exception Ex){
            system.debug('::::::Exception Domain3Id::::::');
        }
        
        try{
        CustSingleView__c CustSingleView6 = CustSingleView__c.GetValues('RiskProtect');
        String RiskProtect = CustSingleView6.ReportName__c;
        RiskProtectReportId = [SELECT id FROM report where name = : RiskProtect ].Id;
        }catch(Exception Ex){
           system.debug('::::::Exception Domain4Id::::::');
        }
        
    }

         public void getPipelineReportId()
    {
        try{
        CustSingleView__c CustSingleView1 = CustSingleView__c.GetValues('CreditPipeline');
        String CreditPipeline = CustSingleView1.ReportName__c;
        CreditPipeReportId = [SELECT id FROM report where name = : CreditPipeline ].Id;
        //system.debug('CreditPipeline: '+CreditPipeline);
        //system.debug('CreditPipeReportId: '+CreditPipeReportId);
        }catch(Exception Ex){
             system.debug('::::::Exception GetCreditPipelineId2::::::');
           
        }
        
    }
 
             public void getNonPipelineReportId()
    {
        try{
        
        CustSingleView__c CustSingleView2 = CustSingleView__c.GetValues('NonCreditPipeline');
        String NonCreditPipeline = CustSingleView2.ReportName__c;
        NonCreditPipeReportId = [SELECT id FROM report where name = : NonCreditPipeline ].Id;       

        }catch(Exception Ex){
             system.debug('::::::Exception GetNonCreditPipelineId2::::::');
            
        }
        
    }
  
    
    public void getOnhand()
    {
        
            SumTransBank=0;
            SumDepInvest=0;
            SumFundBorrow=0;
            SumRiskProtect=0;
            TotalTransBank=0;
            TotalDepInvest=0;
            TotalFundBorrow=0;
            TotalRiskProtect=0;
            TotalLimitTransBank=0;
            TotalLimitRiskProtect=0;
            SumActTransBank=0;
            SumActDepInvest=0;
            SumActFundBorrow=0;
            SumActRiskProtect=0;
            System.debug('Product_Information_On_Hand');
            
            //1.Transactional Banking   
            //List<AggregateResult> TransBank = [select COUNT(Id), SUM(Ending_out_Ending_Balance__c) from Product_Information_On_Hand__c where Account__c = :acct.id AND Product_Hierachy_Code__r.Product_Domain__c like '%Transactional%' group by Account__c];
            try{
            List<AggregateResult> TransBank = [select COUNT_DISTINCT(Product_Hierachy_Code__c ), SUM(Ending_out_Ending_Balance__c), SUM(Limit_Balance__c) from Product_Information_On_Hand__c where Account__c = :acct.id AND Product_Hierachy_Code__r.Product_Domain__c like '%Transactional%' group by Account__c];
            for (AggregateResult ar1 : TransBank){
                SumTransBank = Integer.valueOf(ar1.get('expr0'));
                TotalTransBank = double.valueof(ar1.get('expr1'));  
                TotalLimitTransBank = double.valueof(ar1.get('expr2'));  
            }
                }catch(Exception ex){
             system.debug('::::::Exception Ar1::::::');
            }
            //[Fah comment according to CR036] SumActTransBank = [select COUNT() from Product_Information_Detail__c where Product_Information__r.Account__c = :acct.id  AND Product_Information__r.Product_Hierachy_Code__r.Product_Domain__c like '%Transactional%'];
            //TotalTransBank = 1234567890123456.12;  
            
            //2.Deposit investment
            //List<AggregateResult> DepInvest = [select COUNT(Id), SUM(Ending_out_Ending_Balance__c) from Product_Information_On_Hand__c where Account__c = :acct.id AND Product_Hierachy_Code__r.Product_Domain__c like '%Deposit%' group by Account__c];
            try{
            List<AggregateResult> DepInvest = [select COUNT_DISTINCT(Product_Hierachy_Code__c ), SUM(Ending_out_Ending_Balance__c) from Product_Information_On_Hand__c where Account__c = :acct.id AND Product_Hierachy_Code__r.Product_Domain__c like '%Deposit%' group by Account__c];
            for (AggregateResult ar2 : DepInvest){
                SumDepInvest = Integer.valueOf(ar2.get('expr0'));
                TotalDepInvest= double.valueof(ar2.get('expr1'));  
            }
            }catch(Exception ex){
             system.debug('::::::Exception Ar2::::::');
            }
            // TotalDepInvest= 1234567890123456.12;  
            // SumActDepInvest = [select COUNT() from Product_Information_Detail__c where Product_Information__r.Account__c = :acct.id  AND Product_Information__r.Product_Hierachy_Code__r.Product_Domain__c like '%Deposit%'];
            // [Fah comment according to CR036] & Add the new query to count TMB Account ID  
            try{
            List<AggregateResult> DepInvest2 = [select COUNT_DISTINCT(TMB_Account_ID__c) ACCT_NUM from Product_Information_Detail__c where Product_Information__r.Account__c = :acct.id AND Product_Information__r.Product_Hierachy_Code__r.Product_Domain__c like '%Deposit%' group by Customer__c];
            SumActDepInvest = Integer.valueOf(DepInvest2[0].get('ACCT_NUM'));
            }catch(Exception ex){
             system.debug('::::::Exception DepInvest2::::::'+ex);
            }
            //3.Funding and Borrowing
            //List<AggregateResult> FundBorrow = [select COUNT(Id), SUM(Limit_Balance__c) from Product_Information_On_Hand__c where Account__c = :acct.id AND Product_Hierachy_Code__r.Product_Domain__c like '%Funding%' group by Account__c];
            try{
            List<AggregateResult> FundBorrow = [select COUNT_DISTINCT(Product_Hierachy_Code__c ), SUM(Limit_Balance__c) from Product_Information_On_Hand__c where Account__c = :acct.id AND Product_Hierachy_Code__r.Product_Domain__c like '%Funding%' group by Account__c];
            for (AggregateResult ar3 : FundBorrow){
                SumFundBorrow = Integer.valueOf(ar3.get('expr0'));
                TotalFundBorrow = double.valueof(ar3.get('expr1'));  
            }
            }catch(Exception ex){
             system.debug('::::::Exception Ar3::::::');
            }    
            // [Fah comment according to CR036] SumActFundBorrow = [select COUNT() from Product_Information_Detail__c where Product_Information__r.Account__c = :acct.id  AND Product_Information__r.Product_Hierachy_Code__r.Product_Domain__c like '%Funding%'];

            //4.Risk Protection
            //List<AggregateResult> RiskProtect = [select COUNT(Id), SUM(Ending_out_Ending_Balance__c) from Product_Information_On_Hand__c where Account__c = :acct.id AND Product_Hierachy_Code__r.Product_Domain__c like '%Risk%' group by Account__c];
            try{
                List<AggregateResult> RiskProtect = [select COUNT_DISTINCT(Product_Hierachy_Code__c ), SUM(Ending_out_Ending_Balance__c), SUM(Limit_Balance__c) from Product_Information_On_Hand__c where Account__c = :acct.id AND Product_Hierachy_Code__r.Product_Domain__c like '%Risk%' group by Account__c];
                for (AggregateResult ar4 : RiskProtect){
                    SumRiskProtect = Integer.valueOf(ar4.get('expr0'));
                    TotalRiskProtect = double.valueof(ar4.get('expr1'));  
                    TotalLimitRiskProtect = double.valueof(ar4.get('expr2'));  
                }
             }catch(Exception ex){
             system.debug('::::::Exception Ar4::::::');
            }   
            // [Fah comment according to CR036] SumActRiskProtect = [select COUNT() from Product_Information_Detail__c where Product_Information__r.Account__c = :acct.id  AND Product_Information__r.Product_Hierachy_Code__r.Product_Domain__c like '%Risk%'];

            getReportId();
  
       
    }
    

}