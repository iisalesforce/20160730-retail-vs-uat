public with sharing  class ContactUpdateExtension {
    //**********************************************************//
    // Modify log
    //**********************************************************//
    // Change No. : CH01
    // Change By : Uttaporn L.
    // Change Date : 2015.06.22
    // Change Detail : Edit When select Sub-District to --None-- Return Error
    // Case No. : 00002582
    //**********************************************************//  
    public Contact contact;
    public Boolean isUpdateSuccess {get;set;}
    public boolean isDisqualified {get;set;}
    public boolean isIDValid {get;set;}
    public boolean isNotCoreBank {get;set;}
    public static final String DML_EXCEPTION_CODE = '1000';
    public static final String CALLOUT_EXCEPTION_CODE = '1002';
    public static final String QUERY_EXCEPTION_CODE = '1001';   
    private Contact oldCont;
    public boolean IsCountryAsTH {get;set;} 
    public boolean IsOtherCountry {get;set;}
    public String selected {get;set;}
    public Province__C province;
    public District__c district;
    public Sub_District__c subdis;
    public Map<String,id> provinceMap {get;set;}
    public static Map<String,id> districtMap {get;set;}
    public Map<String,id> subdisMap {get;set;}
    public Map<String,id> postcodes {get;set;}
    public String selectedProvince {get;set;}
    public String selectedDistrict {get;set;}
    public String selectedSubDistrict {get;set;}
    public String selectedPostcode {get;set;}
    public List<Province__c> ProvinceList{get;set;}
    public List<District__c> DistrictList{get;set;}
    public List<Sub_District__c> subdisList {get;set;}
    public Set<String> postcodeSet {get;set;}
    public List<SelectOption> ProvinceOptionList {get;set;}
    public List<SelectOption> DistrictOptionList {get;set;}
    public List<SelectOption> SubDistrictOptionList {get;set;}
    public List<SelectOption> PostcodeOptionList {get;set;}
    public Map<id,String> ProvinceNameMap {get;set;}
    public Map<id,String> DistrictNameMap {get;set;}
    public Map<id,String> SubDistrictNameMap {get;set;}
    public string selectedCountry {get;set;}
    public Map<String,String> CountryMap {get;set;}
    public  List<SelectOption> CountriesOptionList {get;set;}
    public  List<Country__c> CountriesList {get;set;}    
    
    
    //constructor
    public ContactUpdateExtension(ApexPages.StandardController controller) {
        
        contact = (Contact)controller.getRecord();
        Contact tmpCont = [SELECT AccountId,Province_Temp__c,District_Temp__c,Sub_District_Temp__c,Zip_Code_Temp__C,isDisqualified__c FROM Contact WHERE ID = :contact.id LIMIT 1];
        contact.isDisqualified__c = tmpCont.isDisqualified__c;
        isDisqualified = contact.IsDisqualified__c;
        contact.Province_Temp__c = tmpCont.Province_Temp__c;
        contact.District_Temp__c = tmpCont.District_Temp__c;
        contact.Sub_District_Temp__c = tmpCont.Sub_District_Temp__c;
        contact.Zip_Code_Temp__c = tmpCont.Zip_Code_Temp__c;  
        contact.AccountID = tmpCont.AccountID;   
        oldCont = new Contact();
        ProvinceList = new List<Province__c>();
        DistrictList = new List<District__C>();
        subdisList = new List<Sub_District__c>();
        provinceMap = new Map<String, id>();
        districtMap = new Map<String,id>();
        subdisMap = new Map<String,id>();
        postcodes = new Map<String,Id>();
        postcodeSet = new Set<String>(); 
        ProvinceNameMap = new Map<id,String>();
        DistrictNameMap = new Map<id,String>();
        SubDistrictNameMap = new Map<id,String>(); 
        selectedProvince = contact.Province_Temp__c;
        selectedDistrict = contact.District_Temp__c;
        selectedSubDistrict = contact.Sub_District_Temp__c; 
        selectedPostCode = contact.Zip_code_Temp__c;
        ProvinceOptionList = new List<SelectOption>();
        ProvinceOptionList.add(new SelectOption('','--None--'));
        ProvinceList = new List<Province__c>( [SELECT Id,Name,Code__c FROM Province__C WHERE id != null ORDER BY Name]);
        for(Province__C pro : ProvinceList){
            ProvinceOptionList.add(new SelectOption(pro.id,pro.Name));
            provinceMap.put(pro.Name,pro.id);
            provinceNameMap.put(pro.id, pro.name);
        }
        DistrictOptionList = new List<SelectOption>();
        SubDistrictOptionList = new List<SelectOption>();
        PostcodeOptionList = new List<SelectOption>();    
        
        
        
        CountryMap = new Map<String,String>();
        CountriesOptionList = new List<SelectOption>();
        CountriesOptionList.add(new SelectOption('','--None--'));
        CountriesList = new List<Country__c>( [SELECT ID,Name,Code__c FROM Country__c WHERE Name != '' AND Code__c != null ORDER BY Name]);
        for(Country__c coun : CountriesList){
            CountriesOptionList.add(new SelectOption(coun.Code__c,coun.Name));
            CountryMap.put(coun.Code__c, coun.Name);
        }
        
        IsCountryAsTH =true;
        
    }
    
    //retrieve sensitive data from CRMDB
    public PageReference ContactServiceCallout(){
        // ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.CONFIRM,' record : '+acct.id));
        isNotCoreBank = true;
        try{
            Contact con = [SELECT ID,AccountID,isDisqualified__c,ID_Type_Temp__c,RecordTypeID FROM Contact WHERE ID =: contact.id];
            RecordType conRec =  [SELECT Name,id,SobjectType FROM RecordType WHERE id=:con.RecordTypeId AND SobjectType='Contact' LIMIT 1];
            System.debug('Contact : '+conRec);
            if(con.isDisqualified__c == true){
                //Account acc = [SELECT ID,Name FROM Account WHERE ID=:con.AccountID];
                PageReference pageRef = viewContact();
                //PageReference pageRef = new ApexPages.StandardController(acc).view();
                return pageRef;
            }else if(conRec.Name =='Core Bank'){
                isNotCoreBank =false;
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3015').Status_Message__c));
            return null;
                
            }else{
                TMBServiceProxy.TMBServiceProxySoap tmbService = new TMBServiceProxy.TMBServiceProxySoap();
                tmbService.timeout_x = 120000;       
                 // ktc add for rsa soap service call
                tmbService = SoapRsa.setSecureHeader(tmbService); 
                Type_of_ID__c Typevalue;
                if(contact.ID_Type_Temp__c !=null){
                    Typevalue = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name =:contact.ID_Type_Temp__c LIMIT 1];
                }else{
                    Typevalue = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name =:'--None--' LIMIT 1];
                }
                System.debug('CONID : '+con.id);
                System.debug('ACID : '+con.AccountId);
                System.debug('IDTYPE : '+typeValue.Value__c);
                TMBServiceProxy.QueryContactResultDTO queryContactResult =  tmbService.QueryContact(con.AccountId,con.id,Typevalue.Value__c,  UserInfo.getUserId(), UserInfo.getUserName());
                
                TMBServiceProxy.ArrayOfQueryContactData arrayOfContact = queryContactResult.Datas;
                TMBServiceProxy.QueryContactData [] querycontactdata = arrayOfContact.QueryContactData ;
                if(querycontactdata !=null){    
                    TMBServiceProxy.QueryContactData contactdata = querycontactdata[0];
                    System.debug('QUERY CONTACT: '+contactdata.ID_TYPE);
                    System.debug('QUERY CONTACT: '+contactdata.ID_NUMBER);
                    System.debug('QUERY CONTACT: '+contactdata.SF_CON_ID);
                    System.debug('QUERY CONTACT: '+contactdata.CRM_ID);
                    System.debug('QUERY CONTACT: '+contactdata.FNAME);
                    System.debug('QUERY CONTACT: '+contactdata.LNAME);
                    System.debug('QUERY CONTACT: '+contactdata.CITIZEN_ID);
                    System.debug('QUERY CONTACT: '+contactdata.PASSPORT_ID);
                    System.debug('QUERY CONTACT: '+contactdata.BRN_ID);
                    System.debug('QUERY CONTACT: '+contactdata.ALIEN_ID);
                    System.debug('QUERY CONTACT: '+contactdata.WORK_PERMIT_ID);
                    System.debug('QUERY CONTACT: '+contactdata.DATE_OF_BIRTH);
                    System.debug('QUERY CONTACT: '+contactdata.POSITION);
                    System.debug('QUERY CONTACT: '+contactdata.MOBILE_PH_NBR);
                    System.debug('QUERY CONTACT: '+contactdata.PRI_PH_NBR);
                    System.debug('QUERY CONTACT: '+contactdata.PRI_PH_EXT);
                    System.debug('QUERY CONTACT: '+contactdata.FAX_PH_NBR);
                    System.debug('QUERY CONTACT: '+contactdata.EMAIL_ADDRESS);
                    System.debug('QUERY CONTACT: '+contactdata.CUSTOMER_AGE);
                    System.debug('QUERY CONTACT: '+contactdata.DECISION_MAP_LEVEL);
                    System.debug('QUERY CONTACT: '+contactdata.PRODUCT_DICISION);
                    System.debug('QUERY CONTACT: '+contactdata.VALUE_STYLE);
                    System.debug('QUERY CONTACT: '+contactdata.DIFFICULTY_TO_DEAL);
                    System.debug('QUERY CONTACT: '+contactdata.NOTE);
                    System.debug('QUERY CONTACT: '+contactdata.GENDER);
                    System.debug('QUERY CONTACT: '+contactdata.FACEBOOK);
                    System.debug('QUERY CONTACT: '+contactdata.LINK_IN);
                    System.debug('QUERY CONTACT: '+contactdata.RM_OWNER);
                    System.debug('QUERY CONTACT: '+contactdata.TMB_CUSTOMER_FLAG);
                    System.debug('QUERY CONTACT: '+contactdata.AUTHO_FLAG);
                    System.debug('QUERY CONTACT: '+contactdata.BOD_FLAG);
                    System.debug('QUERY CONTACT: '+contactdata.DATE_CREATE);
                    System.debug('QUERY CONTACT: '+contactdata.DATE_MODIFY);
                    System.debug('QUERY CONTACT: '+contactdata.PRI_ADDR1);
                    System.debug('QUERY CONTACT street : '+contactdata.PRI_ADDR2);
                    System.debug('QUERY CONTACT soi : '+contactdata.PRI_ADDR3);
                    System.debug('QUERY CONTACT subdist : '+contactdata.PRI_ADDR4);
                    System.debug('QUERY CONTACT dis: '+contactdata.PRI_ADDR5);
                    System.debug('QUERY CONTACT province : '+contactdata.PRI_CITY);
                    System.debug('QUERY CONTACT zip code : '+contactdata.PRI_POSTAL_CD);
                    System.debug('QUERY CONTACT country: '+contactdata.PRI_COUNTRY);              
                    
                    
                    
                    Type_of_ID__c typeid;
                    if(contactdata.ID_TYPE!=null && contactdata.ID_TYPE != '--None--'&& contactdata.ID_TYPE !=''){
                        System.debug('Has ID TYPE : '+contactdata.ID_TYPE);
                        typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Value__c =:contactdata.ID_TYPE LIMIT 1];
                        contact.ID_TYPE_Temp__c = typeid.Name;
                    }
                    contact.First_Name_Temp__c = contactdata.FNAME;
                    contact.Last_Name_Temp__c = contactdata.LNAME;
                    contact.Mobile_No_Temp__c = contactdata.MOBILE_PH_NBR;
                    contact.Fax_No_Temp__c = contactdata.FAX_PH_NBR;
                    contact.Office_No_Temp__c = contactdata.PRI_PH_NBR;
                    contact.ID_number_Temp__c = contactdata.ID_NUMBER;
                    contact.Address_No_Temp__c = contactdata.PRI_ADDR1;
                    contact.Street_Temp__c = contactdata.PRI_ADDR2;
                    contact.Soi_Temp__c = contactdata.PRI_ADDR3;
                    contact.Sub_District_Temp__c = contactdata.PRI_ADDR4;
                    contact.District_Temp__c = contactdata.PRI_ADDR5;
                    contact.Country_Temp__c =  contactdata.PRI_COUNTRY;
                    contact.Zip_code_Temp__c = contactdata.PRI_POSTAL_CD;
                    System.debug('Age : '+contact.Customer_Age__c);
                    System.debug(contact.Date_of_Birth__c);
                    
                    DateTime d = Date_Of_Birth__c.getValues('Default').Value__c;
                    DateTime birthdate = contact.Date_of_Birth__c;
                    
                    System.debug(d);
                    //Change Default Birth Date equal null
                    if(d == birthdate){
                       contact.Date_of_Birth__c =null;
                    }
                    if(contactdata.PRI_COUNTRY != null && contactdata.PRI_COUNTRY != ''){
                        selectedCountry = contactdata.PRI_COUNTRY;
                    }
                    
                    
                    if(selectedCountry == 'TH')
                    {
                        
                        if(contactdata.PRI_CITY != null && contactdata.PRI_CITY != '')
                        {
                            
                            selectedProvince = provinceMap.get(contactdata.PRI_CITY);
                            Provinceselected();
                        }
                        if(contactdata.PRI_ADDR5 != null && contactdata.PRI_ADDR5 != '')
                        {
                            
                            selectedDistrict = districtMap.get(contactdata.PRI_ADDR5);
                            
                            Districtselected();
                        }
                        if(contactdata.PRI_ADDR4 != null && contactdata.PRI_ADDR4 != '')
                        {
                            selectedSubDistrict = subdisMap.get(contactdata.PRI_ADDR4);
                            SubDistrictSelected();
                            selectedPostcode = (contactdata.PRI_POSTAL_CD=='')?null:contactdata.PRI_POSTAL_CD;
                        }
                        IsCountryAsTH =true;    
                        IsOtherCountry = false;
                    }
                    else
                    {
                        IsCountryAsTH =false;    
                        IsOtherCountry = true;                  
                        
                    }
                    
                    
                    
                    
                    
                    
                    oldCont = contact.clone(false,true,false,false);
                    return null;
                }else{
                    ApexPages.addmessage(ErrorHandler.Messagehandler(CALLOUT_EXCEPTION_CODE, Status_Code__c.GetValues('1002').Status_Message__c));
                    System.debug(logginglevel.ERROR,'Callout Error: SOAP service doesnt return any data');
                    return null;    
                }  
                
                
                
            }
            
        }catch(CalloutException e){
            ApexPages.addmessage(ErrorHandler.Messagehandler(CALLOUT_EXCEPTION_CODE, ''));
            System.debug(logginglevel.ERROR,'Callout Error: '+e.getMessage());
            return null;
            
        }catch(QueryException e){
            ApexPages.addMessage(ErrorHandler.Messagehandler(QUERY_EXCEPTION_CODE, ''));
            System.debug(logginglevel.ERROR,'Query Exception: '+e.getMessage());
            return null;
        }
    }
    
    public PageReference save(){ 
        //contact.Country_Temp__c = selectedCountry;
        
        system.debug('selectedCountry in save action : '+ selectedCountry );
        
        System.debug(' IDTYPE : '+contact.ID_Type_Temp__c);
        System.debug('ID NUMBER : '+contact.ID_Number_Temp__c);
        if( contact.ID_Type_Temp__c=='--None--'){
            contact.ID_Type_Temp__c = null;
        }
        if(contact.First_Name_Temp__c==null||contact.First_Name_Temp__c==''){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3009').Status_Message__c));
            return null;
        }
        if(contact.ID_Type_Temp__c!=null &&contact.ID_Number_Temp__c==null){
            isUpdateSuccess = false;
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3004').Status_Message__c));
            return null;
        }
        if(!isIDValid&&contact.ID_Type_Temp__c=='Citizen ID'){
            isUpdateSuccess = false;
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3001').Status_Message__c));
            return null;
        }
        String custAge;
        if(contact.Date_Of_Birth__c !=null){
          custAge = calculateAge(contact.Date_of_Birth__c)+'';  
        }else{
            custAge = '0';
        }
        
        Date_Of_Birth__c dateOfBirth = Date_Of_Birth__c.getValues('Default');
        DateTime d = (contact.Date_of_Birth__c==null)?dateOfBirth.Value__c:contact.Date_Of_Birth__c;
        contact.Date_of_Birth__c = date.newinstance(d.year(), d.month(), d.day());
        String myDate =  d.format('yyyy-MM-dd') ;

        
        System.debug('contact: '+contact);
        
        Type_of_ID__c typeid;
        if(contact.ID_Type_Temp__c !=null){
            typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name =:contact.ID_Type_Temp__c LIMIT 1];
        }else{
            typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name ='--None--' LIMIT 1];
        }
        if(IsCountryAsTH)
        {
            if(selectedProvince == null || selectedProvince == '--None--'){
                contact.Province_Temp__c = '';
            }else{
                contact.Province_Temp__c = ProvinceNameMap.get(selectedProvince);
                
            }
            
            if(selectedDistrict == null || selectedDistrict == '--None--'){
                contact.District_Temp__c = '';
            }else{
                contact.District_Temp__c = DistrictNameMap.get(selectedDistrict);
                
            }
            
            if(selectedSubDistrict == null || selectedSubDistrict == '--None--'){
                contact.Sub_District_Temp__c = '';
            }else{
                contact.Sub_District_Temp__c = SubDistrictNameMap.get(selectedSubDistrict);
                
            }
            
            
            if(selectedPostCode == null || selectedPostCode == '--None--'){
                contact.Zip_code_Temp__c = '';
            }else{
                contact.Zip_code_Temp__c = selectedPostCode;           
            }
        }
        
        try{         
            if(isSensitiveUpdate()){       
                TMBServiceProxy.TMBServiceProxySoap tmbService = new TMBServiceProxy.TMBServiceProxySoap();
                tmbService.timeout_x = 120000;
                 // ktc add for rsa soap service call
                tmbService = SoapRsa.setSecureHeader(tmbService);
                System.debug('UPDATE CONTACT : '+contact.id);
                System.debug('UPDATE CONTACT AccountId: '+contact.AccountId                 );
                System.debug('UPDATE CONTACT First_Name_Temp__c: '+contact.First_Name_Temp__c       );
                System.debug('UPDATE CONTACT Last_Name_Temp__c: '+contact.Last_Name_Temp__c     );
                System.debug('UPDATE CONTACT TYPE: '+typeid.Value__c        );
                System.debug('UPDATE CONTACT ID_number_Temp__c: '+contact.ID_number_Temp__c);
                System.debug('UPDATE CONTACT : '+mydate);
                System.debug('UPDATE CONTACT Position__c: '+contact.Position__c);
                System.debug('UPDATE CONTACT Mobile_No_Temp__c: '+contact.Mobile_No_Temp__c);
                System.debug('UPDATE CONTACT Office_No_Temp__c: '+contact.Office_No_Temp__c);
                System.debug('UPDATE CONTACT Fax_No_Temp__c: '+contact.Fax_No_Temp__c);
                System.debug('UPDATE CONTACT Email_Temp__c: '+contact.Email_Temp__c);
                System.debug('UPDATE CONTACT : '+custAge);
                System.debug('UPDATE CONTACT Decision_Map__c: '+contact.Decision_Map__c);
                System.debug('UPDATE CONTACT Product_Decision__c: '+contact.Product_Decision__c);
                System.debug('UPDATE CONTACT Value_Style__c: '+contact.Value_Style__c);
                System.debug('UPDATE CONTACT Difficult_to_deal__c: '+contact.Difficult_to_deal__c);
                System.debug('UPDATE CONTACT Description__c: '+contact.Description__c);
                System.debug('UPDATE CONTACT Salutation: '+contact.Salutation);
                System.debug('UPDATE CONTACT Address_No_Temp__c: '+contact.Address_No_Temp__c);                                                                       
                System.debug('UPDATE CONTACT Sub_District_Temp__c: '+contact.Sub_District_Temp__c);                                                                         
                System.debug('UPDATE CONTACT District_Temp__c: '+contact.District_Temp__c);                                                                          
                System.debug('UPDATE CONTACT Province_Temp__c: '+contact.Province_Temp__c);                                                                                              
                System.debug('UPDATE CONTACT Zip_code_Temp__c: '+contact.Zip_code_Temp__c);                                 
                System.debug('UPDATE CONTACT Country_Temp__c: '+contact.Country_Temp__c);                                               
                System.debug('UPDATE CONTACT Authorized_person_of_signature__c : '+contact.Authorized_person_of_signature__c);
                TMBServiceProxy.ContactResultDTO UpdateResult = tmbService.UpdateContact(contact.id,
                                                                                         contact.AccountId,
                                                                                         (contact.First_Name_Temp__c==null)?'':contact.First_Name_Temp__c,
                                                                                         (contact.Last_Name_Temp__c==null)?'':contact.Last_Name_Temp__c,
                                                                                         typeid.Value__c,
                                                                                         (contact.ID_number_Temp__c==null)?'':contact.ID_number_Temp__c,
                                                                                         mydate,
                                                                                         (contact.Position__c==null)?'':contact.Position__c,
                                                                                         (contact.Mobile_No_Temp__c==null)?'':contact.Mobile_No_Temp__c,
                                                                                         (contact.Office_No_Temp__c==null)?'':contact.Office_No_Temp__c,
                                                                                         (contact.Fax_No_Temp__c==null)?'':contact.Fax_No_Temp__c,
                                                                                         (contact.Email_Temp__c==null)?'':contact.Email_Temp__c,
                                                                                         custAge,
                                                                                         (contact.Decision_Map__c==null)?'':contact.Decision_Map__c,
                                                                                         (contact.Product_Decision__c==null)?'':contact.Product_Decision__c,
                                                                                         (contact.Value_Style__c==null)?'':contact.Value_Style__c,
                                                                                         (contact.Difficult_to_deal__c==null)?'':contact.Difficult_to_deal__c,
                                                                                         (contact.Description__c==null)?'':contact.Description__c,                                                                                                 
                                                                                         (contact.Salutation==null)?'':contact.Salutation,
                                                                                         (contact.Address_No_Temp__c==null)?'':contact.Address_No_Temp__c,
                                                                                         (contact.Street_Temp__c==null)?'':contact.Street_Temp__c,
                                                                                         (contact.Soi_Temp__c==null)?'':contact.Soi_Temp__c,                                                                         
                                                                                         (contact.Sub_District_Temp__c==null)?'':contact.Sub_District_Temp__c,                                                                         
                                                                                         (contact.District_Temp__c==null)?'':contact.District_Temp__c,                                                                          
                                                                                         (contact.Province_Temp__c==null)?'':contact.Province_Temp__c,                                                                                              
                                                                                         (contact.Zip_code_Temp__c==null)?'':contact.Zip_code_Temp__c,                                                                           
                                                                                         (contact.Country_Temp__c==null)?'':contact.Country_Temp__c,
                                                                                         (contact.Authorized_person_of_signature__c==true)?'Y':'N',
                                                                                          UserInfo.getUserId(), UserInfo.getUserName()); 
                System.debug('Status :' +UpdateResult.status);
                 System.debug('Massage :' +UpdateResult.massage);
                
                
                if(UpdateResult.status == '0000'/* && UpdateResult.massage ==''*/){
                    clearTempField();       
                    update contact;
                    isUpdateSuccess = true; 
                    
                }else{
                    isUpdateSuccess = false;
                    ApexPages.addmessage(ErrorHandler.Messagehandler(UpdateResult.status,UpdateResult.totalrecord,UpdateResult.massage));           
                }
            }else{
                clearTempField();
                update contact;
                isUpdateSuccess = true;

            }
            return null;          
        }catch(CalloutException e){
            Disqualified_Reason__c reason = Disqualified_Reason__c.getValues('Webservice_Timeout');
            contact.isDisqualified__c = true;
            contact.Disqualified_Reason__c = reason.Value__c;    
            update contact;
            ApexPages.addmessage(ErrorHandler.Messagehandler(CALLOUT_EXCEPTION_CODE, ''));
            System.debug(logginglevel.ERROR,'Callout Error: '+e.getMessage());
            return null;
        }catch(DMLException e){
            if(contact.Date_of_Birth__c !=null){
                if(contact.Date_of_Birth__c > System.today()){
                 ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,Status_Code__c.GetValues('3019').Status_Message__c) );
            }
            }else{
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('1000').Status_Message__c));
            }
            return null;
        }
    }
    
    public Boolean isSensitiveUpdate(){
        if(oldCont.First_Name_Temp__c!=contact.First_Name_Temp__c
           ||oldCont.Last_Name_Temp__c!=contact.Last_Name_Temp__c
           ||oldCont.Mobile_No_Temp__c!=contact.Mobile_No_temp__c
           ||oldCont.Fax_No_Temp__c !=contact.Fax_No_Temp__c
           ||oldCont.Office_No_Temp__c!=contact.Office_No_Temp__c
           ||oldCont.ID_Type_Temp__c !=contact.ID_Type_Temp__c 
           ||oldCont.ID_Number_Temp__c  != contact.ID_Number_Temp__c 
           ||oldCont.Date_of_Birth__c != contact.Date_of_Birth__c
           ||oldCont.Position__c != contact.Position__c
           ||oldCont.Email_Temp__c != contact.Email_Temp__c
           ||oldCont.Customer_Age__c != contact.Customer_Age__c
           ||oldCont.Decision_Map__c != contact.Decision_Map__c
           ||oldCont.Product_Decision__c != contact.Product_Decision__c
           ||oldCont.Value_Style__c != contact.Value_Style__c
           ||oldCont.Difficult_to_deal__c != contact.Difficult_to_deal__c
           ||oldCont.Description__c != contact.Description__c
           ||oldCont.Country_Temp__c !=  contact.Country_Temp__c
           || Test.isRunningTest()
            // ========    Fix Contry selected  =======
            // SITDefectLogs20141212_revise with TMB.xlsx
            // No.12
            // Item. Edit Contact
            // Fix by : KTC
            // Email : ktc@ii.co.th
          )
            return true;
        
        return false;
        
        
    }
    
    public PageReference viewContact(){
        PageReference ContactPage;
        ContactPage = new ApexPages.StandardController(contact).view();
        ContactPage.setRedirect(true);          
        return ContactPage; 
    }
    
    public void clearTempField(){
        //contact.ID_Type_Temp__c = null;
        contact.ID_Number_Temp__c = null;
        contact.First_Name_Temp__c = null;
        contact.Last_Name_Temp__c = 'Contact Info';
        contact.Mobile_No_Temp__c = null;
        contact.Fax_No_Temp__c = null;
        contact.Office_No_Temp__c = null;
        DateTime defaultdate = Date_Of_Birth__c.getValues('Default').Value__c;
        DateTime birthdate = contact.Date_of_Birth__c;
                    //Change Default Birth Date equal null
                    if(defaultdate == birthdate){
                       contact.Date_of_Birth__c =null;
                    }
    }
    
   private String provinceholder;
    
        public void Provinceselected(){
            boolean provincechange =false;
             selectedSubDistrict =null;
            districtMap = New Map<String,id>();
            DistrictNameMap = New Map<id,String>();
        SubDistrictOptionList = new List<SelectOption>();
        selectedPostcode = null;
            System.debug('selectedDistrict : '+selectedDistrict+' : '+contact.District_Temp__c);
        contact.Province_Temp__c = null;
        PostcodeOptionList =  new List<SelectOption>();
            if(provinceholder ==null){
                provinceholder = selectedProvince;
                
            }else{
                if(selectedProvince !=null && selectedprovince !='' && provinceholder != selectedProvince){
                    provincechange = true;
                }
            }
          	
            System.debug('selectedProvince : '+selectedProvince);
            if((selectedProvince !=null && selectedProvince != '' && selectedProvince !='null')||provincechange){
               
              DistrictOptionList = new List<SelectOption>();
                contact.Province_Temp__c = [SELECT ID,NAME FROM Province__C WHERE ID =:selectedProvince LIMIT 1].Name;
            
             DistrictList = new List<District__c>([SELECT ID,Name,Code__c FROM District__c WHERE Province__c =:selectedProvince]);
             DistrictOptionList.add(new SelectOption('','--None--'));
               for(District__C dis : DistrictList){
                  DistrictOptionList.add(new SelectOption(dis.id,dis.Name));
                   districtMap.put(dis.name, dis.id);
                   DistrictNameMap.put(dis.id, dis.name);
                    }
                provinceholder = selectedprovince;
      }else{
          
        selectedDistrict = null;
        selectedSubDistrict = null;
        selectedpostcode = null;
        DistrictOptionList = new List<SelectOption>();
        contact.District_Temp__c =null;
        contact.Sub_District_Temp__c =null;  
        contact.Zip_Code_Temp__c = null;  
        postcodeSet = new Set<String>(); 
        SubDistrictOptionList = new List<SelectOption>();
         PostcodeOptionList = new List<SelectOption>();
      }  
           
      
    }
    
    public void DistrictSelected(){
       System.debug('selectedDistrict : '+selectedDistrict+' : '+contact.District_Temp__c);
    if(selectedDistrict !=null && selectedDistrict != '' && selectedDistrict !='null' ){
        SubDistrictOptionList = new List<SelectOption>();
        PostcodeOptionList = new List<SelectOption>();
        PostcodeOptionList.add(new SelectOption('','--None--'));
        selectedSubDistrict = null;
        postcodeSet = new Set<String>();
        subdisMap = new Map<String,ID>();
        SubDistrictNameMap = new Map<ID,String>();
    contact.District_Temp__c = [SELECT ID,NAME FROM District__c WHERE ID =:selectedDistrict LIMIT 1].Name;
    System.debug('DISTRICT : '+contact.District_Temp__c);
    subdisList = new List<Sub_District__c>([SELECT ID,Name,Code__c,Zip_code__c,Location_Code__c FROM Sub_District__c WHERE District__c =:selectedDistrict]);
    subDistrictOptionList.add(new SelectOption('','--None--'));
    for(Sub_District__c subdis : subdisList){
      SubDistrictOptionList.add(new SelectOption(subdis.id,subdis.Name));
        subdisMap.put(subdis.name,subdis.id);
        SubDistrictNameMap.put(subdis.id,subdis.name);
      postcodeSet.add(subdis.Zip_code__c);
    }  
         }else{
             selectedSubDistrict = null;
             selectedSubDistrict = null;
             selectedPostcode = null;
             contact.District_Temp__c = null;
             contact.Sub_District_Temp__c =null;  
        	 contact.Zip_Code_Temp__c = null;  
             SubDistrictOptionList = new List<SelectOption>();
             PostcodeOptionList = new List<SelectOption>();
             postcodeSet = new Set<String>(); 
           
         }
    }
    
     public void SubDistrictSelected(){
        
        if(selectedSubDistrict !=null && selectedSubDistrict != '' && selectedSubDistrict !='null' ){
    Sub_District__c sub = [SELECT ID,Name,Zip_code__c FROM Sub_District__c WHERE ID=: selectedSubDistrict LIMIT 1];
    contact.Sub_District_Temp__c = sub.Name;
    contact.Zip_Code_Temp__c = sub.Zip_code__c;
    selectedpostcode =null;
    PostcodeOptionList = new List<SelectOption>();
        PostcodeOptionList.add(new SelectOption('','--None--'));
    for(String Postcode : postcodeSet){
            
      PostcodeOptionList.add(new SelectOption(Postcode,Postcode)); 
            System.debug(Postcode);
    }
        }else{
           PostcodeOptionList = new List<SelectOption>();
           selectedpostcode =null;
             contact.Sub_District_Temp__c =null;
    		 contact.Zip_Code_Temp__c = null;
        }
    }
   
        public static Integer calculateAge(Date birthDate){
        Integer Age;

        Date day = Date.today();
        
        if(day >= birthDate)
        {
          Age =   day.year() - birthDate.year();
        }
        else{
            Age =   day.year() - (birthDate.year() - 1);
        }  
        return Age;
    }
    
    
    
    public void CheckCountry(){ 
        // ========    Fix Contry selected  =======
        // SITDefectLogs20141212_revise with TMB.xlsx
        // No.12
        // Item. Edit Contact
        // Fix by : KTC
        // Email : ktc@ii.co.th
        contact.Country_Temp__c = selectedCountry;
        System.debug('SELECT : '+selectedCountry);
        if(selectedCountry =='TH'){
            IsCountryAsTH =true;
            IsOtherCountry = false;
            
            
            
        }else{
            IsCountryAsTH =false;
            IsOtherCountry = true;
            contact.Province_Temp__c ='';
            contact.District_Temp__c = '';
            contact.Sub_District_Temp__c ='';
            Contact.Zip_code_Temp__c = '';
            contact.Street_Temp__c = '';
            contact.Soi_Temp__c ='';
        }
        
    }
    
    
}