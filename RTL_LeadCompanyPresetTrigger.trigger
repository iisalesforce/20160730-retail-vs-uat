trigger RTL_LeadCompanyPresetTrigger on Lead (before insert,before update) {
    List<RecordType> recIds = [SELECT Id FROM RecordType WHERE DeveloperName='Retail_Banking' limit 1];
    Id retailLead = recIds[0].Id;
    //Schema.Sobjecttype.Lead.getRecordTypeInfosByName().get('Retail Banking').getRecordTypeId();
    for(Lead lead: trigger.new){
        if(lead.RecordTypeId == retailLead)
            lead.Company = (lead.FirstName != null? lead.FirstName + ' ' + lead.LastName: lead.LastName);
    }
}