public without sharing class AcctPlanGroupProjectedPerformanceCtrlV2 {
	private ApexPages.StandardController standardController { get; set; }
	public ViewState ViewState { get; set; }
	public String CompanyID { get; set; }
	public String GroupID { get; set; }
	public String WalletID { get; set; }
	public String mode { get; set; }

	public AcctPlanCompanyProfile__c companyprofile { get; set; }
	public Boolean isConsoleMode { get; set; }
	public boolean isHasProdStrategy { get; set; }
	public boolean isHasActionPlan { get; set; }
	public Boolean isMiniView { get; set; }
	public boolean isDisabled { get; set; }
	public String InfoMessage { get; set; }
	public boolean IsLock { get; set; }

	/*Account Team*/
	public List<Account> AccountList { get; set; }
	public Set<Account> AccountSet { get; set; }
	public Set<ID> AuthorizedSet { get; set; }
	public List<AcctPlanCompanyProfile__c> CustomerProfileList { get; set; }
	public String MasterGroupID { get; set; }
	public boolean isHasPermission { get; set; }
	Map<ID, String> AccountTeamMap { get; set; }
	public Group__c mGroup { get; set; }
	public AcctPlanGroupProfile__c groupprofile { get; set; }
	public boolean isHasAuthorized { get {

			isHasAuthorized = false;
			if (AuthorizedSet.contains(Userinfo.getUserId())) {
				isHasAuthorized = true;
			}

			return isHasAuthorized;

		} set; }
	/*End Account Team*/

	public AcctPlanGroupProjectedPerformanceCtrlV2(ApexPages.StandardController controller) {
		// View State for this view
		ViewState = new ViewState();
		standardController = controller;
		getParameter();

		if (CompanyID != null && CompanyID != '') {
			companyprofile = AccountPlanUtilities.QueryAcctPlanCompanyProfileByID(CompanyID);

			ishasProdStrategy = companyprofile.isHasProdStrategy__c;
			isHasActionPlan = companyprofile.isHasActionPlan__c;
			System.debug(AccountPlanUtilities.OWNERSEGMENT);
			if (AccountPlanUtilities.OWNERSEGMENT != null) {
				isMiniView = AccountPlanUtilities.ISMINIVIEW;
			} else {
				isMiniView = false;
			}
		}
		/*Account Team*/
		//Check Permission
		isHasPermission = false;
		List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id = :userinfo.getProfileId() LIMIT 1];
		String MyProflieName = PROFILE[0].Name;
		List<ObjectPermissions> obplist = [SELECT Id, SObjectType, PermissionsRead, PermissionsEdit, PermissionsCreate
		                                   FROM ObjectPermissions
		                                   WHERE SObjectType = :'AcctPlanGroupProfile__c' AND parentid in(select id from permissionset where
		                                                                                                  PermissionSet.Profile.Name = :MyProflieName)];

		if (obplist.get(0).PermissionsEdit || obplist.get(0).PermissionsCreate) {
			isHasPermission = true;
		}

		AuthorizedSet = new Set<ID> ();
		if (GroupID != null && GroupID != '') {
			groupprofile = AccountPlanUtilities.QueryGroupProfileByID(GroupID).get(0);
			AccountList = AccountPlanUtilities.QueryAccountByGroupID(GroupProfile.Group__c);
			
			if (AccountList.size() > 0) {
				AccountSet = new Set<Account> ();
				AccountSet.addAll(AccountList);
				AccountTeamMap = new Map<ID, String> ();

				List<AccountTeamMember> acctTeam = [SELECT ID, AccountAccessLevel, AccountId,
				                                    IsDeleted, TeamMemberRole, UserId FROM AccountTeamMember
				                                    WHERE AccountId IN :AccountSet
				                                    AND USerId = :Userinfo.getUserId()
				                                   ];

				for (AccountTeamMember acctT : AcctTeam) {
					AccountTeamMap.put(acctT.AccountId, acctT.AccountAccessLevel);
					if (acctT.AccountAccessLevel == 'Edit' || acctT.AccountAccessLevel == 'All') {
						AuthorizedSet.add(acctT.UserId);
					}
				}

				for (Account acct : AccountSet) {
					AuthorizedSet.add(acct.OwnerId);
				}
            }
		}
		system.debug('isDisabled=' + isDisabled);
		system.debug('isHasPermission=' + isHasPermission);
		system.debug('isHasAuthorized=' + isHasAuthorized);
        //if(AccountList.size() > 0 && AccountList.size() <=60){
            createViewModel();
        /*}else{
            isDisabled = true;
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO,'Number of customers in the group exceeds the systen threshold. Please contact administrator for this problem.'));
        }*/
	}
	private void createViewModel() {
		if (GroupID != null && GroupID != '') {

			try {
                ViewState.GroupProfile = AccountPlanGroupProfileSelector.getGroupProfileById(GroupID);
                if(AccountList.size() >60){
                    isDisabled = true;
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO,'Number of customers in the group exceeds the system threshold. Please contact administrator for this problem.'));
                    //return null;
                }else{
                    
                    ViewState.ViewModel = AccountPlanProductStrategyService.getGroupCompaniesProductStrategyInfoV2(ViewState.GroupID);
                    IsLock = AcctPlanGroupWalletLockService.IsLock(ViewState.ViewModel.GroupId);
                }
			}
			catch(Exception ex) {
				ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
				system.debug(ex.getMessage());
			}
		}
	}
	//region 
	public void getParameter() {
		GroupID = ApexPages.currentPage().getParameters().get('GroupID');
		CompanyID = ApexPages.currentPage().getParameters().get('CompanyID');
		WalletID = ApexPages.currentPage().getParameters().get('WalletID');
		mode = ApexPages.currentPage().getParameters().get('mode');
		//===================================================================//
		ViewState.GroupID = GroupID;
		ViewState.CompanyID = CompanyID;
		ViewState.WalletID = WalletID;
		ViewState.Mode = mode;

		if (mode == 'console') {
			isConsoleMode = true;
		} else {
			isConsoleMode = false;
		}
	}
	//endregion
	public PageReference Refresh() {
		InfoMessage = '';
		try {
			IsLock = AcctPlanGroupWalletLockService.IsLock(ViewState.ViewModel.GroupId);
			if (!IsLock) {
				// Lock 
				AcctPlanGroupWalletLockService.Lock(ViewState.ViewModel.GroupId);
				GroupPerformanceRefreshEntranceBatch batch =
				new GroupPerformanceRefreshEntranceBatch(ViewState.ViewModel.AccountWithAccountPlan
				                                         , ViewState.ViewModel.AccountWithoutAccountPlan
				                                         , ViewState.ViewModel.GroupId
				                                         , ViewState.ViewModel.Year
				                                         , ViewState.GroupID /*Group Profile*/);
				Database.executeBatch(batch, 25);
				IsLock = true;
			}
		}
		catch(Exception ex) {
			createNotificationMessageError(ex.getMessage());
		}
		createNotificationMessage();
		return null;
	}

	private void createNotificationMessageError(string message) {
		InfoMessage = ' <div class="alert alert-danger"> '
		+ '     <a href="#" class="close" data-dismiss="alert">&times;</a> '
		+ '    <b>' + message + '</b>'
		+ ' </div> ';
	}
private void createNotificationMessage() {
		InfoMessage = ' <div class="alert alert-success" style="  clear:both;  text-align: left;    color: white;    background-color: #FF4000;"> '
		+ '     <a href="#" class="close" data-dismiss="alert">&times;</a> '
		+ '<p><b>System will take approx 5-10 mins to refresh data. Email notification will be sent after finished. Please click the link from an email or Press F5 on screen.</b></p> '
		+ '<p><b>(ระบบได้รับ request แล้ว  จะใช้เวลาประมาณ 5-10 นาที คุณจะได้รับอีเมล์หลังจากระบบประมวลผลเสร็จ   กรุณาคลิกลิงค์จากอีเมล์ หรือ กด F5 จากหน้าจอเพื่อดูข้อมูลใหม่)</b> </p> '

		+ ' </div> <script> jsHideToggle();  </script> ';
	}
	public void initGroupWalletV2(List<AccountPlanProductStrategyService.AcctPlanProdStrategyInfo> acctPlanProdStrategyByGroupList) {

	}
	public Boolean initGroupWallet() {
		return true;
	}


	public class ViewState {
		//Account Navigation information
		public String CompanyID { get; set; }
		public String GroupID { get; set; }
		public String WalletID { get; set; }
		public String Mode { get; set; }
		public AcctPlanGroupProfile__c GroupProfile { get; set; }
		//DTO
		public AccountPlanProductStrategyService.AcctPlanProdStrategyCompanyInfo ViewModel { get; set; }
	}


}