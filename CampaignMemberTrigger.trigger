trigger CampaignMemberTrigger on CampaignMember (before insert, after insert, after delete) {

    Boolean RunTrigger = AppConfig__c.getValues('runtrigger').Value__c == 'true' ; 

    if(Trigger.isBefore && Trigger.isInsert)
    {
        system.debug('CampaignMemberTrigger before insert ');
        if( RunTrigger || Test.isRunningTest() ){
            CampaignMemberTriggerHandler.checkPrimaryCampaign(Trigger.new);
        }
    }
    
    if(Trigger.isAfter && Trigger.isInsert)
    {
        system.debug('CampaignMemberTrigger after insert ');
        if( RunTrigger || Test.isRunningTest() ){
            //CampaignMemberTriggerHandler.addCampaignProductToProductInterest(Trigger.new);
            CampaignMemberTriggerHandler.addLeadToCampaingMember(Trigger.new);
        }
    }
    
    if(Trigger.isAfter && Trigger.isDelete)
    {
        system.debug('CampaignMemberTrigger after delete');
        if( RunTrigger || Test.isRunningTest() ){
            //CampaignMemberTriggerHandler.removeCampaignProductFromLead(Trigger.old, new list<string>());
            CampaignMemberTriggerHandler.deleteCampaignMember(Trigger.old);
        }
    }

}