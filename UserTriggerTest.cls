@IsTest
public class UserTriggerTest {
    public static integer n;

    public static testmethod void positiveTest() {
        System.debug(':::: positiveTest Start ::::');
        Test.startTest();
        TestUtils.createObjUserPermission();
        TestUtils.createAppConfig();
		Appconfig__c Aconfig = [SELECT NAME,Value__c from Appconfig__c WHERE Name ='runtrigger' LIMIT 1];
        Aconfig.Value__c ='true';
        update Aconfig;
        TestUtils.CreateTriggerMsg();
        TestInit.createUser(true);  //  Create User by User Run Test
        User u = TestInit.us;
        user u2 = TestInit.us2;
        System.runAs(u) { // User in List
            // Insert
            n = 1;
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());
            System.debug('Current User Id: ' + UserInfo.getUserId());
            //TestUtils.createUsers(1,'TestUtilTest', 'Utiltest','testUtil@TestUtil.com', true);  //  Test Create User
            try{
                userTest = createUsers(n,'TestUtil', 'Utiltest','testUtil@TestUtil.com', true);  //  Test Create User
            }
            catch (Exception e){
                System.assert(e.getMessage().contains('You are not allowed to insert user information.'));
            }
            
            // Update
            List<User> usr = [select Id,FirstName,LastName,UserName,Email,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname,ProfileId,TimeZoneSidKey,isActive,Employee_ID__c from User];
            List<User> UpUsr1 = new List<User>();
            for(User usr1 : usr) {
                if(usr1.Email == n + 'testUtil@TestUtil.com') {
                    UpUsr1.add(usr1);
                }
            }
            try{
                update UpUsr1;
            }
            catch (Exception e){
                System.assert(e.getMessage().contains('You are not allowed to change user information.'));
            }
        }
        
        System.runAs(userTest) { // User Not in List
            // Insert
            n = 2;
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());
            System.debug('Current User Id: ' + UserInfo.getUserId());
            //TestUtils.createUsers(2,'TestUtil', 'Utiltest','testUtil@TestUtil.com', true);  //  Test Create User
            try{
                userTest = createUsers(n,'TestUtil', 'Utiltest','testUtil@TestUtil.com', true);  //  Test Create User
            }
            catch (Exception e){
                System.assert(e.getMessage().contains('You are not allowed to insert user information'));
            }
            
            // Update
            n = 1;
            List<User> usr = [select Id,FirstName,LastName,UserName,Email,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname,ProfileId,TimeZoneSidKey,isActive,Employee_ID__c from User];
            List<User> UpUsr2 = new List<User>();
            for(User usr2 : usr) {
                if(usr2.Email == n + 'testUtil@TestUtil.com') {
                    UpUsr2.add(usr2);
                }
            }
            try{
                update UpUsr2;
            }
            catch (Exception e){
                System.debug(e.getMessage());
                System.assert(e.getMessage().contains('You are not allowed to change user information'));
            }
        }

        Test.stopTest();
        System.debug(':::: positiveTest End ::::');
    }
    
    public static User userTest {
        get
        {
            if(userTest == null){
                userTest = new User();
            }
            return userTest;
            
        }set;
    }
    
    public static User createUsers(Integer no,String FName,String LName,String Email,Boolean doInsert) {
        List<User> userToCreate = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Read Only'];
        User newUser = new User(FirstName=FName+no, LastName=LName, 
                                UserName=FName+no+LName+'TestTMB@tmb.com', 
                                Email=no+Email, Alias='Testtmb'+no,
                                LocaleSidKey='en_US', LanguageLocaleKey='en_US', EmailEncodingKey='ISO-8859-1', 
                                CommunityNickname=Fname+LName+no+'Testtmb',
                                ProfileId = p.Id, TimeZoneSidKey='America/New_York',isActive = true,
                                Employee_ID__c ='Test'+no);
        userToCreate.add(newUser);

        userTest = newUser;
        if(doInsert){
            insert newUser;
        }

        return newUser;
    }
}