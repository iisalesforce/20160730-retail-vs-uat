public class UploadleadLogCSV {
    
    public transient List<UploadLead> logsuccess      {get; set;}
    public transient List<UploadLead> logerror        {get; set;}
    public transient List<UploadLead> logall          {get; set;}
    public transient LeadLogHeader__c header  {get; set;}
    
    public void logdetail() {
        String logheader ='';
        logheader = ApexPages.currentPage().getParameters().get('idname');
        system.debug('##idname = '+logheader); 
        
        logall      = new List<Uploadlead>();
        logsuccess  = new List<UploadLead>();
        logerror    = new List<UploadLead>();
        header      = new LeadLogHeader__c();
        
        header = [Select Id,name from LeadLogHeader__c where Id =: logheader];
        List<LeadLogDetail__c> logdetailone = [Select Id,name,Content__c,Error_Message__c
                                               ,Lead_ID__c,Parrent__c,Lead_Valid__c,Success__c 
                                               from LeadLogDetail__c where Parrent__c =: logheader
                                             LIMIT 2000];
       
        System.debug(header.name);
        header.name = header.name.replaceAll('[^a-zA-Z0-9]','');
        for(LeadLogDetail__c logde : logdetailone)
        {
                UploadLead tr = new UploadLead();
                String refinedString = logde.Content__c.replace('null',''); 
                string[] csvRecordData      = refinedString.split(',',-1);
                
                tr.leadOwner                = csvRecordData[0] ;
                tr.PrimaryCampaign          = csvRecordData[1];
                tr.LeadOwnerExceptionFlag   = csvRecordData[2];
                tr.LinktoCustomer           = csvRecordData[3];
                tr.CustomerType             = csvRecordData[4]; 
                tr.CustomerName             = csvRecordData[5];
                tr.CustomerNameEN           = csvRecordData[6];
                tr.IDType                   = csvRecordData[7];
                tr.IDNumber                 = csvRecordData[8];
                tr.ContactTitle             = csvRecordData[9];
                tr.ContactFirstname         = csvRecordData[10];
                tr.ContactLastname          = csvRecordData[11];
                tr.ContactPosition          = csvRecordData[12];
                tr.DecisionMap              = csvRecordData[13];
                tr.ContactPhoneNumber       = csvRecordData[14];
                tr.Industry                 = csvRecordData[15];
                tr.Groupname                = csvRecordData[16];
                tr.NoOfyears                = csvRecordData[17];
                tr.SalesAmountperyear       = csvRecordData[18];
                tr.LeadSource               = csvRecordData[19];
                tr.OtherSource              = csvRecordData[20];
                tr.BranchedReferred         = csvRecordData[21];
                tr.ReferralStaffID          = csvRecordData[22];
                tr.ReferralStaffName        = csvRecordData[23];
                tr.TotalExpectedRevenue     = csvRecordData[24];
                tr.Address                  = csvRecordData[25];
                tr.SubDistrict              = csvRecordData[26];
                tr.District                 = csvRecordData[27];
                tr.Province                 = csvRecordData[28];
                tr.ZipCode                  = csvRecordData[29];
                tr.Country                  = csvRecordData[30];
                tr.MobileNo                 = csvRecordData[31];
                tr.OfficeNo                 = csvRecordData[32];
                tr.Ext                      = csvRecordData[33];
                tr.Email                    = csvRecordData[34];
                tr.Rating                   = csvRecordData[35];
            	tr.prescreen				= csvRecordData[36];
                tr.Status					= csvRecordData[37];
            if(csvRecordData.size() > 38) {
                tr.Remark                   = csvRecordData[38];
            }
                tr.Success                  = logde.Success__c;
                tr.Invialid                 = logde.Lead_Valid__c;
                tr.id                       = logde.Lead_ID__c;
                tr.Errormessage             = logde.Error_Message__c;
                logall.add(tr);
            	
                if(tr.Invialid == false)
                {
                    logsuccess.add(tr);
                }
                else if(tr.Invialid == true)
                {
                    logerror.add(tr);
                }
          } 
        
        
        System.debug('logsuccess'+logsuccess.size());
        
    }
}