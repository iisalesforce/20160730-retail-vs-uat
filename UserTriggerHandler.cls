public class UserTriggerHandler {
    static Boolean detectError = false;
    static String errorException = '';
    static String STR_INSERT = 'insert';
    static String STR_UPDATE = 'update';
    static String STR_DELETE = 'delete';
    Static List<Trigger_Msg__c> TriggerMsgList = [SELECT NAME,Description__c FROM Trigger_Msg__c 
                                                  WHERE Name ='User_trigger_error_change'
                                                  OR Name = 'User_trigger_error_insert'];
    // NOTE :  When we codding in Before Context we not use update in Accout list because it will update autometically!!!
    
    public static void handleBeforeInsert(List<User> usersNew)
    {
        boolean ch = false;
        for (ObjUserPermission__c oup : ObjUserPermission__c.getAll().values())
        {
            if(oup.Set_ID__c == UserInfo.getUserId() || oup.Set_ID__c == UserInfo.getProfileId() || oup.Name == UserInfo.getUserName())
            {
                ch = true;
                break;
            }
        }
        
        if(!ch)
        {
            system.debug('Get Data from Custom setting 1');
            for(User uses : usersNew)
            {
                for(Trigger_Msg__c msg : TriggerMsgList){
                    if(msg.Name == 'User_trigger_error_insert'){
                      uses.addError(msg.Description__c); 
                    }
                }
               
                break;
            }
        }
    }
    
    public static void handleBeforeUpdate(List<User> usersNew,List<User> usersOld)
    {
        boolean ch = false;
        for (ObjUserPermission__c oup : ObjUserPermission__c.getAll().values())
        {
            if(oup.Set_ID__c == UserInfo.getUserId() || oup.Set_ID__c == UserInfo.getProfileId() || oup.Name == UserInfo.getUserName())
            {
                ch = true;
                break;
            }
        }
        if(!ch)
        {
            system.debug('Get Data from Custom setting 2');
            for(User uses : usersNew)
            {
				for(Trigger_Msg__c msg : TriggerMsgList){
                    if(msg.Name == 'User_trigger_error_change'){
                      uses.addError(msg.Description__c); 
                    }
                }
                break;
            }
        }
    }
    
    public static void handleAfterUpdate(List<User> usersNew,List<User> usersOld){

    }
    
}