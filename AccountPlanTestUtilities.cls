public with sharing class AccountPlanTestUtilities { 
    public static final Id SYSADMIN_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;
    public static final Id TMB_RM_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB RM Profile' LIMIT 1].Id;
    public static final Id TMB_BDM_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB BDM Profile' LIMIT 1].Id;
    public static final Id TMB_SEGMENTHEAD_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB Segment Head Profile' LIMIT 1].Id; 
    public static Id niTargetId {get;set;}

    public static List<Account> accountList {
        get
        {
            if(accountList == null){ 
                accountList = new List<Account>();
            }
            return accountList;
            
        }set;
    }
    public static List<User> userList {
        get
        {
            if(userList == null){
                userList = new List<User>();
            }
            return userList;
            
        }set;
    }   
    public static List<Group__c> groupList {get{
        if(groupList ==null){
            groupList = new List <Group__c>();
        }
        return groupList;
    }set;} 

    public static List<Target__c> targetList { 
        get
        {
            if(targetList == null){
                targetList = new List<Target__c>();
            }
            return targetList;
                
                }set;
    
    }

    public static AcctPlanPortfolio__c portfolio {get;set;}
	
    public static List<Account_Plan_Fiscal_Year__c> getFiscalYear{get;set;}
    
    public static void getAcctPlanMode (){
        
            AcctPlanMode__C semode = new AcctPlanMode__C();
            semode.Name ='SE';
            seMode.isMiniMode__c = true;
            insert seMode;
            AcctPlanMode__C BBmode = new AcctPlanMode__C();
            BBmode.Name ='BB';
            BBmode.isMiniMode__c = false;
            insert BBmode;
    }



	public static List<User> createUsers(Integer size,String FName,String LName,String Email,ID setProfileID,Boolean isSESegment,Boolean doInsert) {
        List<User> userToCreate = new List<User>();
        for(Integer i = 0;i<size;i++){
            String segment;
            if(isSESegment){
                segment = 'SE';
            }else{
                Segment = 'BB';
            }
            User newUser = new User(FirstName=FName+i, LastName=LName+i, 
                                    UserName=FName+i+LName+i+'_TMBTestTMB@tmb.com',
                                    Email=Email, Alias='Testtmb'+i,Segment__c =segment,
                                    LocaleSidKey='en_US', LanguageLocaleKey='en_US', EmailEncodingKey='ISO-8859-1', 
                                    CommunityNickname=Fname+i+'_'+LName+i+'TMBTesttmb'+i,
                                    ProfileId = SYSADMIN_PROFILE_ID, TimeZoneSidKey='America/New_York',isActive = true,
                                    Employee_ID__c ='T001'+i);
            userToCreate.add(newUser);
        }
        
        
        if(doInsert){            
            insert userToCreate;
            userList.addAll(userToCreate);
        }
        
        return userToCreate;
    }


	public static List<Account> createAccounts(Integer size , String Fname,String CType,ID SalesOwner,Boolean AccountPlanFlag, Boolean doInsert)
    {
        List<Account> newAccountList = new List<Account>();
        for(Integer i =0 ; i<size ;++i)
        {   
            Account Acct = new Account(); 
            Acct.Name = 'Test Account CUS-'+i;
            //Acct.Company_Name_Temp__c  =  'TestCompanyAccount'+i;
            Acct.ID_Type_PE__c  = 'Passport ID';
            Acct.ID_Number_PE__c  = '1510'+i;
            //Acct.Fax_No_Temp__c  = '1001';
            Acct.Office_Address_Line_1_PE__c  = 'Address1';
            Acct.Office_Address_Line_2_PE__c  = 'Address2';
            Acct.Office_Address_Line_3_PE__c  = 'Address3';
            Acct.Primary_Address_Line_1_PE__c  = 'Primary Address1';
            Acct.Primary_Address_Line_2_PE__c  = 'Primary Address2';
            Acct.Primary_Address_Line_3_PE__c  = 'Primary Address3';
            Acct.Province_Primary_PE__c  = 'Chiang Mai';
            Acct.Zip_Code_Primary_PE__c  = '50000';
            Acct.Zip_Code_Office_PE__c   = '1001';
            Acct.Customer_Type__c = CType;
            Acct.First_name_PE__c  = Fname+''+i;
            Acct.First_Name_ENG_PE__c = Fname+''+i;
            Acct.Last_name_PE__c  = 'TestMock'+i;
            Acct.Last_Name_PE_Eng__c   = 'TestMock'+i;
            //Acct.Office_Number_Temp__c = '0876213284';
            Acct.Phone ='050111222';
            Acct.Mobile_Number_PE__c  = '0801112233';
            Acct.Rating = 'Hot';
            Acct.IsDisqualified__c = false;
            Acct.OwnerId = SalesOwner;
            Acct.Owner_name_ID__c = SalesOwner+'OWN-'+i;
            if(AccountPlanFlag){
                 acct.Account_Plan_Flag__c = 'Yes';
            }
            
            
            
            
            newAccountList.add(acct);
            
        }
      
        if(doInsert){

                insert newAccountList;
            accountList.addAll(newAccountList);
        }
        
        return newAccountList;
    }

	public static List<Target__c> createTargetNI(Integer size, ID SalesOwnerID,boolean isInsert){
        
         List<RecordType> recordTypeId = [select Id from RecordType where name = 'NI Target' limit 1];
        niTargetId = recordTypeId[0].Id;
        
        for(integer i=0;i<size;i++){
            Target__c targetNI = new Target__c();
            targetNI.OwnerId = SalesOwnerID;
            targetNI.Monthly_Target_Date__c = System.today();
            targetNI.NI_Target_Monthly__c = 1000000;
            targetNI.RecordTypeId = niTargetId;
            targetList.add(targetNI);
        }
        
        if(isInsert){
            insert targetList;
        }
        
        return targetList;
	}
    
    
    
	public static AcctPlanPortfolio__c createAccountPlanPortfolio(ID SalesOwnerID,String PortYear,double TargetNI,boolean isInsert){
        portfolio = new AcctPlanPortfolio__c();
		portfolio.SalesOwner__c = SalesOwnerID;    	
    	portfolio.Year__c =PortYear;
        portfolio.TargetNI__c = TargetNI;
    	
        if(isInsert){
            insert portfolio;
        }
        return portfolio;
	}
    
    public static List<Group__c> createGroupMaster(Integer size,String GroupName,boolean hasParent,boolean isInsert){
        List <group__c> Newgroup = new List<group__c>();
        for(integer i=0;i<size;i++){
             Group__c groupmaster = new Group__c ();
        	groupmaster.Name = GroupName;
        	groupmaster.GroupCompany__c	 ='GroupCompany'+i;
        	groupmaster.GroupIndustry__c = 'Financial Institutions (Bank, Non-Bank)';
        	if(hasParent){
            groupmaster.Parent_Company__c  = createAccounts(1,'GroupParent','Juristic',Userinfo.getUserId(),true,true).get(0).id;
            groupmaster.ParentIndustry__c = 'Financial';
	        }
        Newgroup.add(groupmaster);
        }
        if(isInsert){
            insert Newgroup;
            groupList.addall(Newgroup);
        }
        
        return NewGroup;
    }

    public static List<AcctPlanGroupPort__c> createGroupPortbyGroupMaster(List<Group__c> groupmasterlist,AcctPlanPortfolio__c existingportfolio,boolean isInsert){
        List <AcctPlanGroupPort__c> groupportList = new List<AcctPlanGroupPort__c>();
        for(Group__c groupmaster : groupmasterlist){
            AcctPlanGroupPort__c groupport = new AcctPlanGroupPort__c ();
            groupport.Group__c = groupmaster.id;
            groupport.Group_Name__c = groupmaster.Name;
            groupport.Account_Plan_Portfolio__c = existingportfolio.id;
            groupportList.add(groupport);
        }
        
        if(isInsert){
            insert groupportList;
        }
        return groupportList;
    }

    public static List<AcctPlanCompanyPort__c> createCompanyPortByAccount(List<Account> existingAccountList,AcctPlanPortfolio__c existingportfolio,boolean isInsert){
       List <AcctPlanCompanyPort__c> comportList = new List<AcctPlanCompanyPort__c>();
        for(Account existingaccount : existingAccountList){
            AcctPlanCompanyPort__c  comport = new AcctPlanCompanyPort__c();
            comport.Account__c = existingaccount.id;
            comport.Account_Name__c = existingaccount.First_Name__c+' '+existingaccount.Last_Name__c;
            comport.Account_Plan_Portfolio__c = existingportfolio.id;
            comportList.add(comport);
        }
        
        if(isInsert){
            insert comportList;
        }
        return comportList;
    }
    
    public static List< AcctPlanCompanyProfile__c> createCompanyProfileByAccount(List<Account> existingAccountList,boolean isInsert){
        
        List< AcctPlanCompanyProfile__c> companyprofileList = new List< AcctPlanCompanyProfile__c>();
        for(Account masteracct : existingAccountList){
             AcctPlanCompanyProfile__c companyprofile = new AcctPlanCompanyProfile__c();
                //companyprofile.name = masteracct.First_Name__c+' '+masteracct.Last_Name__c;
                companyprofile.Account__c = masteracct.id;
                companyprofile.Company_Industry__c = masteracct.Industry;
                companyprofile.NoOfEmployee__c = masteracct.NumberOfEmployees;
                companyprofile.AccountName__c =masteracct.First_Name__c+' '+masteracct.Last_Name__c;
                companyprofile.EstablishedSince__c  = masteracct.ESTABLISH_DT__c;
                companyprofile.OwnerId = masteracct.OwnerID;
                //companyprofile.isMiniMode__c = AcctPlanMode__c.GetValues(masteracct.Owner.Segment__c).isMiniMode__c;
                Boolean isMiniMode = AcctPlanMode__c.GetValues(masteracct.Owner.Segment__c).isMiniMode__c;
                	if(isMiniMode){
                        companyprofile.isMiniMode__c = true;
                    }else if(masteracct.Account_Plan_Form__c == 'Short Form'){
                        companyprofile.isMiniMode__c = true;
                    }else{
                        companyprofile.isMiniMode__c = false;
                    }
                companyprofile.CustomerSince__c = masteracct.Customer_Creation_Date__c;
                companyprofile.Parent_Company_Info__c = masteracct.ParentID;
                companyprofile.Parent_Industry__c = masteracct.Parent_Industry__c;
                companyprofile.Business_Code__c = masteracct.Business_Type_Code__c==null?'':masteracct.Business_Type_Code__c +
                    ' '+
                    masteracct.Business_Type_Description__c==null?'':masteracct.Business_Type_Description__c; 
                companyprofile.Status__c = 'In Progress';
            	companyprofile.Parent_Company_Name__c = masteracct.ParentID;
            
                    
              companyprofileList.add(companyprofile);  
        }
        if(isInsert){
            insert companyprofileList;
        }
        
        return companyprofileList;
    }
    
    public static List<AcctPlanGroupProfile__c> createGroupProfilebyGroup (List<Group__c> mGroupList,boolean isInsert){
        	List<AcctPlanGroupProfile__c> groupProfileList = new List<AcctPlanGroupProfile__c>();
        for(Group__c mGroup : mGroupList){
            AcctPlanGroupProfile__c groupprofile = new AcctPlanGroupProfile__c(); 
            groupprofile.Group__c = mGroup.id;
            groupprofile.GroupName__c = mGroup.Name;
            groupprofile.Name = mGroup.Name;
            groupprofile.GroupIndustry__c = mGroup.GroupIndustry__c;  
            groupprofile.ParentIndustry__c = mGroup.ParentIndustry__c;
            groupprofile.UltimateParent__c = mGroup.UltimateParent__c;
           	groupprofile.Parent_Company__c = mGroup.Parent_Company__c; 
            groupProfileList.add(groupprofile);
        }
        
        if(isInsert){
         Insert groupProfileList; 
        }
        
        return groupProfileList;
    }
   
    public static List<AcctPlanContribution__c> createRenevueContribution (integer size,ID GroupID,ID CompanyID){
     	List<AcctPlanContribution__c> contributionList = new List<AcctPlanContribution__c>();
        
        for(integer i=0;i<size;i++){
            AcctPlanContribution__c contri = new AcctPlanContribution__c();
            contri.EBITDAContributionPercent__c = 10;
            contri.RevenueContributionPercent__c = 10;
            
            if(CompanyID !=null){
                contri.Account_Plan_Company_Profile__c =CompanyID;
            }
            if(GroupID !=null){
              contri.AccountPlanGroupProfile__c =GroupID;   
            }
           
            contri.RevenueContributionType__c = 'Contribution by service & product';
            contributionList.add(contri);
        }
        
        insert contributionList;
        return contributionList;
    }
    
    public static List<AcctPlanSupplierOrBuyer__c> CreateSupplierOrBuyer (integer size,ID CompanyID){
        List<AcctPlanSupplierOrBuyer__c> supOrbuyList = new List<AcctPlanSupplierOrBuyer__c>();
        for(integer i=0;i<size;i++){
        AcctPlanSupplierOrBuyer__c suporbuy = new AcctPlanSupplierOrBuyer__c();
            suporbuy.Account_Plan_Company_Profile__c = companyID;
            suporbuy.Segment__c = 'Distribution';
            suporbuy.Percent__c = 10;
            supOrbuyList.add(suporbuy);
        }
        
        insert supOrbuyList;
        
        return supOrbuyList;
    }
    
    
    public static List<Account_Plan_Company_Top_5__c> CreateTop5 (integer size,ID CompanyID){
        List<Account_Plan_Company_Top_5__c> top5List = new List<Account_Plan_Company_Top_5__c>();
        for(integer i=0;i<size;i++){
        Account_Plan_Company_Top_5__c top5 = new Account_Plan_Company_Top_5__c();
            top5.Account_Plan_Company_Profile__c = companyID;
            top5.Top_5_Types__c = 'Buyers';
            top5.Percent__c = 10;
            top5List.add(top5); 
        } 
        
        insert top5List;
        
        return top5List;
    }
    
}