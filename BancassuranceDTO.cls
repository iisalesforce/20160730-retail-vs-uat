Global class BancassuranceDTO {
	/*----------------------------------------------------------------------------------
	  Author:        Keattisak Chinburarat
	  Company:       I&I Consulting 
	  Description:   Data Transfer Objects
	  Inputs:        None
	  Test Class:    -
	  History
	  <Date>      <Authors Name>     <Brief Description of Change>
	  ----------------------------------------------------------------------------------*/
	global string PolicyNo { get; set; }
	global string SubProductGroup { get; set; }
	global string ProductName { get; set; }
	global string InsuranceCompany { get; set; }
	global Date OpenedDate { get; set; }
	global string Status { get; set; }
	global Decimal SumInsure { get; set; }
	global Decimal AFYP { get; set; }
	global Date ExpiryDate { get; set; }
	// Error Handler message
	global String SoapStatus { get; set; }
	global String SoapMessage { get; set; }

	global string Params {
		get {

			string p = PolicyNo;
			return UrlHelper.encryptParams(p);

		}
	}
    
    global String convertedOpenedDate{
        get{
            return NumberHelper.DateFormat(OpenedDate);
        }
    }
}