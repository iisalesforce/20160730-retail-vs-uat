@isTest
public class GroupRefreshEntranceBatchTest {
	static {


		List<AppConfig__c> mc = new List<AppConfig__c> ();
		mc.Add(new AppConfig__c(Name = 'CertName', Value__c = 'TMB_RSA'));
		mc.Add(new AppConfig__c(Name = 'runtrigger', Value__c = 'false'));
		insert mc;

	}

	@isTest
	private static void Test_BatchRefreshGroupWallet() {

		Account acc = new Account();
		acc.Name = 'xxx';
        acc.Phone ='050111222';
        acc.Mobile_Number_PE__c  = '0801112233';
		insert acc;



		II_TestUtility utility = new II_TestUtility();
		IAccountPlanRefreshService mock = new AccountPlanRefreshServiceBatchMock();
		AccountPlanRefreshService.setMockservice(mock);




		Set<Id> AccountWithAccountPlan = new Set<Id> { utility.getFakeId(Account.SObjectType), acc.Id };
		Set<Id> AccountWithoutAccountPlan = new Set<Id> { utility.getFakeId(Account.SObjectType), acc.id };
		Id GroupId = utility.getFakeId(Group__c.SObjectType);
		string Year = Datetime.now().year() + '';
		Id GroupProfileId = utility.getFakeId(AcctPlanGroupProfile__c.SObjectType);





		//AcctPlanGroupWalletLockService.Lock(ViewState.ViewModel.GroupId);
		GroupWalletRefreshEntranceBatch batch =
		new GroupWalletRefreshEntranceBatch(AccountWithAccountPlan
		                                    , AccountWithoutAccountPlan
		                                    , GroupId
		                                    , Year
		                                    , GroupProfileId /*Group Profile*/);
		Database.executeBatch(batch, 25);


	}

	@isTest
	private static void Test_BatchRefreshGroupPerformance() {
		Account acc = new Account();
		acc.Name = 'xxx';
        acc.Phone ='050111222';
        acc.Mobile_Number_PE__c  = '0801112233';
		insert acc;



		II_TestUtility utility = new II_TestUtility();
		IAccountPlanRefreshService mock = new AccountPlanRefreshServiceBatchMock();
		AccountPlanRefreshService.setMockservice(mock);




		Set<Id> AccountWithAccountPlan = new Set<Id> { utility.getFakeId(Account.SObjectType), acc.Id };
		Set<Id> AccountWithoutAccountPlan = new Set<Id> { utility.getFakeId(Account.SObjectType), acc.id };
		Id GroupId = utility.getFakeId(Group__c.SObjectType);
		string Year = Datetime.now().year() + '';
		Id GroupProfileId = utility.getFakeId(AcctPlanGroupProfile__c.SObjectType);


		GroupPerformanceRefreshEntranceBatch batch =
		new GroupPerformanceRefreshEntranceBatch(AccountWithAccountPlan
		                                         , AccountWithoutAccountPlan
		                                         , GroupId
		                                         , Year
		                                         , GroupProfileId /*Group Profile*/);
		Database.executeBatch(batch, 25);















	}


	public class AccountPlanRefreshServiceBatchMock implements IAccountPlanRefreshService {
		public Boolean RefreshDepositInter(String acctPlanCompanyId, String acctPlanWallet, String acctPlanGruopId) {
			return true;
		}
		public Boolean RefreshDepositDomestic(String acctPlanCompanyId, String acctPlanWallet, String acctPlanGruopId) {
			return true;
		}
		public Boolean refreshWalletAndAnnualPerformanceRolling12Month(Set<ID> listPortfolioId) {
			return true;
		}

		public Boolean RefreshCompanyPerformanceProductStrategyForStep6(String companyProfileId) {
			return true;
		}
		public Boolean RefreshGroupPerformanceProductStrategyForStep6(String groupProfileId) { return true; }
		public Boolean RefreshPortfolioPerformanceProductStrategyForStep0(String portfolioId) { return true; }
		public List<AcctPlanProdStrategy__c> RefreshProductStrategyAndWalletByDomain(String acctPlanCompanyId, String acctPlanWallet, String acctPlanGruopId) {
			return new List<AcctPlanProdStrategy__c> ();

		}
		public void RefreshProductStrategyPort(Set<Id> accountIds, string year) { }
		public void RefreshProductStrategy(Set<Id> accountIds, string year, Id groupProfilesId) { }
		public void RefreshNameProductStrategyPort(Set<Id> accountIds, string year) { }
		public void RefreshGroupPerformance(Set<Id> accountId, string year) { }

		public List<AccountPlanRefreshService.CustomerWalletInfo> initialStep0(string accountIds) {
			return new List<AccountPlanRefreshService.CustomerWalletInfo> ();
		}

	}
}