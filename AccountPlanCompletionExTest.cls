@isTest
global class AccountPlanCompletionExTest {
    static{
        TestUtils.createAppConfig();
        TestUtils.createStatusCode(); 
        TestUtils.createDisqualifiedReason(); 
        TestUtils.CreateAddress();
        AccountPlanTestUtilities.getAcctPlanMode();
    }
 
     static testmethod void AccountPlanCompletionExTest(){
         User SalesOwner = AccountPlanTestUtilities.createUsers(1, 'RM', 'PortfolioMngTest', 'portfoliomng@test.com', AccountPlanTestUtilities.TMB_RM_PROFILE_ID,false, true).get(0);
        
        List<Account> AccountList = AccountPlanTestUtilities.createAccounts(5, 'GroupPro', 'Individual', SalesOwner.id, true, true);
         List<Account> NewAccountList = AccountPlanTestUtilities.createAccounts(5, 'GroupMaster', 'Individual', SalesOwner.id, true, true);
         List<Account> acctForCompanyProfile = new List<Account>();
         acctForCompanyProfile.add(AccountList.get(0));
         acctForCompanyProfile.add(AccountList.get(2));
         acctForCompanyProfile.add(AccountList.get(3));
        List<AcctPlanCompanyProfile__c> comprofileList = AccountPlanTestUtilities.createCompanyProfileByAccount(acctForCompanyProfile, true);
         AcctPlanCompanyProfile__c comprofile = comprofileList.get(0);
         AcctPlanCompanyProfile__c comprofile2 = comprofileList.get(1);
        List <group__c> groupList = AccountPlanTestUtilities.createGroupMaster(2,'PortMngtest', false, true);
		Group__c mastergroup = groupList.get(0);
        Group__c newGroup = groupList.get(1);
         for(account acct  : AccountList){
            acct.Group__c =mastergroup.id;
        }
         for(account Newacct  : NewAccountList){
            Newacct.Group__c =newGroup.id;
        }
         
         
        update AccountList;
        update NewAccountList;

        List<group__c> mgroupList = new List <group__c>();
        mgroupList.add(mastergroup);
     	AcctPlanGroupProfile__c groupprofile = AccountPlanTestUtilities.createGroupProfilebyGroup(mgroupList,true).get(0);
         comprofile.AcctPlanGroup__c = groupprofile.id;
         comprofile.Parent_Company_Info__c = NewAccountList.get(0).id;
         update comprofile;
         
          
        
 AccountPlanCompletionEx completionExCompany = new AccountPlanCompletionEx(comprofile.id);
         
         List<AcctPlanContribution__c> contributionlist = AccountPlanTestUtilities.createRenevueContribution(3,null, comprofile.id);
          contributionlist.get(0).RevenueContributionType__c = 'Contribution by service & product';
          contributionlist.get(1).RevenueContributionType__c = 'Contribution by business unit';
          contributionlist.get(2).RevenueContributionType__c = 'Contribution by regional';
         update contributionlist;
         Test.setMock(WebServiceMock.class, new TMBAccountPlanServiceProxyMock());
         AccountTeamMember teammember = new AccountTeamMember ();
         teammember.AccountID = comprofile.Account__c;
         teammember.TeamMemberRole = 'Sponser';
         teammember.UserId = SalesOwner.id;
         insert teammember;
         AccountTeamMember teammember2 = new AccountTeamMember ();
         teammember2.AccountID = comprofile.Account__c;
         teammember2.TeamMemberRole = 'Sponser';
         teammember2.UserId = Userinfo.getUserId();
         insert teammember2;
         
         Account Newacct = NewAccountList.get(1);
         AcctPlanWallet__c wallet = new AcctPlanWallet__c ();
         wallet.AcctPlanCompanyProfile__c = comprofile.id;
         wallet.TotalDomesticPercent__c = 0.50;
         wallet.TotalPaymentDomesticPercent__c  = 0.50;
         wallet.BusinessHasSeasonal__c = 'Yes';
         wallet.HighSeasonStartMonth__c ='4';
         wallet.HighSeasonEndMonth__c = '8';
         insert wallet;
          AcctPlanCollectionOrPaymentCurrency__c  cCurrency = new AcctPlanCollectionOrPaymentCurrency__c();
          cCurrency.CollectionPortion__c =100;
          cCurrency.Currency_Type__c = 'Collection';
          cCurrency.AcctPlanWallet__c = wallet.id;
         insert cCurrency;
         AcctPlanCollectionOrPaymentCurrency__c  pCurrency = new AcctPlanCollectionOrPaymentCurrency__c();
          pCurrency.CollectionPortion__c =100;
          pCurrency.Currency_Type__c = 'Payment';
          pCurrency.AcctPlanWallet__c = wallet.id;
         insert pCurrency;
         AcctPlanCollectionDomestic__c colDomestic = new AcctPlanCollectionDomestic__c();
         colDomestic.AcctPlanCollectionMethodDomestic__c =wallet.id;
         colDomestic.NoOfTransaction__c =100;
         colDomestic.Volume__c = 50;
         colDomestic.Label__c = '3) Bill Payment';
         insert ColDomestic;
         AcctPlanCollectionExport__c colExport = new AcctPlanCollectionExport__c();
         colExport.AcctPlanCollectionMethodExport__c = wallet.id;
         colExport.Label__c = 'B/C Term (D/A)';
         colExport.Volumn__c = 20;
         insert colExport;
         AcctPlanPaymentDomestic__c PayDomestic = new AcctPlanPaymentDomestic__c();
         PayDomestic.AcctPlanPaymentMethodDomestic__c = wallet.id;
         payDomestic.Label__c = '1) Cash';
         payDomestic.Volume__c = 20;
         insert paydomestic;
         AcctPlanPaymentImport__c payImport = new AcctPlanPaymentImport__c();
		 payImport.AcctPlanPaymentMethodImport__c = wallet.id;
		 payImport.Label__c ='L/C Sight';
		 payImport.Volume__c = 20;
		 insert payImport;

         
         
         AcctPlanWorkingCapital__c wCap = new AcctPlanWorkingCapital__c();
         wcap.AcctPlanWallet__c = wallet.id;
         wcap.RecordType__c = 'Normal Period';
         wcap.AvgARDays__c = 14;
         wcap.NimcPercent__c = 0.5;
         wcap.FrontEndFee__c = 500;
         
         insert wcap;
         AcctPlanQuestionnaire__c questionnaire = new AcctPlanQuestionnaire__c();
         questionnaire.Answer__c = 'Yes';
         questionnaire.AcctPlanWallet__c = wallet.id;
         questionnaire.Segment__c ='Credit life insurance';
         insert questionnaire;
         AccountPlanTestUtilities.CreateTop5(2,comprofile.id);
         AccountPlanTestUtilities.CreateSupplierOrBuyer(2,comprofile.id);

         SalesOwner.Segment__c = 'SE';
         		update SalesOwner;
         Test.startTest();
          PageReference comProxyPage = Page.AccountPlanMandatory;
               comProxyPage.getParameters().put('GroupID',groupprofile.id);
         	   comProxyPage.getParameters().put('CompanyID',comprofile.id);
         	   comProxyPage.getParameters().put('WalletID',wallet.id);
         	   comProxyPage.getParameters().put('acctKey',AccountList.get(4).id);
               Test.setCurrentPage(comProxyPage); 
         	
 			AccountPlanCompletionEx completionEx = new AccountPlanCompletionEx(comprofile.id);
         	wallet.BusinessHasSeasonal__c = 'No';
         	update wallet;
         	AccountPlanCompletionEx completionEx2 = new AccountPlanCompletionEx(comprofile.id);
         	AccountPlanCompletionEx.MandatoryWrapper mandatoryWrapper1 = new AccountPlanCompletionEx.MandatoryWrapper();
         	
          Account_Plan_Mandatory__c manda1 = new Account_Plan_Mandatory__c();
         manda1.SEQ__c = 1;
         manda1.Domain__C = 'Step 1 Group Company';
         manda1.Section__c = 'Group of Company Profile';
         manda1.Required_Field__c = 'Group Name';
         manda1.Condition__c = 'Mandatory Field';
         
         insert manda1;
         mandatoryWrapper1.Mandatory = manda1;
         mandatoryWrapper1.isHasValue = true;
         String styleClass = mandatoryWrapper1.styleClass;
         String DomainStyle = mandatoryWrapper1.DomainStyle;
          Account_Plan_Mandatory__c manda2 = new Account_Plan_Mandatory__c();
         manda2.SEQ__c = 14;
         manda2.Domain__C = 'Step 2 Customer Profile';
         manda2.Section__c = 'General Information';
         manda2.Required_Field__c = 'Name';
         manda2.Condition__c = 'Mandatory Field';
         insert manda2;
         
         mandatoryWrapper1.Mandatory = manda2;
         mandatoryWrapper1.isHasValue = true;
         String styleClass2 = mandatoryWrapper1.styleClass;
         String DomainStyle2 = mandatoryWrapper1.DomainStyle;
         
          Account_Plan_Mandatory__c manda3 = new Account_Plan_Mandatory__c();
         manda3.SEQ__c = 14;
         manda3.Domain__C = 'Step 3 Wallet By Domain - Domain I';
         manda3.Section__c = 'General Information';
         manda3.Required_Field__c = 'Name';
         manda3.Condition__c = 'Mandatory Field';
         insert manda3;
         mandatoryWrapper1.Mandatory = manda3;
         mandatoryWrapper1.isHasValue = true;
         String styleClass3 = mandatoryWrapper1.styleClass;
         String DomainStyle3 = mandatoryWrapper1.DomainStyle;
         
         try{
              AccountPlanCompletionEx completionEx1 = new AccountPlanCompletionEx();
         }	catch(exception E){
             System.debug(e.getMessagE());
         }
        
             
         
         
         
         Test.stopTest();
         
         
         
         
     }
    
    
    
    
    
}