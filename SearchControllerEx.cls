//region
/*  Note
* =================================
* Follow the CamelCase Java conventions, except for VF pages and components start with a lower case letter.
*  Triggers:
*  
*     <ObjectName>Trigger - The trigger itself. One per object.
*     <ObjectName>TriggerHandler - Class that handles all functionality of the trigger
*     <ObjectName>TriggerTest
* 
*  Controllers:
*  
*     <ClassName>Controller
*     <ClassName>ControllerExt
*     <ClassName>ControllerTest
*     <ClassName>ControllerExtTest
*  Classes:
*  
*   <ClassName>
*   <ClassName>Test (These might be Util classes or Service classes or something else).
*  Visualforce pages and components:
*  
*  <ControllerClassName>[optionalDescription] (without the suffix Controller). There might be multiple views so could also have an extra description suffix.
*  Object Names and custom Fields
*  
*  Upper_Case_With_Underscores
*  Variables/properties/methods in Apex
*  
*  camelCaseLikeJava - more easily differentiated from fields
*  Test methods in test classes
*  
*  test<methodOrFunctionalityUnderTest><ShortTestCaseDesc> - For example, testSaveOpportunityRequiredFieldsMissing, testSaveOpportunityRequiredFieldsPresent, etc. 
* 

//  true    1003    Webservice Error : Connection Timeout
true    0001    has an error message from respond :
true    ++++++++++++++++++++++++++++++++++++++++++0004  No record found While inquiring data in CRM database
true    0003    Sequence contains no elements
true    3009    First Name is required
false   3014    'ID Type cannot be None when updating with ID Number '
false   3012    ID Type and ID Number cannot be None when Customer type is Juristic
false   3013    Mobile or Office Phone Number must be entered
false   3011    Please input any information
false   3010    insert to CRM DB success
false   3016    Prospect ID is duplicated with another prospect
false   2002    Update Prospect - Cannot communicate to CRM-DB
false   3002    Customer type is always required
false   3006    ID Type cannot be None when searching ID Number
false   3004    ID Number is required when ID Type is picked
false   3000    Search success
false   3007    Warning: Prospect name is duplicated with another RM/BDM
false   2101    Create Contact - Cannot communicate to CRM-DB
false   2102    Update Contact - Cannot communicate to CRM-DB
true    3008    Prospect Duplicated in the system
false   3005    TMB Customer ID is required
false   2001    Create Prospect - Cannot communicate to CRM-DB
false   3001    Citizen ID is invalid
false   3003    First Name/Company Name is required
true    3019    Not allow to select Birthdate in the future
false   3015    Access Denied - This contact is not allow to edit
false   3018    Access Denied - You do not have the permission to create prospect
false   0005    Prospect search requires more criteria
false   3017    Access Denied - This Prospect is not allow to edit
true    1010    Prospect is duplicate in the system
true    1001    Salesforce Error : Query Error
false   0000    No duplication, can create prospect
true    1011    Prospect is duplicate in the system
true    1002    Salesforce Error : Webservice Callout Error
true    1000    Salesforce Error : DML Error
*/  
//endregion
public class SearchControllerEx  extends AccountExtensionBase { 
    private ApexPages.StandardController standardController;    
    public Search__c account {get;set;}
    public User ownerUser;
    public Account viewAccount;
    public Map<ID,Account> accountMap;
    // Display in Gridview
    public List<Account> accounts;     
    // Redirect variable
    public ID acctId{get;set;}
    public ID ownerId {get; set;}    
    // Enable / Disable 
    public static Boolean isValid {get; set;}   
    public static Boolean isButtonDisabled {get; set;} 
    public Boolean isSearchNameonly {get;set;}
    public Boolean isSameOwner {get;set;}
    public Boolean isIDValid {get;set;}
    public Boolean isInformation {get;set;}
    public Boolean isCusonly {get;set;}
    public Boolean isLasNameDisabled {get;set;}
    public List<SelectOption> CustTypeOptionList {get;set;}
    public List<SelectOption> IDTypeOptionList {get;set;}
    
    public String IDTypestr {get;set;}
    public String IDNumberstr {get;set;}
    public String CustTypestr {get;set;}
    public String TMBCustStr {get;set;}   
    
    //<!-- Server Hidden Field -->
    public String hdCusType {get;set;}
    public String hdIdType {get;set;}
    public String hdCheckBoxMode {get;set;}
    // Extension Controller Constructor //
    public SearchControllerEx(ApexPages.StandardController controller){
        standardController = controller;
        account = (Search__c)standardController.getRecord();
        account.Customer_Type_Temp__c = 'Individual';
        accounts = new List<Account>();
        isButtonDisabled = true;   
        hdCusType ='--None--';
        IDTypestr='--None--';
        hdIdType = '--None--';
    } 
    
    // list for the account responsed from api
    public List<Account> getAccounts(){
        return accounts;
    }   
    public PageReference search(){
        
        
        System.debug('== Search Start ==');
        System.debug('1. Customer Type :'+  hdCusType); 
        System.debug('2. First Name/Company Name :'+  account.First_name__c); 
        System.debug('3. Last Name :'+  account.Last_name__c); 
        System.debug('4. Mobile Number :'+  account.Mobile_Number_Temp__c); 
        System.debug('5. Office Number :'+  account.Office_Number_Temp__c); 
        System.debug('6. ID Type :'+  hdIdType); 
        System.debug('7. ID Number :'+  account.ID_Number_Temp__c);
        System.debug('8. TMB Customer ID :'+  account.TMB_Customer_ID_Temp__c);
        System.debug('9. Mode :'+  hdCheckBoxMode);
        System.debug('10. hdIdType :'+  hdIdType);
        
        
        account.Customer_Type_Temp__c = hdCusType;
        isButtonDisabled = true;
        isSearchNameonly =false;
        isSameOwner = false;    
        Boolean mobileOnly = false;
        // Fix Cust Type Must be select
        if( hdIdType == '--None--' || hdIdType == null|| hdIdType == '' ){          
            
            System.debug(' 1) Check hdIdType');         
            IDTYpeStr =null;
            account.ID_Type__c = null;            
            System.debug('Set hdIdType : '+   account.ID_Type__c);
            
        }   
        else{
            System.debug(' 2) Check hdIdType'); 
            account.ID_Type__c =hdIdType;
        }
        if(hdCusType == '--None--' || hdCusType == '' || hdCusType == null  ){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,Status_Code__c.GetValues('3002').Status_Message__c));
            return null;
        }
        
        if(   (account.ID_Number_Temp__c==null || account.ID_Number_Temp__c =='')
           && (account.ID_Type__c == null)
           &&(account.First_name__c == null || account.First_name__c =='')
           &&(account.Mobile_Number_Temp__c == null || account.Mobile_Number_Temp__c =='')
           &&(account.Last_name__c == null || account.Last_name__c =='')
           &&(account.Office_Number_Temp__c == null || account.Office_Number_Temp__c =='')
           &&(account.TMB_Customer_ID_Temp__c == null || account.TMB_Customer_ID_Temp__c == '')){
               System.debug(' 3) All Null'); 
               ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3011').Status_Message__c));
               return null;
           }
        
        if(account.ID_Type__c=='Citizen ID' && (account.ID_Number_Temp__c == null || account.ID_Number_Temp__c =='')){
            System.debug(' 4) Citizen ID Null');
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3001').Status_Message__c));
            return null;
        }
        if(account.ID_Type__c=='Citizen ID'  &&  isIDValid == false)
        {
            System.debug(' 5) Citizen ID Invalid');
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3001').Status_Message__c));
            return null;
        }
        
        
         System.debug(' 6) ID_Type__c  ' + account.ID_Type__c);
        if(hdCheckBoxMode=='info'){
            if(   (account.ID_Type__c != null) 
               && (account.ID_Number_Temp__c == null || account.ID_Number_Temp__c == '')){
                   System.debug(' 7) info ');
                   
                   System.debug('account.ID_Type__c : ' + account.ID_Type__c);
                   system.debug('++++++++++');
                   system.debug('account.ID_Type__c : '+ account.ID_Type__c);
                   system.debug('account.ID_Number_Temp__c : '+account.ID_Number_Temp__c);
                   
                   ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3004').Status_Message__c));
                   return null;
               }
        }
        else if(hdCheckBoxMode=='cust'){
            if(account.TMB_Customer_ID_Temp__c == null){
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3005').Status_Message__c));
                return null;
            }
        }
        
        //System.debug('Account ID Type : '   + account.ID_Type__c);
        //System.debug('Account ID Numner : ' + account.ID_Number_Temp__c);
        
        if(account.ID_Type__c == null && account.ID_Number_Temp__c != null){
            System.debug(' 7) Citizen ID Invalid');
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3006').Status_Message__c));
            return null;
        }
        
        try{
            
            TMBServiceProxy.SearchResultDTO searchResult;
            string accttype = account.Customer_Type_Temp__c.substring(0,1);
            Type_of_ID__c typeid = null;
            String acctname=null;             
            accounts = new List<Account>();
            System.debug('Button : '+isButtonDisabled);           
            //calling api to check the duplication 2 minit
            TMBServiceProxy.TMBServiceProxySoap tmbService = new TMBServiceProxy.TMBServiceProxySoap();
            tmbService.timeout_x = 120000 ;       

            
            // ktc add for rsa soap service call
            tmbService = SoapRsa.setSecureHeader(tmbService);


            /*
            AppConfig__c mc = AppConfig__c.getValues('CertName');
            System.debug(' Cername : '+ mc.Value__c);
            string userName =  ii_UserInfoEx.EmployeeId;           
            
            Map<String,String> keys = new  Map<String,String>();
            string signed = ii_SecurityCryptoManager.SignWithCertificate(userName, mc.Value__c); 
            keys.put('SFDC-SIGN', signed);
            keys.put('SFDC-USR-ID', userName);
            tmbService.inputHttpHeaders_x = keys; 
            */
            // tmbService.inputHttpHeaders_x 


            if(account.ID_Type__c != null  &&  account.ID_Type__c != '' && account.ID_Type__c !='--None--'){
                typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name =:account.ID_Type__c LIMIT 1];
            }
            else{
                typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name ='--None--' LIMIT 1];
            }
            // Juristic Company name
            if(account.Customer_Type_Temp__c =='Juristic'){
                acctname = account.First_name__c;
            }
            // Fist name only
            else if(account.First_name__c != null && account.Last_name__c == null){
                acctname = account.First_name__c+' *';
            }
            // Last name only
            else if(account.First_name__c == null && account.Last_name__c != null){
                acctname ='* '+account.Last_Name__c;                
            }
            // Individual Full name
            else{
                acctname = account.First_name__c+' '+account.Last_name__c;
            }
            
            
            //Check Partial search, First Name and Last Name should be more than 5 digits.
            //Edit by: danudath.lee@tmbbank.com
            if((acctname != null && acctname != '') && 
              (account.ID_number_Temp__c == '' || account.ID_number_Temp__c == null) &&              
              (account.Office_Number_Temp__c == '' || account.Office_Number_Temp__c == null ) && 
              (account.Mobile_Number_Temp__c == '' || account.Mobile_Number_Temp__c == null)){
                String nameRemoveSpec = acctname.replaceAll(' ', '');
                nameRemoveSpec = nameRemoveSpec.replace('*', '');                     
                if(nameRemoveSpec.length() < 5 && nameRemoveSpec != ''){               
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3022').Status_Message__c));
                    return null;
                }
            }
            
            System.debug('Searce Name : ' + acctname);
            System.debug('Account Type[J,I] : ' + accttype);
            // Custom Setting
            // Type of Id
            System.debug('Id Type : ' + typeid.Value__c);
            System.debug('Id Number : ' + account.ID_Number_Temp__c);
            System.debug('Customer Id : ' + account.TMB_Customer_ID_Temp__c);
            
            
            if(account.ID_Number_Temp__c != null  && typeid.Value__c != null ){
                System.debug('::: Search Only Id ::::');
                //searchResult = tmbService.Search_x('',accttype,typeid.Value__c,account.ID_number_Temp__c,'','','',  UserInfo.getUserId(), UserInfo.getUserName());
                searchResult = tmbService.SearchPartial('',accttype,typeid.Value__c,account.ID_number_Temp__c,'','','',  UserInfo.getUserId(), UserInfo.getUserName(),'true');
            }
            else if(acctname != null && acctname != 'null null'){ 
                System.debug('ONLY NAME :'+acctname);
                System.debug(':::: Search Only Name ::::');
                isSearchNameonly = true;
                //System.debug('Search Value : '+acctname+' : '+accttype+' : '+account.Office_Number_Temp__c+' : '+account.Mobile_Number_Temp__c+' : '+UserInfo.getUserId()+' : '+UserInfo.getUserName());
                //searchResult = tmbService.Search_x(acctname,accttype,'','',account.Office_Number_Temp__c,'',account.Mobile_Number_Temp__c,  UserInfo.getUserId(), UserInfo.getUserName()); 
                searchResult = tmbService.SearchPartial(acctname,accttype,'','',account.Office_Number_Temp__c,'',account.Mobile_Number_Temp__c,  UserInfo.getUserId(), UserInfo.getUserName(),'true'); 
           
            }
            // FIX :  When User Not Input Name But input Only Phone Number           
            else if(
                (acctname == 'null null' || acctname =='null' || acctname == null) 
                && 
                (account.Office_Number_Temp__c != null || account.Mobile_Number_Temp__c != null) 
            ){
                
                System.debug(':::: Search Only PHONE ::::');
                //searchResult = tmbService.Search_x('',accttype,'','',account.Office_Number_Temp__c,'',account.Mobile_Number_Temp__c,  UserInfo.getUserId(), UserInfo.getUserName()); 
                searchResult = tmbService.SearchPartial('',accttype,'','',account.Office_Number_Temp__c,'',account.Mobile_Number_Temp__c,  UserInfo.getUserId(), UserInfo.getUserName(),'true');
                // isSearchNameonly = false;
                mobileOnly = true;
            }            
            else if(account.TMB_Customer_ID_Temp__c != null || account.TMB_Customer_ID_Temp__c != '' ){
                System.debug(':::: Search TMB CUST ID TEMP ::::');
                System.debug('tmbService.Search_X : '+accttype); 
                System.debug('tmbService.Search_X : '+account.TMB_Customer_ID_Temp__c);
                System.debug('tmbService.Search_X : '+UserInfo.getUserId());
                System.debug('tmbService.Search_X : '+ UserInfo.getUserName());
               /* searchResult =  tmbService.Search_x('',accttype,'','','',
                                                    ((account.TMB_Customer_ID_Temp__c==null)?'':account.TMB_Customer_ID_Temp__c+''),
                                                    '',  UserInfo.getUserId(), UserInfo.getUserName()); */
                searchResult =  tmbService.SearchPartial('',accttype,'','','',
                                                    ((account.TMB_Customer_ID_Temp__c==null)?'':account.TMB_Customer_ID_Temp__c+''),
                                                    '',  UserInfo.getUserId(), UserInfo.getUserName(), 'true');
            }
            
            // SAOP Result
            System.debug('status : '+searchResult.status);
            System.debug('total record : '+searchResult.totalrecord);
            System.debug('message : '+searchResult.massage);        
            Status_Code__c statuscode = [SELECT Name,Status_Message__c,isError__c FROM Status_Code__c WHERE Name =: searchResult.status];
            
            // Case 1) Call Success
            if(!statuscode.isError__c){         
                TMBServiceProxy.ArrayOfSearchDataDTO arrayOfSearch = searchResult.Datas;           
                TMBServiceProxy.SearchDataDTO[] searchArr = arrayOfSearch.SearchDataDTO;                       
                //if Datas return data(duplication occurs)
                
                //Edit search result by : danudath.lee@tmbbank.com
                //Date 27/10/2015
                if(searchArr!=null){
                    boolean isHas = false;                   
                    List<ID> SfIdList = new List<ID>();
                    for(TMBServiceProxy.SearchDataDTO search : searchArr){                                       
                        if(search.SF_ID != null  && search.SF_ID !=''){                            
                            SfIdList.add(search.SF_ID); 
                            isHas =true;
                        }                            
                
                    } 

                    if(!isHas){                      
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,Status_Code__c.GetValues('1012').Status_Message__c));
                        System.debug(logginglevel.ERROR,'Return Data From CRM DB does\'t have salesforce id');
                        isButtonDisabled = true;
                        return null;
                    }
              
                    try{                   
                        accountMap = new Map<ID,Account>([SELECT OwnerId,Owner.Name ,Owner.Phone ,Owner.Segment__c,Segment2__c, Owner.MobilePhone FROM Account WHERE ID IN:SfIdList]);                        
                    }
                    catch(QueryException E){
                        isButtonDisabled = true;
                      
                        System.debug(logginglevel.ERROR,'Query Exception: '+e.getMessage());
                        return null;
                    }
                    System.debug('SIZE : '+accountMap.size());
                    System.debug('Account List = '+accountMap);
                    if(accountMap.size()>0){
                    	for(TMBServiceProxy.SearchDataDTO search : searchArr){ 
                            if(search.SF_ID != null  && search.SF_ID != ''){
                                Account newAcct = accountMap.get(search.SF_ID);                             
                                if(newAcct!=null){
                                    System.debug('Current User : '+UserInfo.getUserId());
                                    System.debug('Account Owner : '+newAcct.OwnerId);
                                    if(UserInfo.getUserId()==newAcct.OwnerId){
                                        System.debug('Same Owner');
                                        isSameOwner =true; 
                                    }
                                    newAcct.Mobile_Number_Temp__c = search.PRI_PH_NBR;
                                    newAcct.Company_Name_Temp__c  =  search.FNAME +' '+search.LNAME;
                                    newAcct.Customer_Type__c = search.CUST_TYPE_CD;
                                    newAcct.ID_Type_Temp__c = search.ID_TYPE;
                                    newAcct.ID_Number_Temp__c = search.ID_NUMBER;
                                    newAcct.Account_Type__c = search.CUST_PROS_TYPE;
                                    newAcct.Segment__c = search.SUGGEST_SEGMENT;
                                    newAcct.Address_Line_1_Temp__c = search.PRI_ADDR1;
                                    newAcct.Address_Line_2_Temp__c = search.PRI_ADDR2;
                                    newAcct.Address_Line_3_Temp__c = search.PRI_ADDR3;
                                    newAcct.Province_Temp__c = search.PRI_CITY;
                                    newAcct.Zip_Code_Temp__c = search.PRI_POSTAL_CD;
                                    accounts.add(newAcct);
                                }
                            }
                        }
                    }
                    //check whether     
                    System.debug('issame :'+isSameOwner);
                    
                    if(searchResult.status =='0005'){
                        System.debug('Not Allow To Create');                        
                        ApexPages.addmessage(ErrorHandler.Messagehandler(FOUND_DUP_MORE_THAN_30_NOT_ALLOW_CREATE, ''));
                        
                        isButtonDisabled = true;
                    }
                    
                    else if(mobileOnly)
                    {
                        System.debug('Allow To Create');
                        ApexPages.addmessage(ErrorHandler.Messagehandler(FOUND_DUP_ALLOW_CREATE, ''));
                        isButtonDisabled = false;
                    }                    
                    else if(ErrorHandler.isAllowToCreate(searchResult)&&isSearchNameonly&&isSameOwner==false){
                        System.debug('Allow To Create');
                        ApexPages.addmessage(ErrorHandler.Messagehandler(FOUND_DUP_ALLOW_CREATE, ''));
                        isButtonDisabled = false;
                    }
                    else{
                        ApexPages.addmessage(ErrorHandler.Messagehandler(FOUND_DUP_NOT_ALLOW_CREATE, ''));
                        // KTC Change 2014-11-21 12:41
                        // isButtonDisabled = true;
                        System.debug('Not Allow to Create');
                        isButtonDisabled = true;
                    }              
                    return null;               
                }
                //if searchArr == null, allow create
                isButtonDisabled = false;
            }
            else{
                isButtonDisabled = true;
            }
            System.debug('Buttonif : '+isButtonDisabled);
            System.debug('Status Code : '+searchResult.status);
            ApexPages.addmessage(ErrorHandler.Messagehandler(searchResult.status, searchResult.totalRecord,searchResult.massage));
            return null;
        }
        catch(CalloutException e){
            isButtonDisabled = true;
            ApexPages.addmessage(ErrorHandler.Messagehandler(CALLOUT_EXCEPTION_CODE, Status_Code__c.GetValues('1002').Status_Message__c));
            System.debug(logginglevel.ERROR,'Callout Error: '+e.getMessage());
            return null;
        }
        
        return null;
    }
    public void resetNextButton(){
        isButtonDisabled = true;        
    }
    
    //region  Redirect
    
    public PageReference next(){        
        PageReference accountCreationPage = Page.AccountCreation;
        accountCreationPage.setRedirect(true);
        accountCreationPage.getParameters().put('customer_type',account.Customer_Type_Temp__c);
        accountCreationPage.getParameters().put('company_name',account.First_name__c); //First Name / Company name
        accountCreationPage.getParameters().put('first_name',account.First_name__c);
        accountCreationPage.getParameters().put('last_name',account.Last_name__c);
        accountCreationPage.getParameters().put('mobile_number',account.Mobile_Number_Temp__c);
        accountCreationPage.getParameters().put('office_number',account.Office_Number_Temp__c);
        accountCreationPage.getParameters().put('id_type',account.ID_Type__c);
        accountCreationPage.getParameters().put('id_number',account.ID_Number_Temp__c);
        accountCreationPage.getParameters().put('tmb_cust_id',account.TMB_Customer_ID_Temp__c);
        System.debug('NEXT BUTTON : '+account.ID_Type__c);
        return accountCreationPage;
    }     
    
    // When user click to view sales
    public PageReference viewOwner(){
        PageReference userPage;
        try{
            System.debug('OWNER : '+ownerId);
            ownerUser = [SELECT Name FROM User WHERE ID=:ownerId];        
            userPage = new ApexPages.StandardController(ownerUser).view();
            userPage.setRedirect(true);
            
        }catch(QueryException E){
            ApexPages.addmessage(ErrorHandler.Messagehandler(QUERY_EXCEPTION_CODE, Status_Code__c.GetValues('1001').Status_Message__c));
            System.debug(logginglevel.ERROR,'Query Exception: '+e.getMessage());
            return null;
        }       
        return ApexPages.hasMessages() ? null : userPage;   
    }
    
    
    // When user click to view prospect or customer
    public PageReference viewProspect(){
        PageReference accountPage;
        try{    
            viewAccount = [SELECT Name FROM Account WHERE ID=:acctId];
            // Set Id to standard view for redirect
            accountPage = new ApexPages.StandardController(viewAccount).view();
            accountPage.setRedirect(true);
            
        }catch(QueryException e){
            /* QueryException   
* Any problem with SOQL queries, such as assigning a query that returns no records 
* or more than one record to a singleton sObject variable.
* */
            ApexPages.addmessage(ErrorHandler.Messagehandler(QUERY_EXCEPTION_CODE, Status_Code__c.GetValues('1001').Status_Message__c));
            System.debug(logginglevel.ERROR,'Query Exception: '+e.getMessage());          
        }
        return ApexPages.hasMessages() ? null : accountPage;       
        
    }
    
    //endregion
}