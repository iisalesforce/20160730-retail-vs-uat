/*
* Create by : Keattisak Chinburarat
* Email : ktc@ii.co.th
* Create Date : 2015-09-03
* */
@isTest
private class TMBAccountPlanServiceProxyTest {    
    @isTest static void coverGeneratedCodeTypes() {
        Test.setMock(WebServiceMock.class, new TMBAccountPlanServiceProxyMock());
        
        TMBAccountPlanServiceProxy.AccountPlantProxySoap prarent =   new TMBAccountPlanServiceProxy.AccountPlantProxySoap();
        new TMBAccountPlanServiceProxy.QueryCustomerInfosResponse_element();
        new TMBAccountPlanServiceProxy.CustomerInfoDTO();
        new TMBAccountPlanServiceProxy.QueryCustomerInfos_element();
        new TMBAccountPlanServiceProxy.ArrayOfCUSTOMER_INFO();
        new TMBAccountPlanServiceProxy.CUSTOMER_INFO();
    }    
    @isTest static void coverCodeForQueryCustomerInfos(){
        Test.setMock(WebServiceMock.class, new TMBAccountPlanServiceProxyMock());
        TMBAccountPlanServiceProxy.AccountPlantProxySoap testObject = new TMBAccountPlanServiceProxy.AccountPlantProxySoap();		
        System.assertNotEquals(null, testObject.QueryCustomerInfos('XXXXXX'));
        
    }    
    // Factory Service Code
    // Static helper method
    @isTest static void P_FoundOneCustomerById(){
        Test.setMock(WebServiceMock.class, new TMBAccountPlanServiceProxyMock());
        // 1)  Arrange
        // Set up  Custom Setting for SoapRsa
        List<AppConfig__c> mc = new List<AppConfig__c>();
        mc.Add(new  AppConfig__c(Name ='CertName' , Value__c= 'TMB_RSA'));
        mc.Add(new  AppConfig__c(Name ='runtrigger' , Value__c= 'false'));
        insert mc;
        
        test.startTest();
        // 2) Act
        
        TMBAccountPlanServiceProxy.CUSTOMER_INFO[] result = TMBAccountPlanServiceProxy.getCustomerByIds('sfidmockdata');  
        test.stopTest();
        
        // 3) Assert 
        System.assertNotEquals(null, result);      
        // Should Found 1 Customer With SF_ID : sfidmockdata
        System.assertEquals(1, result.size());        
        System.assertEquals('sfidmockdata', result[0].SF_ID);
    }
}