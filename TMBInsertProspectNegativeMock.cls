@isTest
global class TMBInsertProspectNegativeMock implements WebServiceMock{
	public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType) {
       
		System.debug(LoggingLevel.INFO, 'TMBServiceProxyMockImpl.doInvoke() - ' +
			'\n request: ' + request +
			'\n response: ' + response +
			'\n endpoint: ' + endpoint +
			'\n soapAction: ' + soapAction +
			'\n requestName: ' + requestName +
			'\n responseNS: ' + responseNS +
			'\n responseName: ' + responseName +
			'\n responseType: ' + responseType);
                
                
              TMBServiceProxy.ProspectResultDTO InsertResult = new TMBServiceProxy.ProspectResultDTO(); 
                InsertResult.status = '0001';
                InsertResult.totalrecord = '0';
                InsertResult.massage = 'insert failed';
                
                
                TMBServiceProxy.InsertPospectResponse_element respondelement = new TMBServiceProxy.InsertPospectResponse_element();
       			respondelement.InsertPospectResult = InsertResult;
                response.put('response_x', respondelement);
            }
}