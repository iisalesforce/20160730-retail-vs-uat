@isTest
public class AccountConsoleExtensionTest {
  
   
  static testMethod void validateAccountConsole() {

        TestUtils.createIdType();
        TestUtils.createAppConfig();
        TestUtils.createStatusCode();
        TestUtils.createDisqualifiedReason();
        TestUtils.createDateOfBirth();
         Map<String,ID> addressmap = TestUtils.CreateAddress();
        
        Account acct = TestUtils.createAccounts(1,'webviceTESTACCOUNT','Individual', true).get(0);
  
        ApexPages.StandardController sc = new ApexPages.StandardController(acct);
        AccountConsoleExtension console = new AccountConsoleExtension(sc);  
        
      try{
         Product2 TransBankp = new Product2(Name = 'TransBankp',Product_Domain__c = '1. Transactional Banking' );
         Product2 DepVestp = new Product2(Name = 'DepVestp',Product_Domain__c = '2. Deposit and Investment' );
         Product2 FundBorrowp = new Product2(Name = 'FundBorrowp',Product_Domain__c = '3. Funding & Borrowing' );  
         Product2 Riskp = new Product2(Name = 'Riskp',Product_Domain__c = '4. Risk Protection' );
              insert TransBankp;
              insert DepVestp;
              insert FundBorrowp;
              insert Riskp;
      
         //Product_Information_On_Hand__c TransBankId = [select id,Product_Hierachy_Code__r.Id from Product_Information_On_Hand__c where Product_Hierachy_Code__r.Product_Domain__c = '1. Transactional Banking' LIMIT 1];
        Id TransBankId = [SELECT id FROM Product2 where Product_Domain__c = '1. Transactional Banking' LIMIT 1].Id;
        Id DepVestId = [SELECT id FROM Product2 where Product_Domain__c = '2. Deposit and Investment' LIMIT 1].Id;
        Id FundBorrowId = [SELECT id FROM Product2 where Product_Domain__c = '3. Funding & Borrowing' LIMIT 1].Id;
        Id RiskId = [SELECT id FROM Product2 where Product_Domain__c = '4. Risk Protection' LIMIT 1].Id;
         //System.debug('TransBankId '+TransBankId);
          /*System.debug('DepVestId '+DepVestId);
          System.debug('FundBorrowId '+FundBorrowId);
          System.debug('RiskId '+RiskId);*/
          if(TransBankId!= null){
            Product_Information_On_Hand__c TransBank = new Product_Information_On_Hand__c( Account__c =  acct.id,Product_Hierachy_Code__c =  TransBankId );
            //Product_Information_On_Hand__c TransBank = new Product_Information_On_Hand__c( Account__c =  acct.id,Product_Hierachy_Code__c =  '01t90000004tQTbAAM' );
            insert TransBank;
          }
        if(DepVestId!= null){  
            Product_Information_On_Hand__c DepVest = new Product_Information_On_Hand__c( Account__c =  acct.id,Product_Hierachy_Code__c =  DepVestId );
          //Product_Information_On_Hand__c DepVest = new Product_Information_On_Hand__c( Account__c =  acct.id,Product_Hierachy_Code__c =  '01t90000004tQTOAA2' );
          insert DepVest;
        }
        if(FundBorrowId!= null){    
          Product_Information_On_Hand__c FundBorrow = new Product_Information_On_Hand__c( Account__c =  acct.id,Product_Hierachy_Code__c =  FundBorrowId );
          //Product_Information_On_Hand__c FundBorrow = new Product_Information_On_Hand__c( Account__c =  acct.id,Product_Hierachy_Code__c =  '01t90000004tQTFAA2' );
          insert FundBorrow;
        }
         if(RiskId!= null){     
          Product_Information_On_Hand__c Risk = new Product_Information_On_Hand__c( Account__c =  acct.id,Product_Hierachy_Code__c =  RiskId );
          //Product_Information_On_Hand__c Risk = new Product_Information_On_Hand__c( Account__c =  acct.id,Product_Hierachy_Code__c =  '01t90000004tQTUAA2' );
          insert Risk;
         } 
      }catch(Exception ex){
          
      } 
        //console.getLoanClassification();
        //console.getSCFScore();
        //console.getServiceClass();
        console.getPieData();
        console.getOnhand();
        console.getPipelineReportId();
        console.getReportId();
        console.getNonPipelineReportId();
        console.ActivitySummary();
        console.getWallet();
        system.assertEquals(1, console.SumTransBank);
        system.assertEquals(1, console.SumDepInvest);
        system.assertEquals(1, console.SumFundBorrow);
        system.assertEquals(1, console.SumRiskProtect);
      
        console.acct.Loan_Classification__c='NNN';
        //console.getLoanClassification();
        
        console.acct.SCF_Score__c=20;
        //console.getSCFScore();
        
        //console.acct.Service_class__c='Silver';
        //console.getServiceClass();

        //console.acct.Service_class__c='gold';
        //console.getServiceClass();
      
        //console.acct.Service_class__c='platinum';
        //console.getServiceClass();
            
  }
    
}