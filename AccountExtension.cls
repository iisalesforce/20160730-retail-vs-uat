public virtual without sharing class AccountExtension implements Database.AllowsCallouts {
     /*
    public Account acct;
    public ID acctId{get;set;}
    public ID ownerId {get; set;}
    public String mode {get;set;}
    public String owner_id {get;set;}
    public List<Account> accounts; 
    public Map<ID,Account> accountMap;
    public static Boolean isValid;    
    public static Boolean isButtonDisabled {get; set;}   
    public static final String SUCCESS_CODE = '0000';
    public static final String QUERY_EXCEPTION_CODE = '1001';
    public static final String CALLOUT_EXCEPTION_CODE = '1002';
    public static final String FOUND_DUP_ALLOW_CREATE = '1010';
    public static final String FOUND_DUP_NOT_ALLOW_CREATE = '1011';
    private User ownerUser;
    private Account viewAccount;
    private Type_of_ID__c typeid;
    private String accttype;
    private String acctname;
    public boolean isDisqualified {get;set;}
    public boolean isIDNumberRequired {get;set;}
    public boolean isCusonly {get;set;}
    public boolean isInformation {get;set;}
    public boolean isIDValid {get;set;}
    public String selectconid {get;set;}
    public boolean isSearchNameonly {get;set;}
    public boolean isSameOwner {get;set;}
    public ID selectedID;
    private List<ID> SfIdList;
    //Attributes on Search page
    public String FirstNamestr {get;set;}
    public String LastNamestr {get;set;}
    public String MobileNumstr {get;set;}
    public String OfficeNumstr {get;set;}
    public String IDTypestr {get;set;}
    public List<SelectOption> IDTypeOptionList {get;set;}
    public String IDNumberstr {get;set;}
    public String CustTypestr {get;set;}
    public List<SelectOption> CustTypeOptionList {get;set;}
    public String TMBCustStr {get;set;}
    //constructor
    public AccountExtension(ApexPages.StandardController controller){
        acct = (Account)controller.getRecord();
        accounts = new List<Account>();
        //accountMap = new Map<ID,Account>();
        isButtonDisabled = true;
        IDTypeOptionList = new List<SelectOption>();
        IDTypeOptionList.add(new SelectOption('','--None--'));
        SetSelectOptionList();
        //Account Acctt = [SELECT ID,IsDisqualified__c FROM Account WHERE ID =: acct.id LIMIT 1];
        //isDisqualified = Acctt.IsDisqualified__c;
    }
    
    public AccountExtension(ApexPages.StandardSetController controller){
        acct = (Account)controller.getRecord();
        acct.Customer_Type__c = 'Individual';
        //accountMap = new Map<ID,Account>();
        IDTypeOptionList = new List<SelectOption>();
        IDTypeOptionList.add(new SelectOption('','--None--'));
        accounts = new List<Account>();
        isButtonDisabled = true;
        SetSelectOptionList();
    }
    //list for the account responsed from api
    public List<Account> getAccounts(){
        return accounts;
    }
    
    
    public void SetSelectOptionList (){

        CustTypeOptionList = new List<SelectOption>();
        CustTypeOptionList.add(new SelectOption('','--None--'));
        CustTypeOptionList.add(new SelectOption('Individual','Individual'));
        CustTypeOptionList.add(new SelectOption('Juristic','Juristic'));
   
    }
    
    
    public PageReference search(){
        isButtonDisabled = true;
        isSearchNameonly =false;
        isSameOwner = false;
        if(IDTypeStr == ''){
            IDTYpeStr =null;
        }
        if(IDNumberStr ==''){
            IDNumberStr =null;
        }
        if(FirstNameStr ==''){
            FirstNameStr =null;
        }
                if(MobileNumStr ==''){
            MobileNumStr =null;
        }
                if(LastNameStr ==''){
            LastNameStr =null;
        }
                if(OfficeNumStr ==''){
            OfficeNumStr =null;
        }
        if(TMBCustStr ==''){
            TMBCustStr =null;
        }
        
                    accounts = new List<Account>();
         if(CustTypeStr == '--None--'){
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3002').Status_Message__c));
                   return null;
         }
        if(!isIDValid&&IDTypeStr=='Citizen ID'){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3001').Status_Message__c));
            return null;
        }


        
        if(isInformation){
        
        if(CustTypestr =='Individual'
           &&(
               (IDNumberStr==null)
           &&(IDTypeStr == null )
          &&(FirstNameStr == null )
          &&(MobileNumStr == null)
          &&(LastNameStr == null )
          &&(OfficeNumStr == null )
          )
           ){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3011').Status_Message__c));
            return null;
        }
        if(CustTypeStr=='Juristic'
           &&(
               (IDNumberStr==null)
           &&(IDTypeStr == null )
          &&(FirstNameStr == null )
          &&(MobileNumStr == null)
          &&(LastNameStr == null )
          &&(OfficeNumStr == null )
          )
          ){
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3011').Status_Message__c));
            return null;            
        }
             
            if((IDTypeStr!=null)&&(IDnumberStr==null)){
               ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3004').Status_Message__c));
               return null;
            }
        }else if(isCusOnly){
            if(TMBCustStr == null){
               ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3005').Status_Message__c));
               return null;
            }
        }
        
        System.debug('Acct ID TYpe : '+IDTypeStr);
        System.debug('Acct ID Number : '+IDNumberStr);
        
        if(IDTypeStr == null && IDNumberStr !=null){
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3006').Status_Message__c));
                   return null;
        }
            
                   
        try{

            System.debug('Button : '+isButtonDisabled);
            //calling api to check the duplication
            TMBServiceProxy.TMBServiceProxySoap tmbService = new TMBServiceProxy.TMBServiceProxySoap();
            tmbService.timeout_x = 120000 ;
            // Sequence (String name,String customerType,String idType,String idNumber,String officeNumber,String customerId,String mobileNumber)
        
            if(CustTypeStr!=null && CustTypeStr!=''){
               accttype = CustTypeStr.substring(0,1); 
            }else{
                accttype='';
            }
            
            if(IDTypeStr !=null && IDTypeStr !=''&& IDTypeStr !='null'){
                typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name =:IDTypeStr LIMIT 1];
            }else{
                typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name ='--None--' LIMIT 1];
            }
               
            if(CustTypeStr =='Juristic'){
                acctname = FirstNameStr;
            }
            else if((FirstNameStr != null)&& (LastNameStr==null)   ){
                acctname = FirstNameStr+' *';
            }
            else if((FirstNameStr == null ) && (LastNameStr!=null) ){
            
                 acctname ='* '+LastNameStr;
                
            }
            else{
                     acctname = FirstNameStr+' '+LastNameStr;
             }
            
            System.debug('acctname: '+acctname);
            System.debug('accttype: '+accttype);
            System.debug('id type: '+typeid.Value__c);
            System.debug('CUSID : '+CustTypeStr);
        
            //call search api   
            //
                            TMBServiceProxy.SearchResultDTO searchResult;
            
            System.debug('acct.ID_Number_Temp__c : '+IDNumberStr);
            System.debug('typeid.Value__c : '+typeid.Value__c);
            
             if((IDNumberStr !=null )
                   && 
                 typeid.Value__c != null){
                    System.debug('::: Search Only Id ::::');
                   searchResult = tmbService.Search_x('',accttype,typeid.Value__c,IdNumberStr,'','','',  UserInfo.getUserId(), UserInfo.getUserName()); 

                }
            else if (acctname != null && acctname != 'null null'){
                 
             
            System.debug('ONLY NAME :'+acctname);
                     System.debug(':::: Search Only Name ::::');
                    isSearchNameonly = true;
                    System.debug('Search Value : '+acctname+' : '+accttype+' : '+OfficeNumStr+' : '+MobileNumStr+' : '+UserInfo.getUserId()+' : '+UserInfo.getUserName());
                
                    searchResult = tmbService.Search_x(acctname,accttype,'','',OfficeNumStr,'',MobileNumStr,  UserInfo.getUserId(), UserInfo.getUserName()); 
                    
             }
            //` FIX :  When User Not Input Name But input Only Phone Number
            else if ((acctname != null && acctname != 'null null') && ((OfficeNumStr != null ) || (MobileNumStr != null ))  ){
                     System.debug(':::: Search Only PHONE ::::');
                    searchResult = tmbService.Search_x('',accttype,'','',OfficeNumStr,'',MobileNumStr,  UserInfo.getUserId(), UserInfo.getUserName()); 
                    
             }
            
            else if(TMBCustStr != null){
                    System.debug(':::: Search TMB CUST ID TEMP ::::');
                System.debug('tmbService.Search_X : '+accttype); 
                System.debug('tmbService.Search_X : '+TMBCustStr);
                System.debug('tmbService.Search_X : '+UserInfo.getUserId());
                System.debug('tmbService.Search_X : '+ UserInfo.getUserName());
               searchResult =  tmbService.Search_x('',accttype,'','','',
                                                   ((TMBCustStr==null || TMBCustStr =='')?'':TMBCustStr+''),
                                                                           '',  UserInfo.getUserId(), UserInfo.getUserName());
            }
            
            


            System.debug('status : '+searchResult.status);
            System.debug('total record : '+searchResult.totalrecord);
            System.debug('message : '+searchResult.massage);
        
            Status_Code__c statuscode = [SELECT Name,Status_Message__c,isError__c FROM Status_Code__c WHERE Name =: searchResult.status];
            
            //callout success
            if(!statuscode.isError__c){
            
                TMBServiceProxy.ArrayOfSearchDataDTO arrayOfSearch = searchResult.Datas;
                System.debug('array Of Search'+arrayOfSearch);
                TMBServiceProxy.SearchDataDTO[] searchArr = arrayOfSearch.SearchDataDTO;
                System.debug('search Arr'+searchArr);
        
                //if Datas return data(duplication occurs)
                if(searchArr!=null){
                    SfIdList = new List<ID>();
                    for(TMBServiceProxy.SearchDataDTO search : searchArr){

                            System.debug('ID : '+search.SF_ID);
                            String str = search.SF_ID;
                            System.debug(str.length());
                        if(str.length() == 18 || str.length() == 15){
                            SfIdList.add(search.SF_ID); 
                        }                       

                    }
                    
                    try{                   
                        accountMap = new Map<ID,Account>([SELECT Segment2__c,OwnerId,Owner.Name ,Owner.Phone , Owner.MobilePhone FROM Account WHERE ID IN:sfIDList]);                        
                        }catch(QueryException E){
                            isButtonDisabled = true;
                            ApexPages.addmessage(ErrorHandler.Messagehandler(QUERY_EXCEPTION_CODE, Status_Code__c.GetValues('1001').Status_Message__c));
                            System.debug(logginglevel.ERROR,'Query Exception: '+e.getMessage());
                            return null;
                        }
                   System.debug('SIZE : '+accountMap.size());
                    for(TMBServiceProxy.SearchDataDTO search : searchArr){
                        if(accountMap.size()>0){
                            Account newAcct = accountMap.get(search.SF_ID);
                            if(newAcct!=null){
                                System.debug('Current User : '+UserInfo.getUserId());
                                System.debug('Account Owner : '+newAcct.OwnerId);
                                if(UserInfo.getUserId()==newAcct.OwnerId){
                                    System.debug('Same Owner');
                                   isSameOwner =true; 
                                }
                                newAcct.Mobile_Number_Temp__c = search.PRI_PH_NBR;
                                newAcct.Company_Name_Temp__c  =  search.FNAME +' '+search.LNAME;
                                newAcct.Customer_Type__c = search.CUST_TYPE_CD;
                                newAcct.ID_Type_Temp__c = search.ID_TYPE;
                                newAcct.ID_Number_Temp__c = search.ID_NUMBER;
                                newAcct.Account_Type__c = search.CUST_PROS_TYPE;
                                newAcct.Segment__c = search.SUGGEST_SEGMENT;
                                newAcct.Address_Line_1_Temp__c = search.PRI_ADDR1;
                                newAcct.Address_Line_2_Temp__c = search.PRI_ADDR2;
                                newAcct.Address_Line_3_Temp__c = search.PRI_ADDR3;
                                newAcct.Province_Temp__c = search.PRI_CITY;
                                newAcct.Zip_Code_Temp__c = search.PRI_POSTAL_CD;
                                accounts.add(newAcct);
                            }
                        }
                    }
                    //check whether     
                    System.debug('issame :'+isSameOwner);
                    
                    if(searchResult.status =='0005'){
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('0005').Status_Message__c));
                         isButtonDisabled = true;
                                           return null;
                    }
                    
                    
                    if(ErrorHandler.isAllowToCreate(searchResult)&&isSearchNameonly&&isSameOwner==false){
                        System.debug('Allow To Create');
                        ApexPages.addmessage(ErrorHandler.Messagehandler(FOUND_DUP_ALLOW_CREATE, ''));
                        isButtonDisabled = false;
                    }else{
                        ApexPages.addmessage(ErrorHandler.Messagehandler(FOUND_DUP_NOT_ALLOW_CREATE, ''));
                        // KTC Change 2014-11-21 12:41
                        // isButtonDisabled = true;
                        System.debug('Not Allow to Create');
                        isButtonDisabled = true;
                    }              
                    return null;               
                }
                //if searchArr == null, allow create
                isButtonDisabled = false;
            }else{
                isButtonDisabled = true;
            }
              System.debug('Buttonif : '+isButtonDisabled);
              System.debug('Status Code : '+searchResult.status);
            ApexPages.addmessage(ErrorHandler.Messagehandler(searchResult.status, searchResult.totalRecord,searchResult.massage));
            return null;  
        }catch(CalloutException e){
            isButtonDisabled = true;
            ApexPages.addmessage(ErrorHandler.Messagehandler(CALLOUT_EXCEPTION_CODE, Status_Code__c.GetValues('1002').Status_Message__c));
            System.debug(logginglevel.ERROR,'Callout Error: '+e.getMessage());
            return null;
        }     
            
                
        
    }
    
    public PageReference next(){
    String tet = IDNumberStr;
        PageReference accountCreationPage = Page.AccountCreation;
        accountCreationPage.setRedirect(true);
        accountCreationPage.getParameters().put('customer_type',CustTypeStr);
        accountCreationPage.getParameters().put('company_name',acct.Company_Name_Temp__c);
        accountCreationPage.getParameters().put('first_name',FirstNameStr);
        accountCreationPage.getParameters().put('last_name',LastNameStr);
        accountCreationPage.getParameters().put('mobile_number',MobileNumStr);
        accountCreationPage.getParameters().put('office_number',OfficeNumStr);
        accountCreationPage.getParameters().put('id_type',IDTypeStr);
        accountCreationPage.getParameters().put('id_number',IDNumberStr);
        accountCreationPage.getParameters().put('tmb_cust_id',TMBCustStr);
        System.debug('NEXT BUTTON : '+IDTypeStr);
        return accountCreationPage;
    }
    
    
    public PageReference viewOwner(){
        PageReference userPage;
        try{
            System.debug('OWNER : '+ownerId);
            ownerUser = [SELECT Name FROM User WHERE ID=:ownerId];        
            userPage = new ApexPages.StandardController(ownerUser).view();
            userPage.setRedirect(true);
         
        }catch(QueryException E){
            ApexPages.addmessage(ErrorHandler.Messagehandler(QUERY_EXCEPTION_CODE, Status_Code__c.GetValues('1001').Status_Message__c));
            System.debug(logginglevel.ERROR,'Query Exception: '+e.getMessage());
            return null;
        }       
       return userPage;
    }
    
    public PageReference viewProspect(){
        PageReference accountPage;
        try{    
            viewAccount = [SELECT Name FROM Account WHERE ID=:acctId];
            accountPage = new ApexPages.StandardController(viewAccount).view();
            accountPage.setRedirect(true);
           
        }catch(QueryException e){
            ApexPages.addmessage(ErrorHandler.Messagehandler(QUERY_EXCEPTION_CODE, Status_Code__c.GetValues('1001').Status_Message__c));
            System.debug(logginglevel.ERROR,'Query Exception: '+e.getMessage());
            return null;
        }
        return accountPage;
        
        
    }
    
    public void resetNextButton(){
        isButtonDisabled = true;
        
    }
    
    public void CheckCustType(){
        
        if(CustTypeStr=='Individual'){
        IDTypeOptionList = new List<SelectOption>();
        IDTypeOptionList.add(new SelectOption('','--None--'));
        IDTypeOptionList.add(new SelectOption('Alien ID','Alien ID'));
        IDTypeOptionList.add(new SelectOption('Citizen ID','Citizen ID'));
        IDTypeOptionList.add(new SelectOption('Passport ID','Passport ID'));
        IDTypeOptionList.add(new SelectOption('Work Permit ID','Work Permit ID'));
        } else if(CustTypeStr =='Juristic'){
                    IDTypeOptionList = new List<SelectOption>();
        IDTypeOptionList.add(new SelectOption('','--None--'));
        IDTypeOptionList.add(new SelectOption('BRN ID','BRN ID'));
        }else{
            IDTypeOptionList = new List<SelectOption>();
        IDTypeOptionList.add(new SelectOption('','--None--'));
        }
    }
    
    
*/
    
    


}