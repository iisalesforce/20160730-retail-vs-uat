public class LeadProxyExtensionCtrl {    
    private final ApexPages.StandardController std ;
    private final Lead Leadrec;
    private RecordType recTypes;
    private string recordTypeId;
	public boolean isCommercial;
	    
    public LeadProxyExtensionCtrl(ApexPages.StandardController controller){
        std = controller;  
        isCommercial = false;
        Leadrec = (Lead)std.getRecord();
		recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');        
        // Get Record TypeID and Name
        System.debug('=== Lead RecordType === ' + recordTypeId);
       
    }
    public PageReference redirectPage(){         
        system.debug(':::: Lead Id  = ' + Lead.Id);
        string url ='/';
        
        //if is Edit Mode
         if(LeadRec.id!=null){
              recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                    where Id  =:Leadrec.RecordTypeId
                   LIMIT 1];
             
             //Commercial
             //
             //
             if(recTypes.DeveloperName.containsIgnoreCase('Commercial')){
                 url = '/apex/LeadCreation?id='+ Leadrec.Id; 
                 isCommercial =true;
             }
             
             //Retail
             if(recTypes.DeveloperName.containsIgnoreCase('Retail')){
                 url = '/apex/RetailLeadCreation?id='+ Leadrec.Id; 
             }
	
         }else{ // Create Mode
			Schema.DescribeSObjectResult dsr = Lead.SObjectType.getDescribe();
            Schema.RecordTypeInfo defaultRecordType;
            
             for(Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
                if(rti.isDefaultRecordTypeMapping()) {
                    defaultRecordType = rti;
                }
            }
            
             if(null == recordTypeId){
                 recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                    	 where Id  =:defaultRecordType.getRecordTypeId()
                   		 LIMIT 1];
             }else{
                 recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                    	 where Id  =:recordTypeId
                   		 LIMIT 1];
             }
             
             //Commercial 
			 if(recTypes.DeveloperName.containsIgnoreCase('Commercial')){
                url = '/apex/LeadCreation';  
                isCommercial =true;
             }
             
             //Retail
             if(recTypes.DeveloperName.containsIgnoreCase('Retail')){
                url = '/apex/RetailLeadCreation';  
             }
                          
        }   
        
        System.debug('URL : '+url);
        if(isCommercial){ 
            PageReference page = new PageReference(url); 
            page.setRedirect(true); 
            return page; 
        }else{
            PageReference page = new PageReference(url); 
            page.setRedirect(true); 
            return page; 
        }
    }

    
 public PageReference redirectMobile(){         
        system.debug(':::: Lead Id  = ' + LeadRec.Id);
         
     	string url ='/';
     	//Edit Mode
         if(LeadRec.id !=null){
              recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                    where Id  =:Leadrec.RecordTypeId
                   LIMIT 1];
             
             //Commercial
             //
             //
             if(recTypes.DeveloperName.containsIgnoreCase('Commercial')){
                 url = '/apex/LeadCreation?id='+ Leadrec.Id;  
                 isCommercial =true;
             }
             
             //Retail
             if(recTypes.DeveloperName.containsIgnoreCase('Retail')){
                url = '/apex/RetailLeadCreation?id='+ Leadrec.Id;
             }     
			
         }
     //Create Mode
     else{
             
			Schema.DescribeSObjectResult dsr = Lead.SObjectType.getDescribe();
            Schema.RecordTypeInfo defaultRecordType;
            for(Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
                if(rti.isDefaultRecordTypeMapping()) {
                    defaultRecordType = rti;
                }
            }
			
         	if(null == recordTypeId){
                 recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                    	 where Id  =:defaultRecordType.getRecordTypeId()
                   		 LIMIT 1];
             }else{
                 recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                    	 where Id  =:recordTypeId
                   		 LIMIT 1];
             }
  			//Commercial
			if(recTypes.DeveloperName.containsIgnoreCase('Commercial')){
				url = '/apex/LeadCreation';  
                isCommercial=true;
            }
         	//Retail
            if(recTypes.DeveloperName.containsIgnoreCase('Retail')){
                url = '/apex/RetailLeadCreation';  
            }    
        }   
        
         if(isCommercial){
             PageReference result = Page.LeadCreateMobile;
             if(LeadRec.id!=null){
                   result.getParameters().put('id',LeadRec.id);
             }
              
             if (recTypes != null ){
                  result.getParameters().put('RecordType',recTypes.id);
             }
               
         
            result.setRedirect(true); 
            return result;
         }else{
            PageReference page = new PageReference(url); 
            page.setRedirect(true); 
            return page; 
         }
    }

    
}