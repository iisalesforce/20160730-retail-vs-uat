public class AccountTriggerHandler {
    static Boolean detectError = false;
    static String errorException = '';
    static String STR_INSERT = 'insert';
    static String STR_UPDATE = 'update';
    static String STR_DELETE = 'delete';
    // NOTE :  When we codding in Before Context we not use update in Accout list because it will update autometically!!!
    public static void handleBeforeInsert(List<Account> accsNew){
        User currentUser = [SELECT ID,employee_id__c FROM USER WHERE ID =: Userinfo.getUserId()];
        for(account acct : accsNew){
           acct.Owner_name_ID__c = currentUser.employee_id__c;
        }
        
        
        
        
        //      DisqualifiedProcessBuilder.CheckDisqualifiedAction(accsNew,null,STR_INSERT);
        
      
    }
    
    public static void handleBeforeUpdate(List<Account> accsNew,List<Account> accsOld){
        //DisqualifiedProcessBuilder.CheckDisqualifiedAction(accsNew,accsOld,STR_UPDATE);
        
        List<Account> listNew = checkConditionT01(accsNew,accsOld);
        if( listNew.size() > 0 ){
            Trigger_T01( listNew ,accsOld,STR_UPDATE);    
        }
        /*
        listNew = checkConditionT04(accsNew,accsOld);
        if( listNew.size() > 0 ){
            Trigger_T04( listNew ,accsOld,STR_UPDATE);    
        }
        */
        listNew = checkConditionT05(accsNew,accsOld);
        if( listNew.size() > 0 ){
            Trigger_T05( listNew ,accsOld,STR_UPDATE);    
        }
        
      
    }
    
    public static void handleAfterUpdate(List<Account> accsNew,List<Account> accsOld){
        List<Account> listNew = checkConditionT06(accsNew,accsOld);
        
        if( listNew.size() > 0 ){
            Trigger_T06( listNew ,accsOld,STR_UPDATE); 
            ChangeOwnerAcctplan(listNew ,accsOld,STR_UPDATE); 
            
        }   
         // DisqualifiedProcessBuilder.CheckDisqualified(accsNew,accsOld);
      
    }
    
    public static List<Account> checkConditionT01(List<Account> accsNew,List<Account> accsOld){
        List<Account> listReturn = new List<Account>();
        Map<Id,Account> mapAccsOld = new Map<Id,Account>();
        if( accsOld != null && accsOld.size() > 0 ){
            mapAccsOld.putAll(accsOld);
        }
                
        for(Account acc : accsNew){
            if( (acc.Re_assign_prospect__c == 'yes' && acc.Re_assign_prospect__c != mapAccsOld.get(acc.Id).Re_assign_prospect__c)
               && ( acc.Account_Type__c == 'Prospect' || acc.Account_Type__c == 'Qualified Prospect' )
               && acc.Suggested_Sub_Segment__c != ''){
                   listReturn.add(acc);
               }
        }
        return listReturn;
    }
    /*
    public static List<Account> checkConditionT04(List<Account> accsNew,List<Account> accsOld){
        List<Account> listReturn = new List<Account>();
        Map<Id,Account> mapAccsOld = new Map<Id,Account>();
        if( accsOld != null && accsOld.size() > 0 ){
            mapAccsOld.putAll(accsOld);
        }
        
        for(Account acc : accsNew){
            if( acc.Account_Type__c == 'Qualified Prospect' && acc.Account_Type__c != mapAccsOld.get(acc.Id).Account_Type__c){
                listReturn.add(acc);
            }
        }
        return listReturn;
    }
    */
    public static List<Account> checkConditionT05(List<Account> accsNew,List<Account> accsOld){
        List<Account> listReturn = new List<Account>();
        Map<Id,Account> mapAccsOld = new Map<Id,Account>();
        if( accsOld != null && accsOld.size() > 0 ){
            mapAccsOld.putAll(accsOld);
        }
        
        for(Account acc : accsNew){
            if(acc.Account_Approval_Status__c == 'Final Approved' 
               && acc.Account_Approval_Status__c != mapAccsOld.get(acc.Id).Account_Approval_Status__c){
                listReturn.add(acc);
            }
        }
        return listReturn;
    }
    
    public static List<Account> checkConditionT06(List<Account> accsNew,List<Account> accsOld){
        List<Account> listReturn = new List<Account>();
        Map<Id,Account> mapAccsOld = new Map<Id,Account>();
        if( accsOld != null && accsOld.size() > 0 ){
            mapAccsOld.putAll(accsOld);
        }
        
        for(Account acc : accsNew){
            if( acc.OwnerId != mapAccsOld.get(acc.Id).OwnerId ){
                listReturn.add(acc);
            }
        }
        return listReturn;
    }
    
    public static void Trigger_T01(List<Account> accsNew,List<Account> accsOld,String eventMode){
        System.debug(':::: Trigger_T01 Start ::::');
        List<Account> listAccNew = new List<Account>();
        Map<String,String> listSuggestedSubSegment = new Map<String,String>();
        List<Prospect_Owner_Assignment__c> listTMBStaffID =  Prospect_Owner_Assignment__c.getall().values();    
        List<User> listUser = [select Id,Employee_ID__c from User];
        Map<String,String> listEmployeeId = new Map<String,String>();
        Map<Id,Account> listAccsOld = new Map<Id,Account>();
        if( eventMode == STR_UPDATE ){
            listAccsOld.putAll(accsOld);    
        }
        
        for (Prospect_Owner_Assignment__c poa : listTMBStaffID){
            if(poa.Name != null && poa.TMB_Staff_ID__c != null){
                listSuggestedSubSegment.put(poa.Name, poa.TMB_Staff_ID__c);
            }
        }
        
        for (User u : listUser){
            if(u.Employee_ID__c != null){
                listEmployeeId.put(u.Employee_ID__c, u.Id);
            }
            
        }
        
        //System.debug(':: '+listSuggestedSubSegment);
        
        for(Account acc : accsNew){
            if( (acc.Re_assign_prospect__c == 'yes' && acc.Re_assign_prospect__c != listAccsOld.get(acc.Id).Re_assign_prospect__c)
               && ( acc.Account_Type__c == 'Prospect' || acc.Account_Type__c == 'Qualified Prospect' )
               && acc.Suggested_Sub_Segment__c != ''){
                   detectError = false;
                   errorException = '';
                   String tmbId = '';
                   Id userId;
                   
                   //System.debug('::::: '+acc.Id +' | '+acc.Name + ' | '+acc.Suggested_Sub_Segment__c);
                   //System.debug('::::: detail input : '+acc);
                   if( listSuggestedSubSegment.containsKey(acc.Suggested_Sub_Segment__c) ){
                       tmbId = listSuggestedSubSegment.get(acc.Suggested_Sub_Segment__c);
                   }else{
                       detectError = true;
                       errorException = 'error containsKey Suggested_Sub_Segment__c : '+acc.Suggested_Sub_Segment__c;
                       acc.Re_assign_prospect__c = 'No';
                       acc.addError( Trigger_Msg__c.getValues('Not_Found_Suggested_Segment').Description__c  ,false); 
                       
                   }
                   
                   if( listEmployeeId.containsKey(tmbId) ){
                       userId = listEmployeeId.get(tmbId);
                   }else{
                       detectError = true;
                       errorException = 'error containsKey tmbId : '+tmbId;
                   }
                   
                   if( !detectError ){
                       acc.OwnerId = userId;
                       acc.Owner_change_notification__c = true;
                       //listAccNew.add(acc);    
                       System.debug('::::: Account Id : '+acc.id +' : Owner Change !! :::::');
                       System.debug('::::: Account Id Old : '+acc.id +' : '+listAccsOld.get(acc.Id).OwnerId+' , '+listAccsOld.get(acc.Id).Owner_change_notification__c+' :::::');
                       System.debug('::::: Account Id New : '+acc.id +' : '+acc.OwnerId+' , '+acc.Owner_change_notification__c+' :::::');
                   }else{
                       System.debug('::::: Account Id : '+acc.id +' : '+errorException+' :::::');
                   }             
               }
        }
        
        //if(listAccNew.size() > 0){
        // update listAccNew; // <===   Not Do this !!  if we want to update in same object in trigger we not update  list like this . Force.com will take care for us
        // }
        //System.debug('::::: List for update '+listAccNew.size()+' row :::::');
        System.debug(':::: Trigger_T01 End ::::');
    }
    /*
    public static void Trigger_T04(List<Account> accsNew,List<Account> accsOld,String eventMode){
        System.debug(':::: Trigger_T04 Start ::::');
        List<Id> ids = new List<Id>();
        Map<Id,Account> listAccsOld = new Map<Id,Account>();
        if( eventMode == STR_UPDATE ){
            listAccsOld.putAll(accsOld);
        }
        for( Account acc : accsNew ){
            ids.add(acc.Id);
        }
        
        Map<Id,AggregateResult> listOpp = new Map<Id,AggregateResult>([select AccountId Id,count(id) counts
                                                                       from Opportunity 
                                                                       where AccountId IN :ids group by AccountId]);
        Map<Id,AggregateResult> listLogACall = new Map<Id,AggregateResult>([select AccountId Id,count(id) counts 
                                                                            from Task
                                                                            where Subject = 'Call' and AccountId IN :ids group by AccountId]);
        Map<Id,AggregateResult> listVisitReport = new Map<Id,AggregateResult>([select Customer_name__c Id,count(id) counts
                                                                               FROM Call_Report__c 
                                                                               WHERE Customer_name__c IN :ids group by Customer_name__c]);
        
        for(Account acc : accsNew){
            if( acc.Account_Type__c == 'Qualified Prospect' && acc.Account_Type__c != listAccsOld.get(acc.Id).Account_Type__c){
                Integer countOpp = 0;
                Integer countLogACall = 0;
                Integer countVisitReport = 0;
                
                if( listOpp.containsKey(acc.Id) ){
                    countOpp = (Integer)listOpp.get(acc.Id).get('counts');
                }
                if( listLogACall.containsKey(acc.Id) ){
                    countLogACall = (Integer)listLogACall.get(acc.Id).get('counts');
                }
                if( listVisitReport.containsKey(acc.Id) ){
                    countVisitReport = (Integer)listVisitReport.get(acc.Id).get('counts');
                }
                
                if( countOpp != 0 && ( countLogACall != 0 || countVisitReport  !=0) ){
                    acc.Account_Type__c = 'Qualified Prospect';
                    acc.Prospect_Converted_Date__c = Date.today();
                    System.debug('::::: Converted Date Success : ' + Date.today()+' :::::');
                }
                System.debug('::::: AccountId is : '+acc.Id+' Opp :' +countOpp+ ' , LogACall : '+countLogACall+' , VisitReport : '+countVisitReport+' :::::');
            }
        }
        System.debug(':::: Trigger_T04 End ::::');
    }
    */
    public static void Trigger_T05(List<Account> accsNew,List<Account> accsOld,String eventMode){
        System.debug(':::: Trigger_T05 Start ::::');
        Map<Id,Account> listAccsOld = new Map<Id,Account>();
        if( eventMode == STR_UPDATE ){
            listAccsOld.putAll(accsOld);
        }
        for(Account acc : accsNew){
            if(acc.Account_Approval_Status__c == 'Final Approved' 
               && acc.Account_Approval_Status__c != listAccsOld.get(acc.Id).Account_Approval_Status__c){
                   acc.OwnerId = acc.Change_to_owner__c;
                   System.debug('::::: Pre Account ID : ' +acc.Id+' : OwnerId : '+acc.OwnerId+' : Change to '+acc.Change_to_owner__c+' ::::');
                   //acc.Owner_change_notification__c = true; //Fah comment 23-02-15
                   acc.Change_to_owner__c = null;
                   System.debug('::::: Post Account ID : ' +acc.Id+' : OwnerId : '+acc.OwnerId+' :::::');
                   //System.debug('::::: Account ID : ' +acc.Id+' : notification : '+acc.Owner_change_notification__c+' :::::');
               }
        }
        
        System.debug(':::: Trigger_T05 End ::::');
    }
    
    public static void Trigger_T06(List<Account> accsNew,List<Account> accsOld,String eventMode){
        System.debug(':::: Trigger_T06 Start ::::');
        List<Id> ids = new List<Id>();
        List<Opportunity> listOppForUpdate = new List<Opportunity>();
        List<Call_Report__c> listReportForUpdate = new List<Call_Report__c>();
        List<Call_Report__Share> callReportShareForInsert = new List<Call_Report__Share>();
        list<task> listTaskForUpdate = new list<task>();
        
        set<string> oldOwnerId = new set<string>();
        for (account tmpold : accsOld)
        {
            oldOwnerId.add(tmpold.ownerid);
        }
        
        Map<Id,Account> listAccsOld = new Map<Id,Account>();
        if( eventMode == STR_UPDATE ){
            listAccsOld.putAll(accsOld);
        }
        for( Account acc : accsNew ){
            ids.add(acc.Id);
        }
        
        List<Opportunity> listOpps =[select Id,AccountId, OwnerId ,PrevOwnerId__c
                                     from Opportunity 
                                     where AccountId IN :ids
                                     and isClosed = false
                                     and StageName IN ('Analysis','Develop & Propose Solution','Follow Up')];
        Map<String,Opportunity> mapOpps = new Map<String,Opportunity>();
        for(Opportunity opp : listOpps){
            if(opp.AccountId != null){
                mapOpps.put(opp.AccountId,opp);
            }  
        }
        List<Opportunity> listForUpdateReverse = new List<Opportunity>(); 
        List<Opportunity> listOppsReverse = [select Id,AccountId, OwnerId ,PrevOwnerId__c
                                     from Opportunity 
                                     where AccountId IN :ids
                                     and isClosed = false
                                     and StageName NOT IN ('Analysis','Develop & Propose Solution','Follow Up')];
        
        List<Call_Report__c> listReports = [select Id,Customer_name__c,OwnerId
                                            from Call_Report__c 
                                            where Status__c like '%Open%' 
                                            and Customer_name__c IN :ids];
        Map<String,Call_Report__c> mapReports = new Map<String,Call_Report__c>();
        for(Call_Report__c report : listReports){
            if(report.Customer_name__c != null){
                mapReports.put(report.Customer_name__c,report);
            }            
        }
        List<Call_Report__c> queryCallReport = [select Id,Customer_name__c,OwnerId
                                            from Call_Report__c 
                                            where (not Status__c like '%Open%')
                                            and Customer_name__c IN :ids];
                                            
        list<task> queryTask = [select id,accountid,ownerId from task where isClosed = false 
                                and accountid in: ids and ownerid in: oldOwnerId];
        
        for(Account acc : accsNew){
            // if change owner
            if( acc.OwnerId != listAccsOld.get(acc.Id).OwnerId ){
                
                for( Opportunity o : listOppsReverse ){
                    if( o.AccountId == acc.Id ){
                        //o.OwnerId = listAccsOld.get(acc.Id).OwnerId;
                        o.Trigger_Flag__c = true;
                        o.ownerId = o.PrevOwnerId__c; 
                        listOppForUpdate.add(o);
                        //listForUpdateReverse.add(o);
                    }
                }
                
                for( Opportunity o : listOpps ){
                    if( o.AccountId == acc.Id ){
                        o.OwnerId = acc.OwnerId;
                        o.Trigger_Flag__c = true;                        
                        listOppForUpdate.add(o);
                        System.debug('::::: List Opp : Change Owner '+listAccsOld.get(acc.Id).OwnerId+' to '+acc.OwnerId+' :::::');
                    }
                }
                
                /*if( mapOpps.containsKey(acc.Id) ){
                    mapOpps.get(acc.Id).OwnerId = acc.OwnerId;
                    listOppForUpdate.add( mapOpps.get(acc.Id) );
                    System.debug('::::: List Opp : Change Owner '+listAccsOld.get(acc.Id).OwnerId+' to '+acc.OwnerId+' :::::');
                }else{
                    System.debug('::::: List Opp : error containsKey '+acc.Id+' :::::');
                }*/
                
                for(Call_Report__c r : listReports){
                    if( r.Customer_name__c == acc.Id ){
                        r.OwnerId = acc.OwnerId;
                        listReportForUpdate.add(r);
                        System.debug('::::: List Report : Change Owner '+listAccsOld.get(acc.Id).OwnerId+' to '+acc.OwnerId+' :::::');
                    }
                }
                
                for( Call_Report__c eachCallReport : queryCallReport ){                    
                    if( eachCallReport.Customer_name__c == acc.Id ){ 
                        Call_Report__Share callReportShare = new Call_Report__Share();
                        callReportShare.ParentId = eachCallReport.Id;
                        callReportShare.UserOrGroupId = acc.OwnerId;
                        callReportShare.AccessLevel = 'Read';
                        callReportShare.RowCause = Schema.Call_Report__Share.RowCause.Manual;
                        callReportShareForInsert.add(callReportShare);
                        System.debug('::::: callReportShare : '+callReportShare+' :::::');
                    }
                }
                
                /*if( mapReports.containsKey(acc.Id) ){
                    mapReports.get(acc.Id).OwnerId = acc.OwnerId;
                    listReportForUpdate.add( mapReports.get(acc.Id) );
                    System.debug('::::: List Report : Change Owner '+listAccsOld.get(acc.Id).OwnerId+' to '+acc.OwnerId+' :::::');
                }else{
                    System.debug('::::: List Report : error containsKey '+acc.Id+' :::::');
                }*/
                
                for (task t : queryTask)
                {
                    if(t.accountid == acc.id)
                    {
                        t.ownerid = acc.ownerid;
                        listTaskForUpdate.add(t);
                        system.debug('::::: Task : Change Owner '+listAccsOld.get(acc.Id).OwnerId+' to '+acc.OwnerId+' :::::');
                    }
                }
                
            }
        }
        /*
        if(listForUpdateReverse.size() > 0){
            try{
                update listForUpdateReverse;    
            }catch (DmlException e){
                System.debug('error : '+e.getMessage());
            }
        } */          
        
        if(listOppForUpdate.size() > 0){
            try{
                update listOppForUpdate;    
            }catch (DmlException e){
                System.debug('error : '+e.getMessage());
            }
        }
        
        if(listReportForUpdate.size() > 0){
            try{
                update listReportForUpdate;
            }catch (DmlException e){
                System.debug('error : '+e.getMessage());
            }            
        }
        System.debug('::::: callReportShareForInsert update size of '+callReportShareForInsert.size()+' row:::::');
        if( callReportShareForInsert.size() > 0 ){
            try{
                insert callReportShareForInsert;
            }catch (DmlException e){
                System.debug('error : '+e.getMessage());
            }
        }
        if (listTaskForUpdate.size() > 0)
        {
            try{
                update listTaskForUpdate;
            }catch(DmlException ex){
                system.debug('error : '+ex.getMessage());
            }
        }
        
        System.debug('::::: ListOpp for update '+listOppForUpdate.size()+' row :::::');
        System.debug('::::: ListOppReverse for update '+listForUpdateReverse.size()+' row :::::');
        System.debug('::::: ListReport for update '+listReportForUpdate.size()+' row :::::');
        System.debug('::::: LstTask for update '+listTaskForUpdate.size()+' row :::::');
        
        System.debug(':::: Trigger_T06 End ::::');
        
    }
    
    //Jo ChangeOwner for AccountPlan
     public static void ChangeOwnerAcctplan(List<Account> accsNew,List<Account> accsOld,String eventMode){
        System.debug(':::: ChangeOwnerAcctplan Start ::::');
        List<Id> ids = new List<Id>();
        List<AcctPlanCompanyProfile__c> listComForUpdate = new List<AcctPlanCompanyProfile__c>();
        List<Lead> listLeadForUpdate = new List<Lead>();
        
        Map<Id,Account> listAccsOld = new Map<Id,Account>();
        
        if( eventMode == STR_UPDATE ){
            listAccsOld.putAll(accsOld);
        }
        for( Account acc : accsNew ){
            ids.add(acc.Id);
        }
        
        Date currentDate = Date.today();
        Integer currentMonth = currentDate.month();
        Integer currentYear = currentDate.year();
        if(currentYear>2557) currentYear = currentDate.year() - 543;

        System.debug(':::: currentMonth ::::'+currentMonth);
        System.debug(':::: currentYear ::::'+currentYear);
        
        String YearStr = String.ValueOf(currentYear);
        
        List<AcctPlanCompanyProfile__c> listCompany = [SELECT Account__c,OwnerId,Year__c FROM AcctPlanCompanyProfile__c where Account__c IN :ids and Year__c  >=: YearStr ] ;
        
        List<Lead> listLead = [select id,ownerid,Account__c from lead where Account__c in: ids and status in ('Open','Passed Prescreening','Contacted')];
        
       
        for(Account acc : accsNew){
            // if change owner
            if( acc.OwnerId != listAccsOld.get(acc.Id).OwnerId ){

                for( AcctPlanCompanyProfile__c com : listCompany ){
                    if( com.Account__c == acc.Id ){
                        com.ownerId = acc.OwnerId;
                        listcomForUpdate.add(com);
                        System.debug('::::: List Com : Change Owner '+listAccsOld.get(acc.Id).OwnerId+' to '+acc.OwnerId+' :::::');
                    }
                }
                
                /*** R8 change account owner -> change lead owner ***/
                for (lead l : listLead)    
                {
                    if (l.Account__c == acc.id)
                    {
                        l.ownerid = acc.ownerid;
                        listLeadForUpdate.add(l);
                    }
                }
                
            }
            
        }
 
        
        if(listComForUpdate.size() > 0 || listLeadForUpdate.size() > 0){
            try{
                update listComForUpdate;  
                update listLeadForUpdate;  
            }catch (DmlException e){
                System.debug('error : '+e.getMessage());
            }
        }
        

        
        System.debug('::::: ListCompanyProfile for update '+listComForUpdate.size()+' row :::::');
        
        System.debug(':::: ChangeOwnerAcctplan End ::::');
        
    }
    
    
}