@isTest
public class AccountCreationCheckDisqualifiedTest {

    
      public static String STR_POSITIVE = 'positive';
    public static String STR_NEGATIVE = 'negative';
    public static User u;
    public static User u2;
   

    
    static{
        TestUtils.CreateProceBookAccess();
        TestUtils.createAppConfig();
        TestUtils.createStatusCode();        
        TestUtils.createIdType();
        TestUtils.createTriggerMsg();
        TestUtils.createObjUserPermission();
        
        List<Status_Code__C> statusList = new List<Status_Code__C>();
         Status_Code__c error61 = new Status_Code__c();
        error61.isError__c = false;
        error61.Name = '6001';
        error61.Status_Message__c = 'Test'; 
         Status_Code__c error62 = new Status_Code__c();
        error62.isError__c = false;
        error62.Name = '6002';
        error62.Status_Message__c = 'Test'; 
        statusList.add(error61);
        statusList.add(error62);
        
        insert statusList;
    }
    
    
    
    public static testmethod void RunDisqualifiedProspectbyID(){
        System.debug(':::: RunDisqualifiedProspect Start ::::');
        
        u = [SELECT ID,Segment__c FROM User WHERE ID=:UserInfo.getUserId() LIMIT 1];
        
        System.runAs(u) {
            // The following code runs as user 'u' 
            
           
        LisT<Account> acct =  TestUtils.createAccounts(2,'testDisqualified','Juristic', false);
            Account firstAcct = acct.get(0);
            firstAcct.ID_Type_PE__c = 'BRN ID';
            firstAcct.ID_Number_PE__c = '15551';
            firstAcct.Phone = '423432';
            insert firstacct;
             Id [] fixedSearchResults= new Id[1];
           fixedSearchResults[0] = firstacct.id;
           Test.setFixedSearchResults(fixedSearchResults);
            
            Account secondAcct = acct.get(1);
            secondAcct.Name = 'secondAcct';
            secondAcct.ID_Type_PE__c = 'BRN ID';
            secondAcct.ID_Number_PE__c = '15551';
            secondAcct.Phone ='123123';
            
            
            ApexPages.StandardController controllerinsert = new ApexPages.StandardController(secondAcct);
            AccountCreationCheckDisqualified acctdisinsert = new AccountCreationCheckDisqualified(controllerinsert);
            acctdisinsert.CheckDisqualified();
            acctdisinsert.viewCustomer();
            acctdisinsert.cancelCutomer();
            boolean isupdate =acctdisInsert.isUpdateSuccess;
            secondAcct.Name = 'secondAcct';
            secondAcct.ID_Type_PE__c = 'BRN ID';
            secondAcct.ID_Number_PE__c = '15552';
            secondAcct.Phone ='123123';
            insert secondAcct;
            
             Id [] fixedSearchResultstwo= new Id[1];
           fixedSearchResultstwo[0] = firstacct.id;
           Test.setFixedSearchResults(fixedSearchResultstwo);
            ApexPages.StandardController controller = new ApexPages.StandardController(secondAcct);
            AccountCreationCheckDisqualified acctdis = new AccountCreationCheckDisqualified(controller);
            acctdis.oldAcct = secondAcct.clone(true,true,true,true);
            acctdis.oldAcct.ID_Number_PE__c = '15552';
            secondAcct.ID_Number_PE__c = '15551';
            acctdis.CheckDisqualified();
        }    
        
        System.debug(':::: RunDisqualifiedProspect End ::::');
    }
    
    public static testmethod void RunDisqualifiedProspectbyName(){
      System.debug(':::: RunDisqualifiedProspect Start ::::');
        
        u = [SELECT ID,Segment__c FROM User WHERE ID=:UserInfo.getUserId() LIMIT 1];
        
        System.runAs(u) {
            // The following code runs as user 'u' 
            
           
        LisT<Account> acct =  TestUtils.createAccounts(2,'testDisqualified','Individual', false);
            Account firstAcct = acct.get(0);
            firstacct.Name='FirstAcct';
            firstacct.OwnerID = u.id;
            firstacct.ID_Number_PE__c =null;
            firstacct.ID_Type_PE__c =null;
            insert firstacct;
             Id [] fixedSearchResults= new Id[1];
           fixedSearchResults[0] = firstacct.id;
           Test.setFixedSearchResults(fixedSearchResults);
            
            Account secondAcct = acct.get(1);
            secondAcct.Name ='SecondAcct';
            secondAcct.OwnerId = u.id;
            secondAcct.ID_Number_PE__c =null;
            secondAcct.ID_Type_PE__c =null;
            
            ApexPages.StandardController controllerinsert = new ApexPages.StandardController(secondAcct);
            AccountCreationCheckDisqualified acctdisinsert = new AccountCreationCheckDisqualified(controllerinsert);
            acctdisinsert.CheckDisqualified();
            
           
             Id [] fixedSearchResultstwo= new Id[1];
           fixedSearchResultstwo[0] = firstacct.id;
           Test.setFixedSearchResults(fixedSearchResultstwo);
            ApexPages.StandardController controller = new ApexPages.StandardController(secondAcct);
            AccountCreationCheckDisqualified acctdis = new AccountCreationCheckDisqualified(controller);
            acctdis.oldAcct = secondAcct.clone(true,true,true,true);
            acctdis.oldAcct.Name ='SecoundAcct';
            secondAcct.Name = 'FirstAcct';
            acctdis.CheckDisqualified();
        }    
        
        System.debug(':::: RunDisqualifiedProspect End ::::');
    }
    
   
    
}