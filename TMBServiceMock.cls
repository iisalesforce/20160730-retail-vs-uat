@isTest
public class TMBServiceMock implements WebServiceMock {
    public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
       
        System.debug(LoggingLevel.INFO, 'TccServiceMockImpl.doInvoke() - ' +
            '\n request: ' + request +
            '\n response: ' + response +
            '\n endpoint: ' + endpoint +
            '\n soapAction: ' + soapAction +
            '\n requestName: ' + requestName +
            '\n responseNS: ' + responseNS +
            '\n responseName: ' + responseName +
            '\n responseType: ' + responseType);

         if(request instanceOf TMBServiceProxy.InsertPospect_element) {
                      TMBServiceProxy.ProspectResultDTO InsertResult = new TMBServiceProxy.ProspectResultDTO(); 
                InsertResult.status = '0000';
                InsertResult.totalrecord = '1';
                InsertResult.massage = '';
                
                
                TMBServiceProxy.InsertPospectResponse_element respondelement = new TMBServiceProxy.InsertPospectResponse_element();
                respondelement.InsertPospectResult = InsertResult;
                response.put('response_x', respondelement);
         }else if(request instanceOf TMBServiceProxy.Search_element) {
                   TMBServiceProxy.SearchResultDTO SearchResult1 = new TMBServiceProxy.SearchResultDTO(); 
                SearchResult1.status ='0000';
                SearchResult1.totalrecord = '1';
                SearchResult1.massage = '';
               
                    
             TMBServiceProxy.ArrayOfSearchDataDTO ArrayOfSearch = new TMBServiceProxy.ArrayOfSearchDataDTO();
             TMBServiceProxy.SearchDataDTO[] searchArr = New List<TMBServiceProxy.SearchDataDTO>();   
             TMBServiceProxy.SearchDataDTO searchdata = new TMBServiceProxy.SearchDataDTO();
                    Account acct = TestUtils.createAccounts(1,'TestResult','AccountCreateExtension',false).get(0);
                    searchdata.ID_TYPE = null;
                    searchdata.ID_NUMBER = null;
                    searchdata.SF_ID = acct.id;
                    searchdata.FNAME = acct.First_name__c;
                    searchdata.LNAME = acct.Last_name__c;
                    //searchdata.LNAME = '';
                    searchdata.CUST_PROS_TYPE = acct.Account_Type__c;
                    searchdata.PRI_ADDR1 = acct.Address_Line_1_Temp__c;
                    searchdata.PRI_ADDR2 = acct.Address_Line_2_Temp__c; 
                    searchdata.PRI_ADDR3 = acct.Address_Line_3_Temp__c;
                    searchdata.PRI_POSTAL_CD = acct.Zip_Code_Temp__c;
                    searchdata.PRI_PH_NBR = acct.Mobile_Number_Temp__c;
                    
            searchArr.add(searchdata);
            ArrayOfSearch.SearchDataDTO = searchArr;
            SearchResult1.Datas = ArrayOfSearch;
            TMBServiceProxy.SearchResponse_element searchResponse = new TMBServiceProxy.SearchResponse_element();
            searchResponse.SearchResult = SearchResult1;
           response.put('response_x', searchResponse); 
        }else if(request instanceOf TMBServiceProxy.QueryProspect_element) {
                    TMBServiceProxy.QueryProspectResultDTO queryresult = new TMBServiceProxy.QueryProspectResultDTO(); 
            queryresult.status ='0000';
            queryresult.totalrecord = '1';
            queryresult.massage = '';
           
                
         TMBServiceProxy.ArrayOfQueryProspectData ArrayOfQueryProspect = new TMBServiceProxy.ArrayOfQueryProspectData();
         TMBServiceProxy.QueryProspectData[] queryprospect = New List<TMBServiceProxy.QueryProspectData>();      
         TMBServiceProxy.QueryProspectData prospectdata = new TMBServiceProxy.QueryProspectData();
                Account acct = TestUtils.accountlist.get(0);
                
                        Type_of_ID__c typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name =:acct.ID_Type_Temp__c LIMIT 1];
                    System.debug('ACCT : '+acct);
                prospectdata.CUST_TYPE_CD = (acct.Customer_Type_Temp__c=='Individual')?'I':'J';
                prospectdata.ID_NUMBER = acct.ID_Number_Temp__c+'123214';
                prospectdata.ID_TYPE = typeid.Value__c;
                prospectdata.FNAME = acct.First_name__c;
                prospectdata.LNAME = acct.Last_name__c;
                prospectdata.PRI_ADDR1 = acct.Primary_Address_Line_1_Temp__c;
                prospectdata.PRI_ADDR2 = acct.Primary_Address_Line_2_Temp__c; 
                prospectdata.PRI_ADDR3 = acct.Primary_Address_Line_3_Temp__c;
                prospectdata.PRI_POSTAL_CD = acct.Zip_Code_Temp__c;
                prospectdata.PRI_PH_NBR = acct.Mobile_Number_Temp__c;
                prospectdata.PRI_CITY = acct.Province_Primary_Temp__c;
                prospectdata.CRM_ID = '151';
                prospectdata.PROSPECT_ID = '151';
                prospectdata.CITIZEN_ID = '151';
                prospectdata.BRN_ID = '151';
                prospectdata.PASSPORT_ID = '151';
                prospectdata.SALUTATION ='';
                prospectdata.DEL_FLAG  ='';
                prospectdata.RM_OWNER  ='';
                prospectdata.EMAIL_ADDRESS ='';
                prospectdata.NBR_EMP = 0;
                prospectdata.PRI_PH_EXT ='';
                prospectdata.SALES_VOLUME = 0;
                prospectdata.TAXID  ='';
                prospectdata.ESTABLISH_DATE = System.today();
                prospectdata.MOBILE_PH_NBR ='';
                prospectdata.DATE_CREATE = System.today();
                prospectdata.DATE_MODIFY = System.today();
                prospectdata.FACEBOOK = '';
                prospectdata.COMPANY_URL = '';
                prospectdata.LINK_IN = '';
                prospectdata.ALIEN_ID = '';
                prospectdata.WORK_PERMIT_ID = '';
                prospectdata.CREATE_BY_RO_CODE = '';
                prospectdata.RATING_CD = 'Cold';
                prospectdata.BUSINESS_TYPE  = '';
                prospectdata.AGE_OF_BUSINESS  = 0;
                prospectdata.PREFERRED_BRANCH ='';
                prospectdata.GROUP_COVERAGE ='';
                prospectdata.CUSTOMER_AGE = 25;
                prospectdata.SUGGEST_SEGMENT ='';
                prospectdata.SUB_SEGGEST_SEGMENT = '';
        queryprospect.add(prospectdata);
        ArrayOfQueryProspect.QueryProspectData = queryprospect;
        queryresult.Datas = ArrayOfQueryProspect;
        TMBServiceProxy.QueryProspectResponse_element respondelement = new TMBServiceProxy.QueryProspectResponse_element();
        respondelement.QueryProspectResult = queryresult;

       response.put('response_x', respondelement); 
        }
        else if(request instanceOf TMBServiceProxy.UpdatePospect_element) {
             TMBServiceProxy.ProspectResultDTO UpdateResult = new TMBServiceProxy.ProspectResultDTO(); 
            UpdateResult.status ='0000';
            UpdateResult.totalrecord = '1';
            UpdateResult.massage = '';
     
                
        TMBServiceProxy.UpdatePospectResponse_element UpdateElement = new TMBServiceProxy.UpdatePospectResponse_element();
        UpdateElement.UpdatePospectResult = UpdateResult;
       response.put('response_x', UpdateElement); 
    }
                
            }
    

}