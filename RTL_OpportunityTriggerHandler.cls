public without sharing class RTL_OpportunityTriggerHandler extends TriggerHandler {
/*------------------------------------------------------------------------
Author:        Debi Prasad Baral
Company:       Salesforce
Description:   A class created to manage trigger actions from the Opportunity object 
               Responsible for:
               1 - Inserting referral history record when a new oppertunity is created
               2 - Creating new referral history records when the owner of the opportunity changed and 
               the channel is different than the original channel
History
<Date>            <Authors Name>    <Brief Description of Change> 
--------------------------------------------------------------------------*/
    
    Set<id> opportunityId = new Set<id>(); // Set of Opportunity Ids  
    
    public RTL_OpportunityTriggerHandler(){
        if(Test.isRunningTest()){
            this.setMaxLoopCount(10);
        }
        else{
            this.setMaxLoopCount(2);
        }
        
        System.Debug( 'TMB: -> OpportunityTriggerHandler Invoked' );
    }
    
    protected override void beforeInsert(List<SObject> oppsNew) {
		addOpptInfo(oppsNew);
    }
    
    protected override void afterInsert(map<id,sObject> newMap) {
        insertChannelReferralRecords(newMap);
    }
    
	protected override void beforeUpdate(map<id,sObject> oldMap, map<id,sObject> newMap) {
		updateOpptInfo(oldMap, newMap);
	}
    
    protected override void afterUpdate(map<id,sObject> oldMap, map<id,sObject> newMap) {
        //List of channel records to be inserted
        List<RTL_Channel_Referral__c> channelReferralsToInsert = new List<RTL_Channel_Referral__c>();
        //List of channel records to be updated
        List<RTL_Channel_Referral__c> channelReferralsToUpdate = new List<RTL_Channel_Referral__c>();
        
        //Store old and new opportunity owners to map to user object
        Set<Id> opptOwnerIds = new Set<Id>();
        for (Id lId:oldMap.keySet()) opptOwnerIds.add(((opportunity)oldMap.get(lId)).OwnerId);
        for (Id lId:newMap.keySet()) opptOwnerIds.add(((opportunity)newMap.get(lId)).OwnerId);
        //Keep the list of the opportunity owner as users
        Map<Id, User> userMap = new Map<Id, User>();
        for(User u: [Select Id, RTL_Branch_Code__c, Region_Code__c, Zone_Code__c, RTL_Channel__c from User where id in :opptOwnerIds])
            userMap.put(u.Id, u);
        
        for (Id oppId:newMap.keySet()){
            Opportunity newOppObj = (opportunity)newMap.get(oppId);
            Opportunity oldOppObj = (opportunity)oldMap.get(oppId);
              
            if (RTL_Utility.getObjectRecordTypeIdsByDevNamePrefix(Opportunity.SObjectType, 'Retail').contains(newOppObj.RecordTypeId)) {//only continue if it's retail record type
                //get old and new user channel details for comparision
                User oldUser = userMap.get(oldOppObj.OwnerId);
                User newUser = userMap.get(newOppObj.OwnerId);                                                
                if (oldOppObj.OwnerId != newOppObj.OwnerId && 
                    oldUser.RTL_Channel__c != newUser.RTL_Channel__c)
                {
                    //Create New Channel Referral record and then associate with the opportunity
                    channelReferralsToInsert.add(new RTL_Channel_Referral__c ( RTL_Branch_Code__c = newUser.RTL_Branch_Code__c,
                    RTL_Opportunity__c = newOppObj.Id,
                    RTL_Start_Date__c =DateTime.now(),
                    RTL_Owner__c = newUser.Id,
                    Name = newUser.RTL_Channel__c));
                    
                    //Get the associated Channel Referral record with blank end date and update the 
                    //channel referral record with end date as current datetime
                    RTL_Channel_Referral__c channelReferralToUpdate = 
                        [SELECT Id, RTL_End_Date__c FROM RTL_Channel_Referral__c 
                                              WHERE RTL_End_Date__c = : null and RTL_Opportunity__c = : newOppObj.Id limit 1];                        
                    channelReferralToUpdate.RTL_End_Date__c = DateTime.now();
                    channelReferralsToUpdate.add(channelReferralToUpdate);
                }
            }
        }

        if(channelReferralsToInsert.size() > 0){
            // Insert the channel referral records
            Database.SaveResult[] lsr = Database.insert(channelReferralsToInsert, false);
            // Iterate through each returned result
            for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted opportunity channel referrals.');
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(logginglevel.ERROR, 'There is error inserting opportunity channel referrals. Error Message is: ' + err.getMessage());
                    }
                }
            }           
        }

        if(channelReferralsToUpdate.size() > 0){
            // Update the channel referral records
            Database.SaveResult[] lsr = Database.update(channelReferralsToUpdate, false);
            // Iterate through each returned result
            for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated opportunity channel referrals.');
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(logginglevel.ERROR, 'There is error updating opportunity channel referrals. Error Message is: ' + err.getMessage());
                    }
                }
            }
        }
        
        System.Debug('TMB: -> OpportunityTriggerHandler end of afterUpdate');
    }
    
    //====================================================================
    
    /* private methods */
    //====================================================================
    /**
     * @desc This method would insert the first channel referral history record for each opportunity
     * @param [map<id,sObject> newMap]
     */
    private void insertChannelReferralRecords(map<id,sObject> newMap){
        System.Debug('TMB: -> OpportunityTriggerHandler start of afterInsert for channel referral' + opportunityId.size());    
        // Create a set of OpportunityId
        opportunityId.addAll(newMap.keySet());
        //List of channel records to be inserted
        List<RTL_Channel_Referral__c> channelReferralsToInsert = new List<RTL_Channel_Referral__c>();
        
         //Store new opportunity owners to map to user object
        Set<Id> opptOwnerIds = new Set<Id>();
        for (Id lId:newMap.keySet()) opptOwnerIds.add(((opportunity)newMap.get(lId)).OwnerId);
        //Keep the list of the opportunity owner as users
        Map<Id, User> userMap = new Map<Id, User>();
        for(User u: [Select Id, RTL_Branch_Code__c, Region_Code__c, Zone_Code__c, RTL_Channel__c from User where id in :opptOwnerIds])
            userMap.put(u.Id, u);       
          
        for (Id oppId:newMap.keySet()){
            Opportunity oppObj = (opportunity)newMap.get(oppId);
              
            if (RTL_Utility.getObjectRecordTypeIdsByDevNamePrefix(Opportunity.SObjectType, 'Retail').contains(oppObj.RecordTypeId)) {//only continue if it's retail record type
                //Get the owner details to populate referral history record
                User user = userMap.get(oppObj.OwnerId);
                
               //Create New Channel Referral record and then associate with the opportunity
                channelReferralsToInsert.add(new RTL_Channel_Referral__c ( RTL_Branch_Code__c = user.RTL_Branch_Code__c,
                    RTL_Opportunity__c = oppObj.Id,
                    RTL_Start_Date__c =DateTime.now(),
                    RTL_Owner__c = user.Id,
                    Name = user.RTL_Channel__c,
                    RTL_First_Entry__c = true));//only set the 1st channel referral to true
            }
        }
        
        if(channelReferralsToInsert.size() > 0){
            // Insert the channel referral records
            Database.SaveResult[] lsr = Database.insert(channelReferralsToInsert, false);
            // Iterate through each returned result
            for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted opportunity channel referrals.');
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(logginglevel.ERROR, 'There is error inserting opportunity channel referrals. Error Message is: ' + err.getMessage());
                    }
                }
            }           
        }

        System.Debug('TMB: -> OpportunityTriggerHandler end of afterInsert');  
    }
    
    /**
    * This method is to add additional opportunity information before insertion
    */
    private static void addOpptInfo (List<Opportunity> oppsNew) {
    	//Store opportunity owners to map to user object
        Set<Id> opptOwnerIds = new Set<Id>();
        for (Opportunity newOppObj: oppsNew) opptOwnerIds.add(newOppObj.OwnerId);

        //Keep the list of the opportunity owner as users
        Map<Id, User> ownerMap = new Map<Id, User>();
        Set<String> ownerBranchCodeMap = new Set<String>();
        for(User u: [Select Id, RTL_Branch__c, RTL_Branch_Code__c, Region_Code__c, Zone_Code__c, RTL_Channel__c from User where id in :opptOwnerIds]) {
            ownerMap.put(u.Id, u);
            ownerBranchCodeMap.add(u.RTL_Branch_Code__c);
        }
        
        //Keep the owner's branch_and_zone list in map
        Map<String, Id> ownerBranchCodeIdMap = new Map<String, Id>();
        for(Branch_and_Zone__c branchzone : [Select Id, Branch_Code__c from Branch_and_Zone__c where Branch_Code__c in :ownerBranchCodeMap]) {
            ownerBranchCodeIdMap.put(branchzone.Branch_Code__c, branchzone.Id);
        }            
        
    	for (Opportunity newOppObj: oppsNew){
    		newOppObj.RTL_Branch_Team_Name_Code_Rpt__c = ownerMap.get(newOppObj.OwnerId).RTL_Branch__c;
			newOppObj.RTL_Branch_Code_Rpt__c = ownerBranchCodeIdMap.get(ownerMap.get(newOppObj.OwnerId).RTL_Branch_Code__c);
    		newOppObj.RTL_Region_Code_Rpt__c = ownerMap.get(newOppObj.OwnerId).Region_Code__c;
    		newOppObj.RTL_Zone_Code_Rpt__c = ownerMap.get(newOppObj.OwnerId).Zone_Code__c;
    		newOppObj.RTL_Oppt_Channel__c = ownerMap.get(newOppObj.OwnerId).RTL_Channel__c;
    	}
    }
    
    /**
    * This method is to update additional opportunity information before update
    */
    private static void updateOpptInfo(map<id,SObject> oldMap, map<id,SObject> newMap){
        //Store old and new opportunity owners to map to user object
        Set<Id> opptOwnerIds = new Set<Id>();
        for (Id lId:oldMap.keySet()) opptOwnerIds.add(((opportunity)oldMap.get(lId)).OwnerId);
        for (Id lId:newMap.keySet()) opptOwnerIds.add(((opportunity)newMap.get(lId)).OwnerId);

        //Keep the list of the opportunity owner as users
        Map<Id, User> ownerMap = new Map<Id, User>();
        Set<String> ownerBranchCodeMap = new Set<String>();
        for(User u: [Select Id, RTL_Branch__c, RTL_Branch_Code__c, Region_Code__c, Zone_Code__c, RTL_Channel__c from User where id in :opptOwnerIds]) {
            ownerMap.put(u.Id, u);
            ownerBranchCodeMap.add(u.RTL_Branch_Code__c);
        }
        
        //Keep the owner's branch_and_zone list in map
        Map<String, Id> ownerBranchCodeIdMap = new Map<String, Id>();
        for(Branch_and_Zone__c branchzone : [Select Id, Branch_Code__c from Branch_and_Zone__c where Branch_Code__c in :ownerBranchCodeMap]) {
            ownerBranchCodeIdMap.put(branchzone.Branch_Code__c, branchzone.Id);
        }   
          
        for (Id oppId:newMap.keySet()){
            Opportunity oldOppObj = (opportunity)oldMap.get(oppId);
            Opportunity newOppObj = (opportunity)newMap.get(oppId);
            if (oldOppObj.OwnerId != newOppObj.OwnerId) {
            	newOppObj.RTL_Branch_Team_Name_Code_Rpt__c = ownerMap.get(newOppObj.OwnerId).RTL_Branch__c;
	    		newOppObj.RTL_Branch_Code_Rpt__c = ownerBranchCodeIdMap.get(ownerMap.get(newOppObj.OwnerId).RTL_Branch_Code__c);
	    		newOppObj.RTL_Region_Code_Rpt__c = ownerMap.get(newOppObj.OwnerId).Region_Code__c;
	    		newOppObj.RTL_Zone_Code_Rpt__c = ownerMap.get(newOppObj.OwnerId).Zone_Code__c;
	    		newOppObj.RTL_Oppt_Channel__c = ownerMap.get(newOppObj.OwnerId).RTL_Channel__c;
            }
        }    	
    }
    
    /* End of private methods */
    //====================================================================
   
 }