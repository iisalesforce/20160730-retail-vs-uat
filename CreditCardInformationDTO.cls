global class CreditCardInformationDTO {
	// Input fields
	global string CardNumber { get; set; }
	global string CreditCardType { get; set; } /** SubProductGroup It is used to derive the Product Name and Product Type (Sub Group) from Product Hierarchy.*/
	global decimal CreditLimit { get; set; }
	global string ProductName { get; set; }
    global string ProductType {get;set;}
	//information 
	global decimal TemporaryLine { get; set; }
	global decimal CycleCut { get; set; }
	global string UsageStatus { get; set; }
	global string BlockCode { get; set; }
	global Date NextExpiredPointOn { get; set; }
	global string CashChillChill { get; set; }
	global string CashWithdrawalAccountNumber { get; set; }
	global Date OpenedDate { get; set; }
	global Decimal CurrentBalance { get; set; } // Used to Outstanding
	global Decimal TemporaryLinePeriod { get; set; }
	global Date PaymentDue { get; set; }
	global string PaymentBehavior { get; set; }
	global Decimal RewardPoints { get; set; }
	global Decimal NextExpiredPoints { get; set; }
	global string CashChillChillStatus { get; set; }
	global string DirectDebitAccountNumber { get; set; }

	global string UsageBehavior { get; set; }
	global Date LastPaymentDate { get; set; }
    
    // Error Handler message
	global String SoapStatus { get; set; }
	global String SoapMessage { get; set; }
    
	//Method
	global CreditCardInformationDTO() {
		SoGoodPayPlan = new SoGoodPayPlanDTO();
		SupplementaryInformations = new List<SupplementaryInformationDTO> ();
		NextExpiredPoints=TemporaryLinePeriod=CurrentBalance=CreditLimit=TemporaryLine=CycleCut=CreditLimit =0;
	}
	global string MarkedCardNumber {
		get {
			return NumberHelper.MarkCardNo(NumberHelper.TrimCardNo(CardNumber));

		}
	}

	global string MarkedCreditLimit {
		get {
			return NumberHelper.MarkDecimal(CreditLimit);

		}
	}
	global string MarkedCurrentBalance {
		get {
			return NumberHelper.ConvertDecimalToMarkedFormatString(CurrentBalance);

		}
	}
    global string MarkedCashWithdrawalAccountNumber {
		get {
			return NumberHelper.MarkAccountNo(CashWithdrawalAccountNumber);

		}
	}
    global string MarkedDirectDebitAccountNumber {
		get {
			return NumberHelper.MarkAccountNo(DirectDebitAccountNumber);

		}
	}
    global String convertedOpenedDate{
        get{
            return NumberHelper.DateFormat(OpenedDate);
        }
    }
    
    global String convertedPaymentDue{
        get{
            return NumberHelper.DateFormat(PaymentDue);
        }
    }
    
    global String convertedLastPaymentDate{
        get{
            return NumberHelper.DateFormat(LastPaymentDate);
        }
    }
    
    global String convertedNextExpiredPointOn{
        get{
            return NumberHelper.DateFormat(NextExpiredPointOn);
        }
    }
    

	// Nested Class
	public SoGoodPayPlanDTO SoGoodPayPlan { get; set; }

	public List<SupplementaryInformationDTO> SupplementaryInformations { get; set; }
}