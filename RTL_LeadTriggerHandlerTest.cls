@IsTest

public class RTL_LeadTriggerHandlerTest {
    static {
        TestUtils.createAppConfig();
        createProductMaster();
    }
    
    public static testmethod void createProductMaster() {
        
    }
        
    public static testmethod void RunPositiveTestLeadConversionWithOppt(){
        System.debug(':::: RunPositiveTestLeadConversionWithOppt Start ::::');      
        
        TEST.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            //create retail master product
            RTL_TestUtility.createRetailMasterProducts(true);
            //set opportunity record type mapping in custom settings
            RTL_TestUtility.setOpptRecordTypeMapping(true);
            //create test lead      
            List<Lead> leads = RTL_TestUtility.createLeads(1, true);
            //create interested products for lead
            RTL_TestUtility.createInterestedProducts(leads, true);
            //create one task and one event
            RTL_TestUtility.createLeadTaskAndEvent(leads, true);
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];         
            
            //1st test case, update lead, convert with both account and opportunity
            Lead lead = leads[0];
            lead.Status = 'Qualified';
            lead.RTL_Mobile_Number__c='0987654321';
            update lead;
            //convert lead
            database.leadConvert lc = new database.leadConvert();
            lc.setLeadId(lead.id);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            //check if the lead conversion is success
            System.assert(lcr.isSuccess());
            //refresh the updated lead
            lead = [select Id, company, status from lead where Id=:lead.id];
            //check if the lead is converted
            System.assertEquals(convertStatus.MasterLabel, lead.status);
            //check if account is created with the name as lead company
            Id acctId = lcr.getAccountId();
            Account acct = [select name, Account_Type__c from Account where Id=:acctId];
            System.assertEquals(lead.company, acct.name);
            //check if account stage is 'Retail Prospect'
            System.assertEquals('Retail Prospect', acct.Account_Type__c);
            //check if account is associated with 2 interested products
            List<RTL_Interested_products_c__c> interestedProductList = [select Id, Product_Name__c from RTL_Interested_products_c__c where Customer__c=:acctId];
            System.assertEquals(2, interestedProductList.size());
            //check if account is associate with 1 task
            List<Task> taskList = [select Id from Task where AccountId =: acctId];
            System.assertEquals(1, taskList.size());
            //check if account is associate with 1 event
            List<Event> eventList = [select Id from Event where AccountId =: acctId];
            //System.assertEquals(1, eventList.size());
            //check if opportunity is created for converted account
            List<Opportunity> oppts = [select name, StageName, RTL_Product_Name__c from Opportunity where Account.Id=:acctId];
            System.assertEquals(1, oppts.size());
            //check if opportunity has the same name as lead company
            System.assertEquals(lead.company, oppts[0].name);
            //check if opportunity is associate with primary product only
            RTL_product_master__c opptProduct = [Select Id, product_group__c from RTL_product_master__c where Product_Code__c = : '0001' and Active__c = : true limit 1];
            System.assertEquals(opptProduct.Id, oppts[0].RTL_Product_Name__c);
            //check if opportunity stage is 'Prospect'
            System.assertEquals('Prospect', oppts[0].StageName);                
        }   
        
        TEST.stopTest();
        System.debug(':::: RunPositiveTestLeadConversionWithOppt End ::::');
    }
    
    public static testmethod void RunPositiveTestLeadConversionWithoutOppt(){
        System.debug(':::: RunPositiveTestLeadConversionWithoutOppt Start ::::');   
        
        TEST.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            //create retail master product
            RTL_TestUtility.createRetailMasterProducts(true);
            //create test lead      
            List<Lead> leads = RTL_TestUtility.createLeads(1, true);
            //create interested products for lead
            RTL_TestUtility.createInterestedProducts(leads, true);
            //create one task and one event
            RTL_TestUtility.createLeadTaskAndEvent(leads, true);
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];         
            
            //1st test case, update lead, convert with both account and opportunity
            Lead lead = leads[0];
            lead.Status = 'Qualified';
            lead.RTL_Mobile_Number__c='0987654321';
            update lead;
            //convert lead
            database.leadConvert lc = new database.leadConvert();
            lc.setLeadId(lead.id);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            lc.setDoNotCreateOpportunity(true);//do not create opportunity during lead conversion
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            //check if the lead conversion is success
            System.assert(lcr.isSuccess());
            //refresh the updated lead
            lead = [select Id, company, status from lead where Id=:lead.id];
            //check if the lead is converted
            System.assertEquals(convertStatus.MasterLabel, lead.status);
            //check if account is created with the name as lead company
            Id acctId = lcr.getAccountId();
            Account acct = [select name, Account_Type__c from Account where Id=:acctId];
            System.assertEquals(lead.company, acct.name);
            //check if account stage is 'Retail Prospect'
            System.assertEquals('Retail Prospect', acct.Account_Type__c);
            //check if account is associated with 2 interested products
            List<RTL_Interested_products_c__c> interestedProductList = [select Id, Product_Name__c from RTL_Interested_products_c__c where Customer__c=:acctId];
            System.assertEquals(2, interestedProductList.size());
            //check if account is associate with 1 task
            List<Task> taskList = [select Id from Task where AccountId =: acctId];
            System.assertEquals(1, taskList.size());
            //check if account is associate with 1 event
            List<Event> eventList = [select Id from Event where AccountId =: acctId];
            //System.assertEquals(1, eventList.size());
            //check if opportunity is created for converted account. expected to be 0
            List<Opportunity> oppts = [select name, StageName, RTL_Product_Name__c from Opportunity where Account.Id=:acctId];
            System.assertEquals(0, oppts.size());               
        }   
        
        TEST.stopTest();
        System.debug(':::: RunPositiveTestLeadConversionWithoutOppt End ::::');
    }    
    
     public static testmethod void RunNegativeTestLeadConversionDupCustomer(){
        System.debug(':::: RunNegativeTestLeadConversionDupCustomer Start ::::');   
        
        TEST.startTest();
        Database.LeadConvertResult lcr = null;
        Database.SaveResult sr = null;
        boolean errMsgMatch = false;    
        TestInit.createUser(false);
        User adminUser = TestInit.us;  
        System.runAs(adminUser) {
            //create a customer account
            RTL_TestUtility.createAccounts(1, true);
        } 
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            //create retail master product
            RTL_TestUtility.createRetailMasterProducts(true);
            //set opportunity record type mapping in custom settings
            RTL_TestUtility.setOpptRecordTypeMapping(true);
            //create test lead with duplicate ID with customer account
            List<Lead> leads = new List<Lead>();
            leads.add(RTL_TestUtility.createLeadWithDupID(true));
            //create interested products for lead
            RTL_TestUtility.createInterestedProducts(leads, true);
            //create one task and one event
            RTL_TestUtility.createLeadTaskAndEvent(leads, true);
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];         
            
            //1st test case, update lead, convert with both account and opportunity
            Lead lead = leads[0];
            lead.Status = 'Qualified';
            update lead;
            database.leadConvert lc = new database.leadConvert();
            lc.setLeadId(lead.id);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            try{
                lcr = Database.convertLead(lc);
                //expected lead conversion result is fail
                System.assert(!lcr.isSuccess());
            } catch (Exception ex) {
                System.debug('lead conversion negative test case err msg = ' + ex.getMessage());
                errMsgMatch = ex.getMessage().contains(System.Label.LeadError_DupCustomerBeforeConversion);
                //System.assert(errMsgMatch);
            }
        }
                
        TEST.stopTest();
        System.debug(':::: RunNegativeTestLeadConversionDupCustomer End ::::');     
     }    
    
     public static testmethod void RunNegativeTestLeadConversion(){
        System.debug(':::: RunNegativeTestLeadConversion Start ::::');      
        
        TEST.startTest();
        Database.LeadConvertResult lcr = null;
        Database.SaveResult sr = null;
        boolean errMsgMatch = false;
        String errMsg = null;
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            //create retail master product
            RTL_TestUtility.createRetailMasterProducts(true);
            //create test lead      
            List<Lead> leads = RTL_TestUtility.createLeads(1, true);
            /*1st test case: 
             *test lead without interested products and not Qualified
            */
            Lead lead = leads[0];//lead status = 'New'
            database.leadConvert lc = new database.leadConvert();
            lc.setLeadId(lead.id);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            try{
                lcr = Database.convertLead(lc);
            } catch (Exception ex) {
                System.debug('1st lead conversion negative test case err msg = ' + ex.getMessage());
                errMsgMatch = ex.getMessage().contains(System.Label.LeadError_QualifiedBeforeConversion);
                //System.assert(errMsgMatch);
            }
            /*2nd test case: 
             *test lead Qualified but without interested products
            */
            lead.Status = 'Qualified';
            try{
                sr = Database.update(lead);
            } catch (Exception ex) {
                System.debug('2nd lead conversion negative test case err msg = ' + ex.getMessage());
                errMsg = ex.getMessage();
                System.assert(errMsg.length()>0);//the error is thrown from validation rule, can't hardcode the exact message here
            }    
        }   
        
        TEST.stopTest();
        System.debug(':::: RunNegativeTestLeadConversion End ::::');
    }   
     
    public static testmethod void RunPositiveTestWebToLead(){
        System.debug(':::: RunPositiveTestWebToLead Start ::::');   
        
        TEST.startTest();
        TestInit.createUser(false);
        User adminUser = TestInit.us;       
        System.runAs(adminUser) {
            //create test lead queue
            Map<String, Group> leadQueues = RTL_TestUtility.createLeadQueues(true);

            //create test branch
            RTL_TestUtility.createBranchZone(1, true);
            //create test product and web-to-lead assignment criterias
            RTL_TestUtility.createLeadAssignCriterias(true);
            //create custom settings of Lead Assignment/Auto Assign/Enable__c = true
            RTL_TestUtility.enableWebToLeadAutoAssign(true);
            //create positive test data
            RTL_TestUtility.createPositiveWebToLead();          
            //retrieve from db
            List<Lead> testLeads = [select Id, FirstName, LastName, RTL_Count_InterestedProducts_Primary__c, OwnerId from Lead where LastName = 'WebToLead'];   
            //make sure 4 positive leads are created
            System.assertEquals(4, testLeads.size());       
            
            //check result of 1st WebToLead
            //check if the lead is created
            Lead lead = testLeads[0];
            System.assertEquals('Test1', lead.FirstName);
            //check lead queue name = RTLQ_Outbound
            Group queue = leadQueues.get('RTLQ_Outbound');
            //System.assertEquals(queue.Id, lead.OwnerId);
            //check the number of primary interested products
            //System.assertEquals(1, lead.RTL_Count_InterestedProducts_Primary__c);
            //System.assertEquals(1, [select count() from RTL_Interested_products_c__c where Lead__c = :lead.Id and Is_Primary__c = true]);
            //check the number of total interested products
            List<RTL_Interested_products_c__c> interestedProducts = [select Lead__r.Id, Product_Name__r.Name, Is_Primary__c from RTL_Interested_products_c__c where Lead__r.Id=:lead.Id];
            System.assertEquals(1, interestedProducts.size());
            //check the name of the product is 'So Fast Test'
            System.assertEquals('So Fast Test', interestedProducts[0].Product_Name__r.Name);
            
            //check result of 2nd WebToLead
            //check if the lead is created
            lead = testLeads[1];
            System.assertEquals('Test2', lead.FirstName);
            //check lead queue name = RTLQ_001
            queue = leadQueues.get('RTLQ_001');
            System.assertEquals(queue.Id, lead.OwnerId);
            //check the number of primary interested products
            System.assertEquals(1, lead.RTL_Count_InterestedProducts_Primary__c);
            //System.assertEquals(1, [select count() from RTL_Interested_products_c__c where Lead__c = :lead.Id and Is_Primary__c = true]);
            //check the number of total interested products
            interestedProducts = [select Lead__r.Id, Product_Name__r.Name, Is_Primary__c from RTL_Interested_products_c__c where Lead__r.Id=:lead.Id];
            System.assertEquals(1, interestedProducts.size());
            //check the name of the product is 'So Fast Test'
            System.assertEquals('So Fast Test', interestedProducts[0].Product_Name__r.Name);        
            
            //check result of 3rd WebToLead
            //check if the lead is created
            lead = testLeads[2];
            System.assertEquals('Test3', lead.FirstName);
            //check lead queue name = RTLQ_001
            queue = leadQueues.get('RTLQ_001');
            System.assertEquals(queue.Id, lead.OwnerId);
            //check the number of primary interested products
            System.assertEquals(1, lead.RTL_Count_InterestedProducts_Primary__c);
            //System.assertEquals(1, [select count() from RTL_Interested_products_c__c where Lead__c = :lead.Id and Is_Primary__c = true]);
            //check the number of total interested products
            interestedProducts = [select Lead__r.Id, Product_Name__r.Name, Is_Primary__c from RTL_Interested_products_c__c where Lead__r.Id=:lead.Id];
            System.assertEquals(2, interestedProducts.size());
            //check the 1st name of the product is 'So Fast Test'
            System.assertEquals('So Fast Test', interestedProducts[0].Product_Name__r.Name);
            System.assertEquals(True, interestedProducts[0].Is_Primary__c);
            //check the 2nd name of the product is 'So Smart Test'
            System.assertEquals('So Smart Test', interestedProducts[1].Product_Name__r.Name);   
            
            //check result of 4th WebToLead
            //check if the lead is created
            lead = testLeads[3];
            System.assertEquals('Test4', lead.FirstName);
            //check lead queue name = RTL_Outbound
            queue = leadQueues.get('RTLQ_Outbound');
            System.assertEquals(queue.Id, lead.OwnerId);
            //check the number of primary interested products
            System.assertEquals(1, lead.RTL_Count_InterestedProducts_Primary__c);
            //System.assertEquals(1, [select count() from RTL_Interested_products_c__c where Lead__c = :lead.Id and Is_Primary__c = true]);
            //check the number of total interested products
            interestedProducts = [select Lead__r.Id, Product_Name__r.Name, Is_Primary__c from RTL_Interested_products_c__c where Lead__r.Id=:lead.Id];
            System.assertEquals(1, interestedProducts.size());
            //check the name of the product is 'Home Loan Test'
            System.assertEquals('Home Loan Test', interestedProducts[0].Product_Name__r.Name);                
        }   
        
        TEST.stopTest();
        System.debug(':::: RunPositiveTestWebToLead End ::::');
    }
    
    public static testmethod void RunNegativeTestWebToLead(){
        System.debug(':::: RunNegativeTestWebToLead Start ::::');

        TEST.startTest();
        TestInit.createUser(false);
        User adminUser = TestInit.us;       
        System.runAs(adminUser) {
            //create test lead queue
            Map<String, Group> leadQueues = RTL_TestUtility.createLeadQueues(true);
            //create test branch
            RTL_TestUtility.createBranchZone(1, true);
            //create test product and web-to-lead assignment criterias
            RTL_TestUtility.createLeadAssignCriterias(true);
            //create custom settings of Lead Assignment/Auto Assign/Enable__c = true
            RTL_TestUtility.disableWebToLeadAutoAssign(true);           
            //create negative test data
            RTL_TestUtility.createNegativeWebToLead();

            //retrieve from db
            List<Lead> testLeads = [select Id, FirstName, LastName, RTL_Count_InterestedProducts_Primary__c, OwnerId, RTL_Description__c from Lead where LastName = 'NegativeWebToLead'];
            //make sure 4 negative leads are created
            System.assertEquals(4, testLeads.size());
            
            //check result of 1st WebToLead
            //check if the lead is created
            Lead lead = testLeads[0];
            System.assertEquals('Test1', lead.FirstName);
            //check lead queue name = run user
            System.assertEquals(adminUser.Id, lead.OwnerId);
            //check the number of primary interested products
            System.assertEquals(0, lead.RTL_Count_InterestedProducts_Primary__c);
            //System.assertEquals(0, [select count() from RTL_Interested_products_c__c where Lead__c = :lead.Id and Is_Primary__c = true]);
            //check the number of total interested products
            List<RTL_Interested_products_c__c> interestedProducts = [select Lead__r.Id, Product_Name__r.Name, Is_Primary__c from RTL_Interested_products_c__c where Lead__r.Id=:lead.Id];
            System.assertEquals(0, interestedProducts.size());
            //check if the lead descript is updated with error message
            String leadDesc = lead.RTL_Description__c;
            boolean errDesc = leadDesc.contains('Bad Product');
            System.assert(errDesc);
            
            //check result of 2nd WebToLead
            //check if the lead is created
            lead = testLeads[1];
            System.assertEquals('Test2', lead.FirstName);
            //check lead queue name = RTLQ_001
            //Group queue = leadQueues.get('RTLQ_001');
            //System.assertEquals(queue.Id, lead.OwnerId);//expected result if web-to-lead auto assign is enabled
            System.assertEquals(adminUser.Id, lead.OwnerId);//expected result if web-to-lead auto assign is disabled
            //check the number of primary interested products
            System.assertEquals(1, lead.RTL_Count_InterestedProducts_Primary__c);
            //System.assertEquals(1, [select count() from RTL_Interested_products_c__c where Lead__c = :lead.Id and Is_Primary__c = true]);
            //check the number of total interested products
            interestedProducts = [select Lead__r.Id, Product_Name__r.Name, Is_Primary__c from RTL_Interested_products_c__c where Lead__r.Id=:lead.Id];
            System.assertEquals(1, interestedProducts.size());
            //check the name of the product is 'So Fast Test'
            System.assertEquals('So Fast Test', interestedProducts[0].Product_Name__r.Name);            
            //check if the lead descript is updated with error message
            leadDesc = lead.RTL_Description__c;
            errDesc = leadDesc.contains('Bad Product');
            System.assert(errDesc);
            
            //check result of 3rd WebToLead
            //check if the lead is created
            lead = testLeads[2];
            System.assertEquals('Test3', lead.FirstName);
            //check lead queue name = RTLQ_001
            //queue = leadQueues.get('RTLQ_001');
            //System.assertEquals(queue.Id, lead.OwnerId);//expected result if web-to-lead auto assign is enabled
            System.assertEquals(adminUser.Id, lead.OwnerId);//expected result if web-to-lead auto assign is disabled
            //check the number of primary interested products
            System.assertEquals(1, lead.RTL_Count_InterestedProducts_Primary__c);
            //System.assertEquals(1, [select count() from RTL_Interested_products_c__c where Lead__c = :lead.Id and Is_Primary__c = true]);
            //check the number of total interested products
            interestedProducts = [select Lead__r.Id, Product_Name__r.Name, Is_Primary__c from RTL_Interested_products_c__c where Lead__r.Id=:lead.Id];
            System.assertEquals(1, interestedProducts.size());
            //check the name of the product is 'So Fast Test'
            System.assertEquals('So Fast Test', interestedProducts[0].Product_Name__r.Name);            
            //check if the lead descript is updated with error message
            leadDesc = lead.RTL_Description__c;
            errDesc = leadDesc.contains('Bad Product');
            System.assert(errDesc); 
            
            //check result of 4rd WebToLead
            //check if the lead is created
            lead = testLeads[3];
            System.assertEquals('Test4', lead.FirstName);
            //check lead queue name = RTLQ_001
            //queue = leadQueues.get('RTLQ_001');
            //System.assertEquals(queue.Id, lead.OwnerId);//expected result if web-to-lead auto assign is enabled
            System.assertEquals(adminUser.Id, lead.OwnerId);//expected result if web-to-lead auto assign is disabled
            //check the number of primary interested products
            System.assertEquals(0, lead.RTL_Count_InterestedProducts_Primary__c);
            //System.assertEquals(0, [select count() from RTL_Interested_products_c__c where Lead__c = :lead.Id and Is_Primary__c = true]);
            //check the number of total interested products
            interestedProducts = [select Lead__r.Id, Product_Name__r.Name, Is_Primary__c from RTL_Interested_products_c__c where Lead__r.Id=:lead.Id];
            System.assertEquals(0, interestedProducts.size());          
            //check if the lead descript is updated with error message
            leadDesc = lead.RTL_Description__c;
            errDesc = leadDesc.contains('Bad Product');
            System.assert(errDesc);                     
        }
                
        TEST.stopTest();        
        System.debug(':::: RunNegativeTestWebToLead End ::::');
    }     
    
    public static testmethod void RunPositiveTestLeadChangeUserOwnerWithBranch(){
        System.debug(':::: RunPositiveTestLeadChangeUserOwnerWithBranch Start ::::');
        
        TEST.startTest();
        User retailUser1 = RTL_TestUtility.createRetailTestUser(true);
        User retailUser2 = RTL_TestUtility.createRetailTestUserWithBranch(true);
        Id leadId = null;
        List<Branch_and_Zone__c> branchList = null;
        System.runAs(retailUser1) {
            //create branch and zone
            branchList = RTL_TestUtility.createBranchZone(2, true);
            //create test lead      
            List<Lead> leads = RTL_TestUtility.createLeads(1, true);
            //update lead branch info as this info wasn't created during lead creation
            Lead initLead = leads[0];
            leadId = initLead.Id;
            initLead.RTL_Branch_and_Zone__c = branchList[0].Id;
            update initLead;
            Lead oldLead = [select Id, OwnerId, RTL_Branch_and_Zone__c from Lead where Id = :leadId];
            //check existing RTL_Branch_and_Zone__c, expected to be the same as the 1st branch Id
            System.assertEquals(branchList[0].Id, oldLead.RTL_Branch_and_Zone__c);
            //change lead owner
            oldLead.OwnerId = retailUser2.Id;
            update oldLead;
        }
        System.runAs(retailUser2) {
            Lead newLead = [select Id, OwnerId, RTL_Branch_and_Zone__c from Lead where Id = :leadId];
            //check if the new branch info is updated
            System.assertEquals(branchList[1].Id, newLead.RTL_Branch_and_Zone__c);
        }
        
        TEST.stopTest();        
        System.debug(':::: RunPositiveTestLeadChangeUserOwnerWithBranch End ::::');        
    }   
    
    public static testmethod void RunPositiveTestLeadChangeUserOwnerWithoutBranch(){
        System.debug(':::: RunPositiveTestLeadChangeUserOwnerWithoutBranch Start ::::');
        
        TEST.startTest();
        User retailUser1 = RTL_TestUtility.createRetailTestUser(true);
        User retailUser3 = RTL_TestUtility.createRetailTestUserWithoutBranch(true);
        Id leadId = null;
        List<Branch_and_Zone__c> branchList = null;
        System.runAs(retailUser1) {
            //create branch and zone
            branchList = RTL_TestUtility.createBranchZone(2, true);
            //create test lead      
            List<Lead> leads = RTL_TestUtility.createLeads(1, true);
            //update lead branch info as this info wasn't created during lead creation
            Lead initLead = leads[0];
            leadId = initLead.Id;
            initLead.RTL_Branch_and_Zone__c = branchList[0].Id;
            update initLead;
            Lead oldLead = [select Id, OwnerId, RTL_Branch_and_Zone__c from Lead where Id = :leadId];
            //check existing RTL_Branch_and_Zone__c, expected to be the same as the 1st branch Id
            System.assertEquals(branchList[0].Id, oldLead.RTL_Branch_and_Zone__c);
            //change lead owner
            oldLead.OwnerId = retailUser3.Id;
            update oldLead;
        }
        System.runAs(retailUser3) {
            Lead newLead = [select Id, OwnerId, RTL_Branch_and_Zone__c from Lead where Id = :leadId];
            //when new owner doesn't have the branch info, the expected branch value in lead is null
            System.assertEquals(null, newLead.RTL_Branch_and_Zone__c);
        }
        
        TEST.stopTest();        
        System.debug(':::: RunPositiveTestLeadChangeUserOwnerWithoutBranch End ::::');        
    }     
    
    public static testmethod void RunPositiveTestLeadChangeQueueOwnerWithBranch(){
        System.debug(':::: RunPositiveTestLeadChangeQueueOwnerWithBranch Start ::::');
        
        TEST.startTest();
        TestInit.createUser(false);
        User adminUser = TestInit.us;       
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        List<Branch_and_Zone__c> branchList = null;
        Map<String, Group> leadQueues = null;
        List<Lead> leads = null;
        System.runAs(adminUser) {
            //create test lead queue
            leadQueues = RTL_TestUtility.createLeadQueues(true);            
            //create branch and zone
            branchList = RTL_TestUtility.createBranchZone(2, true);
        }
        System.runAs(retailUser) {
            //create test lead      
            leads = RTL_TestUtility.createLeads(1, true);
            //update lead branch info as this info wasn't created during lead creation
            Lead initLead = leads[0];
            initLead.RTL_Branch_and_Zone__c = branchList[0].Id;
            update initLead;
            Lead oldLead = [select Id, OwnerId, RTL_Branch_and_Zone__c from Lead where Id = :leads[0].Id];
            //check existing RTL_Branch_and_Zone__c, expected to be the same as the 1st branch Id
            System.assertEquals(branchList[0].Id, oldLead.RTL_Branch_and_Zone__c);
            //change lead owner
            oldLead.OwnerId = leadQueues.get('RTLQ_001').Id;
            update oldLead;
        }
        System.runAs(adminUser) {
            Lead newLead = [select Id, OwnerId, RTL_Branch_and_Zone__c from Lead where Id = :leads[0].Id];
            //check if the new branch info is updated
            System.assertEquals(branchList[0].Id, newLead.RTL_Branch_and_Zone__c);
        }
        
        TEST.stopTest();        
        System.debug(':::: RunPositiveTestLeadChangeQueueOwnerWithBranch End ::::');        
    }  
    
    public static testmethod void RunPositiveTestLeadChangeQueueOwnerWithoutBranch(){
        System.debug(':::: RunPositiveTestLeadChangeQueueOwnerWithoutBranch Start ::::');
        
        TEST.startTest();
        TestInit.createUser(false);
        User adminUser = TestInit.us;       
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        Map<String, Group> leadQueues = null;
        List<Branch_and_Zone__c> branchList = null;
        List<Lead> leads = null;
        System.runAs(adminUser) {
            //create test lead queue
            leadQueues = RTL_TestUtility.createLeadQueues(true);            
            //create branch and zone
            branchList = RTL_TestUtility.createBranchZone(2, true);
        }
        System.runAs(retailUser) {        
            //create test lead      
            leads = RTL_TestUtility.createLeads(1, true);
            //update lead branch info as this info wasn't created during lead creation
            Lead initLead = leads[0];
            initLead.RTL_Branch_and_Zone__c = branchList[0].Id;
            update initLead;
            Lead oldLead = [select Id, OwnerId, RTL_Branch_and_Zone__c from Lead where Id = :leads[0].Id];
            //check existing RTL_Branch_and_Zone__c, expected to be the same as the 1st branch Id
            System.assertEquals(branchList[0].Id, oldLead.RTL_Branch_and_Zone__c);
            //change lead owner
            oldLead.OwnerId = leadQueues.get('RTLQ_Outbound').Id;
            update oldLead;
        }
        System.runAs(adminUser) {           
            Lead newLead = [select Id, OwnerId, RTL_Branch_and_Zone__c from Lead where Id = :leads[0].Id];
            //check if the new branch info is updated
            System.assertEquals(null, newLead.RTL_Branch_and_Zone__c);
        }
        
        TEST.stopTest();        
        System.debug(':::: RunPositiveTestLeadChangeQueueOwnerWithoutBranch End ::::');        
    }            
}