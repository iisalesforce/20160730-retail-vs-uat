@IsTest
global class TMBSearchPartialMock implements WebServiceMock{
    public void doInvoke(
                Object stub,
                Object request,
                Map<String, Object> response,
                String endpoint,
                String soapAction,
                String requestName,
                String responseNS,
                String responseName,
                String responseType) {
           
            System.debug(LoggingLevel.INFO, 'TMBServiceProxyMockImpl.doInvoke() - ' +
                '\n request: ' + request +
                '\n response: ' + response +
                '\n endpoint: ' + endpoint +
                '\n soapAction: ' + soapAction +
                '\n requestName: ' + requestName +
                '\n responseNS: ' + responseNS +
                '\n responseName: ' + responseName +
                '\n responseType: ' + responseType);
           
            TMBServiceProxy.SearchResultDTO SearchResult1 = new TMBServiceProxy.SearchResultDTO(); 
                SearchResult1.status ='0000';
                SearchResult1.totalrecord = '1';
                SearchResult1.massage = '';
               
                    
             TMBServiceProxy.ArrayOfSearchDataDTO ArrayOfSearch = new TMBServiceProxy.ArrayOfSearchDataDTO();
             TMBServiceProxy.SearchDataDTO[] searchArr = New List<TMBServiceProxy.SearchDataDTO>();   
             TMBServiceProxy.SearchDataDTO searchdata = new TMBServiceProxy.SearchDataDTO();
                   // Account acct = TestUtils.accountlist.get(0);
                    Account acct = TestUtils.createAccounts(1,'SearchMock','AccountCreateExtension',true).get(0);
                    searchdata.ID_TYPE = 'PP';
                    searchdata.ID_NUMBER = acct.ID_Number_Temp__c;
                    searchdata.SF_ID = acct.id;
                    searchdata.FNAME = acct.First_name__c;
                    searchdata.LNAME = acct.Last_name__c;
                    searchdata.CUST_PROS_TYPE = acct.Account_Type__c;
                    searchdata.PRI_ADDR1 = acct.Address_Line_1_Temp__c;
                    searchdata.PRI_ADDR2 = acct.Address_Line_2_Temp__c; 
                    searchdata.PRI_ADDR3 = acct.Address_Line_3_Temp__c;
                    searchdata.PRI_POSTAL_CD = acct.Zip_Code_Temp__c;
                    searchdata.PRI_PH_NBR = acct.Mobile_Number_Temp__c;
                    searchArr.add(searchdata);                   
                                   
           
            ArrayOfSearch.SearchDataDTO = searchArr;
            SearchResult1.Datas = ArrayOfSearch;
            TMBServiceProxy.SearchPartialResponse_element searchResponse = new TMBServiceProxy.SearchPartialResponse_element();
            searchResponse.SearchPartialResult = SearchResult1;
           response.put('response_x', searchResponse); 
       } 
}