public class OpportunityLineItemTriggerHandler {

    public OpportunityLineItemTriggerHandler()
    {
    
    }
    
    public static void CalculateOpportunityTotalVol (list<opportunitylineitem> listOptyLine)
    {
        /*
        decimal TotalVol = 0.00;
        for (opportunitylineitem a: [select quantity, National_Amt_TMB__c from opportunitylineitem 
                                     where opportunityid =: listOptyLine.get(0).opportunityid])
        {
            TotalVol += (a.Quantity == null ? 0 : a.Quantity) * (a.National_Amt_TMB__c == null ? 0 : a.National_Amt_TMB__c);
        }
        
        opportunity op = [select id,Total_Vol__c from opportunity where id =: listOptyLine.get(0).opportunityid];
        op.Total_Vol__c = TotalVol;
        
        update op;
        */
    }

    public static void getDeletedRecord (Map<Id,OpportunityLineItem> mapOld){
        List<Opportunity_Product_History__c> lstOppProductTemp = new List<Opportunity_Product_History__c>();
        for(Id oppProductId : mapOld.keySet()){
            OpportunityLineItem oppProduct = mapOld.get(oppProductId);
            Opportunity_Product_History__c oppProductTemp = new Opportunity_Product_History__c();

            oppProductTemp.Deal_Probability__c = oppProduct.Deal_Probability__c;
            oppProductTemp.Description__c = oppProduct.Description;
            oppProductTemp.Expected_Fee_Rate__c = oppProduct.Expected_Fee_Rate__c;
            oppProductTemp.Expected_NIM__c = oppProduct.Expected_NIM__c;
            oppProductTemp.Expected_Revenue__c = oppProduct.Expected_Revenue__c;
            oppProductTemp.Expected_Revenue_Fee__c = oppProduct.Expected_Revenue_Fee__c;
            oppProductTemp.Expected_Revenue_Total__c = oppProduct.Expected_Revenue_Total__c;
            oppProductTemp.Expected_Util_Year_Fee__c = oppProduct.Expected_Util_Year_Fee__c;
            oppProductTemp.Expected_Util_Year_NI__c = oppProduct.Expected_Util_Year_NI__c;
            oppProductTemp.Expected_Utilization_Vol_Fee__c = oppProduct.Expected_Utilization_Vol_Fee__c;
            oppProductTemp.Expected_Utilization_Vol_NI__c = oppProduct.Expected_Utilization_Vol_NI__c;
            oppProductTemp.Frequency__c = oppProduct.Frequency__c;
            oppProductTemp.FrequencyFee__c = oppProduct.FrequencyFee__c;
            oppProductTemp.Notional_Amount__c = oppProduct.Notional_Amount__c;
            oppProductTemp.Opportunity__c = oppProduct.OpportunityId;
            oppProductTemp.OpportunityLineItemId__c = oppProduct.Id;
            oppProductTemp.OriginalStartMonth__c = oppProduct.OriginalStartMonth__c;
            oppProductTemp.OriginalStartMonthFee__c = oppProduct.OriginalStartMonthFee__c;
            oppProductTemp.OriginalStartYear__c = oppProduct.OriginalStartYear__c;
            oppProductTemp.OriginalStartYearFee__c = oppProduct.OriginalStartYearFee__c;
            oppProductTemp.Product_Domain__c = oppProduct.Product_Domain__c;
            oppProductTemp.Product__c = oppProduct.Product2Id;
            oppProductTemp.Quantity__c = oppProduct.Quantity;
            oppProductTemp.Recurring_Type__c = oppProduct.Recurring_Type__c;
            oppProductTemp.Recurring_Type_Fee__c = oppProduct.Recurring_Type_Fee__c;
            oppProductTemp.RevisedStartMonth__c = oppProduct.RevisedStartMonth__c;
            oppProductTemp.RevisedStartMonthFee__c = oppProduct.RevisedStartMonthFee__c;
            oppProductTemp.RevisedStartYear__c = oppProduct.RevisedStartYear__c;
            oppProductTemp.RevisedStartYearFee__c = oppProduct.RevisedStartYearFee__c;
            oppProductTemp.Tenor_Years__c = oppProduct.Tenor_Years__c;
            oppProductTemp.This_Year_Expected_Fee__c = oppProduct.This_Year_Expected_Fee__c;
            oppProductTemp.This_Year_Expected_NI__c = oppProduct.This_Year_Expected_NI__c;
            oppProductTemp.Total_Volume_Amount__c = oppProduct.Total_Volume_Amount__c;
            oppProductTemp.Type_Of_Reference__c = oppProduct.Type_Of_Reference__c;
            oppProductTemp.Type_Of_Reference_Fee__c = oppProduct.Type_Of_Reference_Fee__c;
            oppProductTemp.UnitPrice__c = oppProduct.UnitPrice;

            lstOppProductTemp.add(oppProductTemp);
        }
        
        insert lstOppProductTemp;

        
    }
}