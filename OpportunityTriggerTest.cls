@IsTest


public class OpportunityTriggerTest {
    public static String STR_POSITIVE = 'positive';
    public static String STR_NEGATIVE = 'negative';
    
        static {

        TestUtils.CreateProceBookAccess();
        TestUtils.createAppConfig();
        TestUtils.createStatusCode();        
        TestUtils.createIdType();
        TestUtils.createTriggerMsg();
        TestUtils.createObjUserPermission();
        TestUtils.createDisqualifiedReason();
        TestUtils.CreateOpportunityStage();
      
    }
    
    public static testmethod void RunPositiveTestOpportunity(){
        System.debug(':::: RunPositiveTestOpportunity Start ::::');
        
        TestInit.createUser(false);
        User u = TestInit.us;
        user u2 = TestInit.us2;
        System.runAs(u) {
            
            TestInit.createAccount( 3 );
            List<Account> accountList = TestInit.accountList.values();
            
            TestInit.createTarget( TestInit.accountList.values() ); 
            
            
            TEST.startTest();
            accountList.get(0).OwnerId = u2.Id;
            update accountList.get(0);
            TestInit.createOpportunity( TestInit.accountList.values() ); 
            List<Opportunity> opportunityList = TestInit.opportunityList.values();
            accountList.get(0).OwnerId = u.Id;
            update accountList.get(0);
            TestInit.createOpportunity( TestInit.accountList.values() ); 
            opportunityList = TestInit.opportunityList.values();
            List<String> listStageName = new List<String>();
            listStageName.add('CA-Prep');
            listStageName.add('Approval Process');
            listStageName.add('Post Approval');
            listStageName.add('Limit Setup');
            Integer sumAmount = 0;
            List<Opportunity> listOpp = [select Id,Name,StageName from Opportunity where Id IN :opportunityList order by Id asc];
            List<Target__c> listTarget = [select OwnerId,Monthly_Close_Sales_Actual__c,Monthly_Close_Sales_Amount__c,
                                          Monthly_App_In_Actual__c,Monthly_App_In_Amount__c,
                                          Monthly_Approved_Actual__c,Monthly_Approved_Amount__c,
                                          Monthly_Set_Up_Actual__c,Monthly_Set_Up_Amount__c
                                          from Target__c 
                                          where Id IN :TestInit.targetList.keySet() Order by Id asc];
            for(Integer i = 0 ; i < listStageName.size() ; i++){ 
               
                for( Opportunity o : listOpp ){
                    o.StageName = listStageName.get(i);
                    o.Approved_Amount__c = 10*(i+1);
                }
                update listOpp;                   
            }
            //Test For CAID
            List<AccountTeamMember> accountTeamMemberList = TestInit.createAccountTeamMember(1,accountList.get(0).id,UserInfo.getUserId());
            List<Opportunity> listTest = new List<Opportunity>();
            for( Integer i = 0 ; i<1 ; i++ ){
                Opportunity o = new Opportunity();
                o.OwnerId = UserInfo.getUserId();
                o.StageName = 'Analysis';
                o.Name = 'test-'+i;
                o.CloseDate = Date.today();
                o.AccountId = accountList.get(0).Id;
                o.Trigger_flag__c = true;
                listTest.add(o);
            }
            insert listTest;
            System.debug('::::: insert ListTest is : ' + listTest.size() +' row :::::');
            
            TEST.stopTest();
        }
        System.debug(':::: RunPositiveTestOpportunity End ::::');
    }
    
    public static void flowTest(String testMode){
        //Test For T07,T08,T09,T10
        List<String> listStageName = new List<String>();
        listStageName.add('CA-Prep');
        listStageName.add('Approval Process');
        listStageName.add('Post Approval');
        listStageName.add('Limit Setup');
        Integer sumAmount = 0;
        List<Opportunity> listOpp = [select Id,Name,StageName from Opportunity where Id IN :TestInit.opportunityList.keySet() order by Id asc];
        List<Target__c> listTarget = [select OwnerId,Monthly_Close_Sales_Actual__c,Monthly_Close_Sales_Amount__c,
                                      Monthly_App_In_Actual__c,Monthly_App_In_Amount__c,
                                      Monthly_Approved_Actual__c,Monthly_Approved_Amount__c,
                                      Monthly_Set_Up_Actual__c,Monthly_Set_Up_Amount__c
                                      from Target__c 
                                      where Id IN :TestInit.targetList.keySet() Order by Id asc];
        for(Integer i = 0 ; i < listStageName.size() ; i++){ 
            System.debug('oBefore : '+listOpp);
            for( Opportunity o : listOpp ){
                o.StageName = listStageName.get(i);
                o.Approved_Amount__c = 10*(i+1);
            }
            System.debug('oAfter : '+listOpp);
            update listOpp;
            
            Date currentDate = Date.today();
            Integer currentYear = currentDate.year();
            Integer currentMonth = currentDate.month();
            
            for( Target__c target : listTarget ){
                /*if( i == 0){
System.assertEquals(1, target.Monthly_Close_Sales_Actual__c);
System.assertEquals(10, target.Monthly_Close_Sales_Amount__c);  
}else if( i == 1){
System.assertEquals(1, target.Monthly_App_In_Actual__c);
System.assertEquals(20, target.Monthly_App_In_Amount__c);
}else if( i == 2){
System.assertEquals(1, target.Monthly_Approved_Actual__c);
System.assertEquals(30, target.Monthly_Approved_Amount__c);
}else if( i == 3){
System.assertEquals(1, target.Monthly_Set_Up_Actual__c);
System.assertEquals(40, target.Monthly_Set_Up_Amount__c);
}
*/
            }      
        }
    }
}