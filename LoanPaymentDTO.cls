global class LoanPaymentDTO {

	global string PaymentMethod { get; set; }
	global string SavingAccount { get; set; }
	global decimal InstallmentBalance { get; set; }
	global decimal PayrollDeductionUnit { get; set; }




}