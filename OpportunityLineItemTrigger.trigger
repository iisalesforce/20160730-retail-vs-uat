trigger OpportunityLineItemTrigger On OpportunityLineItem (before insert, after insert, before update, after update, after delete){

    Boolean RunTrigger = AppConfig__c.getValues('runtrigger').Value__c == 'true' ; 

    if(Trigger.isBefore && Trigger.isInsert) 
    {  
        system.debug('OpportunityLineItemTrigger before insert');

    }
    else if(Trigger.isAfter && Trigger.isInsert) 
    {  
        system.debug('OpportunityLineItemTrigger after insert');
        if(RunTrigger || Test.isRunningTest()){
            //OpportunityLineItemTriggerHandler.CalculateOpportunityTotalVol(Trigger.new); 
            //OpportunityLineItemTriggerHandler.StampOriginalStart(Trigger.new); 
        }
    }
    else if(Trigger.isBefore && Trigger.isUpdate) 
    {  
        system.debug('OpportunityLineItemTrigger before update');

    }
    else if(Trigger.isAfter && Trigger.isUpdate) 
    {  
        system.debug('OpportunityLineItemTrigger after update');
        if(RunTrigger || Test.isRunningTest()){
            //OpportunityLineItemTriggerHandler.CalculateOpportunityTotalVol(Trigger.new); 
            //OpportunityLineItemTriggerHandler.StampOriginalStart(Trigger.new); 
        }
    }
    else if(Trigger.isAfter && Trigger.isDelete) 
    {
        system.debug('OpportunityLineItemTrigger after delete');
        if(RunTrigger || Test.isRunningTest()){
			OpportunityLineItemTriggerHandler.getDeletedRecord(Trigger.oldMap);
            //OpportunityLineItemTriggerHandler.CalculateOpportunityTotalVol(Trigger.old); 
        }
    }

}