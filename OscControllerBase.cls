public virtual class OscControllerBase {
	public static String serviceType = '';
	static {
		AppConfig__c cf = AppConfig__c.getValues('SVR_MODE');
		serviceType = (cf == null ? 'soap' : cf.Value__c);
	}
	protected Map<String, String> params;
	//public String instanceUrl { get; set; }
	//public String returnedContinuationId { get; set; }
	protected final Integer TIMEOUT_INT_SECS = 60;

	@testVisible
	protected String SVR_MODE = OscControllerBase.serviceType;
	public String PageMessage { get; set; }

	//Re-Used Code
	public Continuation startRequest() {
		PageMessage = '';
		Continuation cont = new Continuation(TIMEOUT_INT_SECS);
		if (SVR_MODE == 'soap')
		{
			CallSOAP(cont);
		}
		return cont;
	}

	public virtual void CallSOAP(Continuation cont){


	}

}