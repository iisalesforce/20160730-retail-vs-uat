@isTest
global class TMBInsertContactMock implements WebServiceMock{
	public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType) {
       
		System.debug(LoggingLevel.INFO, 'TMBServiceProxyMockImpl.doInvoke() - ' +
			'\n request: ' + request +
			'\n response: ' + response +
			'\n endpoint: ' + endpoint +
			'\n soapAction: ' + soapAction +
			'\n requestName: ' + requestName +
			'\n responseNS: ' + responseNS +
			'\n responseName: ' + responseName +
			'\n responseType: ' + responseType);
                
                
              TMBServiceProxy.ContactResultDTO InsertResult = new TMBServiceProxy.ContactResultDTO(); 
                

                InsertResult.status = '0000';
                InsertResult.totalrecord = '1';
                InsertResult.massage = '';
                
                
                TMBServiceProxy.InsertContactResponse_element respondelement = new TMBServiceProxy.InsertContactResponse_element();
       			respondelement.InsertContactResult = InsertResult;
                
                response.put( 'response_x', respondelement);
            }
}