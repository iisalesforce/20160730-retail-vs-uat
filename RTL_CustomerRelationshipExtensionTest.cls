@isTest
public class RTL_CustomerRelationshipExtensionTest {
    static {
        TestUtils.createAppConfig();
    }
    	
    public static testMethod void testExtController() {
		Account acct = new Account(Name='TestAcct1',Mobile_Number_PE__c='0123456789',Account_Type__c='Retail Customer');
    	insert acct;
        Test.setMock(WebServiceMock.class, new RTL_CvsAnalyticsDataServiceMockImpl());
        
        TEST.startTest();
    	//1st time the two values should be blank
    	acct = [select Id, RTL_OTC_ATM_ADM_IB_MIB__c, RTL_MIB_Status__c from Account where Id=:acct.Id];
    	System.assertEquals(null, acct.RTL_OTC_ATM_ADM_IB_MIB__c);
    	System.assertEquals(null, acct.RTL_MIB_Status__c);
    	//after calling controller, the value is retrieved from webservices
    	ApexPages.StandardController controller = new ApexPages.StandardController(acct);
    	RTL_CustomerRelationshipExtension extController = new RTL_CustomerRelationshipExtension(controller);
    	// as controller calls online service, there is no value for the two fields. 
    	// add test values
    	extController.rtl_OtcAtmAdmIbMib='20:10:10:20:20:20';
    	extController.rtl_MibStatus='Applied';
    	//after calling action method, the value is persistent in DB
    	extController.updateAccount();
    	acct = [select Id, RTL_OTC_ATM_ADM_IB_MIB__c, RTL_MIB_Status__c from Account where Id=:acct.Id];
    	System.assert(acct.RTL_OTC_ATM_ADM_IB_MIB__c.length() > 0);
    	System.assert(acct.RTL_MIB_Status__c.length() > 0);   
        System.assertEquals(false, extController.isSF1);
        Test.stopTest();
    }
}