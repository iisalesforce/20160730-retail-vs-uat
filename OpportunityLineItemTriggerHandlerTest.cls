@isTest
public class OpportunityLineItemTriggerHandlerTest {
	public static testMethod void RunPositiveTestOpptyTeam(){
        System.debug(':::: RunPositive Test Opportunity Team Member Start ::::');
        TestInit.createUser(false);
        System.runAs(TestInit.us) {
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());
            System.debug('Current User Id: ' + UserInfo.getUserId()); 
           	// --- create account ---
		    List<Account> accList = TestUtils.createAccounts(1, 'test', 'CType', true);
            system.debug('### accList : '+accList.size());
            
            system.debug('### Test ');
            // --- create opp ---
            List<Opportunity> oppList = TestUtils.createOpportunity(10, accList.get(0).Id, true);
            system.debug('### oppList : '+oppList.size());
            // ------		
			List<Product2> prod = TestUtils.createProduct(250, true);
            system.debug('###prod :'+prod.size());
            Id pricebookId = Test.getStandardPricebookId();
            List<PricebookEntry> priceEntry = TestUtils.createPricebookEntry(200, pricebookId, prod, true);
            system.debug('###priceEntry :'+priceEntry.size());
            
            Test.startTest();
			RunSingleRecord(oppList,priceEntry);
            Run200Records(oppList,priceEntry);
            Test.stopTest();
        }
        System.debug(':::: RunPositive Test Opportunity Team Member End ::::');
    }
    
    public static void RunSingleRecord(List<Opportunity> oppList , List<PricebookEntry> priceEntry){
        List<OpportunityLineItem> oppProd = TestUtils.createOpportunityProduct(1, oppList.get(0).Id, priceEntry, true);
        system.debug('### oppProd : '+oppProd.size());

			try{
				delete oppProd;
			}catch(DmlException e){
				System.debug('### The following exception has occurred: ' + e.getMessage());
			}catch(Exception e){
				System.debug('### An exception occurred: ' + e.getMessage());
			}
        // --- check number of records ---
		List<OpportunityLineItem> oppProdAfterDel = new List<OpportunityLineItem>([SELECT Id FROM OpportunityLineItem]);
		system.debug('### oppProdAfterDel : '+oppProdAfterDel.size());
		System.assertEquals(oppProdAfterDel.size(),0);

    }
    
    public static void Run200Records(List<Opportunity> oppList , List<PricebookEntry> priceEntry){
        system.debug('### test : ');
        List<OpportunityLineItem> oppProd1 = TestUtils.createOpportunityProduct(50, oppList.get(1).Id, priceEntry, true);
        List<OpportunityLineItem> oppProd2 = TestUtils.createOpportunityProduct(50, oppList.get(2).Id, priceEntry, true);
        List<OpportunityLineItem> oppProd3 = TestUtils.createOpportunityProduct(50, oppList.get(3).Id, priceEntry, true);
        List<OpportunityLineItem> oppProd4 = TestUtils.createOpportunityProduct(50, oppList.get(4).Id, priceEntry, true);
        List<OpportunityLineItem> oppProdAll = new List<OpportunityLineItem>();
        oppProdAll.addAll(oppProd1);
        oppProdAll.addAll(oppProd2);
        oppProdAll.addAll(oppProd3);
        oppProdAll.addAll(oppProd4);
        
		system.debug('### oppProdAll : '+oppProdAll.size());
			try{
				delete oppProdAll;
			}catch(DmlException e){
				System.debug('### The following exception has occurred: ' + e.getMessage());
			}catch(Exception e){
				System.debug('### An exception occurred: ' + e.getMessage());
			}
        // --- check number of records ---
		List<OpportunityLineItem> oppProdAfterDel2 = new List<OpportunityLineItem>([SELECT Id FROM OpportunityLineItem]);
		system.debug('### oppProdAfterDel2 : '+oppProdAfterDel2.size());
		System.assertEquals(oppProdAfterDel2.size(),0);
    }
}