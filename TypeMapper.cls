public class TypeMapper {
	/*----------------------------------------------------------------------------------
	  Author:        Keattisak Chinburarat
	  Company:       I&I Consulting 
	  Description:   Auto mapper from SOAP Model to DTO
	  Inputs:        -	   
	  Base Class:    -
	  Test Class:    -
	  History
	  <Date>      <Authors Name>     <Brief Description of Change>
	  2016-05-09   Keattisak.C        First Draft
	  ----------------------------------------------------------------------------------*/
	public static Object MappingSoapToDTO(Object request)
	{
		Object ret; 
		/*--------------------------------------------------------------------------------
		  OSC01
		  ----------------------------------------------------------------------------------*/
		if (request instanceOf RTL_CustomerProductHoldingsService.CustomerProductHoldings) {
			RTL_CustomerProductHoldingsService.CustomerProductHoldings item = ((RTL_CustomerProductHoldingsService.CustomerProductHoldings) request);
			ProductHoldingDTO mapping = new ProductHoldingDTO();
			// Error Handler message
			mapping.SoapStatus = item.Status == null ? '' : item.Status;
			mapping.SoapMessage = TypeMapper.errorMessageMapping(TypeMapper.DefaultValue(item.Message));
			//STPE 1 ) RTL_CustomerProductHoldingsService.DepositAccount[]

			if (null != item.DepositAccounts && item.DepositAccounts.DepositAccount != null) {
				for (RTL_CustomerProductHoldingsService.DepositAccount dep : item.DepositAccounts.DepositAccount) {
					DepositeProductDTO depObj = new DepositeProductDTO();
					//========================= Mapping =============================//
					depObj.DepositAccountNumber = dep.AccountNumber;
					depObj.ProductType = dep.ProductType;
					depObj.DepositProductCode = dep.ProductCode;
                    depObj.ProjectCode = TypeMapper.DefaultValue(dep.ProjectCode);
                    depObj.DataSource = TypeMapper.DefaultValue(dep.DataSource);
					depObj.OpenedDate = dep.OpenedDate; 
					depObj.Status = dep.AccountStatus;
					depObj.OutStanding = dep.Outstanding;
					depObj.AvgOutStanding = dep.AvgOutstanding;
					depObj.IntEarning = dep.InterestEarned;
					depObj.Other = dep.Other;
					depObj.HasJoint = dep.HasJoint;
					mapping.DepositeProducts.add(depObj);
					System.debug('Outstanding :: '+depObj.OutStanding);
					System.debug('AvgOutstanding :: '+depObj.AvgOutStanding);
				}
			}
			//System.debug('MULESOFT (OSC01): => RTL_CustomerProductHoldingsService.CreditCardAccount Status = ' + (item.CreditCardAccounts.Status == null ? 'null : Error' : item.CreditCardAccounts.Status));
			//System.debug('MULESOFT (OSC01): => RTL_CustomerProductHoldingsService.CreditCardAccount Size = ' + (item.CreditCardAccounts.CreditCardAccount != null ? item.CreditCardAccounts.CreditCardAccount.size() + ' Items' : 'SOAP Object null'));
			if (null != item.CreditCardAccounts && item.CreditCardAccounts.CreditCardAccount != null)
			{
				//STPE 2) RTL_CustomerProductHoldingsService.CreditCardAccount[]
				for (RTL_CustomerProductHoldingsService.CreditCardAccount credit : item.CreditCardAccounts.CreditCardAccount) {
					CreditCardRDCProductDTO creditObj = new CreditCardRDCProductDTO();
					//========================= Mapping =============================//
					creditObj.CardNumber = credit.CardNumber;
					creditObj.CreditCardType = credit.CreditCardType;
					creditObj.SubProductGroup = '';
					creditObj.ProductName = '';
					creditObj.OpenedDate = credit.OpenedDate;
					creditObj.Status = credit.AccountStatus;
					creditObj.Outstanding = credit.Outstanding == null ? 0 : credit.Outstanding;
					creditObj.VLimit = credit.Limit_x == null ? 0 : credit.Limit_x;
					mapping.CreditCardRDCProducts.add(creditObj);
				}
			}
			//System.debug('MULESOFT (OSC01): =>  RTL_CustomerProductHoldingsService.LoanAccount Status = ' + (item.LoanAccounts.Status == null ? 'null : Error' : item.LoanAccounts.Status));
			//System.debug('MULESOFT (OSC01): => RTL_CustomerProductHoldingsService.LoanAccount Size = ' + (item.LoanAccounts.LoanAccount != null ? item.LoanAccounts.LoanAccount.size() + ' Items' : 'SOAP Object null'));

			if (null != item.LoanAccounts && item.LoanAccounts.LoanAccount != null)
			{

				//STPE 3) RTL_CustomerProductHoldingsService.CreditCardAccount[] 
				for (RTL_CustomerProductHoldingsService.LoanAccount loan : item.LoanAccounts.LoanAccount) {
					LoanProductDTO loanObj = new LoanProductDTO();
					//========================= Mapping =============================//
					loanObj.LoanAccountNumber = loan.AccountNumber;
					loanObj.LoanProductCode = loan.ProductCode;
                    loanObj.ProjectCode = TypeMapper.DefaultValue(loan.ProjectCode);
                    loanObj.DataSource = TypeMapper.DefaultValue(loan.DataSource);
					loanObj.ProductType = loan.ProductType;
					loanObj.OpenedDate = loan.OpenedDate;
					loanObj.Status = loan.AccountStatus;
					loanObj.Outstanding = loan.Outstanding == null ? 0 : loan.Outstanding;
					loanObj.VLimit = loan.Limit_x == null ? 0 : loan.Limit_x;
					loanObj.MuturityDate = loan.MaturityDate;
					loanObj.HasCoBorrower = loan.HasCoborrower;
					mapping.LoanProducts.add(loanObj);
				}
			}
			//System.debug('MULESOFT (OSC01): => RTL_CustomerProductHoldingsService.BancassuranceAccount Status = ' + (item.BancassuranceAccounts.Status == null ? 'null : Error' : item.BancassuranceAccounts.Status));
			//System.debug('MULESOFT (OSC01): => RTL_CustomerProductHoldingsService.BancassuranceAccount Size = ' + (item.BancassuranceAccounts.BancassuranceAccount != null ? item.BancassuranceAccounts.BancassuranceAccount.size() + ' Items' : 'SOAP Object null'));

			if (null != item.BancassuranceAccounts && item.BancassuranceAccounts.BancassuranceAccount != null)
			{
				//STPE 4) RTL_CustomerProductHoldingsService.CreditCardAccount[] 
				for (RTL_CustomerProductHoldingsService.BancassuranceAccount ban : item.BancassuranceAccounts.BancassuranceAccount) {
					BancassuranceDTO banObj = new BancassuranceDTO();
					//========================= Mapping =============================//
					banObj.PolicyNo = ban.PolicyNumber;
					banObj.SubProductGroup = ban.SubProductGroup;
					banObj.ProductName = ban.ProductName;
					banObj.InsuranceCompany = ban.InsuranceCompany;
					banObj.OpenedDate = ban.OpenedDate;
					banObj.Status = ban.PolicyStatus;
					banObj.SumInsure = ban.SumInsure == null ? 0 : ban.SumInsure;
					banObj.AFYP = ban.AFVP == null ? 0 : ban.AFVP;
					banObj.ExpiryDate = ban.ExpiryDate;
					mapping.Bancassurances.add(banObj);
				}
			}
			//System.debug('MULESOFT (OSC01): => RTL_CustomerProductHoldingsService.InvestmentAccount Status = ' + (item.InvestmentAccounts.Status == null ? 'null : Error' : item.InvestmentAccounts.Status));
			//System.debug('MULESOFT (OSC01): => RTL_CustomerProductHoldingsService.InvestmentAccount Size = ' + (item.InvestmentAccounts.InvestmentAccount != null ? item.InvestmentAccounts.InvestmentAccount.size() + ' Items' : 'SOAP Object null'));

			if (null != item.InvestmentAccounts && item.InvestmentAccounts.InvestmentAccount != null)
			{
				//STPE 5) RTL_CustomerProductHoldingsService.CreditCardAccount[] 
				for (RTL_CustomerProductHoldingsService.InvestmentAccount inv : item.InvestmentAccounts.InvestmentAccount) {
					InvestmentProductDTO invObj = new InvestmentProductDTO();
					//========================= Mapping =============================//
					invObj.UnitHolderNo = inv.UnitHoldNo;
					invObj.SubProductGroup = '' /*cannot mapping*/;
					invObj.ProductName = '' /*cannot mapping*/;
					invObj.AssetClass = '' /*cannot mapping*/;
					invObj.FundCode = inv.FundCode;
					invObj.IssuerFundHouse = inv.Issuer;
					invObj.OpenedDate = inv.OpenedDate;
					invObj.Units = inv.Units;
					invObj.InitialValue = inv.InitialValue;
					invObj.MarketValue = inv.MarketValue;
					invObj.UnrealizedGL = inv.UnrealizedGL;
					mapping.InvestmentProducts.add(invObj);
				}
			}
			ret = mapping;
		}

		/*--------------------------------------------------------------------------------
		  OSC02
		  ----------------------------------------------------------------------------------*/
		if (request instanceOf RTL_DepositProductDetailsService.DepositProductDetails) {
			RTL_DepositProductDetailsService.DepositProductDetails item = ((RTL_DepositProductDetailsService.DepositProductDetails) request);


			//	System.debug('MULESOFT (OSC02): => RTL_DepositProductDetailsService.DepositProductDetails Status = '
			//	             + (item.Status == null ? 'null : Error' : item.Status));

			DepositeProductDetailDTO mapping = new DepositeProductDetailDTO();
			// Error Handler message
			mapping.SoapStatus = item.Status == null ? '' : item.Status;
			mapping.SoapMessage = TypeMapper.errorMessageMapping(TypeMapper.DefaultValue(item.Message));

			/*====================================================/
			 *	DepositAccountInfo
			 *====================================================*/


			if (null != item.DepositAccount)
			{
				string productcode = TypeMapper.DefaultValue(item.DepositAccount.ProductCode);
                productcode = productcode.length()>2?productcode.substring(2):productcode;

				mapping.DepositAccountNumber = TypeMapper.DefaultValue(item.DepositAccount.AccountNumber);
				mapping.DepositProductCode = productcode;
				mapping.SubProductGroup = '';
				mapping.ProductName = '';
				mapping.OpenedDate = item.DepositAccount.OpenedDate;
				mapping.Status = TypeMapper.DefaultValue(item.DepositAccount.AccountStatus);
				mapping.OutStanding = item.DepositAccount.Outstanding;
				//mapping.AvgOutStanding             =  item.;
				mapping.InterestEarned = TypeMapper.DefaultValue(item.DepositAccount.InterestEarned);
				//mapping.Other = item.DepositAccount.;
				mapping.HasJoint = TypeMapper.DefaultValue(item.DepositAccount.HasJoint);
				mapping.AccountName = TypeMapper.DefaultValue(item.DepositAccount.AccountName);
				mapping.InterestRate = TypeMapper.DefaultValue(item.DepositAccount.InterestRate);
				mapping.MaturityDate = item.DepositAccount.MaturityDate;
				mapping.SMSAlertService = TypeMapper.DefaultValue(item.DepositAccount.SMSAlertService);
				mapping.MEAccountBundledBank = TypeMapper.DefaultValue(item.DepositAccount.MEAccountBundledBank);
				mapping.NoActiveDebitCardBundling = TypeMapper.DefaultValue(item.DepositAccount.NumberOfActiveDebitCardBundling);
				mapping.AvgBalanceMTD = item.DepositAccount.AvgBalanceMTD;
				System.debug('Outstanding :: '+item.DepositAccount.Outstanding);
				System.debug('AvgOutstanding :: '+item.DepositAccount.AvgBalanceMTD);
			}
			/*====================================================/
			 *	JointAccountInfo
			 *    NOTE:
			 *         WDSL Return Lists
			 *====================================================*/

			if (null != item.JointAccount && null != item.JointAccount.JointAccount)
			{
				/*JoinAccountInformationDTO joinObj = new JoinAccountInformationDTO();
				mapping.JoinAccountInformations.add(joinObj);
				joinObj.JointAccountOwnerNumber = TypeMapper.DefaultValue(item.JointAccount.JointAccount.OwnerNumber);
				joinObj.Relationship = TypeMapper.DefaultValue(item.JointAccount.JointAccount.Relationship);
				*/
                /*drop 2 will use below code*/
                for(RTL_DepositProductDetailsService.JointAccountInfo jointAcct : item.JointAccount.JointAccount){
					JoinAccountInformationDTO joinObj = new JoinAccountInformationDTO();
					mapping.JoinAccountInformations.add(joinObj);
					joinObj.JointAccountOwnerNumber = TypeMapper.DefaultValue(jointAcct.OwnerNumber);
					joinObj.Relationship = TypeMapper.DefaultValue(jointAcct.Relationship);
                }
			} else {
				JoinAccountInformationDTO joinObj = new JoinAccountInformationDTO();
				mapping.JoinAccountInformations.add(joinObj);
			}

			/*====================================================/
			 *	Last Month Transaction Summary			 
			 *====================================================*/
			if (null != item.LastMonthTransactionSummary)
			{
				LastMonthTransactionSummaryDTO lastmonth = new LastMonthTransactionSummaryDTO();
				mapping.LastMonthTransactionSummary = lastmonth;
				lastmonth.OfDeposit = TypeMapper.DefaultValue(item.LastMonthTransactionSummary.MonthlyAvgDepositTransactions);
				lastmonth.OfWithdraw = TypeMapper.DefaultValue(item.LastMonthTransactionSummary.MonthlyAvgWithdrawTransactions);
				lastmonth.OfTransferOutTMB = TypeMapper.DefaultValue(item.LastMonthTransactionSummary.MonthlyAvgTransfersWithinTMB);
				lastmonth.OfTransferOutOther = TypeMapper.DefaultValue(item.LastMonthTransactionSummary.MonthlyAvgTransfersToOtherBanks);
				lastmonth.OfBillPayment = TypeMapper.DefaultValue(item.LastMonthTransactionSummary.MonthlyAvgBillPaymentTransactions);
			}

			/*====================================================/
			 *	BeneficiaryInfo
			 *    NOTE:
			 *         WDSL Return Lists
			 *====================================================*/
			if (null != item.Beneficiary && null != item.Beneficiary.Beneficiary)
			{
				/*BeneficiaryInformationDTO ben = new BeneficiaryInformationDTO();
				mapping.BeneficiaryInformations.add(ben);
				ben.BeneficiaryName = item.Beneficiary.Beneficiary.Name;
				ben.Relationship = item.Beneficiary.Beneficiary.Relationship;
				ben.BenefitPercent = item.Beneficiary.Beneficiary.BenefitPercentage;
				*/
				//drop 2 will use below code
                for(RTL_DepositProductDetailsService.BeneficiaryInfo benefit : item.Beneficiary.Beneficiary){
                    BeneficiaryInformationDTO ben = new BeneficiaryInformationDTO();
					mapping.BeneficiaryInformations.add(ben);
					ben.BeneficiaryName = benefit.Name;
					ben.Relationship = benefit.Relationship;
					ben.BenefitPercent = benefit.BenefitPercentage;
                }
                

			} else {
				BeneficiaryInformationDTO ben = new BeneficiaryInformationDTO();
				mapping.BeneficiaryInformations.add(ben);
			}

			/*====================================================/
			 *	StandingOrder
			 *    NOTE:
			 *         WDSL Return only 1 item
			 *====================================================*/
			if (null != item.StandingOrder && null != item.StandingOrder.StandingOrder)
			{
				StandingOrderDTO stdOrder = new StandingOrderDTO();
				mapping.StandingOrders.add(stdOrder);
				stdOrder.RequestDate = item.StandingOrder.StandingOrder.RequestDate;
				stdOrder.Frequency = item.StandingOrder.StandingOrder.Frequency;
				stdOrder.SettlementDate = item.StandingOrder.StandingOrder.SettlementDate;
				stdOrder.Type = item.StandingOrder.StandingOrder.Type_x;
				stdOrder.LastAmount = item.StandingOrder.StandingOrder.LastAmount;
				stdOrder.LastTrxDate = item.StandingOrder.StandingOrder.LastTransactionDate;

			}



			ret = mapping;
		}

		/*--------------------------------------------------------------------------------
		  OSC03
		  ----------------------------------------------------------------------------------*/
		if (request instanceof RTL_CreditCardDetailsService.CreditCardDetails) {
			RTL_CreditCardDetailsService.CreditCardDetails item = ((RTL_CreditCardDetailsService.CreditCardDetails) request);
			CreditcardInformationDTO mapping = new CreditcardInformationDTO();
			// Error Handler message
			mapping.SoapStatus = item.Status == null ? '' : item.Status;
			mapping.SoapMessage = TypeMapper.errorMessageMapping(TypeMapper.DefaultValue(item.Message));

			/*====================================================/
			 *	CreditCardInfo
			 *====================================================*/
			if (null != item.CreditCardInfo)
			{
				mapping.CardNumber = TypeMapper.DefaultValue(item.CreditCardInfo.CardNumber);
				mapping.CreditLimit = TypeMapper.DefaultValue(item.CreditCardInfo.CreditLimit);
				mapping.TemporaryLine = TypeMapper.DefaultValue(item.CreditCardInfo.TemporaryLine);
				mapping.CycleCut = TypeMapper.DefaultValue(item.CreditCardInfo.CycleCut);
				mapping.UsageStatus = TypeMapper.DefaultValue(item.CreditCardInfo.UsageStatus);
				mapping.BlockCode = TypeMapper.DefaultValue(item.CreditCardInfo.BlockCode);
				mapping.NextExpiredPointOn = item.CreditCardInfo.NextExpiredPointOn;
				mapping.CashChillChill = TypeMapper.DefaultValue(item.CreditCardInfo.CashChillChill);
				mapping.CashWithdrawalAccountNumber = TypeMapper.DefaultValue(item.CreditCardInfo.CashWithdrawalAccountNumber);
				mapping.OpenedDate = item.CreditCardInfo.OpenedDate;
				mapping.CurrentBalance = TypeMapper.DefaultValue(item.CreditCardInfo.CurrentBalance);
				mapping.TemporaryLinePeriod = TypeMapper.DefaultValue(item.CreditCardInfo.TemporaryLinePeriod);
				mapping.PaymentDue = item.CreditCardInfo.PaymentDue;
				mapping.PaymentBehavior = TypeMapper.DefaultValue(item.CreditCardInfo.PaymentBehavior);
				mapping.RewardPoints = TypeMapper.DefaultValue(item.CreditCardInfo.RewardPoints);
				mapping.NextExpiredPoints = TypeMapper.DefaultValue(item.CreditCardInfo.NextExpiredPoints);
				mapping.CashChillChillStatus = TypeMapper.DefaultValue(item.CreditCardInfo.CashChillChillStatus);
				mapping.DirectDebitAccountNumber = TypeMapper.DefaultValue(item.CreditCardInfo.DirectDebitAccountNumber);
				mapping.UsageBehavior = TypeMapper.DefaultValue(item.CreditCardInfo.UsageBehavior);
				mapping.LastPaymentDate = item.CreditCardInfo.LastPaymentDate;
			}


			/*====================================================/
			 *	SupplementaryInfo		
			 *	NOTE:
			 *         WDSL Return only 1 item
			 *====================================================*/
			if (null != item.SupplementaryInfo && null != item.SupplementaryInfo.SupplementaryInfo)
			{
				SupplementaryInformationDTO supplementInfo = new SupplementaryInformationDTO();
				mapping.SupplementaryInformations.add(supplementInfo);
				supplementInfo.SupplementaryCardNumber = TypeMapper.DefaultValue(item.SupplementaryInfo.SupplementaryInfo.CardNumber);
				supplementInfo.SupplementaryCardholderName = TypeMapper.DefaultValue(item.SupplementaryInfo.SupplementaryInfo.CardHolderName);

			}
			else {
				SupplementaryInformationDTO supplementInfo = new SupplementaryInformationDTO();
				mapping.SupplementaryInformations.add(supplementInfo);
			}
			/*====================================================/
			 *	PayPlanRecord
			 *====================================================*/
			if (null != item.PayPlanRecord)
			{
				SoGoodPayPlanDTO soGood = new SoGoodPayPlanDTO(); 
				mapping.SoGoodPayPlan = soGood;
				soGood.LastCycleTransactionNonInterestCharge = TypeMapper.DefaultValue(item.PayPlanRecord.NumberOfNonInterestChargeTransactions);
				soGood.LastCycleAmountInTHBNonInterestCharge = TypeMapper.DefaultValue(item.PayPlanRecord.AmountOfNonInterestChargeTransactions);
				soGood.LastCycleAmountInTHBNonInterestCharge = TypeMapper.DefaultValue(item.PayPlanRecord.NumberOfInterestChargeTransactions);
				soGood.LastCycleAmountInTHBInterestCharge = TypeMapper.DefaultValue(item.PayPlanRecord.AmountOfInterestChargeTransactions);
				soGood.TotalTransaction = TypeMapper.DefaultValue(item.PayPlanRecord.TotalNumberOfTransactions);
				soGood.TotalTHB = TypeMapper.DefaultValue(item.PayPlanRecord.TotalAmountOfTransactions);
			}
			ret = mapping;

		}
		//OSC04
		if (request instanceOf RTL_LoanProductDetailsService.LoanProductDetails) {
			RTL_LoanProductDetailsService.LoanProductDetails item = ((RTL_LoanProductDetailsService.LoanProductDetails) request);
			LoanProductDetailDTO mapping = new LoanProductDetailDTO();
			// Error Handler message
			mapping.SoapStatus = item.Status == null ? '' : item.Status;
			mapping.SoapMessage = TypeMapper.errorMessageMapping(TypeMapper.DefaultValue(item.Message));

			/*====================================================/
			 *	LoanInfo
			 *====================================================*/
			if (null != item.LoanInformation)
			{
				mapping.LoanAccountNumber = TypeMapper.DefaultValue(item.LoanInformation.AccountNumber);
				mapping.LoanProductCode = TypeMapper.DefaultValue(item.LoanInformation.ProductCode);
				//mapping.ProductName = '';
				//mapping.SubProductGroup = '';
				mapping.TenorMonth = TypeMapper.DefaultValue(item.LoanInformation.Tenor);
				mapping.AccountStatus = TypeMapper.DefaultValue(item.LoanInformation.AccountStatus);
				mapping.CreditLimit = TypeMapper.DefaultValue(item.LoanInformation.CreditLimit);
				mapping.Outstanding = TypeMapper.DefaultValue(item.LoanInformation.Outstanding);
				mapping.OpenDate = item.LoanInformation.OpenDate;
				mapping.ContractEndDate = item.LoanInformation.ContractEndDate;
				mapping.PaymentDueDate = item.LoanInformation.PaymentDueDate;
				mapping.OtherConditions = TypeMapper.DefaultValue(item.LoanInformation.OtherConditions);
				mapping.CampaignName = TypeMapper.DefaultValue(item.LoanInformation.CampaignName);
				mapping.LastPaymentDate = item.LoanInformation.LastPaymentDate;
				mapping.RetentionDate = item.LoanInformation.RetentionDate;
				mapping.CurrentTenor = TypeMapper.DefaultValue(item.LoanInformation.CurrentTenor);
				mapping.HasCoBorrowe = TypeMapper.DefaultValue(item.LoanInformation.HasCoBorrower);
			}


			/*====================================================/
			 *	CoBorrowerInfo
			 *	NOTE:
			 *         WDSL Return Lists
			 *====================================================*/

			if (null != item.CoBorrowerInformation && null != item.CoBorrowerInformation.CoBorrower)
			{
				/*CoBorrowerDTO coBorr = new CoBorrowerDTO();
				mapping.CoBorroweres.add(coBorr);
				coBorr.CoBorrowerName = TypeMapper.DefaultValue(item.CoBorrowerInformation.CoBorrower.Name);
				coBorr.CoBorrowerRelationship = TypeMapper.DefaultValue(item.CoBorrowerInformation.CoBorrower.Relationship);
				*/
				//drop 2 will use below code
                for(RTL_LoanProductDetailsService.CoBorrowerInformation coborw : item.CoBorrowerInformation.CoBorrower){
                    CoBorrowerDTO coBorr = new CoBorrowerDTO();
					mapping.CoBorroweres.add(coBorr);
					coBorr.CoBorrowerName = TypeMapper.DefaultValue(coborw.Name);
					coBorr.CoBorrowerRelationship = TypeMapper.DefaultValue(coborw.Relationship);
                }
                
                
			} else {
				CoBorrowerDTO coBorr = new CoBorrowerDTO();
				mapping.CoBorroweres.add(coBorr);
			}


			/*====================================================/
			 *	InterestedPlanInfo
			 *	NOTE:
			 *         WDSL Return Lists
			 *====================================================*/
			if (null != item.InterestPlan && null != item.InterestPlan.InterestPlan)
			{
			
				/*InterestPlanDTO interestedPlan = new InterestPlanDTO();
				mapping.InterestPlans.add(interestedPlan);
				interestedPlan.Period = item.InterestPlan.InterestPlan.Period;
				interestedPlan.InterestRate = item.InterestPlan.InterestPlan.InterestRate;
				*/
				//drop 2 will use below code
                for(RTL_LoanProductDetailsService.InterestPlan interestP : item.InterestPlan.InterestPlan){
                    InterestPlanDTO interestedPlan = new InterestPlanDTO();
					mapping.InterestPlans.add(interestedPlan);
					interestedPlan.Period = interestP.Period;
					interestedPlan.InterestRate = interestP.InterestRate;
                    
                }
                

			}

			/*====================================================/
			 *	RelatedInsurance
			 *====================================================*/
			if (null != item.RelatedInsuranceInformation && null != item.RelatedInsuranceInformation.RelatedInsurance)
			{
				RelatedInsuranceDTO related = new RelatedInsuranceDTO();
				mapping.RelatedInsurance = related;
				related.Insurance = item.RelatedInsuranceInformation.RelatedInsurance.Insurance;
				related.PolicyNumber = item.RelatedInsuranceInformation.RelatedInsurance.PolicyNumber;
				related.Insurer = item.RelatedInsuranceInformation.RelatedInsurance.Insurer;
				related.SumInsured = item.RelatedInsuranceInformation.RelatedInsurance.SumInsured;
				related.StartDate = item.RelatedInsuranceInformation.RelatedInsurance.StartDate;
				related.EndDate = item.RelatedInsuranceInformation.RelatedInsurance.EndDate;
			}


			/*====================================================/
			 *	LoanPayment
			 *====================================================*/
			if (null != item.LoanPaymentInformation && null != item.LoanPaymentInformation.LoanPayment)
			{
				LoanPaymentDTO loan = new LoanPaymentDTO();
				mapping.LoanPayment = loan;
				loan.PaymentMethod = item.LoanPaymentInformation.LoanPayment.PaymentMethod;
				loan.SavingAccount = item.LoanPaymentInformation.LoanPayment.SavingAccount;
				loan.InstallmentBalance = item.LoanPaymentInformation.LoanPayment.InstallmentBalance;
				loan.PayrollDeductionUnit = item.LoanPaymentInformation.LoanPayment.PayrollDeductionUnit;
			}
			ret = mapping;

		}
		//OSC05
		if (request instanceOf RTL_BancPrdDetailsService.BancassuranceProductDetails) {

			RTL_BancPrdDetailsService.BancassuranceProductDetails item = ((RTL_BancPrdDetailsService.BancassuranceProductDetails) request);
			//System.debug('MULESOFT (OSC05): => RTL_BancPrdDetailsService.BancassuranceProductDetails Status = ' + (item.Status == null ? 'null : Error' : item.Status));
			BancassuranceInformationDTO mapping = new BancassuranceInformationDTO();
			// Error Handler message
			mapping.SoapStatus = item.Status == null ? '' : item.Status;
			mapping.SoapMessage = TypeMapper.errorMessageMapping(TypeMapper.DefaultValue(item.Message));

			//System.debug('MULESOFT (OSC05): => RTL_BancPrdDetailsService.BancassuranceInformation size = ' + (item.BancassuranceInformation == null ? '0' : '1'));

			if (null != item.BancassuranceInformation) {
				/*====================================================/
				 *	BancassuranceInfo
				 *====================================================*/
				mapping.PolicyNo = item.BancassuranceInformation.PolicyNo;
				mapping.ProductType = item.BancassuranceInformation.ProductType;
				mapping.ProductCode = item.BancassuranceInformation.TMBProductCode;
				mapping.ProductName = item.BancassuranceInformation.ProductName;
				mapping.SubProductGroup = '' ;				
				mapping.EffectiveDate = item.BancassuranceInformation.EffectiveDate;
				mapping.PolicyStatus = TypeMapper.DefaultValue(item.BancassuranceInformation.PolicyStatus);
				mapping.PolicyNo = TypeMapper.DefaultValue(item.BancassuranceInformation.PolicyNo);
				mapping.SumInsured = TypeMapper.DefaultValue(item.BancassuranceInformation.SumInsured);
				mapping.AFYP = TypeMapper.DefaultValue(item.BancassuranceInformation.AFYP);
				mapping.ExpiryDate = item.BancassuranceInformation.ExpiryDate;
				mapping.Sales = TypeMapper.DefaultValue(item.BancassuranceInformation.Sales);
			}

			/*====================================================/
			 *	BancassuranceDetail
			 *====================================================*/

			if (null != item.BancassuranceDetails) {
				BancassuranceDetailsDTO bancDetail = new BancassuranceDetailsDTO();
				mapping.BancassuranceDetail = bancDetail;
				bancDetail.InsuranceCompany = item.BancassuranceDetails.InsuranceCompany;
				//bancDetail.Address = '';
				//bancDetail.ContactNumber1 = '';
				//bancDetail.ContactNumber2 = '';

			}
			/*====================================================/
			 *	PaymentInfo
			 *====================================================*/
			if (null != item.PaymentInformation) {
				PaymentInformationDTO paymentInfo = new PaymentInformationDTO();
				mapping.PaymentInformation = paymentInfo;
				paymentInfo.PaymentMode = item.PaymentInformation.PaymentMode;
				paymentInfo.PremiumAmount = TypeMapper.DefaultValue(item.PaymentInformation.PremiumAmount);
				paymentInfo.PaymentMethod = item.PaymentInformation.PaymentMethod;
				paymentInfo.TotalPremiumPaid = TypeMapper.DefaultValue(item.PaymentInformation.TotalPremiumPaid);
				paymentInfo.YearOfPayment = item.PaymentInformation.YearOfPayment;
				paymentInfo.NumberOfTimePremiumPaid = TypeMapper.DefaultValue(item.PaymentInformation.NumberOfTimePremiumPaid);
				paymentInfo.NextDueDate = item.PaymentInformation.NextDueDate;
				paymentInfo.LastPaymentDate = item.PaymentInformation.LastPaymentDate;
			}
			/*====================================================/
			 *	NextCashBackInfo
			 *====================================================*/
			if (null != item.NextCashBackInformation) {
				NextCashBackInformationzmationDTO nextCashBack = new NextCashBackInformationzmationDTO();
				mapping.NextCashBackInformationzmation = nextCashBack;
				nextCashBack.PaymentDate = item.NextCashBackInformation.PaymentDate;
				nextCashBack.Amount = TypeMapper.DefaultValue(item.NextCashBackInformation.Amount);
				nextCashBack.TotalPaid = TypeMapper.DefaultValue(item.NextCashBackInformation.TotalPaid);
			}
			/*====================================================/
			 *	InsuredPropertyAsset
			 *====================================================*/
			if (null != item.InsuredPropertyAsset) {
				InsuredPropertyAssetDTO insuredProperty = new InsuredPropertyAssetDTO();
				mapping.InsuredPropertyAsset = insuredProperty;
				insuredProperty.Address = TypeMapper.DefaultValue(item.InsuredPropertyAsset.Address);
			}
			/*====================================================/
			 *	InsuredAutomobileAsset
			 *====================================================*/
			if (null != item.InsuredAutomobileAsset) {
				InsuredAutomobileAssetDTO insuredAutomobile = new InsuredAutomobileAssetDTO();
				mapping.InsuredAutomobileAsset = insuredAutomobile;
				insuredAutomobile.Brand = TypeMapper.DefaultValue(item.InsuredAutomobileAsset.Brand);
				insuredAutomobile.YearOfManufactured = TypeMapper.DefaultValue(item.InsuredAutomobileAsset.YearOfManufactured);
				insuredAutomobile.Model = TypeMapper.DefaultValue(item.InsuredAutomobileAsset.Model);
				insuredAutomobile.PlateNumber = TypeMapper.DefaultValue(item.InsuredAutomobileAsset.PlateNumber);
			}
			/*====================================================/
			 *	BeneficiaryInfo
			 *    NOTE:
			 *         WDSL Return only 1 item
			 *====================================================*/
			if (null != item.BeneficiaryInformation && null != item.BeneficiaryInformation.Beneficiary) {
                for(RTL_BancPrdDetailsService.BeneficiaryInformation beneficary : item.BeneficiaryInformation.Beneficiary){
                    BeneficiaryInformationDTO beneficaryInfo = new BeneficiaryInformationDTO();
					mapping.BeneficiaryInformations.add(beneficaryInfo);
					beneficaryInfo.BeneficiaryName = TypeMapper.DefaultValue(beneficary.Name);
					beneficaryInfo.Relationship = TypeMapper.DefaultValue(beneficary.Relationship);
					beneficaryInfo.BenefitPercent = TypeMapper.DefaultValue(beneficary.BenefitPercentage);
                }
				
			}
			/*====================================================/
			 *	InsuranceClaimRecord
			 *    NOTE:
			 *         WDSL Return Lists
			 *====================================================*/
			if (null != item.InsuranceClaimRecord && null != item.InsuranceClaimRecord.ClaimRecord) {
                /*InsuranceClaimRecordDTO insuranceClaim = new InsuranceClaimRecordDTO();
				mapping.InsuranceClaimRecords.add(insuranceClaim);
				insuranceClaim.Date_x = item.InsuranceClaimRecord.ClaimRecord.Date_x;
				insuranceClaim.Type_x = TypeMapper.DefaultValue(item.InsuranceClaimRecord.ClaimRecord.Type_x);
				insuranceClaim.Description = TypeMapper.DefaultValue(item.InsuranceClaimRecord.ClaimRecord.Description);
				insuranceClaim.RequestAmount = TypeMapper.DefaultValue(item.InsuranceClaimRecord.ClaimRecord.RequestAmount);
				insuranceClaim.ApprovedAmount = TypeMapper.DefaultValue(item.InsuranceClaimRecord.ClaimRecord.ApprovedAmount);
                */
                //drop 2 will use below code
                for(RTL_BancPrdDetailsService.InsuranceClaimRecord insurance : item.InsuranceClaimRecord.ClaimRecord){
                    InsuranceClaimRecordDTO insuranceClaim = new InsuranceClaimRecordDTO();
					mapping.InsuranceClaimRecords.add(insuranceClaim);
					insuranceClaim.Date_x = insurance.Date_x;
					insuranceClaim.Type_x = TypeMapper.DefaultValue(insurance.Type_x);
					insuranceClaim.Description = TypeMapper.DefaultValue(insurance.Description);
					insuranceClaim.RequestAmount = TypeMapper.DefaultValue(insurance.RequestAmount);
					insuranceClaim.ApprovedAmount = TypeMapper.DefaultValue(insurance.ApprovedAmount);
                }
				
			}
			ret = mapping;
		}
		//OSC06
		if (request instanceOf RTL_InvestmentPrdDetailsService.InvestmentProductDetails) {
			RTL_InvestmentPrdDetailsService.InvestmentProductDetails item = ((RTL_InvestmentPrdDetailsService.InvestmentProductDetails) request);
			InvestmentProductDetailsDTO mapping = new InvestmentProductDetailsDTO();
			// Error Handler message
			mapping.SoapStatus = item.Status == null ? '' : item.Status;
			mapping.SoapMessage = TypeMapper.errorMessageMapping(TypeMapper.DefaultValue(item.Message));

			/*====================================================/
			 *	InvestmentInfo
			 *====================================================*/
			if (null != item.InvestmentInformation)
			{
				InvestmentInformationDTO investmentInfo = new InvestmentInformationDTO();
				mapping.InvestmentInformation = investmentInfo;
				//investmentInfo.ProductName = '';
				//investmentInfo.ProductType = '';
				//investmentInfo.AssetType = '';
				//investmentInfo.FundType = '';

				//investmentInfo.UnitHolderNo = TypeMapper.DefaultValue(item.InvestmentInformation.UnitHolderNo);
				//investmentInfo.FundCode = '';
				investmentInfo.NumberOfUnit = TypeMapper.DefaultValue(item.InvestmentInformation.NumberOfUnit);
				investmentInfo.CostOfInvestment = TypeMapper.DefaultValue(item.InvestmentInformation.CostOfInvestment);
				investmentInfo.MarketValue = TypeMapper.DefaultValue(item.InvestmentInformation.MarketValue);
				investmentInfo.UnrealizedGL = TypeMapper.DefaultValue(item.InvestmentInformation.UnrealizedGL);
				investmentInfo.NAVUnit = TypeMapper.DefaultValue(item.InvestmentInformation.NAVUnit);
                investmentInfo.AipInstruction = TypeMapper.DefaultValue(item.InvestmentInformation.AipInstruction); 
			}


			/*====================================================/
			 *	InvestmentTransaction
			 *====================================================*/
			if (null != item.InvestmentTransaction && null != item.InvestmentTransaction.InvestmentTransaction) {
				for (RTL_InvestmentPrdDetailsService.InvestmentTransaction tran : item.InvestmentTransaction.InvestmentTransaction) {
					InvestmentTransactionDTO investmentTransaction = new InvestmentTransactionDTO();
					mapping.InvestmentTransactions.add(investmentTransaction);
					investmentTransaction.TransactionDate = tran.TransactionDate;
					investmentTransaction.SettlementDate = tran.SettlementDate;
					investmentTransaction.TransactionType = TypeMapper.DefaultValue(tran.TransactionType);
					investmentTransaction.UnitMovement = TypeMapper.DefaultValue(tran.UnitMovement);
					investmentTransaction.UnitOutstanding = TypeMapper.DefaultValue(tran.UnitOutstanding);
					investmentTransaction.TransactionValue = TypeMapper.DefaultValue(tran.TransactionValue);
					investmentTransaction.ValuePerUnit = TypeMapper.DefaultValue(tran.ValuePerUnit);
					investmentTransaction.Statue = TypeMapper.DefaultValue(tran.Statue);
					investmentTransaction.InteractChannel = TypeMapper.DefaultValue(tran.InteractChannel);

				}
			}

			/*====================================================/
			 *	AutoInvestmentPlan
			 *====================================================*/
			if (null != item.AutoInvestmentPlan && null != item.AutoInvestmentPlan.AutoInvestmentPlan) {
				
				//drop 2 will be a list
				for (RTL_InvestmentPrdDetailsService.AutoInvestmentPlan auto : item.AutoInvestmentPlan.AutoInvestmentPlan) {
				//RTL_InvestmentPrdDetailsService.AutoInvestmentPlan auto = item.AutoInvestmentPlan.AutoInvestmentPlan;
				AutoInvestmentPlanDTO autoInvestment = new AutoInvestmentPlanDTO();
				mapping.AutoInvestmentPlans.add(autoInvestment);
				//autoInvestment.Instruction = auto.Instruction;
				autoInvestment.Date_x = auto.Date_x;
				autoInvestment.SinceDate = auto.SinceDate;
				autoInvestment.CreatedChannel = auto.CreatedChannel;
				autoInvestment.Frequency = auto.Frequency;
				autoInvestment.Balance = auto.Balance;
				autoInvestment.SavingAccountBundling = auto.SavingAccountBundling;

				}
			}


			ret = mapping;
		}


		return ret;
	}




	public static string DefaultValue(string item) {

		return item == null ? '' : item;

	}
	public static decimal DefaultValue(decimal item) {

		return item == null ? 0.0 : item;

	}
    
    public static String errorMessageMapping(String errCode){
        if(errCode.toUpperCase() == 'ERR001')
            return System.Label.ERR001;
        if(errCode.toUpperCase() == 'ERR002')
            return System.Label.ERR002;
        System.debug('WebService Return Message :: '+errCode);
        return System.Label.ERR001;
    }
}