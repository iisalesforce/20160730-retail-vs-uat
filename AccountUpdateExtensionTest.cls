@isTest
global class AccountUpdateExtensionTest {
   static {
        TestUtils.createIdType();
        TestUtils.createAppConfig();
        TestUtils.createStatusCode();
        TestUtils.createDisqualifiedReason();
        
        
        Country__c bkk = new Country__c();
        bkk.Name ='Bangkok';
        bkk.Code__c ='10';
        insert bkk;
    }
    
    
     static testmethod void WebserviceMockupTest(){


        Account acct =  TestUtils.createAccounts(1,'AccountUpdateTest','Individual', true).get(0);
        ApexPages.StandardController sc = new ApexPages.StandardController(Acct);
          
         AccountUpdateExtension AccUpEx = new AccountUpdateExtension(sc);
     }
}