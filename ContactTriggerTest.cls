@IsTest
public class ContactTriggerTest {
    
    public static testmethod void positiveTest(){
          TestUtils.createAppConfig();
        	TestUtils.createObjUserPermission();
        
        System.debug(':::: positiveTest Start ::::');
        TestInit.createUser(false);
        User u = TestInit.us;
        user u2 = TestInit.us2;
        System.runAs(u) {            
            // Before Insert
             TestUtils.createUsers(1,'TestUtilTest', 'Utiltest','testUtil@TestUtil.com', true);
            TestInit.createAccount(1);
            List<Account> accountList = TestInit.accountList.values();

             List<Contact> contactList = TestInit.createContacts(1,accountList.get(0).id, true);      
            List<AccountTeamMember> accountTeamMemberList = TestInit.createAccountTeamMember(1,accountList.get(0).id,UserInfo.getUserId());            
            accountList.get(0).OwnerId = u2.Id;
            update accountList.get(0);
            contactList = TestInit.createContacts(1,accountList.get(0).id, true);  
            
            Test.startTest();
            accountList.get(0).OwnerID = TestInit.us.id;
            update accountList;
              List<Contact>  contactList2 = TestInit.createContacts(1,accountList.get(0).id, true);  
            Test.stopTest();
           
            
            
        }
        System.debug(':::: positiveTest End ::::');
    }

}