public class RetailCustomerExtension {
    private Account acct;
        
    public Integer getDisplayS1() {
        return (UserInfo.getUiThemeDisplayed() == 'Theme4t'? 1:2);
    }
    
    public RetailCustomerExtension(ApexPages.StandardController std){
        //displayS1 = getDisplayS1();
        this.acct = (Account)std.getRecord();
    }
    
    public PageReference cancelCustomer(){
        PageReference acctHome = new PageReference('/001/o');
        if(null != acct.id) {
            acctHome = new PageReference('/'+acct.id);
        }
        acctHome.setRedirect(true);
        return acctHome;
    }
    
    public PageReference save(){
        try
        {
        
            update acct;
            
            
        }  
        catch(DMLException e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getDMLMessage(0)));
            System.debug(e.getDmlMessage(0));
            return null;
        }catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            return null;
        }
        
        return  new PageReference('/'+acct.id);
    }
}